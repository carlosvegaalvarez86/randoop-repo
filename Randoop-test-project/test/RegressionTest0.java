import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegressionTest0 {

    public static boolean debug = false;

    @Test
    public void test001() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test001");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 101);
        try {
            java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 101 + "'", int4.equals(101));
    }

    @Test
    public void test002() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test002");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 14, (java.lang.Integer) (-2030));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
    }

    @Test
    public void test003() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test003");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        try {
            java.lang.Integer int3 = operacionesMatematicas0.dividir((java.lang.Integer) (-203), (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
    }

    @Test
    public void test004() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test004");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) 101);
        try {
            java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 797, (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2 + "'", int6.equals(2));
    }

    @Test
    public void test005() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test005");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) (-1));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 2030, (java.lang.Integer) 1);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-11), (java.lang.Integer) 202);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2029 + "'", int10.equals(2029));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 191 + "'", int13.equals(191));
    }

    @Test
    public void test006() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test006");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 10);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 990, (java.lang.Integer) 703051);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) (-676296));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1000 + "'", int9.equals(1000));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-702061) + "'", int12.equals((-702061)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 2108502328 + "'", int15.equals(2108502328));
    }

    @Test
    public void test007() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test007");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 1678);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) 797);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-202979700), (java.lang.Integer) (-337227660));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 166122 + "'", int10.equals(166122));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
    }

    @Test
    public void test008() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test008");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-26796), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) (-6763), (java.lang.Integer) (-4060));
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) (-28474), (java.lang.Integer) 199);
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) 222308, (java.lang.Integer) 202);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-28474) + "'", int20.equals((-28474)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 1 + "'", int23.equals(1));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-28275) + "'", int26.equals((-28275)));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 44906216 + "'", int29.equals(44906216));
    }

    @Test
    public void test009() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test009");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-1566625664), (java.lang.Integer) 242847);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-6451) + "'", int13.equals((-6451)));
    }

    @Test
    public void test010() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test010");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) 101);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) (-1566625664));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2 + "'", int6.equals(2));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 1566625664 + "'", int10.equals(1566625664));
    }

    @Test
    public void test011() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test011");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test012() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test012");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 202);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) (-2030));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 791, (java.lang.Integer) 677082);
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 242847, (java.lang.Integer) (-453522300));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1 + "'", int15.equals(1));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 677084 + "'", int18.equals(677084));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
    }

    @Test
    public void test013() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test013");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 3233, (java.lang.Integer) (-676594));
        try {
            java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-203662590), (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 679827 + "'", int9.equals(679827));
    }

    @Test
    public void test014() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test014");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-480618), (java.lang.Integer) (-42632));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-985129904) + "'", int12.equals((-985129904)));
    }

    @Test
    public void test015() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test015");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-2233), (java.lang.Integer) 199);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-203657735), (java.lang.Integer) (-1));
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) (-1566625664), (java.lang.Integer) (-42632));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-11) + "'", int10.equals((-11)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-203657736) + "'", int13.equals((-203657736)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-1566668296) + "'", int16.equals((-1566668296)));
    }

    @Test
    public void test016() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test016");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-507), (java.lang.Integer) 200871);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-358), (java.lang.Integer) (-679114));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-201378) + "'", int16.equals((-201378)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 243122812 + "'", int19.equals(243122812));
    }

    @Test
    public void test017() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test017");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 473676, (java.lang.Integer) (-2030));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 20300, (java.lang.Integer) 338956);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 453522102, (java.lang.Integer) (-2));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-961562280) + "'", int12.equals((-961562280)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 359256 + "'", int15.equals(359256));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-907044204) + "'", int18.equals((-907044204)));
    }

    @Test
    public void test018() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test018");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) (-203));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-613263) + "'", int12.equals((-613263)));
    }

    @Test
    public void test019() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test019");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 100004, (java.lang.Integer) 0);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 3274, (java.lang.Integer) (-5));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 100004 + "'", int12.equals(100004));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-654) + "'", int15.equals((-654)));
    }

    @Test
    public void test020() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test020");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 101);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 2029, (java.lang.Integer) 2029);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 101 + "'", int4.equals(101));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 4058 + "'", int7.equals(4058));
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test021() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test021");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 202);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) (-2030));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 791, (java.lang.Integer) 677082);
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) 44906216, (java.lang.Integer) (-204));
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-9430), (java.lang.Integer) (-200282));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1 + "'", int15.equals(1));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 677084 + "'", int18.equals(677084));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 44906012 + "'", int26.equals(44906012));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 1888659260 + "'", int29.equals(1888659260));
    }

    @Test
    public void test022() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test022");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 676495, (java.lang.Integer) (-9));
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-1027036680), (java.lang.Integer) 6450721);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 676504 + "'", int17.equals(676504));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-1033487401) + "'", int20.equals((-1033487401)));
    }

    @Test
    public void test023() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test023");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1480217496), (java.lang.Integer) (-1796353182));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-485640240) + "'", int7.equals((-485640240)));
    }

    @Test
    public void test024() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test024");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-26796), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) (-6763), (java.lang.Integer) (-4060));
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) (-28474), (java.lang.Integer) 199);
        java.lang.Integer int29 = operacionesMatematicas0.sumar((java.lang.Integer) (-319237330), (java.lang.Integer) (-86797590));
        java.lang.Integer int32 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1988598960), (java.lang.Integer) (-6763));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-28474) + "'", int20.equals((-28474)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 1 + "'", int23.equals(1));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-28275) + "'", int26.equals((-28275)));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-406034920) + "'", int29.equals((-406034920)));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 1352162704 + "'", int32.equals(1352162704));
    }

    @Test
    public void test025() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test025");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 100004, (java.lang.Integer) 0);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 3021, (java.lang.Integer) 100);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) (-1546915), (java.lang.Integer) 198);
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 100004 + "'", int12.equals(100004));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 30 + "'", int15.equals(30));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-7812) + "'", int18.equals((-7812)));
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test026() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test026");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166122, (java.lang.Integer) (-2030));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 227358, (java.lang.Integer) (-2030));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 199 + "'", int6.equals(199));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-337227660) + "'", int9.equals((-337227660)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 229388 + "'", int13.equals(229388));
    }

    @Test
    public void test027() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test027");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 677082, (java.lang.Integer) 797);
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) (-1378461349), (java.lang.Integer) (-1988598960));
        java.lang.Class<?> wildcardClass27 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 849 + "'", int22.equals(849));
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 0 + "'", int26.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass27);
    }

    @Test
    public void test028() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test028");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) (-1));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 696288, (java.lang.Integer) (-6763));
        try {
            java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 668453649, (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 703051 + "'", int10.equals(703051));
    }

    @Test
    public void test029() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test029");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-2233), (java.lang.Integer) 199);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 20300, (java.lang.Integer) (-676296));
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 673023, (java.lang.Integer) (-673143));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-11) + "'", int10.equals((-11)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1346166 + "'", int16.equals(1346166));
    }

    @Test
    public void test030() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test030");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-237), (java.lang.Integer) (-305));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-542) + "'", int17.equals((-542)));
    }

    @Test
    public void test031() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test031");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-2031), (java.lang.Integer) (-100));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 28345, (java.lang.Integer) 1771579329);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-2131) + "'", int10.equals((-2131)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
    }

    @Test
    public void test032() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test032");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 674850, (java.lang.Integer) (-203658532));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) (-103));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-391), (java.lang.Integer) 1963548060);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-9) + "'", int12.equals((-9)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
    }

    @Test
    public void test033() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test033");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 199);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-202979700), (java.lang.Integer) 1079);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 298 + "'", int9.equals(298));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-202978621) + "'", int12.equals((-202978621)));
    }

    @Test
    public void test034() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test034");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 202);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-50702028), (java.lang.Integer) 703051);
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-23417576), (java.lang.Integer) (-678748));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1 + "'", int15.equals(1));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 2117069372 + "'", int18.equals(2117069372));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 34 + "'", int22.equals(34));
    }

    @Test
    public void test035() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test035");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        try {
            java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 985806408, (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test036() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test036");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) 14);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 227358, (java.lang.Integer) 223);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 166122, (java.lang.Integer) (-11));
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 675040 + "'", int10.equals(675040));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227581 + "'", int13.equals(227581));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 166111 + "'", int16.equals(166111));
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test037() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test037");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) 223300);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-676396), (java.lang.Integer) 1678);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) 99990);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 199, (java.lang.Integer) 2029);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2030) + "'", int9.equals((-2030)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2679600 + "'", int13.equals(2679600));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-678074) + "'", int16.equals((-678074)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-98990) + "'", int19.equals((-98990)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1830) + "'", int22.equals((-1830)));
    }

    @Test
    public void test038() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test038");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 14, (java.lang.Integer) (-204));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-565899369), (java.lang.Integer) (-1077768));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 218 + "'", int9.equals(218));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 525 + "'", int12.equals(525));
    }

    @Test
    public void test039() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test039");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) (-3), (java.lang.Integer) 0);
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 1996158842, (java.lang.Integer) 0);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-3) + "'", int7.equals((-3)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 1996158842 + "'", int10.equals(1996158842));
    }

    @Test
    public void test040() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test040");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-320292228), (java.lang.Integer) (-204));
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) 191);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1570059 + "'", int22.equals(1570059));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
    }

    @Test
    public void test041() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test041");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2, (java.lang.Integer) 99);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 200871, (java.lang.Integer) 688);
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 198 + "'", int15.equals(198));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 200183 + "'", int18.equals(200183));
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test042() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test042");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 473676, (java.lang.Integer) 222612);
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-304));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) 100);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 696288 + "'", int7.equals(696288));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 222308 + "'", int10.equals(222308));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
    }

    @Test
    public void test043() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test043");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) 223300);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-136443584), (java.lang.Integer) (-43315734));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2030) + "'", int9.equals((-2030)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2679600 + "'", int13.equals(2679600));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 3 + "'", int16.equals(3));
    }

    @Test
    public void test044() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test044");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.restar((java.lang.Integer) (-3160), (java.lang.Integer) (-203662590));
        java.lang.Integer int11 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-844));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 203659430 + "'", int8.equals(203659430));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11.equals(0));
    }

    @Test
    public void test045() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test045");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 302, (java.lang.Integer) 675054);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 24, (java.lang.Integer) 168);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 868432494, (java.lang.Integer) (-882132));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-674752) + "'", int10.equals((-674752)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-144) + "'", int13.equals((-144)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-984) + "'", int16.equals((-984)));
    }

    @Test
    public void test046() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test046");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.multiplicar((java.lang.Integer) 386, (java.lang.Integer) 1566625664);
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-28275), (java.lang.Integer) 1089);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + (-872882432) + "'", int14.equals((-872882432)));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-27186) + "'", int17.equals((-27186)));
    }

    @Test
    public void test047() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test047");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-2030), (java.lang.Integer) 677084);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 7458814, (java.lang.Integer) (-635285));
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) (-599940), (java.lang.Integer) 1963548060);
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-679114) + "'", int16.equals((-679114)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 6823529 + "'", int19.equals(6823529));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1964148000) + "'", int22.equals((-1964148000)));
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test048() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test048");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-2233));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) 1980253234, (java.lang.Integer) 30107083);
        try {
            java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-56940188), (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-674366) + "'", int9.equals((-674366)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2010360317 + "'", int12.equals(2010360317));
    }

    @Test
    public void test049() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test049");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-507), (java.lang.Integer) 200871);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-3030), (java.lang.Integer) 1980253234);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 1079, (java.lang.Integer) (-3));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-201378) + "'", int16.equals((-201378)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1980250204 + "'", int19.equals(1980250204));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-359) + "'", int22.equals((-359)));
    }

    @Test
    public void test050() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test050");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 1888659260);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 2108530673, (java.lang.Integer) (-599940));
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2109130613 + "'", int10.equals(2109130613));
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test051() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test051");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) 2030, (java.lang.Integer) 688);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) (-2233));
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.sumar((java.lang.Integer) 298, (java.lang.Integer) 0);
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 44907013, (java.lang.Integer) (-2452019));
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) (-343058591));
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 1001435648, (java.lang.Integer) 2989800);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7.equals(2));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 225533 + "'", int10.equals(225533));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 298 + "'", int14.equals(298));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 47359032 + "'", int17.equals(47359032));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-343058591) + "'", int20.equals((-343058591)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 334 + "'", int23.equals(334));
    }

    @Test
    public void test052() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test052");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-304));
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 191, (java.lang.Integer) 137901733);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-454172969), (java.lang.Integer) 0);
        try {
            java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) (-802065), (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 569427227 + "'", int13.equals(569427227));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
    }

    @Test
    public void test053() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test053");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-26796), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) (-6763), (java.lang.Integer) (-4060));
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) (-28474), (java.lang.Integer) 199);
        java.lang.Class<?> wildcardClass27 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass28 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-28474) + "'", int20.equals((-28474)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 1 + "'", int23.equals(1));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-28275) + "'", int26.equals((-28275)));
        org.junit.Assert.assertNotNull(wildcardClass27);
        org.junit.Assert.assertNotNull(wildcardClass28);
    }

    @Test
    public void test054() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test054");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-26796), (java.lang.Integer) (-2131));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-674366), (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 790454420, (java.lang.Integer) (-9));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) 7809, (java.lang.Integer) 674196);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-28927) + "'", int12.equals((-28927)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-133524468) + "'", int15.equals((-133524468)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1475844812 + "'", int18.equals(1475844812));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-666387) + "'", int21.equals((-666387)));
    }

    @Test
    public void test055() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test055");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 688, (java.lang.Integer) 990);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) 218, (java.lang.Integer) (-2131));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 1771576096, (java.lang.Integer) 2108530673);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 14, (java.lang.Integer) (-747018902));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1678 + "'", int9.equals(1678));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1913) + "'", int12.equals((-1913)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-336954577) + "'", int15.equals((-336954577)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1868330036) + "'", int18.equals((-1868330036)));
    }

    @Test
    public void test056() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test056");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-26796), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) (-6763), (java.lang.Integer) (-4060));
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) 1542, (java.lang.Integer) 2108473854);
        java.lang.Integer int29 = operacionesMatematicas0.restar((java.lang.Integer) 98974848, (java.lang.Integer) 1567919351);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-28474) + "'", int20.equals((-28474)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 1 + "'", int23.equals(1));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 2108475396 + "'", int26.equals(2108475396));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-1468944503) + "'", int29.equals((-1468944503)));
    }

    @Test
    public void test057() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test057");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) 14);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.sumar((java.lang.Integer) 3274, (java.lang.Integer) 1739459091);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 675040 + "'", int10.equals(675040));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 1739462365 + "'", int14.equals(1739462365));
    }

    @Test
    public void test058() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test058");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-2031), (java.lang.Integer) (-100));
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-391), (java.lang.Integer) 191);
        try {
            java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-599940), (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-2131) + "'", int10.equals((-2131)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-74681) + "'", int13.equals((-74681)));
    }

    @Test
    public void test059() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test059");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 173085920, (java.lang.Integer) (-157900046));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-1) + "'", int9.equals((-1)));
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test060() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test060");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166122, (java.lang.Integer) (-2030));
        try {
            java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 1954229806, (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 199 + "'", int6.equals(199));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-337227660) + "'", int9.equals((-337227660)));
    }

    @Test
    public void test061() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test061");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 10, (java.lang.Integer) 302);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-676594), (java.lang.Integer) 298);
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) (-589656652), (java.lang.Integer) 359256);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 3020 + "'", int15.equals(3020));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-676296) + "'", int18.equals((-676296)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1641) + "'", int21.equals((-1641)));
    }

    @Test
    public void test062() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test062");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 199);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 298 + "'", int9.equals(298));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test063() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test063");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-198), (java.lang.Integer) 198);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-98990));
        java.lang.Class<?> wildcardClass21 = operacionesMatematicas0.getClass();
        try {
            java.lang.Integer int24 = operacionesMatematicas0.dividir((java.lang.Integer) (-91808), (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-98890) + "'", int20.equals((-98890)));
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test064() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test064");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166122, (java.lang.Integer) (-2030));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) 703040, (java.lang.Integer) 2005416224);
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 1325219516, (java.lang.Integer) (-594105055));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 199 + "'", int6.equals(199));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-337227660) + "'", int9.equals((-337227660)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2006119264 + "'", int12.equals(2006119264));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1919324571 + "'", int16.equals(1919324571));
    }

    @Test
    public void test065() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test065");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 100004, (java.lang.Integer) 0);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-17), (java.lang.Integer) (-403));
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 14, (java.lang.Integer) 2000);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 1678, (java.lang.Integer) (-203658530));
        java.lang.Integer int24 = operacionesMatematicas0.restar((java.lang.Integer) (-74681), (java.lang.Integer) 1868537856);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 100004 + "'", int12.equals(100004));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 386 + "'", int15.equals(386));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 28000 + "'", int18.equals(28000));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-203656852) + "'", int21.equals((-203656852)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1868612537) + "'", int24.equals((-1868612537)));
    }

    @Test
    public void test066() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test066");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) (-198), (java.lang.Integer) (-453522300));
        java.lang.Integer int26 = operacionesMatematicas0.restar((java.lang.Integer) 676504, (java.lang.Integer) 0);
        java.lang.Integer int29 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-17));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 453522102 + "'", int23.equals(453522102));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 676504 + "'", int26.equals(676504));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 0 + "'", int29.equals(0));
    }

    @Test
    public void test067() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test067");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 101);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 2029, (java.lang.Integer) 2029);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) 99692);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-319239361));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 101 + "'", int4.equals(101));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 4058 + "'", int7.equals(4058));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 30106984 + "'", int10.equals(30106984));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
    }

    @Test
    public void test068() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test068");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-507), (java.lang.Integer) 200871);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-3030), (java.lang.Integer) 1980253234);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) (-1468944503), (java.lang.Integer) (-6451));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-201378) + "'", int16.equals((-201378)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1980250204 + "'", int19.equals(1980250204));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1468950954) + "'", int22.equals((-1468950954)));
    }

    @Test
    public void test069() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test069");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 14, (java.lang.Integer) (-3260));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 173387, (java.lang.Integer) (-802065));
        try {
            java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) (-674376), (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3274 + "'", int12.equals(3274));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 975452 + "'", int15.equals(975452));
    }

    @Test
    public void test070() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test070");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 2354, (java.lang.Integer) (-403));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 680606415, (java.lang.Integer) (-3260));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-5) + "'", int12.equals((-5)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 680609675 + "'", int15.equals(680609675));
    }

    @Test
    public void test071() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test071");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-304));
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) 1771576096);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 402135200 + "'", int14.equals(402135200));
    }

    @Test
    public void test072() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test072");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) (-26796));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 28000, (java.lang.Integer) (-676594));
        java.lang.Integer int28 = operacionesMatematicas0.sumar((java.lang.Integer) (-594099455), (java.lang.Integer) 0);
        java.lang.Integer int31 = operacionesMatematicas0.restar((java.lang.Integer) 905316668, (java.lang.Integer) (-1827));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-86631468) + "'", int22.equals((-86631468)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-594099455) + "'", int28.equals((-594099455)));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 905318495 + "'", int31.equals(905318495));
    }

    @Test
    public void test073() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test073");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-223), (java.lang.Integer) (-1429679577));
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 6, (java.lang.Integer) 3884);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1429679800) + "'", int13.equals((-1429679800)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-3878) + "'", int16.equals((-3878)));
    }

    @Test
    public void test074() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test074");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass9 = operacionesMatematicas0.getClass();
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) 1349700, (java.lang.Integer) (-847810496));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-846460796) + "'", int12.equals((-846460796)));
    }

    @Test
    public void test075() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test075");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) (-292));
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) (-15), (java.lang.Integer) (-203289362));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-882132) + "'", int16.equals((-882132)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
    }

    @Test
    public void test076() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test076");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 101);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 2029, (java.lang.Integer) 2029);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) 99692);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-1072091024), (java.lang.Integer) (-890108686));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-11), (java.lang.Integer) 961562281);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 101 + "'", int4.equals(101));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 4058 + "'", int7.equals(4058));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 30106984 + "'", int10.equals(30106984));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-181982338) + "'", int13.equals((-181982338)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-1987250499) + "'", int16.equals((-1987250499)));
    }

    @Test
    public void test077() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test077");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 173085920, (java.lang.Integer) (-157900046));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-717341912), (java.lang.Integer) 9992694);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-1) + "'", int9.equals((-1)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-71) + "'", int12.equals((-71)));
    }

    @Test
    public void test078() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test078");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 99990, (java.lang.Integer) 298);
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-702086), (java.lang.Integer) (-179683468));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 99692 + "'", int9.equals(99692));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
    }

    @Test
    public void test079() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test079");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1419118905), (java.lang.Integer) (-1243099180));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-1243099180), (java.lang.Integer) 134082361);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 47990220 + "'", int12.equals(47990220));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1377181541) + "'", int15.equals((-1377181541)));
    }

    @Test
    public void test080() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test080");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1727627124), (java.lang.Integer) (-926090540));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + (-495244304) + "'", int8.equals((-495244304)));
    }

    @Test
    public void test081() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test081");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 199);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.multiplicar((java.lang.Integer) 9120300, (java.lang.Integer) 50443);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 298 + "'", int9.equals(298));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 493792228 + "'", int14.equals(493792228));
    }

    @Test
    public void test082() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test082");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 199);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.sumar((java.lang.Integer) 200257, (java.lang.Integer) (-25));
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-2108473877), (java.lang.Integer) (-90));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 298 + "'", int9.equals(298));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 200232 + "'", int14.equals(200232));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-2108473967) + "'", int17.equals((-2108473967)));
    }

    @Test
    public void test083() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test083");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.sumar((java.lang.Integer) 12, (java.lang.Integer) (-480630));
        try {
            java.lang.Integer int11 = operacionesMatematicas0.dividir((java.lang.Integer) 1954931892, (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + (-480618) + "'", int8.equals((-480618)));
    }

    @Test
    public void test084() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test084");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-98975152), (java.lang.Integer) (-676594));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 117, (java.lang.Integer) 1);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-201378), (java.lang.Integer) (-3260));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) 1023986076, (java.lang.Integer) 118);
        java.lang.Integer int24 = operacionesMatematicas0.dividir((java.lang.Integer) (-867152808), (java.lang.Integer) (-1077114894));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 146 + "'", int12.equals(146));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 118 + "'", int15.equals(118));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-198118) + "'", int18.equals((-198118)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1023985958 + "'", int21.equals(1023985958));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 0 + "'", int24.equals(0));
    }

    @Test
    public void test085() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test085");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 99, (java.lang.Integer) (-676396));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) (-317895457), (java.lang.Integer) (-164003));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 676495 + "'", int7.equals(676495));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-317731454) + "'", int10.equals((-317731454)));
    }

    @Test
    public void test086() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test086");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.restar((java.lang.Integer) 7458814, (java.lang.Integer) (-802065));
        java.lang.Integer int14 = operacionesMatematicas0.restar((java.lang.Integer) (-1060594160), (java.lang.Integer) 453522102);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 8260879 + "'", int11.equals(8260879));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + (-1514116262) + "'", int14.equals((-1514116262)));
    }

    @Test
    public void test087() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test087");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass6 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-203761580), (java.lang.Integer) 1427973632);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1593176728, (java.lang.Integer) (-317895457));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 1224212052 + "'", int10.equals(1224212052));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-973376920) + "'", int13.equals((-973376920)));
    }

    @Test
    public void test088() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test088");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 679827);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-86407950), (java.lang.Integer) 419736786);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-485640240), (java.lang.Integer) 677084);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 199, (java.lang.Integer) 569427227);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1566625664), (java.lang.Integer) (-23417576));
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-679014), (java.lang.Integer) 2);
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) (-506915), (java.lang.Integer) 2108530673);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-679726) + "'", int6.equals((-679726)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-835045696) + "'", int12.equals((-835045696)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-569427028) + "'", int15.equals((-569427028)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-5043200) + "'", int18.equals((-5043200)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1358028) + "'", int21.equals((-1358028)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 2108023758 + "'", int24.equals(2108023758));
    }

    @Test
    public void test089() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test089");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 199, (java.lang.Integer) 302);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-58), (java.lang.Integer) (-674366));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2010926408, (java.lang.Integer) 1352434);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 666292770, (java.lang.Integer) (-164003));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-674424) + "'", int13.equals((-674424)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-1060594160) + "'", int16.equals((-1060594160)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1455213478) + "'", int19.equals((-1455213478)));
    }

    @Test
    public void test090() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test090");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-100), (java.lang.Integer) (-203));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 1374, (java.lang.Integer) 28000);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 20300 + "'", int12.equals(20300));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test091() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test091");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 14, (java.lang.Integer) (-204));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-28176), (java.lang.Integer) 99990);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 218 + "'", int9.equals(218));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
    }

    @Test
    public void test092() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test092");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 44906012, (java.lang.Integer) 2679600);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-223), (java.lang.Integer) (-28927));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 2108502328, (java.lang.Integer) 28345);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) (-1418013643), (java.lang.Integer) 791133635);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 42226412 + "'", int7.equals(42226412));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 6450721 + "'", int10.equals(6450721));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2108530673 + "'", int13.equals(2108530673));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-626880008) + "'", int16.equals((-626880008)));
    }

    @Test
    public void test093() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test093");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 1996158842, (java.lang.Integer) 790454420);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 791, (java.lang.Integer) (-1566668296));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 476950759, (java.lang.Integer) 1426890059);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1508354034) + "'", int13.equals((-1508354034)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 2010926408 + "'", int16.equals(2010926408));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-949939300) + "'", int19.equals((-949939300)));
    }

    @Test
    public void test094() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test094");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1913));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-300960), (java.lang.Integer) (-17));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-1077114894), (java.lang.Integer) (-44906012));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 117 + "'", int9.equals(117));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-300943) + "'", int12.equals((-300943)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 23 + "'", int15.equals(23));
    }

    @Test
    public void test095() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test095");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) 688);
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) (-202979314), (java.lang.Integer) 2108530673);
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) (-133524468), (java.lang.Integer) (-985706892));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 12 + "'", int13.equals(12));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 222612 + "'", int17.equals(222612));
        org.junit.Assert.assertNotNull(wildcardClass18);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1983457309 + "'", int21.equals(1983457309));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
    }

    @Test
    public void test096() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test096");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2000);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 315901, (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
    }

    @Test
    public void test097() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test097");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-320292228), (java.lang.Integer) (-204));
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-676396), (java.lang.Integer) 5050);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-3), (java.lang.Integer) (-203761580));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) (-172199), (java.lang.Integer) 1701552187);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2030) + "'", int9.equals((-2030)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1570059 + "'", int13.equals(1570059));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-681446) + "'", int16.equals((-681446)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-203761583) + "'", int19.equals((-203761583)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1701379988 + "'", int22.equals(1701379988));
    }

    @Test
    public void test098() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test098");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2, (java.lang.Integer) 99);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-480630), (java.lang.Integer) (-203658530));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 198 + "'", int15.equals(198));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 203177900 + "'", int18.equals(203177900));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test099() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test099");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) 688);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) (-2031), (java.lang.Integer) 223300);
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) 191, (java.lang.Integer) 3451);
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) (-86797590), (java.lang.Integer) (-1072091024));
        java.lang.Integer int29 = operacionesMatematicas0.dividir((java.lang.Integer) (-2233), (java.lang.Integer) 161877486);
        java.lang.Class<?> wildcardClass30 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 12 + "'", int13.equals(12));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 222612 + "'", int17.equals(222612));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 221269 + "'", int20.equals(221269));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-3260) + "'", int23.equals((-3260)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 0 + "'", int26.equals(0));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 0 + "'", int29.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass30);
    }

    @Test
    public void test100() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test100");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 473676, (java.lang.Integer) (-2030));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 20300, (java.lang.Integer) 338956);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 3021, (java.lang.Integer) 3274);
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-961562280) + "'", int12.equals((-961562280)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 359256 + "'", int15.equals(359256));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 6295 + "'", int18.equals(6295));
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test101() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test101");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 2030);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 990, (java.lang.Integer) (-304));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2005917886, (java.lang.Integer) (-650669));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-2233) + "'", int16.equals((-2233)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-300960) + "'", int19.equals((-300960)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 436681114 + "'", int22.equals(436681114));
    }

    @Test
    public void test102() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test102");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) 0);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-507), (java.lang.Integer) 100004);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-1027036680), (java.lang.Integer) (-26796));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2 + "'", int6.equals(2));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-50702028) + "'", int9.equals((-50702028)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 38327 + "'", int13.equals(38327));
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test103() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test103");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 200183, (java.lang.Integer) 203344022);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 203544205 + "'", int7.equals(203544205));
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test104() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test104");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-26796), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) (-6763), (java.lang.Integer) (-4060));
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) (-28474), (java.lang.Integer) 199);
        java.lang.Class<?> wildcardClass27 = operacionesMatematicas0.getClass();
        java.lang.Integer int30 = operacionesMatematicas0.multiplicar((java.lang.Integer) 902932, (java.lang.Integer) 1983457309);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-28474) + "'", int20.equals((-28474)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 1 + "'", int23.equals(1));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-28275) + "'", int26.equals((-28275)));
        org.junit.Assert.assertNotNull(wildcardClass27);
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + (-1273057980) + "'", int30.equals((-1273057980)));
    }

    @Test
    public void test105() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test105");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass4 = operacionesMatematicas0.getClass();
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) (-204), (java.lang.Integer) (-676296));
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-203656852), (java.lang.Integer) (-110265462));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-904417), (java.lang.Integer) 588844663);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-313922314) + "'", int10.equals((-313922314)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
    }

    @Test
    public void test106() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test106");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 473676, (java.lang.Integer) 222612);
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-304));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 333, (java.lang.Integer) 1515772240);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 532602758, (java.lang.Integer) (-3));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 696288 + "'", int7.equals(696288));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 222308 + "'", int10.equals(222308));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 532602761 + "'", int16.equals(532602761));
    }

    @Test
    public void test107() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test107");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-2030), (java.lang.Integer) 677084);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 134282741, (java.lang.Integer) 218);
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 222308, (java.lang.Integer) 1980250204);
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-679114) + "'", int16.equals((-679114)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-791133534) + "'", int19.equals((-791133534)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-95554576) + "'", int22.equals((-95554576)));
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test108() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test108");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-98975152), (java.lang.Integer) (-300960));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) (-9));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-1988598960), (java.lang.Integer) 442626564);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1963548160 + "'", int9.equals(1963548160));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1827) + "'", int12.equals((-1827)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1545972396) + "'", int15.equals((-1545972396)));
    }

    @Test
    public void test109() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test109");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-204), (java.lang.Integer) 12);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 797, (java.lang.Integer) (-201378));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1199317410), (java.lang.Integer) 203344022);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-198), (java.lang.Integer) 198);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-17) + "'", int6.equals((-17)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1402661432) + "'", int12.equals((-1402661432)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1) + "'", int15.equals((-1)));
    }

    @Test
    public void test110() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test110");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 703051, (java.lang.Integer) 151554579);
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-203678316), (java.lang.Integer) (-28176));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-150851528) + "'", int9.equals((-150851528)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 7228 + "'", int12.equals(7228));
    }

    @Test
    public void test111() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test111");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-100), (java.lang.Integer) (-2233));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 675054, (java.lang.Integer) (-201378));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-86631468), (java.lang.Integer) (-317731454));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223300 + "'", int6.equals(223300));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 473676 + "'", int9.equals(473676));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-404362922) + "'", int12.equals((-404362922)));
    }

    @Test
    public void test112() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test112");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-203657735), (java.lang.Integer) (-98975152));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1079, (java.lang.Integer) 222612);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 99692, (java.lang.Integer) 203177900);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) (-689499353), (java.lang.Integer) (-203289362));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-104682583) + "'", int9.equals((-104682583)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-221533) + "'", int12.equals((-221533)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-203078208) + "'", int15.equals((-203078208)));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 3 + "'", int19.equals(3));
    }

    @Test
    public void test113() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test113");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 4058, (java.lang.Integer) 223300);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-203656745), (java.lang.Integer) 680607504);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-200282), (java.lang.Integer) 3451);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 442626564, (java.lang.Integer) 669335577);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-358), (java.lang.Integer) (-666387));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 227358 + "'", int10.equals(227358));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 476950759 + "'", int13.equals(476950759));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-58) + "'", int16.equals((-58)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 698720868 + "'", int19.equals(698720868));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
    }

    @Test
    public void test114() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test114");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 1079, (java.lang.Integer) 679827);
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-198118), (java.lang.Integer) 674850);
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) 166120, (java.lang.Integer) 200282);
        try {
            java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) 1475844812, (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-678748) + "'", int17.equals((-678748)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-872968) + "'", int20.equals((-872968)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-34162) + "'", int23.equals((-34162)));
    }

    @Test
    public void test115() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test115");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 791, (java.lang.Integer) 222612);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-28474), (java.lang.Integer) 2108502328);
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 1853597184);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 223403 + "'", int22.equals(223403));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 2108473854 + "'", int25.equals(2108473854));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-1853597184) + "'", int28.equals((-1853597184)));
    }

    @Test
    public void test116() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test116");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-100), (java.lang.Integer) (-2233));
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 223, (java.lang.Integer) (-2131));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-2031), (java.lang.Integer) 676495);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223300 + "'", int6.equals(223300));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 2354 + "'", int9.equals(2354));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-678526) + "'", int12.equals((-678526)));
    }

    @Test
    public void test117() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test117");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2, (java.lang.Integer) (-1));
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 30106984, (java.lang.Integer) 99);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-1566668296), (java.lang.Integer) (-1964148000));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-2) + "'", int13.equals((-2)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 30107083 + "'", int16.equals(30107083));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 764151000 + "'", int19.equals(764151000));
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test118() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test118");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.sumar((java.lang.Integer) (-2030), (java.lang.Integer) (-674366));
        java.lang.Integer int11 = operacionesMatematicas0.sumar((java.lang.Integer) 203177900, (java.lang.Integer) 166122);
        java.lang.Integer int14 = operacionesMatematicas0.dividir((java.lang.Integer) (-907044204), (java.lang.Integer) 432849736);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + (-676396) + "'", int8.equals((-676396)));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 203344022 + "'", int11.equals(203344022));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + (-2) + "'", int14.equals((-2)));
    }

    @Test
    public void test119() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test119");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-26796), (java.lang.Integer) (-2131));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-674366), (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 790454420, (java.lang.Integer) (-9));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) (-172199), (java.lang.Integer) (-86408168));
        java.lang.Integer int24 = operacionesMatematicas0.restar((java.lang.Integer) 203544205, (java.lang.Integer) (-317731454));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-28927) + "'", int12.equals((-28927)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-133524468) + "'", int15.equals((-133524468)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1475844812 + "'", int18.equals(1475844812));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 86235969 + "'", int21.equals(86235969));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 521275659 + "'", int24.equals(521275659));
    }

    @Test
    public void test120() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test120");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) (-26796));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 28000, (java.lang.Integer) (-676594));
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) 3, (java.lang.Integer) (-2789518));
        java.lang.Integer int31 = operacionesMatematicas0.restar((java.lang.Integer) 1701379988, (java.lang.Integer) (-127644456));
        java.lang.Class<?> wildcardClass32 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-86631468) + "'", int22.equals((-86631468)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 2789521 + "'", int28.equals(2789521));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 1829024444 + "'", int31.equals(1829024444));
        org.junit.Assert.assertNotNull(wildcardClass32);
    }

    @Test
    public void test121() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test121");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-507), (java.lang.Integer) 200871);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-702061), (java.lang.Integer) (-25));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 198, (java.lang.Integer) 7458814);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 98974848, (java.lang.Integer) (-144));
        java.lang.Integer int28 = operacionesMatematicas0.sumar((java.lang.Integer) 1150848763, (java.lang.Integer) 820922168);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-201378) + "'", int16.equals((-201378)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-702086) + "'", int19.equals((-702086)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1476845172 + "'", int22.equals(1476845172));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 98974704 + "'", int25.equals(98974704));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 1971770931 + "'", int28.equals(1971770931));
    }

    @Test
    public void test122() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test122");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) 688);
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) 179683468, (java.lang.Integer) (-202980456));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 12 + "'", int13.equals(12));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 222612 + "'", int17.equals(222612));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-1352861920) + "'", int20.equals((-1352861920)));
    }

    @Test
    public void test123() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test123");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) (-26796));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 28000, (java.lang.Integer) (-676594));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-674424));
        java.lang.Class<?> wildcardClass29 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass30 = operacionesMatematicas0.getClass();
        java.lang.Integer int33 = operacionesMatematicas0.dividir((java.lang.Integer) 203177900, (java.lang.Integer) 199825);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-86631468) + "'", int22.equals((-86631468)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass29);
        org.junit.Assert.assertNotNull(wildcardClass30);
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + 1016 + "'", int33.equals(1016));
    }

    @Test
    public void test124() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test124");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99990, (java.lang.Integer) 14);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-201378), (java.lang.Integer) (-203657735));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 100004 + "'", int9.equals(100004));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-203859113) + "'", int12.equals((-203859113)));
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test125() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test125");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 1542, (java.lang.Integer) 1888659260);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 1888660802 + "'", int7.equals(1888660802));
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test126() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test126");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 990, (java.lang.Integer) 302);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 7458814, (java.lang.Integer) (-676296));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) 218, (java.lang.Integer) 29684000);
        java.lang.Integer int24 = operacionesMatematicas0.dividir((java.lang.Integer) (-654), (java.lang.Integer) (-28275));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 688 + "'", int15.equals(688));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-11) + "'", int18.equals((-11)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-29683782) + "'", int21.equals((-29683782)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 0 + "'", int24.equals(0));
    }

    @Test
    public void test127() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test127");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 223403, (java.lang.Integer) (-203658530));
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203658830, (java.lang.Integer) 1351444);
        java.lang.Integer int26 = operacionesMatematicas0.restar((java.lang.Integer) 20, (java.lang.Integer) 166120);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-203435127) + "'", int20.equals((-203435127)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-885379048) + "'", int23.equals((-885379048)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-166100) + "'", int26.equals((-166100)));
    }

    @Test
    public void test128() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test128");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 473676, (java.lang.Integer) (-2030));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 20300, (java.lang.Integer) 338956);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 3021, (java.lang.Integer) 3274);
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-43315734), (java.lang.Integer) (-1001435549));
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) 113851, (java.lang.Integer) 1144325196);
        java.lang.Integer int27 = operacionesMatematicas0.restar((java.lang.Integer) 669356576, (java.lang.Integer) 5);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-961562280) + "'", int12.equals((-961562280)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 359256 + "'", int15.equals(359256));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 6295 + "'", int18.equals(6295));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 299478398 + "'", int21.equals(299478398));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-970067068) + "'", int24.equals((-970067068)));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 669356571 + "'", int27.equals(669356571));
    }

    @Test
    public void test129() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test129");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) (-1));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 2030, (java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1304999083));
        java.lang.Class<?> wildcardClass15 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2029 + "'", int10.equals(2029));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test130() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test130");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 100004, (java.lang.Integer) 0);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-2030), (java.lang.Integer) 1954933922);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) (-635285), (java.lang.Integer) 1937735020);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 100004 + "'", int12.equals(100004));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1954931892 + "'", int15.equals(1954931892));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
    }

    @Test
    public void test131() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test131");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 1023986076, (java.lang.Integer) 28000);
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-1796254192), (java.lang.Integer) (-98990));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 419736786, (java.lang.Integer) 6825239);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 166, (java.lang.Integer) 0);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 1024014076 + "'", int7.equals(1024014076));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-1796353182) + "'", int10.equals((-1796353182)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 426562025 + "'", int13.equals(426562025));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 166 + "'", int16.equals(166));
    }

    @Test
    public void test132() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test132");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) 101);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 1050105608, (java.lang.Integer) 953704);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2 + "'", int6.equals(2));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 1101 + "'", int10.equals(1101));
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test133() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test133");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 791, (java.lang.Integer) 222612);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-28474), (java.lang.Integer) 2108502328);
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) 200282, (java.lang.Integer) 134282741);
        java.lang.Integer int31 = operacionesMatematicas0.sumar((java.lang.Integer) 191, (java.lang.Integer) 2117069372);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 223403 + "'", int22.equals(223403));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 2108473854 + "'", int25.equals(2108473854));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-134082459) + "'", int28.equals((-134082459)));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 2117069563 + "'", int31.equals(2117069563));
    }

    @Test
    public void test134() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test134");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-223), (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-4060), (java.lang.Integer) 2030);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 386, (java.lang.Integer) 3062);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 1567919351, (java.lang.Integer) 374697400);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223 + "'", int6.equals(223));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2) + "'", int9.equals((-2)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1193221951 + "'", int16.equals(1193221951));
    }

    @Test
    public void test135() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test135");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1913));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-1566625664), (java.lang.Integer) (-1566625664));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 499871, (java.lang.Integer) (-300960));
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) (-100), (java.lang.Integer) 521275659);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 117 + "'", int9.equals(117));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1 + "'", int12.equals(1));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 198911 + "'", int15.equals(198911));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
    }

    @Test
    public void test136() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test136");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) (-872882432), (java.lang.Integer) (-6763));
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1641), (java.lang.Integer) 200184);
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass25 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 129067 + "'", int20.equals(129067));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-328501944) + "'", int23.equals((-328501944)));
        org.junit.Assert.assertNotNull(wildcardClass24);
        org.junit.Assert.assertNotNull(wildcardClass25);
    }

    @Test
    public void test137() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test137");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-223), (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-4060), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-202979700), (java.lang.Integer) 386);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1913), (java.lang.Integer) (-203658612));
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 1554526525);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223 + "'", int6.equals(223));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2) + "'", int9.equals((-2)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-202979314) + "'", int12.equals((-202979314)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1243099180) + "'", int15.equals((-1243099180)));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1554526525) + "'", int19.equals((-1554526525)));
    }

    @Test
    public void test138() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test138");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-98975152), (java.lang.Integer) (-791133534));
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.multiplicar((java.lang.Integer) 9992694, (java.lang.Integer) 0);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-890108686) + "'", int10.equals((-890108686)));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14.equals(0));
    }

    @Test
    public void test139() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test139");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.restar((java.lang.Integer) (-3160), (java.lang.Integer) (-203662590));
        java.lang.Integer int11 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1913), (java.lang.Integer) 191);
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 203659430 + "'", int8.equals(203659430));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-365383) + "'", int11.equals((-365383)));
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test140() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test140");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2000);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-11), (java.lang.Integer) (-678074));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-391), (java.lang.Integer) 975452);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 438482334, (java.lang.Integer) (-702061));
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) 5050, (java.lang.Integer) 326559708);
        java.lang.Integer int24 = operacionesMatematicas0.dividir((java.lang.Integer) (-6), (java.lang.Integer) (-157900046));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 7458814 + "'", int12.equals(7458814));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 437780273 + "'", int18.equals(437780273));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-140916264) + "'", int21.equals((-140916264)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 0 + "'", int24.equals(0));
    }

    @Test
    public void test141() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test141");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 679827);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-86407950), (java.lang.Integer) 419736786);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-485640240), (java.lang.Integer) 677084);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 199, (java.lang.Integer) 569427227);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 145112, (java.lang.Integer) (-343065842));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-679726) + "'", int6.equals((-679726)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-835045696) + "'", int12.equals((-835045696)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-569427028) + "'", int15.equals((-569427028)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 343210954 + "'", int18.equals(343210954));
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test142() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test142");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 202);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) (-2030));
        try {
            java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) (-1566890908), (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1 + "'", int15.equals(1));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 677084 + "'", int18.equals(677084));
    }

    @Test
    public void test143() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test143");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 677082, (java.lang.Integer) 797);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-86797590), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Integer int29 = operacionesMatematicas0.restar((java.lang.Integer) (-1750448650), (java.lang.Integer) 203659430);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 849 + "'", int22.equals(849));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-1954108080) + "'", int29.equals((-1954108080)));
    }

    @Test
    public void test144() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test144");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-2233));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 227605, (java.lang.Integer) (-1378461349));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-674366) + "'", int9.equals((-674366)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1378688954 + "'", int12.equals(1378688954));
    }

    @Test
    public void test145() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test145");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-507), (java.lang.Integer) 200871);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-702061), (java.lang.Integer) (-25));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 198, (java.lang.Integer) 7458814);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 675054, (java.lang.Integer) (-2031));
        java.lang.Integer int28 = operacionesMatematicas0.sumar((java.lang.Integer) (-164003), (java.lang.Integer) (-891965756));
        java.lang.Integer int31 = operacionesMatematicas0.multiplicar((java.lang.Integer) 28345, (java.lang.Integer) 23);
        java.lang.Integer int34 = operacionesMatematicas0.dividir((java.lang.Integer) 2108373864, (java.lang.Integer) 129067);
        java.lang.Integer int37 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 829469505);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-201378) + "'", int16.equals((-201378)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-702086) + "'", int19.equals((-702086)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1476845172 + "'", int22.equals(1476845172));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 673023 + "'", int25.equals(673023));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-892129759) + "'", int28.equals((-892129759)));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 651935 + "'", int31.equals(651935));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 16335 + "'", int34.equals(16335));
        org.junit.Assert.assertTrue("'" + int37 + "' != '" + 280425486 + "'", int37.equals(280425486));
    }

    @Test
    public void test146() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test146");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1089, (java.lang.Integer) (-1027036680));
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) (-791133534));
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1751447560) + "'", int19.equals((-1751447560)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 791133635 + "'", int22.equals(791133635));
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test147() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test147");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-1085863536), (java.lang.Integer) (-391));
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-203761580), (java.lang.Integer) (-798007));
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.restar((java.lang.Integer) 1374, (java.lang.Integer) 102968);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-1085863927) + "'", int7.equals((-1085863927)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-204559587) + "'", int10.equals((-204559587)));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + (-101594) + "'", int14.equals((-101594)));
    }

    @Test
    public void test148() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test148");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-673143), (java.lang.Integer) (-100));
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-203658530), (java.lang.Integer) (-1072091024));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 218, (java.lang.Integer) (-9));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 6731 + "'", int10.equals(6731));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 868432494 + "'", int13.equals(868432494));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-1962) + "'", int16.equals((-1962)));
    }

    @Test
    public void test149() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test149");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-2030), (java.lang.Integer) 677084);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 7458814, (java.lang.Integer) (-635285));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) 1349700, (java.lang.Integer) 1771576096);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-679114) + "'", int16.equals((-679114)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 6823529 + "'", int19.equals(6823529));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 1772925796 + "'", int23.equals(1772925796));
    }

    @Test
    public void test150() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test150");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) 688);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) (-2031), (java.lang.Integer) 223300);
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) 191, (java.lang.Integer) 3451);
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) (-86797590), (java.lang.Integer) (-1072091024));
        java.lang.Integer int29 = operacionesMatematicas0.dividir((java.lang.Integer) (-2233), (java.lang.Integer) 161877486);
        java.lang.Integer int32 = operacionesMatematicas0.restar((java.lang.Integer) 1868537856, (java.lang.Integer) 1023986076);
        java.lang.Integer int35 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1772181088, (java.lang.Integer) (-101594));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 12 + "'", int13.equals(12));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 222612 + "'", int17.equals(222612));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 221269 + "'", int20.equals(221269));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-3260) + "'", int23.equals((-3260)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 0 + "'", int26.equals(0));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 0 + "'", int29.equals(0));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 844551780 + "'", int32.equals(844551780));
        org.junit.Assert.assertTrue("'" + int35 + "' != '" + 2063594048 + "'", int35.equals(2063594048));
    }

    @Test
    public void test151() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test151");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-98975152), (java.lang.Integer) (-791133534));
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 3241260);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-890108686) + "'", int10.equals((-890108686)));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + (-3241260) + "'", int14.equals((-3241260)));
    }

    @Test
    public void test152() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test152");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-100), (java.lang.Integer) (-2233));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 675054, (java.lang.Integer) (-201378));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-2099619127), (java.lang.Integer) (-680500222));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) 431);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223300 + "'", int6.equals(223300));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 473676 + "'", int9.equals(473676));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1419118905) + "'", int12.equals((-1419118905)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
    }

    @Test
    public void test153() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test153");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-2031), (java.lang.Integer) (-100));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-679014), (java.lang.Integer) 28345);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-304), (java.lang.Integer) (-98975152));
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 13135, (java.lang.Integer) (-2108473877));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-2131) + "'", int10.equals((-2131)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-650669) + "'", int13.equals((-650669)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 98974848 + "'", int16.equals(98974848));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-855249787) + "'", int19.equals((-855249787)));
    }

    @Test
    public void test154() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test154");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 223403, (java.lang.Integer) (-203658530));
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203658830, (java.lang.Integer) 1351444);
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) 1702393088, (java.lang.Integer) (-358));
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1771576096, (java.lang.Integer) (-676296));
        java.lang.Integer int32 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-479791808), (java.lang.Integer) (-5707));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-203435127) + "'", int20.equals((-203435127)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-885379048) + "'", int23.equals((-885379048)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-4755287) + "'", int26.equals((-4755287)));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 1364569856 + "'", int29.equals(1364569856));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + (-2017286592) + "'", int32.equals((-2017286592)));
    }

    @Test
    public void test155() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test155");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) 101);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 28053, (java.lang.Integer) 849);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 2107797558, (java.lang.Integer) (-2010186930));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1352758348), (java.lang.Integer) 985806408);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2 + "'", int6.equals(2));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 23816997 + "'", int10.equals(23816997));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-176982808) + "'", int13.equals((-176982808)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-361282912) + "'", int16.equals((-361282912)));
    }

    @Test
    public void test156() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test156");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 374697400, (java.lang.Integer) 473676);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 44906012);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-1073433704), (java.lang.Integer) 86235969);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 146, (java.lang.Integer) 14);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 791 + "'", int13.equals(791));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-44906012) + "'", int16.equals((-44906012)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-987197735) + "'", int19.equals((-987197735)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 132 + "'", int22.equals(132));
    }

    @Test
    public void test157() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test157");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-203658532), (java.lang.Integer) 797);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 138125136, (java.lang.Integer) (-198));
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-104682583), (java.lang.Integer) (-673143));
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-203566734), (java.lang.Integer) (-320292228));
        java.lang.Class<?> wildcardClass29 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-203657735) + "'", int18.equals((-203657735)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 138124938 + "'", int21.equals(138124938));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-105355726) + "'", int25.equals((-105355726)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 820922168 + "'", int28.equals(820922168));
        org.junit.Assert.assertNotNull(wildcardClass29);
    }

    @Test
    public void test158() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test158");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2000, (java.lang.Integer) 6559757);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1566625664), (java.lang.Integer) (-1546915));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 1593226934, (java.lang.Integer) 1868537856);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 234612112 + "'", int9.equals(234612112));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1442258560 + "'", int12.equals(1442258560));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-833202506) + "'", int15.equals((-833202506)));
    }

    @Test
    public void test159() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test159");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1372443108, (java.lang.Integer) (-59));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1372443167 + "'", int12.equals(1372443167));
    }

    @Test
    public void test160() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test160");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) (-589628652), (java.lang.Integer) (-169435336));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) (-203437481), (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 3 + "'", int19.equals(3));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-203437481) + "'", int22.equals((-203437481)));
    }

    @Test
    public void test161() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test161");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-223), (java.lang.Integer) (-1429679577));
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 134282741, (java.lang.Integer) 102968);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-86408168), (java.lang.Integer) (-985706892));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1429679800) + "'", int13.equals((-1429679800)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1304 + "'", int16.equals(1304));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1072115060) + "'", int19.equals((-1072115060)));
    }

    @Test
    public void test162() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test162");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 2, (java.lang.Integer) 1023986076);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 113851, (java.lang.Integer) (-906878093));
        java.lang.Integer int24 = operacionesMatematicas0.dividir((java.lang.Integer) 168, (java.lang.Integer) (-319239361));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-906764242) + "'", int21.equals((-906764242)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 0 + "'", int24.equals(0));
    }

    @Test
    public void test163() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test163");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-882132), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) (-679114), (java.lang.Integer) 100);
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) 153262720, (java.lang.Integer) 23816997);
        java.lang.Integer int29 = operacionesMatematicas0.dividir((java.lang.Integer) 203654756, (java.lang.Integer) (-2147357664));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-1480217496) + "'", int20.equals((-1480217496)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-679014) + "'", int23.equals((-679014)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 6 + "'", int26.equals(6));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 0 + "'", int29.equals(0));
    }

    @Test
    public void test164() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test164");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) (-203658530), (java.lang.Integer) (-678074));
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) 852483576, (java.lang.Integer) 1089);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 12 + "'", int13.equals(12));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-202980456) + "'", int17.equals((-202980456)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 641678328 + "'", int20.equals(641678328));
    }

    @Test
    public void test165() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test165");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-203658532), (java.lang.Integer) 797);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 138125136, (java.lang.Integer) (-198));
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) 2029);
        java.lang.Integer int27 = operacionesMatematicas0.multiplicar((java.lang.Integer) 42226412, (java.lang.Integer) 1592563465);
        java.lang.Class<?> wildcardClass28 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-203657735) + "'", int18.equals((-203657735)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 138124938 + "'", int21.equals(138124938));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 6559757 + "'", int24.equals(6559757));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 1025515084 + "'", int27.equals(1025515084));
        org.junit.Assert.assertNotNull(wildcardClass28);
    }

    @Test
    public void test166() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test166");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass9 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test167() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test167");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) 101);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2 + "'", int6.equals(2));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test168() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test168");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-100), (java.lang.Integer) (-2233));
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 2679600, (java.lang.Integer) (-406034920));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-1427466717), (java.lang.Integer) (-181782106));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223300 + "'", int6.equals(223300));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 408714520 + "'", int9.equals(408714520));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 7 + "'", int12.equals(7));
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test169() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test169");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 2, (java.lang.Integer) 1023986076);
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test170() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test170");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1089, (java.lang.Integer) (-1027036680));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 23816997, (java.lang.Integer) 222308);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166111, (java.lang.Integer) 1023985958);
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) (-11), (java.lang.Integer) 1418013632);
        java.lang.Integer int31 = operacionesMatematicas0.restar((java.lang.Integer) 153188039, (java.lang.Integer) 19735044);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1751447560) + "'", int19.equals((-1751447560)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-985706892) + "'", int22.equals((-985706892)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1741645850 + "'", int25.equals(1741645850));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-1418013643) + "'", int28.equals((-1418013643)));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 133452995 + "'", int31.equals(133452995));
    }

    @Test
    public void test171() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test171");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 14, (java.lang.Integer) (-3260));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-86408168), (java.lang.Integer) 673023);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3274 + "'", int12.equals(3274));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-827264024) + "'", int15.equals((-827264024)));
    }

    @Test
    public void test172() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test172");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 1678);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) 797);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-343058591), (java.lang.Integer) 961940492);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 1954229806, (java.lang.Integer) 153);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 166122 + "'", int10.equals(166122));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-1304999083) + "'", int16.equals((-1304999083)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 12772743 + "'", int19.equals(12772743));
    }

    @Test
    public void test173() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test173");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 338956, (java.lang.Integer) (-337227660));
        try {
            java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 1973854425, (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
    }

    @Test
    public void test174() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test174");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 34329, (java.lang.Integer) (-1468944503));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 12715818, (java.lang.Integer) (-203435127));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 216150945 + "'", int12.equals(216150945));
    }

    @Test
    public void test175() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test175");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 688, (java.lang.Integer) 990);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1636528032, (java.lang.Integer) (-6451));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-1454200156), (java.lang.Integer) (-328501944));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1678 + "'", int9.equals(1678));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1636534483 + "'", int12.equals(1636534483));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1782702100) + "'", int15.equals((-1782702100)));
    }

    @Test
    public void test176() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test176");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 100004, (java.lang.Integer) 0);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-17), (java.lang.Integer) (-403));
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2114056976), (java.lang.Integer) 99);
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) (-1072091024), (java.lang.Integer) (-2131));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 100004 + "'", int12.equals(100004));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 386 + "'", int15.equals(386));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1161756880 + "'", int18.equals(1161756880));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1072088893) + "'", int21.equals((-1072088893)));
    }

    @Test
    public void test177() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test177");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) (-292));
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) (-5), (java.lang.Integer) (-679014));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) (-501776), (java.lang.Integer) (-717341912));
        java.lang.Integer int26 = operacionesMatematicas0.restar((java.lang.Integer) (-626880008), (java.lang.Integer) 886390221);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-882132) + "'", int16.equals((-882132)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 716840136 + "'", int23.equals(716840136));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-1513270229) + "'", int26.equals((-1513270229)));
    }

    @Test
    public void test178() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test178");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 202);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-50702028), (java.lang.Integer) 703051);
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1 + "'", int15.equals(1));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 2117069372 + "'", int18.equals(2117069372));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test179() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test179");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 202);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) (-2030));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 134282741, (java.lang.Integer) 453522102);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 223, (java.lang.Integer) 203204);
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Integer int29 = operacionesMatematicas0.sumar((java.lang.Integer) (-1750448650), (java.lang.Integer) 203659430);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1 + "'", int15.equals(1));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 677084 + "'", int18.equals(677084));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-319239361) + "'", int22.equals((-319239361)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 203427 + "'", int25.equals(203427));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-1546789220) + "'", int29.equals((-1546789220)));
    }

    @Test
    public void test180() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test180");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) (-3), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.sumar((java.lang.Integer) 703051, (java.lang.Integer) 222308);
        java.lang.Integer int14 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1617522543, (java.lang.Integer) (-250078738));
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) (-1098353173), (java.lang.Integer) 1888660802);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-3) + "'", int7.equals((-3)));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 925359 + "'", int11.equals(925359));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + (-548442062) + "'", int14.equals((-548442062)));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 1307953321 + "'", int17.equals(1307953321));
    }

    @Test
    public void test181() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test181");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) 1853597184);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-480400), (java.lang.Integer) 1624322327);
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 113851, (java.lang.Integer) (-569427028));
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 0 + "'", int4.equals(0));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-1902651632) + "'", int7.equals((-1902651632)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test182() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test182");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-56948000), (java.lang.Integer) (-86631468));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 311282688, (java.lang.Integer) 2941);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 105842 + "'", int12.equals(105842));
    }

    @Test
    public void test183() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test183");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 202);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) (-2030));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 791, (java.lang.Integer) 677082);
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) 44906216, (java.lang.Integer) (-204));
        java.lang.Integer int29 = operacionesMatematicas0.dividir((java.lang.Integer) (-203435127), (java.lang.Integer) (-203437481));
        java.lang.Integer int32 = operacionesMatematicas0.multiplicar((java.lang.Integer) 675040, (java.lang.Integer) 153263227);
        try {
            java.lang.Integer int35 = operacionesMatematicas0.dividir((java.lang.Integer) (-328501944), (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1 + "'", int15.equals(1));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 677084 + "'", int18.equals(677084));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 44906012 + "'", int26.equals(44906012));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 0 + "'", int29.equals(0));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 1636528032 + "'", int32.equals(1636528032));
    }

    @Test
    public void test184() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test184");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 676495, (java.lang.Integer) (-9));
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) 2679600, (java.lang.Integer) 204);
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) 1586840422, (java.lang.Integer) 1471);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 676504 + "'", int17.equals(676504));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 13135 + "'", int20.equals(13135));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 1586841893 + "'", int23.equals(1586841893));
    }

    @Test
    public void test185() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test185");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 703051, (java.lang.Integer) 151554579);
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 1431676816);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-691719897), (java.lang.Integer) (-1796254192));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-150851528) + "'", int9.equals((-150851528)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1806993207 + "'", int15.equals(1806993207));
    }

    @Test
    public void test186() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test186");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass4 = operacionesMatematicas0.getClass();
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) (-204), (java.lang.Integer) (-676296));
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-203656852), (java.lang.Integer) (-110265462));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-1077768), (java.lang.Integer) (-300960));
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-764249990), (java.lang.Integer) (-203656852));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-313922314) + "'", int10.equals((-313922314)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 3 + "'", int13.equals(3));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-560593138) + "'", int16.equals((-560593138)));
    }

    @Test
    public void test187() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test187");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 101);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) (-985706892), (java.lang.Integer) 223403);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 101 + "'", int4.equals(101));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-985930295) + "'", int7.equals((-985930295)));
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test188() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test188");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 2354, (java.lang.Integer) (-403));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 1983457309, (java.lang.Integer) (-104553516));
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 1727250696, (java.lang.Integer) 2137056006);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-5) + "'", int12.equals((-5)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1878903793 + "'", int15.equals(1878903793));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
    }

    @Test
    public void test189() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test189");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 315901, (java.lang.Integer) (-678748));
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 2053450600, (java.lang.Integer) 2107797558);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 994649 + "'", int7.equals(994649));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-133719138) + "'", int10.equals((-133719138)));
    }

    @Test
    public void test190() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test190");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 674850, (java.lang.Integer) (-203658532));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 338956, (java.lang.Integer) (-3030));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2233), (java.lang.Integer) (-1027036680));
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-885379048), (java.lang.Integer) 3274);
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1362967132, (java.lang.Integer) 102968);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1027036680) + "'", int12.equals((-1027036680)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-139629624) + "'", int15.equals((-139629624)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 371921648 + "'", int18.equals(371921648));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-351716320) + "'", int21.equals((-351716320)));
    }

    @Test
    public void test191() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test191");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-882132), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 676495, (java.lang.Integer) 2030);
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass25 = operacionesMatematicas0.getClass();
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) 1702393088, (java.lang.Integer) 299478398);
        java.lang.Integer int31 = operacionesMatematicas0.sumar((java.lang.Integer) 1981352906, (java.lang.Integer) (-694249));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-1480217496) + "'", int20.equals((-1480217496)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 333 + "'", int23.equals(333));
        org.junit.Assert.assertNotNull(wildcardClass24);
        org.junit.Assert.assertNotNull(wildcardClass25);
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 1402914690 + "'", int28.equals(1402914690));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 1980658657 + "'", int31.equals(1980658657));
    }

    @Test
    public void test192() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test192");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-676594), (java.lang.Integer) 3451);
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) 2348, (java.lang.Integer) 200184);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 12 + "'", int13.equals(12));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-673143) + "'", int17.equals((-673143)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-197836) + "'", int20.equals((-197836)));
    }

    @Test
    public void test193() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test193");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 473676, (java.lang.Integer) (-2030));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-292), (java.lang.Integer) 2108502328);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-86797590), (java.lang.Integer) 674850);
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2005917886, (java.lang.Integer) (-961562280));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-961562280) + "'", int12.equals((-961562280)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-589628652) + "'", int19.equals((-589628652)));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-1463775408) + "'", int23.equals((-1463775408)));
    }

    @Test
    public void test194() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test194");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-203656345), (java.lang.Integer) (-1414472163));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
    }

    @Test
    public void test195() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test195");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 200183, (java.lang.Integer) 203344022);
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 3241260, (java.lang.Integer) 1352089752);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 203544205 + "'", int7.equals(203544205));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 1355331012 + "'", int10.equals(1355331012));
    }

    @Test
    public void test196() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test196");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) 688);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) (-1), (java.lang.Integer) (-304));
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 925359, (java.lang.Integer) (-104553516));
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) 852483576, (java.lang.Integer) (-689109));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 12 + "'", int13.equals(12));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 222612 + "'", int17.equals(222612));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-305) + "'", int20.equals((-305)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 0 + "'", int23.equals(0));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-1362728792) + "'", int26.equals((-1362728792)));
    }

    @Test
    public void test197() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test197");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99990, (java.lang.Integer) 14);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-678074), (java.lang.Integer) 3451);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 2063593958, (java.lang.Integer) 268);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 100004 + "'", int9.equals(100004));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1954933922 + "'", int12.equals(1954933922));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 7699977 + "'", int15.equals(7699977));
    }

    @Test
    public void test198() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test198");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 925359, (java.lang.Integer) 28345);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 10672420, (java.lang.Integer) (-679726));
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) (-1480217496), (java.lang.Integer) 1000);
        try {
            java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 241172932, (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 953704 + "'", int13.equals(953704));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 9992694 + "'", int16.equals(9992694));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1480217) + "'", int19.equals((-1480217)));
    }

    @Test
    public void test199() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test199");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 1079, (java.lang.Integer) 679827);
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) 5050, (java.lang.Integer) (-1566625664));
        java.lang.Class<?> wildcardClass21 = operacionesMatematicas0.getClass();
        java.lang.Integer int24 = operacionesMatematicas0.restar((java.lang.Integer) (-702061), (java.lang.Integer) (-702086));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-678748) + "'", int17.equals((-678748)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-129843968) + "'", int20.equals((-129843968)));
        org.junit.Assert.assertNotNull(wildcardClass21);
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 25 + "'", int24.equals(25));
    }

    @Test
    public void test200() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test200");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) (-1646083254), (java.lang.Integer) 227358);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-1646310612) + "'", int10.equals((-1646310612)));
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test201() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test201");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) (-1));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 2030, (java.lang.Integer) 1);
        try {
            java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-1338190428), (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2029 + "'", int10.equals(2029));
    }

    @Test
    public void test202() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test202");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 101);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) (-674366), (java.lang.Integer) 3020);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 101 + "'", int4.equals(101));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-223) + "'", int7.equals((-223)));
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test203() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test203");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) (-2452019), (java.lang.Integer) 591647436);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 673023, (java.lang.Integer) (-5));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 1678, (java.lang.Integer) (-1988598960));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) 1115055585);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-594099455) + "'", int6.equals((-594099455)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-134604) + "'", int9.equals((-134604)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test204() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test204");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 677082, (java.lang.Integer) 797);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-86797590), (java.lang.Integer) 0);
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) 675054, (java.lang.Integer) 1963548060);
        java.lang.Integer int31 = operacionesMatematicas0.restar((java.lang.Integer) 200183, (java.lang.Integer) 4738865);
        java.lang.Integer int34 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 1355141328);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 849 + "'", int22.equals(849));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 1050105608 + "'", int28.equals(1050105608));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-4538682) + "'", int31.equals((-4538682)));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + (-1355141328) + "'", int34.equals((-1355141328)));
    }

    @Test
    public void test205() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test205");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99692, (java.lang.Integer) (-26796));
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 186, (java.lang.Integer) 218);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1623620464 + "'", int15.equals(1623620464));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 40548 + "'", int18.equals(40548));
    }

    @Test
    public void test206() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test206");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) (-26796));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) (-3));
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) 677084, (java.lang.Integer) 204);
        java.lang.Integer int31 = operacionesMatematicas0.dividir((java.lang.Integer) (-300943), (java.lang.Integer) 920644544);
        java.lang.Integer int34 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-299613002), (java.lang.Integer) 14842);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-86631468) + "'", int22.equals((-86631468)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-103) + "'", int25.equals((-103)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 138125136 + "'", int28.equals(138125136));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 0 + "'", int31.equals(0));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + (-1565024324) + "'", int34.equals((-1565024324)));
    }

    @Test
    public void test207() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test207");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 674850, (java.lang.Integer) (-203658532));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test208() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test208");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 2117069372, (java.lang.Integer) (-91798));
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 859521, (java.lang.Integer) 30106984);
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-23062) + "'", int19.equals((-23062)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-29247463) + "'", int22.equals((-29247463)));
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test209() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test209");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-98975152), (java.lang.Integer) (-300960));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 204, (java.lang.Integer) 0);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-140916264), (java.lang.Integer) (-680500910));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1963548160 + "'", int9.equals(1963548160));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 204 + "'", int12.equals(204));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-821417174) + "'", int15.equals((-821417174)));
    }

    @Test
    public void test210() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test210");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 202);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) (-2030));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 791, (java.lang.Integer) 677082);
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) 203881138, (java.lang.Integer) (-172199));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1 + "'", int15.equals(1));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 677084 + "'", int18.equals(677084));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-1183) + "'", int26.equals((-1183)));
    }

    @Test
    public void test211() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test211");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-2030), (java.lang.Integer) 677084);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 134282741, (java.lang.Integer) 218);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 669356581, (java.lang.Integer) (-204));
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 4530202, (java.lang.Integer) (-1264536494));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-679114) + "'", int16.equals((-679114)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-791133534) + "'", int19.equals((-791133534)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 669356785 + "'", int22.equals(669356785));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 150376532 + "'", int25.equals(150376532));
    }

    @Test
    public void test212() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test212");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 1542, (java.lang.Integer) 1888659260);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) (-7), (java.lang.Integer) 0);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 200232, (java.lang.Integer) 0);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 1888660802 + "'", int7.equals(1888660802));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-7) + "'", int10.equals((-7)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 200232 + "'", int13.equals(200232));
    }

    @Test
    public void test213() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test213");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-139407012), (java.lang.Integer) 30106984);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-109300028) + "'", int6.equals((-109300028)));
    }

    @Test
    public void test214() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test214");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) (-3), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.sumar((java.lang.Integer) 703051, (java.lang.Integer) 222308);
        java.lang.Integer int14 = operacionesMatematicas0.restar((java.lang.Integer) (-203654655), (java.lang.Integer) (-895373708));
        java.lang.Integer int17 = operacionesMatematicas0.dividir((java.lang.Integer) 1711856, (java.lang.Integer) 1980273266);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-3) + "'", int7.equals((-3)));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 925359 + "'", int11.equals(925359));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 691719053 + "'", int14.equals(691719053));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
    }

    @Test
    public void test215() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test215");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-320292228), (java.lang.Integer) (-204));
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-589656652));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-1614297536), (java.lang.Integer) (-2030));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2030) + "'", int9.equals((-2030)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1570059 + "'", int13.equals(1570059));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-589654622) + "'", int16.equals((-589654622)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1614295506) + "'", int19.equals((-1614295506)));
    }

    @Test
    public void test216() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test216");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.sumar((java.lang.Integer) 200183, (java.lang.Integer) (-26796));
        java.lang.Class<?> wildcardClass9 = operacionesMatematicas0.getClass();
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 791133635, (java.lang.Integer) 2030);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-2114056976), (java.lang.Integer) (-846460796));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 173387 + "'", int8.equals(173387));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 791131605 + "'", int12.equals(791131605));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 2 + "'", int15.equals(2));
    }

    @Test
    public void test217() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test217");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 10, (java.lang.Integer) 302);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-844), (java.lang.Integer) 675040);
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) (-6451), (java.lang.Integer) (-139629624));
        java.lang.Integer int24 = operacionesMatematicas0.restar((java.lang.Integer) (-674366), (java.lang.Integer) 338956);
        try {
            java.lang.Integer int27 = operacionesMatematicas0.dividir((java.lang.Integer) (-203761583), (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 3020 + "'", int15.equals(3020));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 674196 + "'", int18.equals(674196));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1013322) + "'", int24.equals((-1013322)));
    }

    @Test
    public void test218() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test218");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 473676, (java.lang.Integer) (-2030));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-292), (java.lang.Integer) 2108502328);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-86797590), (java.lang.Integer) 674850);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 225533, (java.lang.Integer) 0);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 1199317608, (java.lang.Integer) 1980250204);
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) 173085920, (java.lang.Integer) (-305));
        java.lang.Integer int31 = operacionesMatematicas0.dividir((java.lang.Integer) (-1338190428), (java.lang.Integer) 1199317608);
        java.lang.Integer int34 = operacionesMatematicas0.restar((java.lang.Integer) 1963548060, (java.lang.Integer) (-847810496));
        java.lang.Class<?> wildcardClass35 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-961562280) + "'", int12.equals((-961562280)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-589628652) + "'", int19.equals((-589628652)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 225533 + "'", int22.equals(225533));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-1115399484) + "'", int25.equals((-1115399484)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-1251598048) + "'", int28.equals((-1251598048)));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-1) + "'", int31.equals((-1)));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + (-1483608740) + "'", int34.equals((-1483608740)));
        org.junit.Assert.assertNotNull(wildcardClass35);
    }

    @Test
    public void test219() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test219");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-337227660), (java.lang.Integer) (-2962));
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-2131));
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-317895457), (java.lang.Integer) 0);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1729412004, (java.lang.Integer) 223);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 113851 + "'", int10.equals(113851));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-317895457) + "'", int16.equals((-317895457)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-888179748) + "'", int19.equals((-888179748)));
    }

    @Test
    public void test220() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test220");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-204), (java.lang.Integer) 12);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 797, (java.lang.Integer) (-201378));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-202979700), (java.lang.Integer) 221269);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-563780412), (java.lang.Integer) 696288);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-17) + "'", int6.equals((-17)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-917) + "'", int12.equals((-917)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-563084124) + "'", int15.equals((-563084124)));
    }

    @Test
    public void test221() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test221");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 791, (java.lang.Integer) 222612);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-28474), (java.lang.Integer) 2108502328);
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) 200282, (java.lang.Integer) 134282741);
        java.lang.Integer int31 = operacionesMatematicas0.dividir((java.lang.Integer) (-1480217496), (java.lang.Integer) 1362965102);
        java.lang.Integer int34 = operacionesMatematicas0.dividir((java.lang.Integer) (-1360503017), (java.lang.Integer) 1988598756);
        java.lang.Class<?> wildcardClass35 = operacionesMatematicas0.getClass();
        java.lang.Integer int38 = operacionesMatematicas0.sumar((java.lang.Integer) 1542, (java.lang.Integer) (-586818265));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 223403 + "'", int22.equals(223403));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 2108473854 + "'", int25.equals(2108473854));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-134082459) + "'", int28.equals((-134082459)));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-1) + "'", int31.equals((-1)));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 0 + "'", int34.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass35);
        org.junit.Assert.assertTrue("'" + int38 + "' != '" + (-586816723) + "'", int38.equals((-586816723)));
    }

    @Test
    public void test222() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test222");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99990, (java.lang.Integer) 14);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-678074), (java.lang.Integer) 3451);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-403), (java.lang.Integer) 0);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-3353), (java.lang.Integer) (-44902128));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 100004 + "'", int9.equals(100004));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1954933922 + "'", int12.equals(1954933922));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-403) + "'", int15.equals((-403)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 44898775 + "'", int18.equals(44898775));
    }

    @Test
    public void test223() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test223");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1913));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-300960), (java.lang.Integer) (-17));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-682380), (java.lang.Integer) 7809);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 117 + "'", int9.equals(117));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-300943) + "'", int12.equals((-300943)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-674571) + "'", int15.equals((-674571)));
    }

    @Test
    public void test224() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test224");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 688, (java.lang.Integer) 990);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1678 + "'", int9.equals(1678));
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test225() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test225");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-304));
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) 227581);
        java.lang.Class<?> wildcardClass15 = operacionesMatematicas0.getClass();
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 10472248, (java.lang.Integer) (-1231212116));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass15);
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1220739868) + "'", int18.equals((-1220739868)));
    }

    @Test
    public void test226() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test226");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-2030), (java.lang.Integer) 677084);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 438482334, (java.lang.Integer) 44540629);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-203654655), (java.lang.Integer) 678545);
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 20300, (java.lang.Integer) 3062);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-679114) + "'", int16.equals((-679114)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1809913974 + "'", int19.equals(1809913974));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-300) + "'", int22.equals((-300)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 6 + "'", int25.equals(6));
    }

    @Test
    public void test227() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test227");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-4060), (java.lang.Integer) (-203658530));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) (-906764242));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-328150775), (java.lang.Integer) 903096066);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203662590) + "'", int6.equals((-203662590)));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-906764242) + "'", int10.equals((-906764242)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
    }

    @Test
    public void test228() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test228");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2, (java.lang.Integer) 1000);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) (-201378));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2000 + "'", int6.equals(2000));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-201378) + "'", int9.equals((-201378)));
    }

    @Test
    public void test229() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test229");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 10, (java.lang.Integer) 302);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-844), (java.lang.Integer) 675040);
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) 797, (java.lang.Integer) (-198118));
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 43818352, (java.lang.Integer) (-1753913018));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) 797, (java.lang.Integer) (-594099455));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 3020 + "'", int15.equals(3020));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 674196 + "'", int18.equals(674196));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-157900046) + "'", int21.equals((-157900046)));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-141165408) + "'", int25.equals((-141165408)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
    }

    @Test
    public void test230() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test230");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2354, (java.lang.Integer) 2679600);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-98990), (java.lang.Integer) (-4060));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-374696609), (java.lang.Integer) 985806408);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 1868537856, (java.lang.Integer) (-5043200));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 24 + "'", int16.equals(24));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1360503017) + "'", int19.equals((-1360503017)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1863494656 + "'", int22.equals(1863494656));
    }

    @Test
    public void test231() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test231");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2, (java.lang.Integer) 1000);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1794580014, (java.lang.Integer) 1727250696);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2000 + "'", int6.equals(2000));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-1625055376) + "'", int9.equals((-1625055376)));
    }

    @Test
    public void test232() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test232");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-2233));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 849, (java.lang.Integer) 925359);
        try {
            java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 203165314, (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-674366) + "'", int9.equals((-674366)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-924510) + "'", int12.equals((-924510)));
    }

    @Test
    public void test233() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test233");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) 688);
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) (-25), (java.lang.Integer) (-747018902));
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 12 + "'", int13.equals(12));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 222612 + "'", int17.equals(222612));
        org.junit.Assert.assertNotNull(wildcardClass18);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-747018927) + "'", int21.equals((-747018927)));
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test234() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test234");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-2233), (java.lang.Integer) 199);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 227358, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 28000, (java.lang.Integer) 675040);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 218, (java.lang.Integer) (-98));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 677082, (java.lang.Integer) 589628875);
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 164521522, (java.lang.Integer) (-1455213478));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-11) + "'", int10.equals((-11)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227358 + "'", int13.equals(227358));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 703040 + "'", int16.equals(703040));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 120 + "'", int19.equals(120));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
    }

    @Test
    public void test235() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test235");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.sumar((java.lang.Integer) (-2030), (java.lang.Integer) (-674366));
        java.lang.Class<?> wildcardClass9 = operacionesMatematicas0.getClass();
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-86631468), (java.lang.Integer) 166122);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-319241594), (java.lang.Integer) 0);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 1167531498, (java.lang.Integer) 1325219516);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + (-676396) + "'", int8.equals((-676396)));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-86797590) + "'", int12.equals((-86797590)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-157688018) + "'", int18.equals((-157688018)));
    }

    @Test
    public void test236() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test236");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 473676, (java.lang.Integer) 222612);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-678074), (java.lang.Integer) (-223));
        java.lang.Integer int14 = operacionesMatematicas0.sumar((java.lang.Integer) 568901, (java.lang.Integer) 101);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 696288 + "'", int7.equals(696288));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 151210502 + "'", int11.equals(151210502));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 569002 + "'", int14.equals(569002));
    }

    @Test
    public void test237() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test237");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-2031), (java.lang.Integer) (-100));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-679014), (java.lang.Integer) 28345);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-304), (java.lang.Integer) (-98975152));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-2125259776), (java.lang.Integer) 166120);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) (-105355726), (java.lang.Integer) 3021);
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) (-1017), (java.lang.Integer) (-667400994));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-2131) + "'", int10.equals((-2131)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-650669) + "'", int13.equals((-650669)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 98974848 + "'", int16.equals(98974848));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-2125425896) + "'", int19.equals((-2125425896)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-105358747) + "'", int22.equals((-105358747)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
    }

    @Test
    public void test238() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test238");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-198), (java.lang.Integer) 198);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-98990));
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2117069372, (java.lang.Integer) (-4060));
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-98890) + "'", int20.equals((-98890)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-1072091024) + "'", int23.equals((-1072091024)));
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test239() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test239");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-223), (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-4060), (java.lang.Integer) 2030);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 386, (java.lang.Integer) 3062);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 42226412, (java.lang.Integer) (-1008434688));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223 + "'", int6.equals(223));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2) + "'", int9.equals((-2)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1050661100 + "'", int16.equals(1050661100));
    }

    @Test
    public void test240() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test240");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-304), (java.lang.Integer) 12);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) (-674366), (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-134604), (java.lang.Integer) 299478398);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 1175508908, (java.lang.Integer) 669356576);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-292) + "'", int7.equals((-292)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-674376) + "'", int10.equals((-674376)));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-299613002) + "'", int15.equals((-299613002)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1 + "'", int18.equals(1));
    }

    @Test
    public void test241() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test241");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-203658532), (java.lang.Integer) 797);
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) (-5600), (java.lang.Integer) (-2233));
        java.lang.Integer int24 = operacionesMatematicas0.dividir((java.lang.Integer) 680609675, (java.lang.Integer) (-406228640));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-203657735) + "'", int18.equals((-203657735)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 2 + "'", int21.equals(2));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1) + "'", int24.equals((-1)));
    }

    @Test
    public void test242() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test242");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) (-26796));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) (-3));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) (-3260), (java.lang.Integer) 44906012);
        java.lang.Integer int31 = operacionesMatematicas0.sumar((java.lang.Integer) 1771579329, (java.lang.Integer) 0);
        java.lang.Integer int34 = operacionesMatematicas0.restar((java.lang.Integer) (-23417576), (java.lang.Integer) (-1402661432));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-86631468) + "'", int22.equals((-86631468)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-103) + "'", int25.equals((-103)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 1771579329 + "'", int31.equals(1771579329));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 1379243856 + "'", int34.equals(1379243856));
    }

    @Test
    public void test243() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test243");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-223), (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-86408168), (java.lang.Integer) (-1480217496));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-336954577), (java.lang.Integer) (-181782106));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 894688107, (java.lang.Integer) (-203678148));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223 + "'", int6.equals(223));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-1566625664) + "'", int9.equals((-1566625664)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-518736683) + "'", int12.equals((-518736683)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1951528724 + "'", int15.equals(1951528724));
    }

    @Test
    public void test244() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test244");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 10);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 990, (java.lang.Integer) 703051);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-203761583), (java.lang.Integer) (-1002));
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 181674816, (java.lang.Integer) (-1077768));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1000 + "'", int9.equals(1000));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-702061) + "'", int12.equals((-702061)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 203354 + "'", int15.equals(203354));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-39033344) + "'", int18.equals((-39033344)));
    }

    @Test
    public void test245() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test245");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) 2030, (java.lang.Integer) 688);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) (-2233));
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.sumar((java.lang.Integer) 298, (java.lang.Integer) 0);
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 44907013, (java.lang.Integer) (-2452019));
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) (-343058591));
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) (-612929), (java.lang.Integer) 1374);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7.equals(2));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 225533 + "'", int10.equals(225533));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 298 + "'", int14.equals(298));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 47359032 + "'", int17.equals(47359032));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-343058591) + "'", int20.equals((-343058591)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-446) + "'", int23.equals((-446)));
    }

    @Test
    public void test246() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test246");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 990, (java.lang.Integer) 302);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 1346231, (java.lang.Integer) (-300));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 688 + "'", int15.equals(688));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1346531 + "'", int18.equals(1346531));
    }

    @Test
    public void test247() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test247");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass6 = operacionesMatematicas0.getClass();
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 688, (java.lang.Integer) (-924510));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 1710, (java.lang.Integer) 6823529);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-907041850), (java.lang.Integer) 0);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-144182695), (java.lang.Integer) 2010926408);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 6825239 + "'", int13.equals(6825239));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-907041850) + "'", int16.equals((-907041850)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2139858193 + "'", int19.equals(2139858193));
    }

    @Test
    public void test248() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test248");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 101);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 485769307, (java.lang.Integer) (-28676780));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-304) + "'", int6.equals((-304)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 514446087 + "'", int9.equals(514446087));
    }

    @Test
    public void test249() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test249");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 203);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2131278928), (java.lang.Integer) (-769573));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + (-204) + "'", int4.equals((-204)));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-277440624) + "'", int7.equals((-277440624)));
    }

    @Test
    public void test250() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test250");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-674366));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-1027036680), (java.lang.Integer) (-28927));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 834392107, (java.lang.Integer) (-343065842));
        java.lang.Integer int28 = operacionesMatematicas0.sumar((java.lang.Integer) (-873146058), (java.lang.Integer) 100004);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-203658532) + "'", int19.equals((-203658532)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 35504 + "'", int22.equals(35504));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-2) + "'", int25.equals((-2)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-873046054) + "'", int28.equals((-873046054)));
    }

    @Test
    public void test251() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test251");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 473676, (java.lang.Integer) (-2030));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 20300, (java.lang.Integer) 338956);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 3021, (java.lang.Integer) 3274);
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1101, (java.lang.Integer) (-86631468));
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) (-65), (java.lang.Integer) 223300);
        java.lang.Integer int27 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-391), (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-961562280) + "'", int12.equals((-961562280)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 359256 + "'", int15.equals(359256));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 6295 + "'", int18.equals(6295));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-891965756) + "'", int21.equals((-891965756)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 223235 + "'", int24.equals(223235));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 0 + "'", int27.equals(0));
    }

    @Test
    public void test252() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test252");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 100004, (java.lang.Integer) 0);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-17), (java.lang.Integer) (-403));
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2114056976), (java.lang.Integer) 99);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 120, (java.lang.Integer) (-1077114894));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 100004 + "'", int12.equals(100004));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 386 + "'", int15.equals(386));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1161756880 + "'", int18.equals(1161756880));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1077114774) + "'", int21.equals((-1077114774)));
    }

    @Test
    public void test253() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test253");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) 223300);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.dividir((java.lang.Integer) (-1418013643), (java.lang.Integer) 834392107);
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) 100004, (java.lang.Integer) 1766523890);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2030) + "'", int9.equals((-2030)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2679600 + "'", int13.equals(2679600));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-1) + "'", int17.equals((-1)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-1139723512) + "'", int20.equals((-1139723512)));
    }

    @Test
    public void test254() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test254");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 14, (java.lang.Integer) (-204));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1034161825), (java.lang.Integer) 100);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-43315734), (java.lang.Integer) (-794241824));
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-319619085), (java.lang.Integer) 9992694);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 218 + "'", int9.equals(218));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-336967396) + "'", int12.equals((-336967396)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-309626391) + "'", int18.equals((-309626391)));
    }

    @Test
    public void test255() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test255");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-882132), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 676495, (java.lang.Integer) 2030);
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass25 = operacionesMatematicas0.getClass();
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) 1702393088, (java.lang.Integer) 299478398);
        java.lang.Integer int31 = operacionesMatematicas0.dividir((java.lang.Integer) 797, (java.lang.Integer) (-680500910));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-1480217496) + "'", int20.equals((-1480217496)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 333 + "'", int23.equals(333));
        org.junit.Assert.assertNotNull(wildcardClass24);
        org.junit.Assert.assertNotNull(wildcardClass25);
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 1402914690 + "'", int28.equals(1402914690));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 0 + "'", int31.equals(0));
    }

    @Test
    public void test256() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test256");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 677082, (java.lang.Integer) 797);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) 221269);
        java.lang.Integer int28 = operacionesMatematicas0.sumar((java.lang.Integer) (-802065), (java.lang.Integer) 4058);
        java.lang.Class<?> wildcardClass29 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 849 + "'", int22.equals(849));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 668453649 + "'", int25.equals(668453649));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-798007) + "'", int28.equals((-798007)));
        org.junit.Assert.assertNotNull(wildcardClass29);
    }

    @Test
    public void test257() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test257");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2, (java.lang.Integer) 1000);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-907044204), (java.lang.Integer) 166111);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2000 + "'", int6.equals(2000));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-906878093) + "'", int9.equals((-906878093)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test258() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test258");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.restar((java.lang.Integer) 342371593, (java.lang.Integer) 791);
        java.lang.Class<?> wildcardClass15 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 342370802 + "'", int14.equals(342370802));
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test259() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test259");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-237), (java.lang.Integer) 101);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-892129759), (java.lang.Integer) (-292));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-23937) + "'", int16.equals((-23937)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-892130051) + "'", int19.equals((-892130051)));
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test260() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test260");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.dividir((java.lang.Integer) 696288, (java.lang.Integer) 674850);
        java.lang.Class<?> wildcardClass9 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 1 + "'", int8.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test261() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test261");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-223), (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-4060), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-202979700), (java.lang.Integer) 386);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1913), (java.lang.Integer) (-203658612));
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 117, (java.lang.Integer) (-673143));
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) (-1614295506), (java.lang.Integer) (-104682583));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223 + "'", int6.equals(223));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2) + "'", int9.equals((-2)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-202979314) + "'", int12.equals((-202979314)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1243099180) + "'", int15.equals((-1243099180)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-78757731) + "'", int18.equals((-78757731)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1718978089) + "'", int21.equals((-1718978089)));
    }

    @Test
    public void test262() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test262");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-203658532), (java.lang.Integer) 2941);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 342371593, (java.lang.Integer) 153721199);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-1959288468) + "'", int6.equals((-1959288468)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 2 + "'", int9.equals(2));
    }

    @Test
    public void test263() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test263");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-204), (java.lang.Integer) 12);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 797, (java.lang.Integer) (-201378));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1199317410), (java.lang.Integer) 203344022);
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-203656345), (java.lang.Integer) 2108475396);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-17) + "'", int6.equals((-17)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1402661432) + "'", int12.equals((-1402661432)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
    }

    @Test
    public void test264() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test264");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-2233));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-676396), (java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 24, (java.lang.Integer) 677084);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) 419736786);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 2037899052, (java.lang.Integer) (-589654622));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-674366) + "'", int9.equals((-674366)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-6763) + "'", int12.equals((-6763)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1667413622) + "'", int22.equals((-1667413622)));
    }

    @Test
    public void test265() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test265");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-676594), (java.lang.Integer) (-926090540));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-1181025384) + "'", int17.equals((-1181025384)));
    }

    @Test
    public void test266() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test266");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-98975152), (java.lang.Integer) (-676594));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 117, (java.lang.Integer) 1);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-201378), (java.lang.Integer) (-3260));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) 1023986076, (java.lang.Integer) 118);
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 1951528724, (java.lang.Integer) 2122583520);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 146 + "'", int12.equals(146));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 118 + "'", int15.equals(118));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-198118) + "'", int18.equals((-198118)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1023985958 + "'", int21.equals(1023985958));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-220855052) + "'", int25.equals((-220855052)));
    }

    @Test
    public void test267() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test267");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) 688);
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) (-202979314), (java.lang.Integer) 2108530673);
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 831667596, (java.lang.Integer) 164521522);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 12 + "'", int13.equals(12));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 222612 + "'", int17.equals(222612));
        org.junit.Assert.assertNotNull(wildcardClass18);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1983457309 + "'", int21.equals(1983457309));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1706937688 + "'", int25.equals(1706937688));
    }

    @Test
    public void test268() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test268");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) 2030, (java.lang.Integer) 688);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) (-2233));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-203656745), (java.lang.Integer) 2000);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-610278), (java.lang.Integer) 0);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-359), (java.lang.Integer) 852483576);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7.equals(2));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 225533 + "'", int10.equals(225533));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-203654745) + "'", int13.equals((-203654745)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 852483217 + "'", int19.equals(852483217));
    }

    @Test
    public void test269() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test269");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass9 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 24, (java.lang.Integer) (-1358028));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-169435527), (java.lang.Integer) (-2017286592));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1358052 + "'", int13.equals(1358052));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-350481344) + "'", int16.equals((-350481344)));
    }

    @Test
    public void test270() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test270");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 298, (java.lang.Integer) (-203658532));
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 3241260, (java.lang.Integer) 0);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 925359, (java.lang.Integer) 0);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 203658830 + "'", int7.equals(203658830));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 3241260 + "'", int10.equals(3241260));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 925359 + "'", int13.equals(925359));
    }

    @Test
    public void test271() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test271");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 166111, (java.lang.Integer) (-594099455));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 990, (java.lang.Integer) 673023);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-504341602), (java.lang.Integer) (-676296));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 14436969);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-593933344) + "'", int13.equals((-593933344)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 666292770 + "'", int16.equals(666292770));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-619745648) + "'", int19.equals((-619745648)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 14436969 + "'", int22.equals(14436969));
    }

    @Test
    public void test272() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test272");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-304));
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) 227581);
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 7458814);
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-104553516), (java.lang.Integer) (-204559587));
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 493792228, (java.lang.Integer) (-1933340520));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14.equals(0));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 7458814 + "'", int17.equals(7458814));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-1253233916) + "'", int20.equals((-1253233916)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 0 + "'", int23.equals(0));
    }

    @Test
    public void test273() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test273");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 925359, (java.lang.Integer) 3451);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-388878032), (java.lang.Integer) (-138799294));
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-403), (java.lang.Integer) 849);
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) 120, (java.lang.Integer) (-78757731));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 268 + "'", int12.equals(268));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-250078738) + "'", int15.equals((-250078738)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-342147) + "'", int18.equals((-342147)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 78757851 + "'", int21.equals(78757851));
    }

    @Test
    public void test274() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test274");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-507), (java.lang.Integer) 200871);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-702061), (java.lang.Integer) (-25));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1304, (java.lang.Integer) 1623620464);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-201378) + "'", int16.equals((-201378)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-702086) + "'", int19.equals((-702086)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-217791872) + "'", int22.equals((-217791872)));
    }

    @Test
    public void test275() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test275");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1678);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1226993776), (java.lang.Integer) 696288);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 338956 + "'", int10.equals(338956));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 967314944 + "'", int13.equals(967314944));
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test276() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test276");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-304));
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 191, (java.lang.Integer) 137901733);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 2010197328, (java.lang.Integer) (-133524468));
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 691719053, (java.lang.Integer) (-109300028));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 569427227 + "'", int13.equals(569427227));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-15) + "'", int16.equals((-15)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1254124044) + "'", int19.equals((-1254124044)));
    }

    @Test
    public void test277() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test277");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 688, (java.lang.Integer) 990);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) 218, (java.lang.Integer) (-2131));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 227581, (java.lang.Integer) 2679600);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 1199317608, (java.lang.Integer) (-1073433704));
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) (-678748), (java.lang.Integer) 2010197494);
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) (-105358747), (java.lang.Integer) 959378576);
        java.lang.Integer int27 = operacionesMatematicas0.dividir((java.lang.Integer) (-674366), (java.lang.Integer) 1698415552);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1678 + "'", int9.equals(1678));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1913) + "'", int12.equals((-1913)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-2452019) + "'", int15.equals((-2452019)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1) + "'", int18.equals((-1)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 854019829 + "'", int24.equals(854019829));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 0 + "'", int27.equals(0));
    }

    @Test
    public void test278() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test278");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) (-593933344), (java.lang.Integer) (-676594));
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-1815850602), (java.lang.Integer) (-1126066292));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-593256750) + "'", int10.equals((-593256750)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-689784310) + "'", int13.equals((-689784310)));
    }

    @Test
    public void test279() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test279");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-319239361), (java.lang.Integer) (-1853597184));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
    }

    @Test
    public void test280() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test280");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-304));
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 191, (java.lang.Integer) 137901733);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 2010197328, (java.lang.Integer) (-133524468));
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2010360317, (java.lang.Integer) (-674376));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 569427227 + "'", int13.equals(569427227));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-15) + "'", int16.equals((-15)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2037583576 + "'", int19.equals(2037583576));
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test281() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test281");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-198), (java.lang.Integer) 198);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-98990));
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2117069372, (java.lang.Integer) (-4060));
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-917), (java.lang.Integer) (-1988598960));
        java.lang.Integer int29 = operacionesMatematicas0.dividir((java.lang.Integer) (-1085863927), (java.lang.Integer) 5);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-98890) + "'", int20.equals((-98890)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-1072091024) + "'", int23.equals((-1072091024)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-1815854480) + "'", int26.equals((-1815854480)));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-217172785) + "'", int29.equals((-217172785)));
    }

    @Test
    public void test282() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test282");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) (-304), (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1959288468), (java.lang.Integer) 975452);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-973376920), (java.lang.Integer) 16434);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-507) + "'", int6.equals((-507)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-127444272) + "'", int9.equals((-127444272)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-973393354) + "'", int13.equals((-973393354)));
    }

    @Test
    public void test283() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test283");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) (-26796));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 28000, (java.lang.Integer) (-676594));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) 1566625664, (java.lang.Integer) (-673143));
        java.lang.Integer int31 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1513001358), (java.lang.Integer) (-176982808));
        java.lang.Integer int34 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2034), (java.lang.Integer) 0);
        java.lang.Integer int37 = operacionesMatematicas0.restar((java.lang.Integer) (-1566625664), (java.lang.Integer) (-1097252056));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-86631468) + "'", int22.equals((-86631468)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-2327) + "'", int28.equals((-2327)));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-485638320) + "'", int31.equals((-485638320)));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 0 + "'", int34.equals(0));
        org.junit.Assert.assertTrue("'" + int37 + "' != '" + (-469373608) + "'", int37.equals((-469373608)));
    }

    @Test
    public void test284() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test284");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-198), (java.lang.Integer) 198);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-98990));
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2117069372, (java.lang.Integer) (-4060));
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-917), (java.lang.Integer) (-1988598960));
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-203761583), (java.lang.Integer) 2108373864);
        java.lang.Integer int32 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-12511), (java.lang.Integer) (-1815850602));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-98890) + "'", int20.equals((-98890)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-1072091024) + "'", int23.equals((-1072091024)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-1815854480) + "'", int26.equals((-1815854480)));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-1948415000) + "'", int29.equals((-1948415000)));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 2024853078 + "'", int32.equals(2024853078));
    }

    @Test
    public void test285() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test285");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-26796), (java.lang.Integer) (-2131));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-674366), (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 790454420, (java.lang.Integer) (-9));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) (-172199), (java.lang.Integer) (-86408168));
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1198623488), (java.lang.Integer) (-78));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) (-1963548160), (java.lang.Integer) 157434168);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-28927) + "'", int12.equals((-28927)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-133524468) + "'", int15.equals((-133524468)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1475844812 + "'", int18.equals(1475844812));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 86235969 + "'", int21.equals(86235969));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-996648448) + "'", int25.equals((-996648448)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-12) + "'", int28.equals((-12)));
    }

    @Test
    public void test286() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test286");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 1678, (java.lang.Integer) (-304));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 1023986076, (java.lang.Integer) (-86631468));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-2091737468), (java.lang.Integer) 2);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1374 + "'", int9.equals(1374));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-11) + "'", int12.equals((-11)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-2091737470) + "'", int15.equals((-2091737470)));
    }

    @Test
    public void test287() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test287");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 3233, (java.lang.Integer) (-676594));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 2010711732, (java.lang.Integer) (-2027022092));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 679827 + "'", int9.equals(679827));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
    }

    @Test
    public void test288() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test288");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-507), (java.lang.Integer) 200871);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-3030), (java.lang.Integer) 1980253234);
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 153262720, (java.lang.Integer) (-100));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 2107797558, (java.lang.Integer) (-44906012));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) (-391), (java.lang.Integer) 10495029);
        try {
            java.lang.Integer int31 = operacionesMatematicas0.dividir((java.lang.Integer) (-129843968), (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-201378) + "'", int16.equals((-201378)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1980250204 + "'", int19.equals(1980250204));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1853597184 + "'", int22.equals(1853597184));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 2062891546 + "'", int25.equals(2062891546));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
    }

    @Test
    public void test289() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test289");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 1678);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-204), (java.lang.Integer) (-669335781));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 953704, (java.lang.Integer) (-1358028));
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 166122 + "'", int10.equals(166122));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 669335577 + "'", int13.equals(669335577));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1923387680 + "'", int16.equals(1923387680));
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test290() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test290");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 100004, (java.lang.Integer) 0);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-17), (java.lang.Integer) (-403));
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 2107797558, (java.lang.Integer) 120);
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1463775408), (java.lang.Integer) 1079);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 100004 + "'", int12.equals(100004));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 386 + "'", int15.equals(386));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 17564979 + "'", int18.equals(17564979));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1134299696 + "'", int21.equals(1134299696));
    }

    @Test
    public void test291() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test291");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 674850, (java.lang.Integer) (-203658532));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 338956, (java.lang.Integer) (-3030));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2233), (java.lang.Integer) (-1027036680));
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-139629624), (java.lang.Integer) 164521512);
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-317895457), (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1027036680) + "'", int12.equals((-1027036680)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-139629624) + "'", int15.equals((-139629624)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1896184000) + "'", int18.equals((-1896184000)));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 12715818 + "'", int22.equals(12715818));
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test292() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test292");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) (-26796));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) (-3));
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) (-678748));
        java.lang.Integer int31 = operacionesMatematicas0.sumar((java.lang.Integer) 696288, (java.lang.Integer) (-681446));
        java.lang.Integer int34 = operacionesMatematicas0.restar((java.lang.Integer) 844551780, (java.lang.Integer) 1346166);
        java.lang.Integer int37 = operacionesMatematicas0.sumar((java.lang.Integer) 329, (java.lang.Integer) (-141707));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-86631468) + "'", int22.equals((-86631468)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-103) + "'", int25.equals((-103)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 678545 + "'", int28.equals(678545));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 14842 + "'", int31.equals(14842));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 843205614 + "'", int34.equals(843205614));
        org.junit.Assert.assertTrue("'" + int37 + "' != '" + (-141378) + "'", int37.equals((-141378)));
    }

    @Test
    public void test293() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test293");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-2099619127), (java.lang.Integer) 591647436);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1507971691) + "'", int15.equals((-1507971691)));
    }

    @Test
    public void test294() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test294");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-2233));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) 1980253234, (java.lang.Integer) 30107083);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 100004, (java.lang.Integer) 161877486);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) (-59), (java.lang.Integer) (-1455213478));
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) (-2452019), (java.lang.Integer) 1740362023);
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) (-319241688), (java.lang.Integer) (-144));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-674366) + "'", int9.equals((-674366)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2010360317 + "'", int12.equals(2010360317));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1737910004 + "'", int21.equals(1737910004));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-319241832) + "'", int24.equals((-319241832)));
    }

    @Test
    public void test295() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test295");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-882132), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) (-679114), (java.lang.Integer) 100);
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) 153262720, (java.lang.Integer) 23816997);
        java.lang.Integer int29 = operacionesMatematicas0.dividir((java.lang.Integer) (-1379066004), (java.lang.Integer) (-204940904));
        java.lang.Integer int32 = operacionesMatematicas0.restar((java.lang.Integer) (-569427028), (java.lang.Integer) (-109300028));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-1480217496) + "'", int20.equals((-1480217496)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-679014) + "'", int23.equals((-679014)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 6 + "'", int26.equals(6));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 6 + "'", int29.equals(6));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + (-460127000) + "'", int32.equals((-460127000)));
    }

    @Test
    public void test296() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test296");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 676504, (java.lang.Integer) (-1378461349));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 952174232, (java.lang.Integer) (-460127000));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1567765512 + "'", int12.equals(1567765512));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1412301232 + "'", int15.equals(1412301232));
    }

    @Test
    public void test297() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test297");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-198), (java.lang.Integer) 198);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-98990));
        java.lang.Class<?> wildcardClass21 = operacionesMatematicas0.getClass();
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-230063104), (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-98890) + "'", int20.equals((-98890)));
        org.junit.Assert.assertNotNull(wildcardClass21);
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 0 + "'", int24.equals(0));
    }

    @Test
    public void test298() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test298");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-676594), (java.lang.Integer) 1678);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-1), (java.lang.Integer) 3233);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-403) + "'", int10.equals((-403)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test299() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test299");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-3), (java.lang.Integer) (-9));
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        try {
            java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) 315901, (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test300() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test300");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-237), (java.lang.Integer) 1593176728);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 12704, (java.lang.Integer) (-1554526525));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) (-4755287), (java.lang.Integer) (-404362922));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1593176491 + "'", int19.equals(1593176491));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1554513821) + "'", int22.equals((-1554513821)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
    }

    @Test
    public void test301() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test301");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) (-200282), (java.lang.Integer) (-702061));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-15), (java.lang.Integer) 473676);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-203654745), (java.lang.Integer) (-312057086));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1045334094 + "'", int13.equals(1045334094));
    }

    @Test
    public void test302() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test302");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2, (java.lang.Integer) (-1));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) 338956);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-300960), (java.lang.Integer) 2679600);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 298, (java.lang.Integer) (-28474));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 902932, (java.lang.Integer) 1739459091);
        java.lang.Integer int28 = operacionesMatematicas0.sumar((java.lang.Integer) 1968086948, (java.lang.Integer) (-217791872));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-2) + "'", int13.equals((-2)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1023986076 + "'", int16.equals(1023986076));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1001435648 + "'", int19.equals(1001435648));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-28176) + "'", int22.equals((-28176)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1740362023 + "'", int25.equals(1740362023));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 1750295076 + "'", int28.equals(1750295076));
    }

    @Test
    public void test303() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test303");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.sumar((java.lang.Integer) 12, (java.lang.Integer) (-480630));
        java.lang.Class<?> wildcardClass9 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + (-480618) + "'", int8.equals((-480618)));
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test304() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test304");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-2030), (java.lang.Integer) 1636755637);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1636757667) + "'", int12.equals((-1636757667)));
    }

    @Test
    public void test305() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test305");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-2233), (java.lang.Integer) 199);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 227358, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 28000, (java.lang.Integer) 675040);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 223403, (java.lang.Integer) 30107083);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 302, (java.lang.Integer) (-678748));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) (-30573510), (java.lang.Integer) (-682380));
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) (-1183), (java.lang.Integer) 153263227);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-11) + "'", int10.equals((-11)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227358 + "'", int13.equals(227358));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 703040 + "'", int16.equals(703040));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 44 + "'", int25.equals(44));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-153264410) + "'", int28.equals((-153264410)));
    }

    @Test
    public void test306() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test306");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-203658532), (java.lang.Integer) 797);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 138125136, (java.lang.Integer) (-198));
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) 2029);
        java.lang.Integer int27 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1750448650), (java.lang.Integer) 1586840422);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-203657735) + "'", int18.equals((-203657735)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 138124938 + "'", int21.equals(138124938));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 6559757 + "'", int24.equals(6559757));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 173359620 + "'", int27.equals(173359620));
    }

    @Test
    public void test307() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test307");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-4060), (java.lang.Integer) (-203658530));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203662590) + "'", int6.equals((-203662590)));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test308() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test308");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-223), (java.lang.Integer) (-1429679577));
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 134282741, (java.lang.Integer) 102968);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1072727885), (java.lang.Integer) (-98890));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-317895457), (java.lang.Integer) (-1455213478));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1429679800) + "'", int13.equals((-1429679800)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1304 + "'", int16.equals(1304));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 663303746 + "'", int19.equals(663303746));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1604801126 + "'", int22.equals(1604801126));
    }

    @Test
    public void test309() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test309");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99990, (java.lang.Integer) 14);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 218, (java.lang.Integer) 203658830);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 166111, (java.lang.Integer) (-203656852));
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) (-3260), (java.lang.Integer) (-1566625664));
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 100004 + "'", int9.equals(100004));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-26796) + "'", int12.equals((-26796)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-203658612) + "'", int15.equals((-203658612)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test310() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test310");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 199, (java.lang.Integer) 302);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-58), (java.lang.Integer) (-674366));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-674424) + "'", int13.equals((-674424)));
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test311() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test311");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 703051, (java.lang.Integer) 191);
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 298, (java.lang.Integer) (-3260));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 222612, (java.lang.Integer) 2108475396);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2354, (java.lang.Integer) (-1868612537));
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1636757667), (java.lang.Integer) 0);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 134282741 + "'", int7.equals(134282741));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-2962) + "'", int10.equals((-2962)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-667400994) + "'", int16.equals((-667400994)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
    }

    @Test
    public void test312() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test312");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 1351444, (java.lang.Integer) (-1796254192));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 199 + "'", int6.equals(199));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-1794902748) + "'", int9.equals((-1794902748)));
    }

    @Test
    public void test313() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test313");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-673143), (java.lang.Integer) (-100));
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166120, (java.lang.Integer) 3233);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 6731 + "'", int10.equals(6731));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 537065960 + "'", int13.equals(537065960));
    }

    @Test
    public void test314() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test314");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) 1853597184);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-1796353182), (java.lang.Integer) 12);
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203344022, (java.lang.Integer) 1161756880);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 0 + "'", int4.equals(0));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-1796353170) + "'", int7.equals((-1796353170)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
    }

    @Test
    public void test315() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test315");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-98975152), (java.lang.Integer) (-300960));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 204, (java.lang.Integer) 0);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-5), (java.lang.Integer) 0);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 24, (java.lang.Integer) (-1338190225));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1963548160 + "'", int9.equals(1963548160));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 204 + "'", int12.equals(204));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-5) + "'", int15.equals((-5)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1338190249 + "'", int18.equals(1338190249));
    }

    @Test
    public void test316() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test316");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) (-198), (java.lang.Integer) (-453522300));
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        java.lang.Integer int27 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-793575866));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 453522102 + "'", int23.equals(453522102));
        org.junit.Assert.assertNotNull(wildcardClass24);
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 0 + "'", int27.equals(0));
    }

    @Test
    public void test317() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test317");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 101);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) (-674366), (java.lang.Integer) 3020);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1915811892), (java.lang.Integer) 701863);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 101 + "'", int4.equals(101));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-223) + "'", int7.equals((-223)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 814305812 + "'", int10.equals(814305812));
    }

    @Test
    public void test318() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test318");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 2354, (java.lang.Integer) (-403));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) (-28474));
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 961562281, (java.lang.Integer) 8260879);
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1554513821), (java.lang.Integer) 2139858193);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-5) + "'", int12.equals((-5)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-6349702) + "'", int15.equals((-6349702)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 116 + "'", int18.equals(116));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-472328813) + "'", int21.equals((-472328813)));
    }

    @Test
    public void test319() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test319");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 1079, (java.lang.Integer) 679827);
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-198118), (java.lang.Integer) 674850);
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) (-2108473967), (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-678748) + "'", int17.equals((-678748)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-872968) + "'", int20.equals((-872968)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-2108473967) + "'", int23.equals((-2108473967)));
    }

    @Test
    public void test320() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test320");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-223), (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-86408168), (java.lang.Integer) (-1480217496));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-336954577), (java.lang.Integer) (-181782106));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223 + "'", int6.equals(223));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-1566625664) + "'", int9.equals((-1566625664)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-518736683) + "'", int12.equals((-518736683)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test321() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test321");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 101);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 797, (java.lang.Integer) (-890108686));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) (-202979700), (java.lang.Integer) 1024014076);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 151442422, (java.lang.Integer) 499871);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 101 + "'", int4.equals(101));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-747018902) + "'", int7.equals((-747018902)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-1226993776) + "'", int10.equals((-1226993776)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 302 + "'", int13.equals(302));
    }

    @Test
    public void test322() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test322");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 223403, (java.lang.Integer) (-203658530));
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) 1766523890, (java.lang.Integer) 967314944);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-203435127) + "'", int20.equals((-203435127)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-1561128462) + "'", int23.equals((-1561128462)));
    }

    @Test
    public void test323() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test323");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) 0);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-507), (java.lang.Integer) 100004);
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-1913), (java.lang.Integer) 29684000);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-922956032), (java.lang.Integer) 28053);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2 + "'", int6.equals(2));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-50702028) + "'", int9.equals((-50702028)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-922927979) + "'", int15.equals((-922927979)));
    }

    @Test
    public void test324() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test324");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) 14);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 227358, (java.lang.Integer) 223);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 166122, (java.lang.Integer) (-11));
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 200232, (java.lang.Integer) (-181982338));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-1944184848), (java.lang.Integer) (-1381013947));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 675040 + "'", int10.equals(675040));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227581 + "'", int13.equals(227581));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 166111 + "'", int16.equals(166111));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-181782106) + "'", int19.equals((-181782106)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1 + "'", int22.equals(1));
    }

    @Test
    public void test325() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test325");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 679827, (java.lang.Integer) 129067);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1566625664, (java.lang.Integer) (-313922314));
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) (-1854937551), (java.lang.Integer) 2037899052);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 808894 + "'", int10.equals(808894));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-4) + "'", int13.equals((-4)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 182961501 + "'", int16.equals(182961501));
    }

    @Test
    public void test326() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test326");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass9 = operacionesMatematicas0.getClass();
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 868432494, (java.lang.Integer) (-747018902));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 186, (java.lang.Integer) (-1468944503));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1126066292) + "'", int12.equals((-1126066292)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1468944689 + "'", int15.equals(1468944689));
    }

    @Test
    public void test327() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test327");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-2233));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-676396), (java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 24, (java.lang.Integer) 677084);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) (-505887068), (java.lang.Integer) 2989800);
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-12), (java.lang.Integer) (-2030));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-674366) + "'", int9.equals((-674366)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-6763) + "'", int12.equals((-6763)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-169) + "'", int19.equals((-169)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 24360 + "'", int22.equals(24360));
    }

    @Test
    public void test328() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test328");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-507), (java.lang.Integer) 200871);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-3030), (java.lang.Integer) 1980253234);
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 153262720, (java.lang.Integer) (-100));
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-201378) + "'", int16.equals((-201378)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1980250204 + "'", int19.equals(1980250204));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1853597184 + "'", int22.equals(1853597184));
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test329() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test329");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-204), (java.lang.Integer) 12);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 797, (java.lang.Integer) (-201378));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 3021, (java.lang.Integer) 200183);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1025253456, (java.lang.Integer) 791);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-17) + "'", int6.equals((-17)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-773335248) + "'", int15.equals((-773335248)));
    }

    @Test
    public void test330() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test330");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) (-26796));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 28000, (java.lang.Integer) (-676594));
        java.lang.Integer int28 = operacionesMatematicas0.sumar((java.lang.Integer) (-594099455), (java.lang.Integer) 0);
        java.lang.Integer int31 = operacionesMatematicas0.restar((java.lang.Integer) (-654), (java.lang.Integer) 153262720);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-86631468) + "'", int22.equals((-86631468)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-594099455) + "'", int28.equals((-594099455)));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-153263374) + "'", int31.equals((-153263374)));
    }

    @Test
    public void test331() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test331");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) (-26796));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) (-3));
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) 677084, (java.lang.Integer) 204);
        java.lang.Integer int31 = operacionesMatematicas0.restar((java.lang.Integer) (-984), (java.lang.Integer) 34527);
        java.lang.Integer int34 = operacionesMatematicas0.sumar((java.lang.Integer) 1214037568, (java.lang.Integer) 1195703928);
        java.lang.Integer int37 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3241260, (java.lang.Integer) (-42632));
        java.lang.Integer int40 = operacionesMatematicas0.multiplicar((java.lang.Integer) 129067, (java.lang.Integer) 229388);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-86631468) + "'", int22.equals((-86631468)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-103) + "'", int25.equals((-103)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 138125136 + "'", int28.equals(138125136));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-35511) + "'", int31.equals((-35511)));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + (-1885225800) + "'", int34.equals((-1885225800)));
        org.junit.Assert.assertTrue("'" + int37 + "' != '" + (-742442848) + "'", int37.equals((-742442848)));
        org.junit.Assert.assertTrue("'" + int40 + "' != '" + (-458350076) + "'", int40.equals((-458350076)));
    }

    @Test
    public void test332() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test332");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 791, (java.lang.Integer) 222612);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-28474), (java.lang.Integer) 2108502328);
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) 200282, (java.lang.Integer) 134282741);
        java.lang.Integer int31 = operacionesMatematicas0.dividir((java.lang.Integer) (-1480217496), (java.lang.Integer) 1362965102);
        java.lang.Integer int34 = operacionesMatematicas0.restar((java.lang.Integer) 996650796, (java.lang.Integer) (-5));
        java.lang.Class<?> wildcardClass35 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 223403 + "'", int22.equals(223403));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 2108473854 + "'", int25.equals(2108473854));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-134082459) + "'", int28.equals((-134082459)));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-1) + "'", int31.equals((-1)));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 996650801 + "'", int34.equals(996650801));
        org.junit.Assert.assertNotNull(wildcardClass35);
    }

    @Test
    public void test333() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test333");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-2233), (java.lang.Integer) 199);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 227358, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 28000, (java.lang.Integer) 675040);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 223403, (java.lang.Integer) 30107083);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 302, (java.lang.Integer) (-678748));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) (-30573510), (java.lang.Integer) (-682380));
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-11) + "'", int10.equals((-11)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227358 + "'", int13.equals(227358));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 703040 + "'", int16.equals(703040));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 44 + "'", int25.equals(44));
        org.junit.Assert.assertNotNull(wildcardClass26);
    }

    @Test
    public void test334() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test334");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166122, (java.lang.Integer) (-2030));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-203658532), (java.lang.Integer) 1566625664);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 55, (java.lang.Integer) (-684291464));
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 199 + "'", int6.equals(199));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-337227660) + "'", int9.equals((-337227660)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1362967132 + "'", int13.equals(1362967132));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 684291519 + "'", int16.equals(684291519));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test335() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test335");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 374697400, (java.lang.Integer) 473676);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 329, (java.lang.Integer) (-144));
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 669356581, (java.lang.Integer) (-1566890908));
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 138125136, (java.lang.Integer) 1089);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 791 + "'", int13.equals(791));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-47376) + "'", int16.equals((-47376)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-996062604) + "'", int19.equals((-996062604)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 138124047 + "'", int22.equals(138124047));
    }

    @Test
    public void test336() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test336");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.dividir((java.lang.Integer) (-202979700), (java.lang.Integer) 722435795);
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-1896184000), (java.lang.Integer) (-35511));
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3464368, (java.lang.Integer) (-984));
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) (-28275), (java.lang.Integer) (-1254124044));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1896219511) + "'", int15.equals((-1896219511)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 886029184 + "'", int18.equals(886029184));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
    }

    @Test
    public void test337() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test337");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 138125136, (java.lang.Integer) 223403);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203654756, (java.lang.Integer) (-846460796));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-985707399), (java.lang.Integer) 2256);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-29247463), (java.lang.Integer) (-12511));
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 318557266, (java.lang.Integer) 86235969);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 2010711732, (java.lang.Integer) 1161756880);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 137901733 + "'", int7.equals(137901733));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-248222832) + "'", int10.equals((-248222832)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-985705143) + "'", int13.equals((-985705143)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 2337 + "'", int16.equals(2337));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1641019694) + "'", int19.equals((-1641019694)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1122498684) + "'", int22.equals((-1122498684)));
    }

    @Test
    public void test338() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test338");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-203658532), (java.lang.Integer) 797);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 138125136, (java.lang.Integer) (-198));
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) 3021, (java.lang.Integer) (-607257));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) (-872882432), (java.lang.Integer) 1954229806);
        java.lang.Integer int31 = operacionesMatematicas0.restar((java.lang.Integer) (-951117697), (java.lang.Integer) 2108475396);
        java.lang.Integer int34 = operacionesMatematicas0.sumar((java.lang.Integer) (-104553516), (java.lang.Integer) (-141165343));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-203657735) + "'", int18.equals((-203657735)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 138124938 + "'", int21.equals(138124938));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 610278 + "'", int25.equals(610278));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 1235374203 + "'", int31.equals(1235374203));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + (-245718859) + "'", int34.equals((-245718859)));
    }

    @Test
    public void test339() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test339");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-674366));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-1027036680), (java.lang.Integer) (-28927));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) (-2099619127), (java.lang.Integer) (-694249));
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-203658532) + "'", int19.equals((-203658532)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 35504 + "'", int22.equals(35504));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 3024 + "'", int25.equals(3024));
        org.junit.Assert.assertNotNull(wildcardClass26);
    }

    @Test
    public void test340() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test340");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 1023986076, (java.lang.Integer) 28000);
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 1016, (java.lang.Integer) (-127644456));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-542), (java.lang.Integer) (-23062));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 1024014076 + "'", int7.equals(1024014076));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-23604) + "'", int13.equals((-23604)));
    }

    @Test
    public void test341() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test341");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 674850, (java.lang.Integer) (-676594));
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1988354286, (java.lang.Integer) 10495029);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 1351444 + "'", int10.equals(1351444));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1448146618) + "'", int13.equals((-1448146618)));
    }

    @Test
    public void test342() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test342");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 199, (java.lang.Integer) 302);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) (-203078208));
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) (-1270063546), (java.lang.Integer) (-1808117328));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203078208 + "'", int13.equals(203078208));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1216786422 + "'", int16.equals(1216786422));
    }

    @Test
    public void test343() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test343");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 791, (java.lang.Integer) 222612);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-28474), (java.lang.Integer) 2108502328);
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) 703040);
        java.lang.Class<?> wildcardClass29 = operacionesMatematicas0.getClass();
        java.lang.Integer int32 = operacionesMatematicas0.dividir((java.lang.Integer) (-1782702100), (java.lang.Integer) (-56940188));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 223403 + "'", int22.equals(223403));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 2108473854 + "'", int25.equals(2108473854));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass29);
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 31 + "'", int32.equals(31));
    }

    @Test
    public void test344() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test344");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-403), (java.lang.Integer) (-2017286592));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
    }

    @Test
    public void test345() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test345");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-28275), (java.lang.Integer) 200184);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-1896219511), (java.lang.Integer) (-669335781));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 102968, (java.lang.Integer) 1954933922);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1729412004 + "'", int12.equals(1729412004));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
    }

    @Test
    public void test346() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test346");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass6 = operacionesMatematicas0.getClass();
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 688, (java.lang.Integer) (-924510));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 2002502299, (java.lang.Integer) 773308450);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1519156547) + "'", int13.equals((-1519156547)));
    }

    @Test
    public void test347() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test347");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 473676, (java.lang.Integer) 222612);
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 2941, (java.lang.Integer) 2109130613);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 696288 + "'", int7.equals(696288));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
    }

    @Test
    public void test348() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test348");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) 223300);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 3020, (java.lang.Integer) (-2030));
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 1351444, (java.lang.Integer) 990);
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1987250499), (java.lang.Integer) 1739459091);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2030) + "'", int9.equals((-2030)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2679600 + "'", int13.equals(2679600));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 5050 + "'", int17.equals(5050));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 1352434 + "'", int20.equals(1352434));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 1989128711 + "'", int23.equals(1989128711));
    }

    @Test
    public void test349() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test349");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-105358747), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-105358747) + "'", int19.equals((-105358747)));
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test350() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test350");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 473676, (java.lang.Integer) (-2030));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-292), (java.lang.Integer) 2108502328);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-86797590), (java.lang.Integer) 674850);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 225533, (java.lang.Integer) 0);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 1199317608, (java.lang.Integer) 1980250204);
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) 173085920, (java.lang.Integer) (-305));
        java.lang.Integer int31 = operacionesMatematicas0.dividir((java.lang.Integer) (-1338190428), (java.lang.Integer) 1199317608);
        java.lang.Integer int34 = operacionesMatematicas0.restar((java.lang.Integer) 1963548060, (java.lang.Integer) (-847810496));
        java.lang.Integer int37 = operacionesMatematicas0.restar((java.lang.Integer) 333, (java.lang.Integer) 1355331012);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-961562280) + "'", int12.equals((-961562280)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-589628652) + "'", int19.equals((-589628652)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 225533 + "'", int22.equals(225533));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-1115399484) + "'", int25.equals((-1115399484)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-1251598048) + "'", int28.equals((-1251598048)));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-1) + "'", int31.equals((-1)));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + (-1483608740) + "'", int34.equals((-1483608740)));
        org.junit.Assert.assertTrue("'" + int37 + "' != '" + (-1355330679) + "'", int37.equals((-1355330679)));
    }

    @Test
    public void test351() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test351");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) (-1001435549), (java.lang.Integer) (-12511));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-1001448060) + "'", int16.equals((-1001448060)));
    }

    @Test
    public void test352() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test352");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-203657735), (java.lang.Integer) (-98975152));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1079, (java.lang.Integer) 222612);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 1937735020, (java.lang.Integer) 1101);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-104682583) + "'", int9.equals((-104682583)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-221533) + "'", int12.equals((-221533)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1937733919 + "'", int15.equals(1937733919));
    }

    @Test
    public void test353() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test353");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2, (java.lang.Integer) 99);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 200871, (java.lang.Integer) 688);
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-42632), (java.lang.Integer) (-4060));
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1806993207, (java.lang.Integer) 29686803);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 198 + "'", int15.equals(198));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 200183 + "'", int18.equals(200183));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 173085920 + "'", int21.equals(173085920));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 1889283349 + "'", int24.equals(1889283349));
    }

    @Test
    public void test354() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test354");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-4060));
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1748873246), (java.lang.Integer) 1355331012);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-1680870648) + "'", int11.equals((-1680870648)));
    }

    @Test
    public void test355() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test355");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) (-1646083254), (java.lang.Integer) 227358);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 7699977, (java.lang.Integer) (-742442848));
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-71), (java.lang.Integer) 78757851);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-1646310612) + "'", int10.equals((-1646310612)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1891089824 + "'", int13.equals(1891089824));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
    }

    @Test
    public void test356() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test356");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) 101);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) (-319239361), (java.lang.Integer) (-2031));
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 100, (java.lang.Integer) 1214289920);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2 + "'", int6.equals(2));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-319237330) + "'", int10.equals((-319237330)));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1169907712 + "'", int15.equals(1169907712));
    }

    @Test
    public void test357() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test357");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-589628652), (java.lang.Integer) 28000);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-87100372), (java.lang.Integer) 3884);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-589656652) + "'", int9.equals((-589656652)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-87104256) + "'", int12.equals((-87104256)));
    }

    @Test
    public void test358() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test358");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-25));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 150376532, (java.lang.Integer) 480630);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 312 + "'", int13.equals(312));
    }

    @Test
    public void test359() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test359");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2, (java.lang.Integer) (-1));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) 338956);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) (-1477849338), (java.lang.Integer) 483076222);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-2) + "'", int13.equals((-2)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1023986076 + "'", int16.equals(1023986076));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-994773116) + "'", int20.equals((-994773116)));
    }

    @Test
    public void test360() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test360");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) (-3), (java.lang.Integer) 0);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) (-1033021970), (java.lang.Integer) 821534);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-678435), (java.lang.Integer) 99692);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-3) + "'", int7.equals((-3)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-1033843504) + "'", int10.equals((-1033843504)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-778127) + "'", int13.equals((-778127)));
    }

    @Test
    public void test361() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test361");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1913));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-403), (java.lang.Integer) 12);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 151554579, (java.lang.Integer) 644054574);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-8230984), (java.lang.Integer) 442013635);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 117 + "'", int9.equals(117));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-391) + "'", int12.equals((-391)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-490567384) + "'", int19.equals((-490567384)));
    }

    @Test
    public void test362() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test362");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) 688);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) (-2031), (java.lang.Integer) 223300);
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) 191, (java.lang.Integer) 3451);
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) (-86797590), (java.lang.Integer) (-1072091024));
        java.lang.Integer int29 = operacionesMatematicas0.dividir((java.lang.Integer) (-2233), (java.lang.Integer) 161877486);
        java.lang.Integer int32 = operacionesMatematicas0.dividir((java.lang.Integer) 419736786, (java.lang.Integer) (-2962));
        java.lang.Integer int35 = operacionesMatematicas0.dividir((java.lang.Integer) (-151004351), (java.lang.Integer) 681653544);
        java.lang.Class<?> wildcardClass36 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 12 + "'", int13.equals(12));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 222612 + "'", int17.equals(222612));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 221269 + "'", int20.equals(221269));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-3260) + "'", int23.equals((-3260)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 0 + "'", int26.equals(0));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 0 + "'", int29.equals(0));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + (-141707) + "'", int32.equals((-141707)));
        org.junit.Assert.assertTrue("'" + int35 + "' != '" + 0 + "'", int35.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass36);
    }

    @Test
    public void test363() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test363");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 101);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-650669), (java.lang.Integer) 28000);
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-2233), (java.lang.Integer) (-747185024));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1702393088, (java.lang.Integer) (-1429679577));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-304) + "'", int6.equals((-304)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-23) + "'", int9.equals((-23)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1387015424) + "'", int15.equals((-1387015424)));
    }

    @Test
    public void test364() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test364");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) (-304), (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1959288468), (java.lang.Integer) 975452);
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 569427227, (java.lang.Integer) (-139473728));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-4060), (java.lang.Integer) 2037899052);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-507) + "'", int6.equals((-507)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-127444272) + "'", int9.equals((-127444272)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-4) + "'", int12.equals((-4)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 2037894992 + "'", int15.equals(2037894992));
    }

    @Test
    public void test365() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test365");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 1888659260);
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-58), (java.lang.Integer) (-203656345));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-1027036680), (java.lang.Integer) 44906216);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-22) + "'", int13.equals((-22)));
    }

    @Test
    public void test366() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test366");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-2233), (java.lang.Integer) 199);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 227358, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 28000, (java.lang.Integer) 675040);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-134082459), (java.lang.Integer) 6731);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 203354, (java.lang.Integer) 834392107);
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-11) + "'", int10.equals((-11)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227358 + "'", int13.equals(227358));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 703040 + "'", int16.equals(703040));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-565899369) + "'", int19.equals((-565899369)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 834595461 + "'", int22.equals(834595461));
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test367() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test367");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 703051, (java.lang.Integer) 191);
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 298, (java.lang.Integer) (-3260));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 222612, (java.lang.Integer) 2108475396);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2354, (java.lang.Integer) (-1868612537));
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 16335, (java.lang.Integer) (-1614295506));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 134282741 + "'", int7.equals(134282741));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-2962) + "'", int10.equals((-2962)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-667400994) + "'", int16.equals((-667400994)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-1614279171) + "'", int20.equals((-1614279171)));
    }

    @Test
    public void test368() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test368");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 146, (java.lang.Integer) 990);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-223), (java.lang.Integer) 3020);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 157225294, (java.lang.Integer) (-300943));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-844) + "'", int7.equals((-844)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-673460) + "'", int10.equals((-673460)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1803047790 + "'", int13.equals(1803047790));
    }

    @Test
    public void test369() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test369");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.sumar((java.lang.Integer) (-2030), (java.lang.Integer) (-674366));
        java.lang.Class<?> wildcardClass9 = operacionesMatematicas0.getClass();
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-319239361), (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-895373708), (java.lang.Integer) 990);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 83, (java.lang.Integer) 137901733);
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) 1150848763, (java.lang.Integer) (-1734056208));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + (-676396) + "'", int8.equals((-676396)));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-319241594) + "'", int12.equals((-319241594)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-904417) + "'", int15.equals((-904417)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1439058049) + "'", int18.equals((-1439058049)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
    }

    @Test
    public void test370() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test370");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-4060), (java.lang.Integer) (-203658530));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) (-906764242));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 30107083, (java.lang.Integer) (-59));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203662590) + "'", int6.equals((-203662590)));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-906764242) + "'", int10.equals((-906764242)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-510289) + "'", int13.equals((-510289)));
    }

    @Test
    public void test371() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test371");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.sumar((java.lang.Integer) (-2030), (java.lang.Integer) (-674366));
        java.lang.Class<?> wildcardClass9 = operacionesMatematicas0.getClass();
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-319239361), (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-895373708), (java.lang.Integer) 990);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-309626391), (java.lang.Integer) (-821707220));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + (-676396) + "'", int8.equals((-676396)));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-319241594) + "'", int12.equals((-319241594)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-904417) + "'", int15.equals((-904417)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 512080829 + "'", int18.equals(512080829));
    }

    @Test
    public void test372() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test372");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 674850, (java.lang.Integer) (-203658532));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-922927979), (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
    }

    @Test
    public void test373() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test373");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.dividir((java.lang.Integer) 675054, (java.lang.Integer) (-674366));
        java.lang.Integer int11 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1349700, (java.lang.Integer) (-136443584));
        java.lang.Integer int14 = operacionesMatematicas0.restar((java.lang.Integer) 1888659260, (java.lang.Integer) (-1480217496));
        java.lang.Integer int17 = operacionesMatematicas0.multiplicar((java.lang.Integer) 333, (java.lang.Integer) (-203859113));
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1980658657, (java.lang.Integer) (-406034920));
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1226993776), (java.lang.Integer) (-1352861920));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + (-1) + "'", int8.equals((-1)));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 1702393088 + "'", int11.equals(1702393088));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + (-926090540) + "'", int14.equals((-926090540)));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 834392107 + "'", int17.equals(834392107));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 835879704 + "'", int20.equals(835879704));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-1460280832) + "'", int23.equals((-1460280832)));
    }

    @Test
    public void test374() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test374");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-304));
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 191, (java.lang.Integer) 137901733);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 2010197328, (java.lang.Integer) (-133524468));
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-593256750), (java.lang.Integer) (-1429679800));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) 263, (java.lang.Integer) 1033577350);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 569427227 + "'", int13.equals(569427227));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-15) + "'", int16.equals((-15)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-2022936550) + "'", int19.equals((-2022936550)));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-1033577087) + "'", int23.equals((-1033577087)));
    }

    @Test
    public void test375() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test375");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-26796), (java.lang.Integer) (-2131));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-674366), (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-678748), (java.lang.Integer) (-678748));
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) 676504, (java.lang.Integer) 153263227);
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) 1358052, (java.lang.Integer) (-793575866));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-28927) + "'", int12.equals((-28927)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-133524468) + "'", int15.equals((-133524468)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-792217814) + "'", int24.equals((-792217814)));
    }

    @Test
    public void test376() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test376");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 688, (java.lang.Integer) 990);
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-343065842), (java.lang.Integer) 1963548060);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-1429679577), (java.lang.Integer) 181674816);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1678 + "'", int9.equals(1678));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1611354393) + "'", int15.equals((-1611354393)));
    }

    @Test
    public void test377() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test377");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-2233), (java.lang.Integer) 199);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 227358, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 28000, (java.lang.Integer) 675040);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 218, (java.lang.Integer) (-98));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-11) + "'", int10.equals((-11)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227358 + "'", int13.equals(227358));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 703040 + "'", int16.equals(703040));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 120 + "'", int19.equals(120));
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test378() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test378");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 99, (java.lang.Integer) (-25));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-11), (java.lang.Integer) 0);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) 342371593, (java.lang.Integer) (-1251598048));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-3) + "'", int6.equals((-3)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-909226455) + "'", int12.equals((-909226455)));
    }

    @Test
    public void test379() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test379");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-23062), (java.lang.Integer) 1968086742);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-1008633599), (java.lang.Integer) 311282688);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1197940124 + "'", int9.equals(1197940124));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-697350911) + "'", int12.equals((-697350911)));
    }

    @Test
    public void test380() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test380");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 2, (java.lang.Integer) 1023986076);
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1304, (java.lang.Integer) (-3353));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-4372312) + "'", int21.equals((-4372312)));
    }

    @Test
    public void test381() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test381");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-674366));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-1027036680), (java.lang.Integer) (-28927));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 834392107, (java.lang.Integer) (-343065842));
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1427973632, (java.lang.Integer) 442626564);
        java.lang.Integer int31 = operacionesMatematicas0.restar((java.lang.Integer) (-166100), (java.lang.Integer) (-952340332));
        java.lang.Integer int34 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-3241260), (java.lang.Integer) (-496153144));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-203658532) + "'", int19.equals((-203658532)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 35504 + "'", int22.equals(35504));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-2) + "'", int25.equals((-2)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 1214289920 + "'", int28.equals(1214289920));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 952174232 + "'", int31.equals(952174232));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 1029847456 + "'", int34.equals(1029847456));
    }

    @Test
    public void test382() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test382");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-507), (java.lang.Integer) 200871);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-3030), (java.lang.Integer) 1980253234);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 2137056006, (java.lang.Integer) (-203658612));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-1033487401), (java.lang.Integer) (-150310400));
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-44902028), (java.lang.Integer) 831667596);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-201378) + "'", int16.equals((-201378)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1980250204 + "'", int19.equals(1980250204));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1933397394 + "'", int22.equals(1933397394));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-1183797801) + "'", int25.equals((-1183797801)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 2135396208 + "'", int28.equals(2135396208));
    }

    @Test
    public void test383() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test383");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99990, (java.lang.Integer) 14);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1570059, (java.lang.Integer) 222308);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) 7228, (java.lang.Integer) (-177104011));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 100004 + "'", int9.equals(100004));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-26796) + "'", int12.equals((-26796)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1144325196 + "'", int15.equals(1144325196));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-207537300) + "'", int20.equals((-207537300)));
    }

    @Test
    public void test384() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test384");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-203657735), (java.lang.Integer) (-98975152));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-872882432), (java.lang.Integer) 1374);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-203), (java.lang.Integer) 3233);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-453522300), (java.lang.Integer) 2108475396);
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) 1593176491, (java.lang.Integer) 684291519);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-104682583) + "'", int9.equals((-104682583)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-635285) + "'", int12.equals((-635285)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1654953096 + "'", int18.equals(1654953096));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 2 + "'", int21.equals(2));
    }

    @Test
    public void test385() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test385");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-2030), (java.lang.Integer) 677084);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 134282741, (java.lang.Integer) 218);
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 222308, (java.lang.Integer) 1980250204);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-86407950), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-679114) + "'", int16.equals((-679114)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-791133534) + "'", int19.equals((-791133534)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-95554576) + "'", int22.equals((-95554576)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-86407950) + "'", int25.equals((-86407950)));
        org.junit.Assert.assertNotNull(wildcardClass26);
    }

    @Test
    public void test386() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test386");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 338956, (java.lang.Integer) (-337227660));
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) (-702086), (java.lang.Integer) (-91808));
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) 173085920, (java.lang.Integer) 203659430);
        java.lang.Integer int31 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2139858193, (java.lang.Integer) (-1231212116));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-610278) + "'", int25.equals((-610278)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-30573510) + "'", int28.equals((-30573510)));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-1109907348) + "'", int31.equals((-1109907348)));
    }

    @Test
    public void test387() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test387");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99990, (java.lang.Integer) 14);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-203654745), (java.lang.Integer) (-90));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-1077768), (java.lang.Integer) 28000);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-517446961), (java.lang.Integer) (-1000630274));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 100004 + "'", int9.equals(100004));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-203654655) + "'", int12.equals((-203654655)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1105768) + "'", int15.equals((-1105768)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1518077235) + "'", int18.equals((-1518077235)));
    }

    @Test
    public void test388() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test388");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) 202);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 674850, (java.lang.Integer) (-2789518));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) 3464368, (java.lang.Integer) (-1753913018));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 98974704, (java.lang.Integer) (-28676780));
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 204 + "'", int6.equals(204));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 3464368 + "'", int9.equals(3464368));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1750448650) + "'", int12.equals((-1750448650)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 127651484 + "'", int16.equals(127651484));
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test389() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test389");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 2030);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 990, (java.lang.Integer) (-304));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 21210);
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) 302, (java.lang.Integer) (-682176));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-2233) + "'", int16.equals((-2233)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-300960) + "'", int19.equals((-300960)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 21210 + "'", int22.equals(21210));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 682478 + "'", int25.equals(682478));
    }

    @Test
    public void test390() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test390");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-56948000), (java.lang.Integer) (-2030));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) (-791356247));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 28053 + "'", int6.equals(28053));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-791356247) + "'", int9.equals((-791356247)));
    }

    @Test
    public void test391() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test391");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99990, (java.lang.Integer) 14);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 218, (java.lang.Integer) 203658830);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 34527, (java.lang.Integer) (-198));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 1466134454, (java.lang.Integer) 1569639104);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 100004 + "'", int9.equals(100004));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-26796) + "'", int12.equals((-26796)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-203658612) + "'", int15.equals((-203658612)));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 34329 + "'", int19.equals(34329));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1259193738) + "'", int22.equals((-1259193738)));
    }

    @Test
    public void test392() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test392");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 101);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 20300);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1554530409, (java.lang.Integer) 4058);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 101 + "'", int4.equals(101));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 20300 + "'", int7.equals(20300));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-1022558102) + "'", int10.equals((-1022558102)));
    }

    @Test
    public void test393() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test393");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 990, (java.lang.Integer) 302);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 7458814, (java.lang.Integer) (-676296));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) 1710, (java.lang.Integer) 168);
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) (-86797590), (java.lang.Integer) (-1646083254));
        java.lang.Integer int27 = operacionesMatematicas0.dividir((java.lang.Integer) (-1827), (java.lang.Integer) (-17));
        java.lang.Integer int30 = operacionesMatematicas0.sumar((java.lang.Integer) 161877486, (java.lang.Integer) 191);
        java.lang.Integer int33 = operacionesMatematicas0.multiplicar((java.lang.Integer) 21210, (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 688 + "'", int15.equals(688));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-11) + "'", int18.equals((-11)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1542 + "'", int21.equals(1542));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1732880844) + "'", int24.equals((-1732880844)));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 107 + "'", int27.equals(107));
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + 161877677 + "'", int30.equals(161877677));
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + 0 + "'", int33.equals(0));
    }

    @Test
    public void test394() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test394");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-2031), (java.lang.Integer) (-100));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-679014), (java.lang.Integer) 28345);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 2108473854);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-86797590), (java.lang.Integer) (-985930295));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-203859113), (java.lang.Integer) (-1045));
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) (-1097252056), (java.lang.Integer) (-403));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-2131) + "'", int10.equals((-2131)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-650669) + "'", int13.equals((-650669)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-2108473854) + "'", int16.equals((-2108473854)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1072727885) + "'", int19.equals((-1072727885)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 195080 + "'", int22.equals(195080));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-1097251653) + "'", int25.equals((-1097251653)));
    }

    @Test
    public void test395() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test395");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1414472163), (java.lang.Integer) (-138799304));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-826855080) + "'", int15.equals((-826855080)));
    }

    @Test
    public void test396() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test396");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-337227660), (java.lang.Integer) (-2962));
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-2131));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3, (java.lang.Integer) (-1280974007));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 113851 + "'", int10.equals(113851));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 452045275 + "'", int17.equals(452045275));
    }

    @Test
    public void test397() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test397");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-507), (java.lang.Integer) 200871);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-702061), (java.lang.Integer) (-25));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 198, (java.lang.Integer) 7458814);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 98974848, (java.lang.Integer) (-144));
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 0);
        java.lang.Integer int31 = operacionesMatematicas0.dividir((java.lang.Integer) 2029, (java.lang.Integer) 1115055585);
        java.lang.Integer int34 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-201378) + "'", int16.equals((-201378)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-702086) + "'", int19.equals((-702086)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1476845172 + "'", int22.equals(1476845172));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 98974704 + "'", int25.equals(98974704));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 0 + "'", int31.equals(0));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 0 + "'", int34.equals(0));
    }

    @Test
    public void test398() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test398");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1089, (java.lang.Integer) (-1027036680));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1545972396), (java.lang.Integer) (-157900046));
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1751447560) + "'", int19.equals((-1751447560)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-979644056) + "'", int22.equals((-979644056)));
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test399() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test399");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-2233));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) 1980253234, (java.lang.Integer) 30107083);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 198, (java.lang.Integer) 1199317608);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 216150945, (java.lang.Integer) 1374);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 7228, (java.lang.Integer) (-1566668296));
        java.lang.Integer int24 = operacionesMatematicas0.restar((java.lang.Integer) (-666387), (java.lang.Integer) 591647436);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-674366) + "'", int9.equals((-674366)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2010360317 + "'", int12.equals(2010360317));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1199317410) + "'", int15.equals((-1199317410)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 216149571 + "'", int18.equals(216149571));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1566661068) + "'", int21.equals((-1566661068)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-592313823) + "'", int24.equals((-592313823)));
    }

    @Test
    public void test400() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test400");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-337227660), (java.lang.Integer) (-2962));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-44906012), (java.lang.Integer) 3884);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 371921648, (java.lang.Integer) (-612929));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 113851 + "'", int10.equals(113851));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-44902128) + "'", int13.equals((-44902128)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 372534577 + "'", int16.equals(372534577));
    }

    @Test
    public void test401() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test401");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 101);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) (-674366), (java.lang.Integer) 3020);
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 200183);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-679114), (java.lang.Integer) (-172199));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 7809, (java.lang.Integer) (-343058591));
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-9), (java.lang.Integer) 0);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 101 + "'", int4.equals(101));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-223) + "'", int7.equals((-223)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 200184 + "'", int10.equals(200184));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-506915) + "'", int13.equals((-506915)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1115055585 + "'", int16.equals(1115055585));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-9) + "'", int19.equals((-9)));
    }

    @Test
    public void test402() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test402");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) (-292));
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2114178049, (java.lang.Integer) 925442);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-882132) + "'", int16.equals((-882132)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2115103491 + "'", int19.equals(2115103491));
    }

    @Test
    public void test403() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test403");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 1079, (java.lang.Integer) 679827);
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-198118), (java.lang.Integer) 674850);
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) 166120, (java.lang.Integer) 200282);
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        java.lang.Integer int27 = operacionesMatematicas0.dividir((java.lang.Integer) 1079, (java.lang.Integer) 241172932);
        java.lang.Class<?> wildcardClass28 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-678748) + "'", int17.equals((-678748)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-872968) + "'", int20.equals((-872968)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-34162) + "'", int23.equals((-34162)));
        org.junit.Assert.assertNotNull(wildcardClass24);
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 0 + "'", int27.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass28);
    }

    @Test
    public void test404() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test404");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99990, (java.lang.Integer) 14);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 218, (java.lang.Integer) 203658830);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 166111, (java.lang.Integer) (-203656852));
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) (-3260), (java.lang.Integer) (-1566625664));
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) 458990, (java.lang.Integer) 1981352906);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 100004 + "'", int9.equals(100004));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-26796) + "'", int12.equals((-26796)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-203658612) + "'", int15.equals((-203658612)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 1981811896 + "'", int24.equals(1981811896));
    }

    @Test
    public void test405() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test405");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) 14);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1024014076, (java.lang.Integer) (-204));
        java.lang.Class<?> wildcardClass15 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 675040 + "'", int10.equals(675040));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 1554526000 + "'", int14.equals(1554526000));
        org.junit.Assert.assertNotNull(wildcardClass15);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test406() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test406");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 138125136, (java.lang.Integer) 223403);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.restar((java.lang.Integer) (-391), (java.lang.Integer) 847782221);
        java.lang.Integer int14 = operacionesMatematicas0.restar((java.lang.Integer) (-1377181541), (java.lang.Integer) (-1219790913));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 137901733 + "'", int7.equals(137901733));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-847782612) + "'", int11.equals((-847782612)));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + (-157390628) + "'", int14.equals((-157390628)));
    }

    @Test
    public void test407() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test407");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) (-2452019), (java.lang.Integer) 591647436);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 673023, (java.lang.Integer) (-5));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2016250560), (java.lang.Integer) (-791236412));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-594099455) + "'", int6.equals((-594099455)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-134604) + "'", int9.equals((-134604)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1321351936) + "'", int12.equals((-1321351936)));
    }

    @Test
    public void test408() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test408");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-100), (java.lang.Integer) (-2233));
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 223, (java.lang.Integer) (-2131));
        try {
            java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-3840564), (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223300 + "'", int6.equals(223300));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 2354 + "'", int9.equals(2354));
    }

    @Test
    public void test409() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test409");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-223), (java.lang.Integer) (-1429679577));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2233), (java.lang.Integer) (-635285));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 727228036, (java.lang.Integer) (-1646310612));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1429679800) + "'", int13.equals((-1429679800)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1418591405 + "'", int16.equals(1418591405));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1921428648) + "'", int19.equals((-1921428648)));
    }

    @Test
    public void test410() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test410");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 298, (java.lang.Integer) (-203658532));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-605856380), (java.lang.Integer) (-998194388));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 203658830 + "'", int7.equals(203658830));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
    }

    @Test
    public void test411() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test411");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) 688);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) (-1), (java.lang.Integer) (-304));
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) (-203658612), (java.lang.Integer) (-23604));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 12 + "'", int13.equals(12));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 222612 + "'", int17.equals(222612));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-305) + "'", int20.equals((-305)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-203682216) + "'", int23.equals((-203682216)));
    }

    @Test
    public void test412() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test412");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-563084124));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 1968086742, (java.lang.Integer) (-1208538464));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-1180735552), (java.lang.Integer) (-10430));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1) + "'", int12.equals((-1)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1180745982) + "'", int15.equals((-1180745982)));
    }

    @Test
    public void test413() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test413");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-202979314), (java.lang.Integer) 223);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-794241824), (java.lang.Integer) 6295);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 138145238, (java.lang.Integer) (-1827));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 1980253234 + "'", int4.equals(1980253234));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-794235529) + "'", int7.equals((-794235529)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 138147065 + "'", int10.equals(138147065));
    }

    @Test
    public void test414() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test414");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 1678);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-204), (java.lang.Integer) (-669335781));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 953704, (java.lang.Integer) (-1358028));
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-1830), (java.lang.Integer) 44906012);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 166122 + "'", int10.equals(166122));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 669335577 + "'", int13.equals(669335577));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1923387680 + "'", int16.equals(1923387680));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 44904182 + "'", int19.equals(44904182));
    }

    @Test
    public void test415() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test415");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 674850, (java.lang.Integer) (-203658532));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) (-103));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-679114), (java.lang.Integer) 3451);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-9) + "'", int12.equals((-9)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-196) + "'", int15.equals((-196)));
    }

    @Test
    public void test416() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test416");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1089, (java.lang.Integer) (-1027036680));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 23816997, (java.lang.Integer) 222308);
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) (-844346128), (java.lang.Integer) (-2031));
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass27 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1751447560) + "'", int19.equals((-1751447560)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-985706892) + "'", int22.equals((-985706892)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 415729 + "'", int25.equals(415729));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertNotNull(wildcardClass27);
    }

    @Test
    public void test417() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test417");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 679827);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-86407950), (java.lang.Integer) 419736786);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-485640240), (java.lang.Integer) 677084);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 199, (java.lang.Integer) 569427227);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1718978089), (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-679726) + "'", int6.equals((-679726)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-835045696) + "'", int12.equals((-835045696)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-569427028) + "'", int15.equals((-569427028)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
    }

    @Test
    public void test418() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test418");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166122, (java.lang.Integer) (-2030));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) 703040, (java.lang.Integer) 2005416224);
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 1739462365, (java.lang.Integer) (-853072144));
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-203656745), (java.lang.Integer) 200871);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 674196, (java.lang.Integer) (-485640239));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 199 + "'", int6.equals(199));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-337227660) + "'", int9.equals((-337227660)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2006119264 + "'", int12.equals(2006119264));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 886390221 + "'", int16.equals(886390221));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 829469505 + "'", int19.equals(829469505));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-484966043) + "'", int22.equals((-484966043)));
    }

    @Test
    public void test419() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test419");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-223), (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-4060), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-343065842), (java.lang.Integer) 138124938);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 1899059129, (java.lang.Integer) (-339888836));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223 + "'", int6.equals(223));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2) + "'", int9.equals((-2)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-204940904) + "'", int12.equals((-204940904)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1559170293 + "'", int15.equals(1559170293));
    }

    @Test
    public void test420() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test420");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 374697400, (java.lang.Integer) 473676);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 44906012);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) (-86631468), (java.lang.Integer) 2);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 1771576096, (java.lang.Integer) 1144325196);
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 791 + "'", int13.equals(791));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-44906012) + "'", int16.equals((-44906012)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-43315734) + "'", int19.equals((-43315734)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1379066004) + "'", int22.equals((-1379066004)));
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test421() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test421");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 199);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.sumar((java.lang.Integer) 200257, (java.lang.Integer) (-25));
        java.lang.Integer int17 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-717311), (java.lang.Integer) (-2027022092));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 298 + "'", int9.equals(298));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 200232 + "'", int14.equals(200232));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-99651340) + "'", int17.equals((-99651340)));
    }

    @Test
    public void test422() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test422");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 34329, (java.lang.Integer) (-1468944503));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 669356785, (java.lang.Integer) 2107797558);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-2076676394) + "'", int13.equals((-2076676394)));
    }

    @Test
    public void test423() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test423");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        try {
            java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-2027022092), (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
    }

    @Test
    public void test424() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test424");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 223403, (java.lang.Integer) (-203658530));
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203658830, (java.lang.Integer) 1351444);
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) 1702393088, (java.lang.Integer) (-358));
        java.lang.Integer int29 = operacionesMatematicas0.sumar((java.lang.Integer) 425815, (java.lang.Integer) (-1865645598));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-203435127) + "'", int20.equals((-203435127)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-885379048) + "'", int23.equals((-885379048)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-4755287) + "'", int26.equals((-4755287)));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-1865219783) + "'", int29.equals((-1865219783)));
    }

    @Test
    public void test425() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test425");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) (-1));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 2030, (java.lang.Integer) 1);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 2010926408, (java.lang.Integer) (-220971));
        try {
            java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 221269, (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2029 + "'", int10.equals(2029));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2010705437 + "'", int13.equals(2010705437));
    }

    @Test
    public void test426() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test426");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 223);
        java.lang.Integer int17 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-134082459), (java.lang.Integer) (-2031));
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) 161877486, (java.lang.Integer) (-2108473877));
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) (-674571), (java.lang.Integer) 1803047790);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + (-223) + "'", int14.equals((-223)));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 1738534581 + "'", int17.equals(1738534581));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 0 + "'", int20.equals(0));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 0 + "'", int23.equals(0));
    }

    @Test
    public void test427() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test427");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-98975152), (java.lang.Integer) (-791133534));
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.restar((java.lang.Integer) (-1033487401), (java.lang.Integer) 1710);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-890108686) + "'", int10.equals((-890108686)));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + (-1033489111) + "'", int14.equals((-1033489111)));
    }

    @Test
    public void test428() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test428");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-223), (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-4060), (java.lang.Integer) 2030);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-1034161825), (java.lang.Integer) 243122812);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1727627124));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223 + "'", int6.equals(223));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2) + "'", int9.equals((-2)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-4) + "'", int13.equals((-4)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
    }

    @Test
    public void test429() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test429");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) 14);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 227358, (java.lang.Integer) 223);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 166122, (java.lang.Integer) (-11));
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 200232, (java.lang.Integer) (-181982338));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) (-485640240));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 959378576, (java.lang.Integer) 727228036);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 675040 + "'", int10.equals(675040));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227581 + "'", int13.equals(227581));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 166111 + "'", int16.equals(166111));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-181782106) + "'", int19.equals((-181782106)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-485640240) + "'", int22.equals((-485640240)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1 + "'", int25.equals(1));
    }

    @Test
    public void test430() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test430");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-223), (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 1050105608, (java.lang.Integer) 6823529);
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 200257, (java.lang.Integer) (-682380));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-569427028), (java.lang.Integer) (-1175987796));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223 + "'", int6.equals(223));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 153 + "'", int9.equals(153));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 606560768 + "'", int15.equals(606560768));
    }

    @Test
    public void test431() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test431");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) (-304), (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 2941, (java.lang.Integer) (-673460));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-1868330036), (java.lang.Integer) (-1019770691));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 2118109232, (java.lang.Integer) 1307953321);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-507) + "'", int6.equals((-507)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 676401 + "'", int9.equals(676401));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1 + "'", int12.equals(1));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-868904743) + "'", int15.equals((-868904743)));
    }

    @Test
    public void test432() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test432");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-203658532), (java.lang.Integer) 797);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 138125136, (java.lang.Integer) (-198));
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) 3021, (java.lang.Integer) (-607257));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) 252, (java.lang.Integer) 1161756880);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-203657735) + "'", int18.equals((-203657735)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 138124938 + "'", int21.equals(138124938));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 610278 + "'", int25.equals(610278));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
    }

    @Test
    public void test433() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test433");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-98975152), (java.lang.Integer) (-300960));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 204, (java.lang.Integer) 0);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-2034), (java.lang.Integer) 676401);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 2074781511, (java.lang.Integer) (-1988356113));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1963548160 + "'", int9.equals(1963548160));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 204 + "'", int12.equals(204));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-678435) + "'", int15.equals((-678435)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-231829672) + "'", int18.equals((-231829672)));
    }

    @Test
    public void test434() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test434");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) 2030, (java.lang.Integer) 688);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) (-2233));
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.sumar((java.lang.Integer) 298, (java.lang.Integer) 0);
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 44907013, (java.lang.Integer) (-2452019));
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) 794, (java.lang.Integer) (-517446961));
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) (-872968), (java.lang.Integer) (-2789518));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7.equals(2));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 225533 + "'", int10.equals(225533));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 298 + "'", int14.equals(298));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 47359032 + "'", int17.equals(47359032));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 0 + "'", int20.equals(0));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 1916550 + "'", int23.equals(1916550));
    }

    @Test
    public void test435() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test435");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 473676, (java.lang.Integer) (-2030));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-292), (java.lang.Integer) 2108502328);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-202979700), (java.lang.Integer) (-887497368));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-961562280) + "'", int12.equals((-961562280)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1227651872) + "'", int19.equals((-1227651872)));
    }

    @Test
    public void test436() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test436");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) (-26796));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) (-3));
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1546915), (java.lang.Integer) 1963748327);
        java.lang.Integer int31 = operacionesMatematicas0.restar((java.lang.Integer) 116, (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-86631468) + "'", int22.equals((-86631468)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-103) + "'", int25.equals((-103)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 1315788267 + "'", int28.equals(1315788267));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 116 + "'", int31.equals(116));
    }

    @Test
    public void test437() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test437");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 203);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 1355141328, (java.lang.Integer) (-2091737468));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + (-204) + "'", int4.equals((-204)));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-848088500) + "'", int7.equals((-848088500)));
    }

    @Test
    public void test438() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test438");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-507), (java.lang.Integer) 200871);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-3030), (java.lang.Integer) 1980253234);
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 153262720, (java.lang.Integer) (-100));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 2107797558, (java.lang.Integer) (-44906012));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) (-391), (java.lang.Integer) 10495029);
        java.lang.Integer int31 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1214289920, (java.lang.Integer) 1515772240);
        java.lang.Integer int34 = operacionesMatematicas0.sumar((java.lang.Integer) 852490804, (java.lang.Integer) (-6451));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-201378) + "'", int16.equals((-201378)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1980250204 + "'", int19.equals(1980250204));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1853597184 + "'", int22.equals(1853597184));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 2062891546 + "'", int25.equals(2062891546));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-1090027520) + "'", int31.equals((-1090027520)));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 852484353 + "'", int34.equals(852484353));
    }

    @Test
    public void test439() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test439");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) (-3), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.sumar((java.lang.Integer) 703051, (java.lang.Integer) 222308);
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-3) + "'", int7.equals((-3)));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 925359 + "'", int11.equals(925359));
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test440() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test440");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-2233));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 849, (java.lang.Integer) 925359);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 1193221951, (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-674366) + "'", int9.equals((-674366)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-924510) + "'", int12.equals((-924510)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1193221951 + "'", int15.equals(1193221951));
    }

    @Test
    public void test441() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test441");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 1079, (java.lang.Integer) 679827);
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-198118), (java.lang.Integer) 674850);
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) 221269, (java.lang.Integer) 1796465853);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-678748) + "'", int17.equals((-678748)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-872968) + "'", int20.equals((-872968)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-1315384639) + "'", int23.equals((-1315384639)));
    }

    @Test
    public void test442() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test442");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 676495, (java.lang.Integer) (-678074));
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 166120, (java.lang.Integer) (-300960));
        try {
            java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 1905147011, (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-134840) + "'", int16.equals((-134840)));
    }

    @Test
    public void test443() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test443");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 1079, (java.lang.Integer) 679827);
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) 3274, (java.lang.Integer) 333);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) (-593256750), (java.lang.Integer) 961562281);
        java.lang.Integer int26 = operacionesMatematicas0.restar((java.lang.Integer) (-1536561213), (java.lang.Integer) (-98));
        java.lang.Integer int29 = operacionesMatematicas0.restar((java.lang.Integer) (-1315384639), (java.lang.Integer) 791133635);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-678748) + "'", int17.equals((-678748)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 2941 + "'", int20.equals(2941));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 0 + "'", int23.equals(0));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-1536561115) + "'", int26.equals((-1536561115)));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-2106518274) + "'", int29.equals((-2106518274)));
    }

    @Test
    public void test444() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test444");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) (-139407012), (java.lang.Integer) 203658830);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 701863, (java.lang.Integer) 1623620464);
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-4), (java.lang.Integer) 14);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-343065842) + "'", int17.equals((-343065842)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 1624322327 + "'", int20.equals(1624322327));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-56) + "'", int23.equals((-56)));
    }

    @Test
    public void test445() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test445");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 101);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) (-985706892), (java.lang.Integer) 223403);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 86235969, (java.lang.Integer) (-679114));
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 101 + "'", int4.equals(101));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-985930295) + "'", int7.equals((-985930295)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 86915083 + "'", int10.equals(86915083));
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test446() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test446");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) 101);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 28053, (java.lang.Integer) 849);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 2107797558, (java.lang.Integer) (-2010186930));
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-9430), (java.lang.Integer) (-304));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2 + "'", int6.equals(2));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 23816997 + "'", int10.equals(23816997));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-176982808) + "'", int13.equals((-176982808)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 31 + "'", int16.equals(31));
    }

    @Test
    public void test447() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test447");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) 223300);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-676396), (java.lang.Integer) 1678);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2131278928), (java.lang.Integer) 1466134454);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) (-2327), (java.lang.Integer) 138319872);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2030) + "'", int9.equals((-2030)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2679600 + "'", int13.equals(2679600));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-678074) + "'", int16.equals((-678074)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-483168480) + "'", int19.equals((-483168480)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-138322199) + "'", int22.equals((-138322199)));
    }

    @Test
    public void test448() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test448");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-304), (java.lang.Integer) 12);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 902932, (java.lang.Integer) 199);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 151210502, (java.lang.Integer) 6);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-548575792), (java.lang.Integer) (-133719138));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-292) + "'", int7.equals((-292)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 179683468 + "'", int10.equals(179683468));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 907263012 + "'", int13.equals(907263012));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-682294930) + "'", int17.equals((-682294930)));
    }

    @Test
    public void test449() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test449");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-134082459), (java.lang.Integer) 23816997);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) 21210);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-110265462) + "'", int15.equals((-110265462)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
    }

    @Test
    public void test450() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test450");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-1085863536), (java.lang.Integer) (-391));
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-203761580), (java.lang.Integer) (-798007));
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.restar((java.lang.Integer) (-2015005451), (java.lang.Integer) 1468944689);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-1085863927) + "'", int7.equals((-1085863927)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-204559587) + "'", int10.equals((-204559587)));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 811017156 + "'", int14.equals(811017156));
    }

    @Test
    public void test451() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test451");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-26796), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) (-6763), (java.lang.Integer) (-4060));
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) 1542, (java.lang.Integer) 2108473854);
        java.lang.Integer int29 = operacionesMatematicas0.restar((java.lang.Integer) (-3162), (java.lang.Integer) (-903099228));
        java.lang.Class<?> wildcardClass30 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-28474) + "'", int20.equals((-28474)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 1 + "'", int23.equals(1));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 2108475396 + "'", int26.equals(2108475396));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 903096066 + "'", int29.equals(903096066));
        org.junit.Assert.assertNotNull(wildcardClass30);
    }

    @Test
    public void test452() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test452");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 1352162704);
        java.lang.Integer int14 = operacionesMatematicas0.sumar((java.lang.Integer) (-1646083254), (java.lang.Integer) (-1646083254));
        java.lang.Integer int17 = operacionesMatematicas0.dividir((java.lang.Integer) (-890108686), (java.lang.Integer) (-679014));
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) 1362967132, (java.lang.Integer) (-678748));
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) (-846460796), (java.lang.Integer) (-1751438130));
        java.lang.Integer int26 = operacionesMatematicas0.restar((java.lang.Integer) 569427227, (java.lang.Integer) 12704);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-388878032) + "'", int11.equals((-388878032)));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 1002800788 + "'", int14.equals(1002800788));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 1310 + "'", int17.equals(1310));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-2008) + "'", int20.equals((-2008)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 0 + "'", int23.equals(0));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 569414523 + "'", int26.equals(569414523));
    }

    @Test
    public void test453() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test453");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 146, (java.lang.Integer) 990);
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 200184, (java.lang.Integer) (-2030));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-136443584), (java.lang.Integer) 198);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 1106533862, (java.lang.Integer) (-98));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 775906878, (java.lang.Integer) (-973376920));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 255987712, (java.lang.Integer) (-791356247));
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 834595461, (java.lang.Integer) 0);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-844) + "'", int7.equals((-844)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-98) + "'", int10.equals((-98)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-689109) + "'", int13.equals((-689109)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1106533960 + "'", int16.equals(1106533960));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1749283798 + "'", int19.equals(1749283798));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 408719360 + "'", int22.equals(408719360));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
    }

    @Test
    public void test454() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test454");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-304), (java.lang.Integer) 12);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) (-674366), (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-292) + "'", int7.equals((-292)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-674376) + "'", int10.equals((-674376)));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test455() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test455");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 1678, (java.lang.Integer) (-304));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 14, (java.lang.Integer) (-676594));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-2030), (java.lang.Integer) 98974848);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-319619085), (java.lang.Integer) 28053);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 203354, (java.lang.Integer) 747408427);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1374 + "'", int9.equals(1374));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1617522543 + "'", int18.equals(1617522543));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 747611781 + "'", int21.equals(747611781));
    }

    @Test
    public void test456() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test456");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-203657735), (java.lang.Integer) (-98975152));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-872882432), (java.lang.Integer) 1374);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-203), (java.lang.Integer) 3233);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) (-223), (java.lang.Integer) 203658830);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 242847, (java.lang.Integer) (-1988598960));
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) 252, (java.lang.Integer) 131302456);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-104682583) + "'", int9.equals((-104682583)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-635285) + "'", int12.equals((-635285)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1988356113) + "'", int21.equals((-1988356113)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1271519456) + "'", int24.equals((-1271519456)));
    }

    @Test
    public void test457() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test457");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2), (java.lang.Integer) 2030);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-44906012), (java.lang.Integer) 0);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-374696609), (java.lang.Integer) (-619745648));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) 25, (java.lang.Integer) 909024306);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-4060) + "'", int12.equals((-4060)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-44906012) + "'", int15.equals((-44906012)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 245049039 + "'", int18.equals(245049039));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-909024281) + "'", int21.equals((-909024281)));
    }

    @Test
    public void test458() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test458");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.sumar((java.lang.Integer) (-847810496), (java.lang.Integer) 3464368);
        java.lang.Integer int14 = operacionesMatematicas0.dividir((java.lang.Integer) 907263012, (java.lang.Integer) (-1105768));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-844346128) + "'", int11.equals((-844346128)));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + (-820) + "'", int14.equals((-820)));
    }

    @Test
    public void test459() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test459");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99990, (java.lang.Integer) 14);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1570059, (java.lang.Integer) 222308);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 727228036, (java.lang.Integer) (-235635084));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 100004 + "'", int9.equals(100004));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-26796) + "'", int12.equals((-26796)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1144325196 + "'", int15.equals(1144325196));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-3) + "'", int18.equals((-3)));
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test460() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test460");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) (-26796));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 28000, (java.lang.Integer) (-676594));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-674424));
        java.lang.Integer int31 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-343065842), (java.lang.Integer) (-1331960192));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-86631468) + "'", int22.equals((-86631468)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 187046656 + "'", int31.equals(187046656));
    }

    @Test
    public void test461() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test461");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-674366));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-203658532) + "'", int19.equals((-203658532)));
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test462() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test462");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) (-26796));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 28000, (java.lang.Integer) (-676594));
        java.lang.Integer int28 = operacionesMatematicas0.sumar((java.lang.Integer) (-594099455), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass29 = operacionesMatematicas0.getClass();
        java.lang.Integer int32 = operacionesMatematicas0.sumar((java.lang.Integer) 1937735020, (java.lang.Integer) 453522102);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-86631468) + "'", int22.equals((-86631468)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-594099455) + "'", int28.equals((-594099455)));
        org.junit.Assert.assertNotNull(wildcardClass29);
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + (-1903710174) + "'", int32.equals((-1903710174)));
    }

    @Test
    public void test463() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test463");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-507), (java.lang.Integer) 200871);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-702061), (java.lang.Integer) (-25));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 198, (java.lang.Integer) 7458814);
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) (-312057086), (java.lang.Integer) 1220739868);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-201378) + "'", int16.equals((-201378)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-702086) + "'", int19.equals((-702086)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1476845172 + "'", int22.equals(1476845172));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
    }

    @Test
    public void test464() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test464");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.dividir((java.lang.Integer) 675054, (java.lang.Integer) (-674366));
        java.lang.Integer int11 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1349700, (java.lang.Integer) (-136443584));
        java.lang.Integer int14 = operacionesMatematicas0.restar((java.lang.Integer) 1888659260, (java.lang.Integer) (-1480217496));
        java.lang.Integer int17 = operacionesMatematicas0.multiplicar((java.lang.Integer) 333, (java.lang.Integer) (-203859113));
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1, (java.lang.Integer) 203654756);
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + (-1) + "'", int8.equals((-1)));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 1702393088 + "'", int11.equals(1702393088));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + (-926090540) + "'", int14.equals((-926090540)));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 834392107 + "'", int17.equals(834392107));
        org.junit.Assert.assertNotNull(wildcardClass18);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 203654756 + "'", int21.equals(203654756));
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test465() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test465");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2000);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-11), (java.lang.Integer) (-678074));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-391), (java.lang.Integer) 975452);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-311834474), (java.lang.Integer) (-222612));
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) 6823429, (java.lang.Integer) (-2034));
        java.lang.Integer int24 = operacionesMatematicas0.restar((java.lang.Integer) (-1483608740), (java.lang.Integer) (-311834474));
        java.lang.Integer int27 = operacionesMatematicas0.dividir((java.lang.Integer) 99990, (java.lang.Integer) 1593177030);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 7458814 + "'", int12.equals(7458814));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-312057086) + "'", int18.equals((-312057086)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-3354) + "'", int21.equals((-3354)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1171774266) + "'", int24.equals((-1171774266)));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 0 + "'", int27.equals(0));
    }

    @Test
    public void test466() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test466");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 677082, (java.lang.Integer) 797);
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) (-1378461349), (java.lang.Integer) (-1988598960));
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-65), (java.lang.Integer) 1355141328);
        java.lang.Integer int32 = operacionesMatematicas0.dividir((java.lang.Integer) (-1460280832), (java.lang.Integer) (-33139200));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 849 + "'", int22.equals(849));
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 0 + "'", int26.equals(0));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 2110126896 + "'", int29.equals(2110126896));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 44 + "'", int32.equals(44));
    }

    @Test
    public void test467() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test467");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-4060));
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1609993824, (java.lang.Integer) 0);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11.equals(0));
    }

    @Test
    public void test468() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test468");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1089, (java.lang.Integer) (-1027036680));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 23816997, (java.lang.Integer) 222308);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166111, (java.lang.Integer) 1023985958);
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) (-11), (java.lang.Integer) 1418013632);
        java.lang.Integer int31 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) (-676594));
        java.lang.Integer int34 = operacionesMatematicas0.sumar((java.lang.Integer) (-140916264), (java.lang.Integer) 2002502299);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1751447560) + "'", int19.equals((-1751447560)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-985706892) + "'", int22.equals((-985706892)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1741645850 + "'", int25.equals(1741645850));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-1418013643) + "'", int28.equals((-1418013643)));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-676594) + "'", int31.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 1861586035 + "'", int34.equals(1861586035));
    }

    @Test
    public void test469() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test469");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) (-1));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 2030, (java.lang.Integer) 1);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 696288, (java.lang.Integer) (-98890));
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-1090027520), (java.lang.Integer) 302);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2029 + "'", int10.equals(2029));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-136443584) + "'", int13.equals((-136443584)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-3609362) + "'", int16.equals((-3609362)));
    }

    @Test
    public void test470() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test470");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) (-1));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) (-2233), (java.lang.Integer) 797);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-403), (java.lang.Integer) 1701552590);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) (-43316026), (java.lang.Integer) (-952340332));
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) (-1468950954), (java.lang.Integer) 353600244);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-3030) + "'", int10.equals((-3030)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1701552187 + "'", int13.equals(1701552187));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 909024306 + "'", int17.equals(909024306));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-1115350710) + "'", int20.equals((-1115350710)));
    }

    @Test
    public void test471() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test471");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) 223300);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-676396), (java.lang.Integer) 1678);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-203654745), (java.lang.Integer) (-365383));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) (-9), (java.lang.Integer) (-169435336));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 1738534581, (java.lang.Integer) (-320292228));
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) 2107797558, (java.lang.Integer) (-44902128));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2030) + "'", int9.equals((-2030)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2679600 + "'", int13.equals(2679600));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-678074) + "'", int16.equals((-678074)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-203289362) + "'", int19.equals((-203289362)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-169435345) + "'", int22.equals((-169435345)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-5) + "'", int25.equals((-5)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-2142267610) + "'", int28.equals((-2142267610)));
    }

    @Test
    public void test472() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test472");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-223), (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-86408168), (java.lang.Integer) (-1480217496));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 2108473854, (java.lang.Integer) 99990);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 1888660802, (java.lang.Integer) 1829857832);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223 + "'", int6.equals(223));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-1566625664) + "'", int9.equals((-1566625664)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2108373864 + "'", int12.equals(2108373864));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 58802970 + "'", int15.equals(58802970));
    }

    @Test
    public void test473() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test473");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 473676, (java.lang.Integer) 222612);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1546789220), (java.lang.Integer) (-3460));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 696288 + "'", int7.equals(696288));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 361450384 + "'", int11.equals(361450384));
    }

    @Test
    public void test474() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test474");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-882132), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) (-679114), (java.lang.Integer) 100);
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) 153262720, (java.lang.Integer) 23816997);
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1751438428, (java.lang.Integer) 6450721);
        java.lang.Class<?> wildcardClass30 = operacionesMatematicas0.getClass();
        java.lang.Integer int33 = operacionesMatematicas0.sumar((java.lang.Integer) (-610278), (java.lang.Integer) (-221533));
        java.lang.Integer int36 = operacionesMatematicas0.sumar((java.lang.Integer) (-1513001358), (java.lang.Integer) (-355611179));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-1480217496) + "'", int20.equals((-1480217496)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-679014) + "'", int23.equals((-679014)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 6 + "'", int26.equals(6));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 326559708 + "'", int29.equals(326559708));
        org.junit.Assert.assertNotNull(wildcardClass30);
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + (-831811) + "'", int33.equals((-831811)));
        org.junit.Assert.assertTrue("'" + int36 + "' != '" + (-1868612537) + "'", int36.equals((-1868612537)));
    }

    @Test
    public void test475() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test475");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-674366));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-1027036680), (java.lang.Integer) (-28927));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 834392107, (java.lang.Integer) (-343065842));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) 47990220, (java.lang.Integer) 1125242523);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-203658532) + "'", int19.equals((-203658532)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 35504 + "'", int22.equals(35504));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-2) + "'", int25.equals((-2)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
    }

    @Test
    public void test476() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test476");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) (-292));
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 6450721, (java.lang.Integer) (-1988598960));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass21 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-882132) + "'", int16.equals((-882132)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2010197328 + "'", int19.equals(2010197328));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test477() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test477");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) 202);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 674850, (java.lang.Integer) (-2789518));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) 3464368, (java.lang.Integer) (-1753913018));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-530028550), (java.lang.Integer) (-222612));
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 8960, (java.lang.Integer) 425815);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 204 + "'", int6.equals(204));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 3464368 + "'", int9.equals(3464368));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1750448650) + "'", int12.equals((-1750448650)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-625983112) + "'", int16.equals((-625983112)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-479664896) + "'", int19.equals((-479664896)));
    }

    @Test
    public void test478() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test478");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 1362967132);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 2005917886, (java.lang.Integer) (-6451));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-1362290048) + "'", int17.equals((-1362290048)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 2005911435 + "'", int20.equals(2005911435));
    }

    @Test
    public void test479() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test479");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 679827);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-203658532), (java.lang.Integer) (-589628652));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-202979314), (java.lang.Integer) 203177900);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 6, (java.lang.Integer) 1898863600);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-1988356113), (java.lang.Integer) (-792217814));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-679726) + "'", int6.equals((-679726)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 961940784 + "'", int9.equals(961940784));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 198586 + "'", int12.equals(198586));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1898863594) + "'", int15.equals((-1898863594)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1196138299) + "'", int18.equals((-1196138299)));
    }

    @Test
    public void test480() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test480");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-237), (java.lang.Integer) 101);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 3274, (java.lang.Integer) (-1440498544));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-23937) + "'", int16.equals((-23937)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test481() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test481");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 138125136, (java.lang.Integer) 223403);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203654756, (java.lang.Integer) (-846460796));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-985707399), (java.lang.Integer) 2256);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-29247463), (java.lang.Integer) (-12511));
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 10472248, (java.lang.Integer) 14);
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 669356576, (java.lang.Integer) (-44568904));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 137901733 + "'", int7.equals(137901733));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-248222832) + "'", int10.equals((-248222832)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-985705143) + "'", int13.equals((-985705143)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 2337 + "'", int16.equals(2337));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 10472262 + "'", int19.equals(10472262));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1673185024 + "'", int22.equals(1673185024));
    }

    @Test
    public void test482() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test482");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 676495, (java.lang.Integer) (-9));
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) 2679600, (java.lang.Integer) 204);
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-680500222), (java.lang.Integer) (-853072144));
        java.lang.Integer int26 = operacionesMatematicas0.restar((java.lang.Integer) 1107075312, (java.lang.Integer) 663303746);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 676504 + "'", int17.equals(676504));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 13135 + "'", int20.equals(13135));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 2122583520 + "'", int23.equals(2122583520));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 443771566 + "'", int26.equals(443771566));
    }

    @Test
    public void test483() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test483");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 302, (java.lang.Integer) 675054);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 24, (java.lang.Integer) 168);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 28000, (java.lang.Integer) 98974848);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2109130613, (java.lang.Integer) (-169433527));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-674752) + "'", int10.equals((-674752)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-144) + "'", int13.equals((-144)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1939697086 + "'", int19.equals(1939697086));
    }

    @Test
    public void test484() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test484");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) 223300);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-676396), (java.lang.Integer) 1678);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1567919351, (java.lang.Integer) (-203656852));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2030) + "'", int9.equals((-2030)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2679600 + "'", int13.equals(2679600));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-678074) + "'", int16.equals((-678074)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 2024308532 + "'", int20.equals(2024308532));
    }

    @Test
    public void test485() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test485");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-320292228), (java.lang.Integer) (-204));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 12715818, (java.lang.Integer) (-1027036680));
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) 678491, (java.lang.Integer) 443771566);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1570059 + "'", int22.equals(1570059));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-1014320862) + "'", int25.equals((-1014320862)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 626268122 + "'", int28.equals(626268122));
    }

    @Test
    public void test486() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test486");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-198), (java.lang.Integer) 198);
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-970066961));
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass18);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test487() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test487");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) (-304), (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 2941, (java.lang.Integer) (-673460));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 473676, (java.lang.Integer) 975452);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-203678316), (java.lang.Integer) 925359);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-141378), (java.lang.Integer) (-151004351));
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) 200871, (java.lang.Integer) 1079);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-507) + "'", int6.equals((-507)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 676401 + "'", int9.equals(676401));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-501776) + "'", int12.equals((-501776)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-202752957) + "'", int15.equals((-202752957)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-151145729) + "'", int18.equals((-151145729)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 216739809 + "'", int21.equals(216739809));
    }

    @Test
    public void test488() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test488");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) 202);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 674850, (java.lang.Integer) (-2789518));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) 3464368, (java.lang.Integer) (-1753913018));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 7, (java.lang.Integer) 868432494);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) (-1077768), (java.lang.Integer) 1704551204);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 204 + "'", int6.equals(204));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 3464368 + "'", int9.equals(3464368));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1750448650) + "'", int12.equals((-1750448650)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
    }

    @Test
    public void test489() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test489");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-1085863536), (java.lang.Integer) (-391));
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-203761580), (java.lang.Integer) (-798007));
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1936223520), (java.lang.Integer) 408714520);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-1085863927) + "'", int7.equals((-1085863927)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-204559587) + "'", int10.equals((-204559587)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-110287616) + "'", int13.equals((-110287616)));
    }

    @Test
    public void test490() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test490");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-2233));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-676396), (java.lang.Integer) 100);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-223), (java.lang.Integer) (-198));
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 14, (java.lang.Integer) 12);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 223, (java.lang.Integer) 0);
        java.lang.Integer int24 = operacionesMatematicas0.dividir((java.lang.Integer) (-907044204), (java.lang.Integer) (-1988598960));
        java.lang.Integer int27 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-8230961), (java.lang.Integer) (-91798));
        java.lang.Integer int30 = operacionesMatematicas0.multiplicar((java.lang.Integer) 374697459, (java.lang.Integer) (-480618));
        java.lang.Integer int33 = operacionesMatematicas0.dividir((java.lang.Integer) 1255479873, (java.lang.Integer) 129067);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-674366) + "'", int9.equals((-674366)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-6763) + "'", int12.equals((-6763)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-25) + "'", int15.equals((-25)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 168 + "'", int18.equals(168));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 223 + "'", int21.equals(223));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 0 + "'", int24.equals(0));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + (-328486218) + "'", int27.equals((-328486218)));
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + 1635371618 + "'", int30.equals(1635371618));
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + 9727 + "'", int33.equals(9727));
    }

    @Test
    public void test491() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test491");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass6 = operacionesMatematicas0.getClass();
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 688, (java.lang.Integer) (-924510));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 1710, (java.lang.Integer) 6823529);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 136, (java.lang.Integer) 2118109232);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 6825239 + "'", int13.equals(6825239));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 2118109368 + "'", int16.equals(2118109368));
    }

    @Test
    public void test492() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test492");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 138125136, (java.lang.Integer) 223403);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass9 = operacionesMatematicas0.getClass();
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-635285), (java.lang.Integer) 654);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 137901733 + "'", int7.equals(137901733));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-635939) + "'", int12.equals((-635939)));
    }

    @Test
    public void test493() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test493");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-203657735), (java.lang.Integer) (-98975152));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-872882432), (java.lang.Integer) 1374);
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) (-202978621), (java.lang.Integer) (-747018902));
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) 2256, (java.lang.Integer) (-847782612));
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) (-136443584), (java.lang.Integer) 100004);
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) (-917), (java.lang.Integer) 1554526000);
        java.lang.Integer int29 = operacionesMatematicas0.sumar((java.lang.Integer) (-1000630274), (java.lang.Integer) 419736786);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-104682583) + "'", int9.equals((-104682583)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-635285) + "'", int12.equals((-635285)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-949997523) + "'", int16.equals((-949997523)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 0 + "'", int20.equals(0));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-136543588) + "'", int23.equals((-136543588)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 0 + "'", int26.equals(0));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-580893488) + "'", int29.equals((-580893488)));
    }

    @Test
    public void test494() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test494");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 2354, (java.lang.Integer) (-403));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 21210, (java.lang.Integer) (-177104011));
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-689109), (java.lang.Integer) (-2327));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-5) + "'", int12.equals((-5)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-177082801) + "'", int15.equals((-177082801)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-691436) + "'", int18.equals((-691436)));
    }

    @Test
    public void test495() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test495");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) (-26796));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) (-3));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) (-3260), (java.lang.Integer) 44906012);
        java.lang.Integer int31 = operacionesMatematicas0.multiplicar((java.lang.Integer) 10, (java.lang.Integer) 99692);
        java.lang.Integer int34 = operacionesMatematicas0.restar((java.lang.Integer) 187046656, (java.lang.Integer) 1807381003);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-86631468) + "'", int22.equals((-86631468)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-103) + "'", int25.equals((-103)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 996920 + "'", int31.equals(996920));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + (-1620334347) + "'", int34.equals((-1620334347)));
    }

    @Test
    public void test496() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test496");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 199, (java.lang.Integer) 302);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1933331090, (java.lang.Integer) (-1477849338));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1869099668) + "'", int13.equals((-1869099668)));
    }

    @Test
    public void test497() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test497");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99692, (java.lang.Integer) (-26796));
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 1954931645, (java.lang.Integer) 686730967);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1623620464 + "'", int15.equals(1623620464));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1653304684) + "'", int18.equals((-1653304684)));
    }

    @Test
    public void test498() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test498");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99990, (java.lang.Integer) 14);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 218, (java.lang.Integer) 203658830);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 1727250696, (java.lang.Integer) (-1508354034));
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-820), (java.lang.Integer) 145112);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 100004 + "'", int9.equals(100004));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-26796) + "'", int12.equals((-26796)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-203658612) + "'", int15.equals((-203658612)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1059362566) + "'", int18.equals((-1059362566)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-118991840) + "'", int21.equals((-118991840)));
    }

    @Test
    public void test499() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test499");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 990, (java.lang.Integer) 302);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 7458814, (java.lang.Integer) (-676296));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) 218, (java.lang.Integer) 29684000);
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1806993207, (java.lang.Integer) 1197940124);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 688 + "'", int15.equals(688));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-11) + "'", int18.equals((-11)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-29683782) + "'", int21.equals((-29683782)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1116430204) + "'", int24.equals((-1116430204)));
    }

    @Test
    public void test500() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest0.test500");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-100), (java.lang.Integer) (-2233));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 1794580014, (java.lang.Integer) 200282);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-1022558102), (java.lang.Integer) 1418591405);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223300 + "'", int6.equals(223300));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 8960 + "'", int9.equals(8960));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 396033303 + "'", int12.equals(396033303));
    }
}

