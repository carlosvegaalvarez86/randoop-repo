import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegressionTest8 {

    public static boolean debug = false;

    @Test
    public void test001() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test001");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 2354, (java.lang.Integer) (-403));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1126066292), (java.lang.Integer) 1077141714);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-5) + "'", int12.equals((-5)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-2037591848) + "'", int16.equals((-2037591848)));
    }

    @Test
    public void test002() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test002");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-26796), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) (-6763), (java.lang.Integer) (-4060));
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) 1542, (java.lang.Integer) 2108473854);
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) 703051, (java.lang.Integer) (-1338190428));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-28474) + "'", int20.equals((-28474)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 1 + "'", int23.equals(1));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 2108475396 + "'", int26.equals(2108475396));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 762560268 + "'", int29.equals(762560268));
    }

    @Test
    public void test003() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test003");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 679827);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-86407950), (java.lang.Integer) 419736786);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-485640240), (java.lang.Integer) 677084);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 199, (java.lang.Integer) 569427227);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1566625664), (java.lang.Integer) (-23417576));
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 565889792, (java.lang.Integer) 652952);
        java.lang.Integer int24 = operacionesMatematicas0.dividir((java.lang.Integer) 179685178, (java.lang.Integer) (-262554756));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-679726) + "'", int6.equals((-679726)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-835045696) + "'", int12.equals((-835045696)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-569427028) + "'", int15.equals((-569427028)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-5043200) + "'", int18.equals((-5043200)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 566542744 + "'", int21.equals(566542744));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 0 + "'", int24.equals(0));
    }

    @Test
    public void test004() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test004");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-507), (java.lang.Integer) 200871);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-702061), (java.lang.Integer) (-25));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 198, (java.lang.Integer) 7458814);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-9430), (java.lang.Integer) (-203078208));
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2003329837), (java.lang.Integer) 1671463914);
        java.lang.Integer int32 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1453691840, (java.lang.Integer) 1224212052);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-201378) + "'", int16.equals((-201378)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-702086) + "'", int19.equals((-702086)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1476845172 + "'", int22.equals(1476845172));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-527912576) + "'", int25.equals((-527912576)));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 213209566 + "'", int29.equals(213209566));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 1160575744 + "'", int32.equals(1160575744));
    }

    @Test
    public void test005() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test005");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-2030), (java.lang.Integer) 677084);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) 578855629);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-679114) + "'", int16.equals((-679114)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
    }

    @Test
    public void test006() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test006");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) 223300);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 3020, (java.lang.Integer) (-2030));
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 1351444, (java.lang.Integer) 990);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 299478398, (java.lang.Integer) (-23937));
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1349700, (java.lang.Integer) (-6351532));
        java.lang.Integer int29 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) (-1304999083));
        java.lang.Integer int32 = operacionesMatematicas0.restar((java.lang.Integer) 1627201958, (java.lang.Integer) (-1438830691));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2030) + "'", int9.equals((-2030)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2679600 + "'", int13.equals(2679600));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 5050 + "'", int17.equals(5050));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 1352434 + "'", int20.equals(1352434));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-12511) + "'", int23.equals((-12511)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 91982416 + "'", int26.equals(91982416));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 1304999083 + "'", int29.equals(1304999083));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + (-1228934647) + "'", int32.equals((-1228934647)));
    }

    @Test
    public void test007() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test007");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.sumar((java.lang.Integer) (-2030), (java.lang.Integer) (-674366));
        java.lang.Class<?> wildcardClass9 = operacionesMatematicas0.getClass();
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-86631468), (java.lang.Integer) 166122);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-319241594), (java.lang.Integer) 0);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-1815854480), (java.lang.Integer) (-3878));
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1945867648), (java.lang.Integer) (-138625856));
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-98660909), (java.lang.Integer) 1270901478);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + (-676396) + "'", int8.equals((-676396)));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-86797590) + "'", int12.equals((-86797590)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1815850602) + "'", int18.equals((-1815850602)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-142221312) + "'", int21.equals((-142221312)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1922250350) + "'", int24.equals((-1922250350)));
    }

    @Test
    public void test008() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test008");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass6 = operacionesMatematicas0.getClass();
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 688, (java.lang.Integer) (-924510));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-678074), (java.lang.Integer) (-138799304));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 2388152, (java.lang.Integer) (-39336909));
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-139477378) + "'", int12.equals((-139477378)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-36948757) + "'", int16.equals((-36948757)));
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test009() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test009");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-320292228), (java.lang.Integer) (-204));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) (-133524468), (java.lang.Integer) 12);
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) 27084870, (java.lang.Integer) 102968);
        java.lang.Integer int31 = operacionesMatematicas0.restar((java.lang.Integer) 743227560, (java.lang.Integer) 245054422);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1570059 + "'", int22.equals(1570059));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-11127039) + "'", int25.equals((-11127039)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 263 + "'", int28.equals(263));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 498173138 + "'", int31.equals(498173138));
    }

    @Test
    public void test010() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test010");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-507), (java.lang.Integer) 200871);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-3030), (java.lang.Integer) 1980253234);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 44906216, (java.lang.Integer) 797);
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) (-480618), (java.lang.Integer) 342371593);
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass27 = operacionesMatematicas0.getClass();
        java.lang.Integer int30 = operacionesMatematicas0.sumar((java.lang.Integer) 1033577350, (java.lang.Integer) (-1263368348));
        java.lang.Integer int33 = operacionesMatematicas0.sumar((java.lang.Integer) (-840498), (java.lang.Integer) (-2130764318));
        java.lang.Integer int36 = operacionesMatematicas0.multiplicar((java.lang.Integer) 85417856, (java.lang.Integer) 11972266);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-201378) + "'", int16.equals((-201378)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1980250204 + "'", int19.equals(1980250204));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 44907013 + "'", int22.equals(44907013));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-342852211) + "'", int25.equals((-342852211)));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertNotNull(wildcardClass27);
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + (-229790998) + "'", int30.equals((-229790998)));
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + (-2131604816) + "'", int33.equals((-2131604816)));
        org.junit.Assert.assertTrue("'" + int36 + "' != '" + 695102208 + "'", int36.equals(695102208));
    }

    @Test
    public void test011() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test011");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2000);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-11), (java.lang.Integer) (-678074));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 10, (java.lang.Integer) (-91808));
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-1566668296), (java.lang.Integer) 222612);
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1751447560), (java.lang.Integer) (-680600602));
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1105768), (java.lang.Integer) 65);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 7458814 + "'", int12.equals(7458814));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-91798) + "'", int15.equals((-91798)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1566890908) + "'", int18.equals((-1566890908)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1985326896) + "'", int21.equals((-1985326896)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-71874920) + "'", int24.equals((-71874920)));
    }

    @Test
    public void test012() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test012");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166122, (java.lang.Integer) (-2030));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 202, (java.lang.Integer) (-1546915));
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) (-1917804781), (java.lang.Integer) (-1266096146));
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) (-2131604816), (java.lang.Integer) (-1961186309));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 199 + "'", int6.equals(199));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-337227660) + "'", int9.equals((-337227660)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1111066369 + "'", int16.equals(1111066369));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1 + "'", int19.equals(1));
    }

    @Test
    public void test013() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test013");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) (-2452019), (java.lang.Integer) 591647436);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 673023, (java.lang.Integer) (-5));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 1161756880, (java.lang.Integer) (-1013322));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 1352434, (java.lang.Integer) 684291519);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 303119, (java.lang.Integer) (-1072091024));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-594099455) + "'", int6.equals((-594099455)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-134604) + "'", int9.equals((-134604)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1146) + "'", int12.equals((-1146)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1071787905) + "'", int18.equals((-1071787905)));
    }

    @Test
    public void test014() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test014");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2, (java.lang.Integer) 1000);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1646083254), (java.lang.Integer) (-1513270229));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1896184000), (java.lang.Integer) 1980273266);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 223235, (java.lang.Integer) (-1980273266));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2000 + "'", int6.equals(2000));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-132813025) + "'", int9.equals((-132813025)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 418510030 + "'", int12.equals(418510030));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1980496501 + "'", int15.equals(1980496501));
    }

    @Test
    public void test015() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test015");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) (-872882432), (java.lang.Integer) (-6763));
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1641), (java.lang.Integer) 200184);
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        java.lang.Integer int27 = operacionesMatematicas0.dividir((java.lang.Integer) (-1896219511), (java.lang.Integer) (-1536561213));
        java.lang.Integer int30 = operacionesMatematicas0.multiplicar((java.lang.Integer) 468320391, (java.lang.Integer) (-970066961));
        java.lang.Class<?> wildcardClass31 = operacionesMatematicas0.getClass();
        java.lang.Integer int34 = operacionesMatematicas0.restar((java.lang.Integer) (-254965600), (java.lang.Integer) (-1925161760));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 129067 + "'", int20.equals(129067));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-328501944) + "'", int23.equals((-328501944)));
        org.junit.Assert.assertNotNull(wildcardClass24);
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 1 + "'", int27.equals(1));
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + (-1347312887) + "'", int30.equals((-1347312887)));
        org.junit.Assert.assertNotNull(wildcardClass31);
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 1670196160 + "'", int34.equals(1670196160));
    }

    @Test
    public void test016() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test016");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 146, (java.lang.Integer) 990);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1017), (java.lang.Integer) (-11));
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-458350076), (java.lang.Integer) (-569427028));
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-114017579), (java.lang.Integer) 1582669994);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 606206594, (java.lang.Integer) (-833202506));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-844) + "'", int7.equals((-844)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 11187 + "'", int10.equals(11187));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 111076952 + "'", int13.equals(111076952));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-1696687573) + "'", int16.equals((-1696687573)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1439409100 + "'", int19.equals(1439409100));
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test017() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test017");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 1888659260);
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-58), (java.lang.Integer) (-203656345));
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 10472262, (java.lang.Integer) (-55679488));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-689784310), (java.lang.Integer) (-1315384639));
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 468320511, (java.lang.Integer) (-1077112226));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1131289600) + "'", int13.equals((-1131289600)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1568306570 + "'", int16.equals(1568306570));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-608791715) + "'", int19.equals((-608791715)));
    }

    @Test
    public void test018() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test018");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-507), (java.lang.Integer) 200871);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-702061), (java.lang.Integer) (-25));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 198, (java.lang.Integer) 7458814);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 98974848, (java.lang.Integer) (-144));
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 0);
        java.lang.Integer int31 = operacionesMatematicas0.dividir((java.lang.Integer) 2029, (java.lang.Integer) 1115055585);
        java.lang.Integer int34 = operacionesMatematicas0.restar((java.lang.Integer) (-455475856), (java.lang.Integer) 680603190);
        java.lang.Integer int37 = operacionesMatematicas0.multiplicar((java.lang.Integer) 53195533, (java.lang.Integer) (-1592512975));
        java.lang.Integer int40 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1761912096), (java.lang.Integer) 1521256841);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-201378) + "'", int16.equals((-201378)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-702086) + "'", int19.equals((-702086)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1476845172 + "'", int22.equals(1476845172));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 98974704 + "'", int25.equals(98974704));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 0 + "'", int31.equals(0));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + (-1136079046) + "'", int34.equals((-1136079046)));
        org.junit.Assert.assertTrue("'" + int37 + "' != '" + (-1618109571) + "'", int37.equals((-1618109571)));
        org.junit.Assert.assertTrue("'" + int40 + "' != '" + 514163168 + "'", int40.equals(514163168));
    }

    @Test
    public void test019() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test019");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 2010711732, (java.lang.Integer) 1106533862);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) (-1945867648), (java.lang.Integer) (-985706892));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) (-32545458), (java.lang.Integer) (-1170123107));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1 + "'", int19.equals(1));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1363392756 + "'", int22.equals(1363392756));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
    }

    @Test
    public void test020() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test020");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) 14);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.restar((java.lang.Integer) (-1827), (java.lang.Integer) (-694249));
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) (-136413280), (java.lang.Integer) (-203));
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 675040 + "'", int10.equals(675040));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 692422 + "'", int14.equals(692422));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-136413077) + "'", int17.equals((-136413077)));
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test021() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test021");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) (-200282), (java.lang.Integer) (-702061));
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-203859113), (java.lang.Integer) 953704);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 340526759, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 663303746, (java.lang.Integer) (-233883));
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-966916520) + "'", int10.equals((-966916520)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-2836) + "'", int16.equals((-2836)));
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test022() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test022");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass9 = operacionesMatematicas0.getClass();
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 98974848);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 1074245607, (java.lang.Integer) (-625983127));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-98974848) + "'", int12.equals((-98974848)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 448262480 + "'", int15.equals(448262480));
    }

    @Test
    public void test023() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test023");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-1085863536), (java.lang.Integer) (-391));
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-203761580), (java.lang.Integer) (-798007));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-895373708), (java.lang.Integer) 1963548060);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-1085863927) + "'", int7.equals((-1085863927)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-204559587) + "'", int10.equals((-204559587)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test024() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test024");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 473676, (java.lang.Integer) (-2030));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-292), (java.lang.Integer) 2108502328);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-86797590), (java.lang.Integer) 674850);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 225533, (java.lang.Integer) 0);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 1199317608, (java.lang.Integer) 1980250204);
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Integer int29 = operacionesMatematicas0.sumar((java.lang.Integer) (-1038405328), (java.lang.Integer) (-1706221142));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-961562280) + "'", int12.equals((-961562280)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-589628652) + "'", int19.equals((-589628652)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 225533 + "'", int22.equals(225533));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-1115399484) + "'", int25.equals((-1115399484)));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 1550340826 + "'", int29.equals(1550340826));
    }

    @Test
    public void test025() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test025");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) (-2452019), (java.lang.Integer) 591647436);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-86408168), (java.lang.Integer) (-1427973632));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1771579329, (java.lang.Integer) (-3));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1028800442), (java.lang.Integer) 1261325568);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-594099455) + "'", int6.equals((-594099455)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-945000448) + "'", int9.equals((-945000448)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1019770691) + "'", int12.equals((-1019770691)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1365839360 + "'", int15.equals(1365839360));
    }

    @Test
    public void test026() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test026");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-337227660), (java.lang.Integer) (-2962));
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-2131));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.dividir((java.lang.Integer) (-2016924794), (java.lang.Integer) 241440958);
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) 988984230, (java.lang.Integer) (-656836292));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 113851 + "'", int10.equals(113851));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-8) + "'", int17.equals((-8)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 1645820522 + "'", int20.equals(1645820522));
    }

    @Test
    public void test027() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test027");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) (-1));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) (-2233), (java.lang.Integer) 797);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-403), (java.lang.Integer) 1701552590);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 1853597184, (java.lang.Integer) (-1885225800));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 2354, (java.lang.Integer) 311282688);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 791133635, (java.lang.Integer) 110594088);
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 1577950580, (java.lang.Integer) 1025253470);
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-3030) + "'", int10.equals((-3030)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1701552187 + "'", int13.equals(1701552187));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-31628616) + "'", int16.equals((-31628616)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-311280334) + "'", int19.equals((-311280334)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 7 + "'", int22.equals(7));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1 + "'", int25.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass26);
    }

    @Test
    public void test028() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test028");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 10, (java.lang.Integer) 302);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-844), (java.lang.Integer) 675040);
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) 797, (java.lang.Integer) (-198118));
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) 2022570152, (java.lang.Integer) 1475499481);
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) 351367552, (java.lang.Integer) (-157390628));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 3020 + "'", int15.equals(3020));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 674196 + "'", int18.equals(674196));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-157900046) + "'", int21.equals((-157900046)));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 547070671 + "'", int25.equals(547070671));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-2) + "'", int28.equals((-2)));
    }

    @Test
    public void test029() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test029");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-3), (java.lang.Integer) (-9));
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) (-1116391877), (java.lang.Integer) 1916887121);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertNotNull(wildcardClass18);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
    }

    @Test
    public void test030() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test030");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 674850, (java.lang.Integer) (-203658532));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 338956, (java.lang.Integer) (-3030));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 1023985958, (java.lang.Integer) 1922560000);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-985930295), (java.lang.Integer) 0);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) (-1264536494), (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1027036680) + "'", int12.equals((-1027036680)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-898574042) + "'", int16.equals((-898574042)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-985930295) + "'", int19.equals((-985930295)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1264536494) + "'", int22.equals((-1264536494)));
    }

    @Test
    public void test031() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test031");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166122, (java.lang.Integer) (-2030));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) 703040, (java.lang.Integer) 2005416224);
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 1739462365, (java.lang.Integer) (-853072144));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 568878, (java.lang.Integer) (-23));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-458350076), (java.lang.Integer) (-1830));
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) 20653, (java.lang.Integer) 1738045857);
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1570673068), (java.lang.Integer) 2102914096);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 199 + "'", int6.equals(199));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-337227660) + "'", int9.equals((-337227660)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2006119264 + "'", int12.equals(2006119264));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 886390221 + "'", int16.equals(886390221));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 568901 + "'", int19.equals(568901));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1262016360 + "'", int22.equals(1262016360));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-1738025204) + "'", int25.equals((-1738025204)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-964288576) + "'", int28.equals((-964288576)));
    }

    @Test
    public void test032() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test032");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) (-26796));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) (-3));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) (-3260), (java.lang.Integer) 44906012);
        java.lang.Integer int31 = operacionesMatematicas0.dividir((java.lang.Integer) 138124938, (java.lang.Integer) 1963748327);
        java.lang.Integer int34 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) (-1566890908));
        java.lang.Integer int37 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1241932232, (java.lang.Integer) 402135200);
        java.lang.Integer int40 = operacionesMatematicas0.dividir((java.lang.Integer) 6731, (java.lang.Integer) (-1939429620));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-86631468) + "'", int22.equals((-86631468)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-103) + "'", int25.equals((-103)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 0 + "'", int31.equals(0));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 1566890908 + "'", int34.equals(1566890908));
        org.junit.Assert.assertTrue("'" + int37 + "' != '" + (-1897407232) + "'", int37.equals((-1897407232)));
        org.junit.Assert.assertTrue("'" + int40 + "' != '" + 0 + "'", int40.equals(0));
    }

    @Test
    public void test033() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test033");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 1761955219, (java.lang.Integer) 406489420);
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 1269950044, (java.lang.Integer) 1307130000);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1988220674, (java.lang.Integer) 1373);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 1355465799 + "'", int7.equals(1355465799));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1448084 + "'", int13.equals(1448084));
    }

    @Test
    public void test034() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test034");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 100004, (java.lang.Integer) 0);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-17), (java.lang.Integer) (-403));
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 14, (java.lang.Integer) 2000);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 1678, (java.lang.Integer) (-203658530));
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) 298, (java.lang.Integer) 131302456);
        java.lang.Integer int27 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-902763319));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 100004 + "'", int12.equals(100004));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 386 + "'", int15.equals(386));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 28000 + "'", int18.equals(28000));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-203656852) + "'", int21.equals((-203656852)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 131302754 + "'", int24.equals(131302754));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 0 + "'", int27.equals(0));
    }

    @Test
    public void test035() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test035");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 677082, (java.lang.Integer) 797);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-86797590), (java.lang.Integer) 0);
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) 675054, (java.lang.Integer) 1963548060);
        java.lang.Integer int31 = operacionesMatematicas0.restar((java.lang.Integer) 200183, (java.lang.Integer) 4738865);
        java.lang.Integer int34 = operacionesMatematicas0.restar((java.lang.Integer) 831035171, (java.lang.Integer) 480878347);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 849 + "'", int22.equals(849));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 1050105608 + "'", int28.equals(1050105608));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-4538682) + "'", int31.equals((-4538682)));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 350156824 + "'", int34.equals(350156824));
    }

    @Test
    public void test036() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test036");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-2031), (java.lang.Integer) (-100));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-679014), (java.lang.Integer) 28345);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 2108473854);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-344077));
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) (-770326209), (java.lang.Integer) 12);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1019770691), (java.lang.Integer) 1605572780);
        java.lang.Integer int28 = operacionesMatematicas0.sumar((java.lang.Integer) 110594088, (java.lang.Integer) 870373812);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-2131) + "'", int10.equals((-2131)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-650669) + "'", int13.equals((-650669)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-2108473854) + "'", int16.equals((-2108473854)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-770326221) + "'", int22.equals((-770326221)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 491296508 + "'", int25.equals(491296508));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 980967900 + "'", int28.equals(980967900));
    }

    @Test
    public void test037() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test037");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-882132), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 676495, (java.lang.Integer) 2030);
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        java.lang.Integer int27 = operacionesMatematicas0.sumar((java.lang.Integer) (-1338190428), (java.lang.Integer) (-139407012));
        java.lang.Class<?> wildcardClass28 = operacionesMatematicas0.getClass();
        java.lang.Integer int31 = operacionesMatematicas0.sumar((java.lang.Integer) 586675200, (java.lang.Integer) 56920617);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-1480217496) + "'", int20.equals((-1480217496)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 333 + "'", int23.equals(333));
        org.junit.Assert.assertNotNull(wildcardClass24);
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + (-1477597440) + "'", int27.equals((-1477597440)));
        org.junit.Assert.assertNotNull(wildcardClass28);
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 643595817 + "'", int31.equals(643595817));
    }

    @Test
    public void test038() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test038");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1701552590, (java.lang.Integer) 182961501);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-422447288), (java.lang.Integer) 91346688);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1199616554) + "'", int12.equals((-1199616554)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-445286400) + "'", int15.equals((-445286400)));
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test039() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test039");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) 223300);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.dividir((java.lang.Integer) (-1418013643), (java.lang.Integer) 834392107);
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) 371015616, (java.lang.Integer) (-1536561115));
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) (-22456172), (java.lang.Integer) 0);
        java.lang.Integer int26 = operacionesMatematicas0.restar((java.lang.Integer) 1963832921, (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2030) + "'", int9.equals((-2030)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2679600 + "'", int13.equals(2679600));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-1) + "'", int17.equals((-1)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-1010944320) + "'", int20.equals((-1010944320)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-22456172) + "'", int23.equals((-22456172)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 1963832921 + "'", int26.equals(1963832921));
    }

    @Test
    public void test040() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test040");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-300), (java.lang.Integer) 448049834);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 781182336, (java.lang.Integer) (-1653802264));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-448050134) + "'", int13.equals((-448050134)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
    }

    @Test
    public void test041() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test041");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-204), (java.lang.Integer) 12);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 797, (java.lang.Integer) (-201378));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-319241688), (java.lang.Integer) 1310380704);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-17) + "'", int6.equals((-17)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
    }

    @Test
    public void test042() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test042");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-98975152), (java.lang.Integer) (-676594));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-702061), (java.lang.Integer) 100004);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 28000, (java.lang.Integer) (-5));
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) 1983457309, (java.lang.Integer) (-203761580));
        java.lang.Integer int24 = operacionesMatematicas0.restar((java.lang.Integer) 473558740, (java.lang.Integer) 14436969);
        java.lang.Integer int27 = operacionesMatematicas0.sumar((java.lang.Integer) 2010360317, (java.lang.Integer) 239705415);
        java.lang.Integer int30 = operacionesMatematicas0.dividir((java.lang.Integer) (-670008196), (java.lang.Integer) 824844167);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 146 + "'", int12.equals(146));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-802065) + "'", int15.equals((-802065)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-5600) + "'", int18.equals((-5600)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-9) + "'", int21.equals((-9)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 459121771 + "'", int24.equals(459121771));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + (-2044901564) + "'", int27.equals((-2044901564)));
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + 0 + "'", int30.equals(0));
    }

    @Test
    public void test043() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test043");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-203657735), (java.lang.Integer) (-98975152));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-872882432), (java.lang.Integer) 1374);
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) (-202978621), (java.lang.Integer) (-747018902));
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2010197328, (java.lang.Integer) 166);
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 10068892, (java.lang.Integer) (-477630));
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223403, (java.lang.Integer) 84409818);
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-136413077), (java.lang.Integer) 1);
        java.lang.Integer int32 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-635285), (java.lang.Integer) (-9122452));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-104682583) + "'", int9.equals((-104682583)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-635285) + "'", int12.equals((-635285)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-949997523) + "'", int16.equals((-949997523)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2010197494 + "'", int19.equals(2010197494));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1158485560 + "'", int22.equals(1158485560));
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-1794826082) + "'", int26.equals((-1794826082)));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-136413077) + "'", int29.equals((-136413077)));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 1446036516 + "'", int32.equals(1446036516));
    }

    @Test
    public void test044() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test044");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) 101);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 28053, (java.lang.Integer) 849);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-86407950), (java.lang.Integer) (-2027625964));
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 1704551204, (java.lang.Integer) 1346531);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 2010197494, (java.lang.Integer) (-1903710174));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 102968, (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2 + "'", int6.equals(2));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 23816997 + "'", int10.equals(23816997));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-2114033914) + "'", int13.equals((-2114033914)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1705897735 + "'", int16.equals(1705897735));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1) + "'", int19.equals((-1)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
    }

    @Test
    public void test045() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test045");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.multiplicar((java.lang.Integer) 386, (java.lang.Integer) 1566625664);
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) 1134299696, (java.lang.Integer) 0);
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1285645195, (java.lang.Integer) (-1865645598));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + (-872882432) + "'", int14.equals((-872882432)));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 1134299696 + "'", int17.equals(1134299696));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 2087119798 + "'", int20.equals(2087119798));
    }

    @Test
    public void test046() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test046");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 101);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) (-674366), (java.lang.Integer) 3020);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 791133635);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-1183937216), (java.lang.Integer) (-480400));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2121745577), (java.lang.Integer) 198911);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 101 + "'", int4.equals(101));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-223) + "'", int7.equals((-223)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-791133635) + "'", int10.equals((-791133635)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2464 + "'", int13.equals(2464));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 2131907497 + "'", int17.equals(2131907497));
    }

    @Test
    public void test047() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test047");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-26796), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) (-6763), (java.lang.Integer) (-4060));
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) (-28474), (java.lang.Integer) 199);
        java.lang.Integer int29 = operacionesMatematicas0.sumar((java.lang.Integer) (-319237330), (java.lang.Integer) (-86797590));
        java.lang.Integer int32 = operacionesMatematicas0.sumar((java.lang.Integer) (-2147357664), (java.lang.Integer) (-1620334347));
        java.lang.Integer int35 = operacionesMatematicas0.restar((java.lang.Integer) 442013635, (java.lang.Integer) 1981352906);
        java.lang.Integer int38 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1791717132, (java.lang.Integer) (-396410220));
        java.lang.Class<?> wildcardClass39 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-28474) + "'", int20.equals((-28474)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 1 + "'", int23.equals(1));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-28275) + "'", int26.equals((-28275)));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-406034920) + "'", int29.equals((-406034920)));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 527275285 + "'", int32.equals(527275285));
        org.junit.Assert.assertTrue("'" + int35 + "' != '" + (-1539339271) + "'", int35.equals((-1539339271)));
        org.junit.Assert.assertTrue("'" + int38 + "' != '" + 1169246960 + "'", int38.equals(1169246960));
        org.junit.Assert.assertNotNull(wildcardClass39);
    }

    @Test
    public void test048() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test048");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99990, (java.lang.Integer) 14);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 218, (java.lang.Integer) 203658830);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 34527, (java.lang.Integer) (-198));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) (-2027625964), (java.lang.Integer) (-680500910));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) (-203678148), (java.lang.Integer) 203204);
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Integer int29 = operacionesMatematicas0.dividir((java.lang.Integer) (-2109747719), (java.lang.Integer) (-1866622333));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 100004 + "'", int9.equals(100004));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-26796) + "'", int12.equals((-26796)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-203658612) + "'", int15.equals((-203658612)));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 34329 + "'", int19.equals(34329));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1586840422 + "'", int22.equals(1586840422));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-1002) + "'", int25.equals((-1002)));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 1 + "'", int29.equals(1));
    }

    @Test
    public void test049() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test049");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2), (java.lang.Integer) 2030);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-221533), (java.lang.Integer) 0);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1351444, (java.lang.Integer) (-28275));
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) 38790450, (java.lang.Integer) (-985930295));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-4060) + "'", int12.equals((-4060)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-221533) + "'", int15.equals((-221533)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 442626564 + "'", int18.equals(442626564));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1790529982) + "'", int21.equals((-1790529982)));
    }

    @Test
    public void test050() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test050");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) 688);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) (-1), (java.lang.Integer) (-304));
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 925359, (java.lang.Integer) (-104553516));
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1420869144), (java.lang.Integer) (-2102407054));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 12 + "'", int13.equals(12));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 222612 + "'", int17.equals(222612));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-305) + "'", int20.equals((-305)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 0 + "'", int23.equals(0));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-1938826928) + "'", int26.equals((-1938826928)));
    }

    @Test
    public void test051() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test051");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-91808), (java.lang.Integer) 386);
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 135279661, (java.lang.Integer) (-40337365));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-847782612), (java.lang.Integer) 0);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-1219790913), (java.lang.Integer) 1141702359);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-237) + "'", int9.equals((-237)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-3) + "'", int12.equals((-3)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-847782612) + "'", int16.equals((-847782612)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1933474024 + "'", int19.equals(1933474024));
    }

    @Test
    public void test052() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test052");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 315901, (java.lang.Integer) (-678748));
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1588443426), (java.lang.Integer) (-125664812));
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.restar((java.lang.Integer) (-28474), (java.lang.Integer) (-11127039));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 994649 + "'", int7.equals(994649));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 445943256 + "'", int10.equals(445943256));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 11098565 + "'", int14.equals(11098565));
    }

    @Test
    public void test053() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test053");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3020, (java.lang.Integer) 990);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-821417174), (java.lang.Integer) 675040);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-699015308), (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2989800 + "'", int12.equals(2989800));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-822092214) + "'", int15.equals((-822092214)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-699015308) + "'", int18.equals((-699015308)));
    }

    @Test
    public void test054() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test054");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-507), (java.lang.Integer) 200871);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 437780273, (java.lang.Integer) 994649);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) (-1266096146), (java.lang.Integer) 43381528);
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 5050, (java.lang.Integer) (-139407012));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-201378) + "'", int16.equals((-201378)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 440 + "'", int19.equals(440));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1222714618) + "'", int22.equals((-1222714618)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
    }

    @Test
    public void test055() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test055");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 101);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 797, (java.lang.Integer) (-890108686));
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1617474503, (java.lang.Integer) (-873046054));
        java.lang.Integer int14 = operacionesMatematicas0.dividir((java.lang.Integer) (-41178544), (java.lang.Integer) (-319241594));
        java.lang.Class<?> wildcardClass15 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 101 + "'", int4.equals(101));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-747018902) + "'", int7.equals((-747018902)));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-2112229258) + "'", int11.equals((-2112229258)));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test056() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test056");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166122, (java.lang.Integer) (-2030));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) 703040, (java.lang.Integer) 2005416224);
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 1739462365, (java.lang.Integer) (-853072144));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 568878, (java.lang.Integer) (-23));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-110177015), (java.lang.Integer) (-1175987796));
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) 200282, (java.lang.Integer) 569427227);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 199 + "'", int6.equals(199));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-337227660) + "'", int9.equals((-337227660)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2006119264 + "'", int12.equals(2006119264));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 886390221 + "'", int16.equals(886390221));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 568901 + "'", int19.equals(568901));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 1160430348 + "'", int23.equals(1160430348));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 1757267326 + "'", int26.equals(1757267326));
    }

    @Test
    public void test057() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test057");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 138125136, (java.lang.Integer) 223403);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.restar((java.lang.Integer) (-391), (java.lang.Integer) 847782221);
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-1468325642), (java.lang.Integer) 6823529);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-245051272), (java.lang.Integer) 200232);
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 137901733 + "'", int7.equals(137901733));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-847782612) + "'", int11.equals((-847782612)));
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1461502113) + "'", int15.equals((-1461502113)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-245251504) + "'", int18.equals((-245251504)));
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test058() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test058");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 674850, (java.lang.Integer) (-203658532));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 338956, (java.lang.Integer) (-3030));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2233), (java.lang.Integer) (-1027036680));
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-678748), (java.lang.Integer) (-198118));
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) (-14473), (java.lang.Integer) (-141378));
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) (-233018427), (java.lang.Integer) (-1865457552));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1027036680) + "'", int12.equals((-1027036680)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-139629624) + "'", int15.equals((-139629624)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-480630) + "'", int18.equals((-480630)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-2098475979) + "'", int24.equals((-2098475979)));
    }

    @Test
    public void test059() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test059");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-203658532), (java.lang.Integer) 797);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 138125136, (java.lang.Integer) (-198));
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) (-181782106), (java.lang.Integer) 166120);
        java.lang.Integer int29 = operacionesMatematicas0.sumar((java.lang.Integer) (-694249), (java.lang.Integer) (-704717197));
        java.lang.Integer int32 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12704, (java.lang.Integer) (-895373708));
        java.lang.Class<?> wildcardClass33 = operacionesMatematicas0.getClass();
        java.lang.Integer int36 = operacionesMatematicas0.sumar((java.lang.Integer) 1988994107, (java.lang.Integer) (-1753913018));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-203657735) + "'", int18.equals((-203657735)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 138124938 + "'", int21.equals(138124938));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-181615986) + "'", int26.equals((-181615986)));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-705411446) + "'", int29.equals((-705411446)));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + (-1754186624) + "'", int32.equals((-1754186624)));
        org.junit.Assert.assertNotNull(wildcardClass33);
        org.junit.Assert.assertTrue("'" + int36 + "' != '" + 235081089 + "'", int36.equals(235081089));
    }

    @Test
    public void test060() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test060");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 2030);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 990, (java.lang.Integer) (-304));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-11), (java.lang.Integer) (-304));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) (-2118996616), (java.lang.Integer) 1954931590);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-2233) + "'", int16.equals((-2233)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-300960) + "'", int19.equals((-300960)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-1) + "'", int25.equals((-1)));
    }

    @Test
    public void test061() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test061");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) 0);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-507), (java.lang.Integer) 100004);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-1562038384), (java.lang.Integer) 654);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2 + "'", int6.equals(2));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-50702028) + "'", int9.equals((-50702028)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1562037730) + "'", int13.equals((-1562037730)));
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test062() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test062");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 202);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) (-2030));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) (-1751447560), (java.lang.Integer) (-9430));
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 6823529, (java.lang.Integer) 1996158842);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1 + "'", int15.equals(1));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 677084 + "'", int18.equals(677084));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1751438130) + "'", int21.equals((-1751438130)));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 2002982371 + "'", int25.equals(2002982371));
    }

    @Test
    public void test063() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test063");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) (-26796));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 28000, (java.lang.Integer) (-676594));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-674424));
        java.lang.Class<?> wildcardClass29 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass30 = operacionesMatematicas0.getClass();
        java.lang.Integer int33 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass34 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-86631468) + "'", int22.equals((-86631468)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass29);
        org.junit.Assert.assertNotNull(wildcardClass30);
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + 0 + "'", int33.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass34);
    }

    @Test
    public void test064() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test064");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 100004, (java.lang.Integer) 222308);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 218, (java.lang.Integer) 166122);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 179683468);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 1787, (java.lang.Integer) 791);
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-165904) + "'", int12.equals((-165904)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-179683468) + "'", int15.equals((-179683468)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 2 + "'", int18.equals(2));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test065() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test065");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-198), (java.lang.Integer) 198);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-98990));
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2117069372, (java.lang.Integer) (-4060));
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-917), (java.lang.Integer) (-1988598960));
        java.lang.Integer int29 = operacionesMatematicas0.sumar((java.lang.Integer) 1291504344, (java.lang.Integer) 822673705);
        java.lang.Integer int32 = operacionesMatematicas0.sumar((java.lang.Integer) 299478398, (java.lang.Integer) (-1077112226));
        java.lang.Integer int35 = operacionesMatematicas0.restar((java.lang.Integer) (-356484816), (java.lang.Integer) (-1596376356));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-98890) + "'", int20.equals((-98890)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-1072091024) + "'", int23.equals((-1072091024)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-1815854480) + "'", int26.equals((-1815854480)));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 2114178049 + "'", int29.equals(2114178049));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + (-777633828) + "'", int32.equals((-777633828)));
        org.junit.Assert.assertTrue("'" + int35 + "' != '" + 1239891540 + "'", int35.equals(1239891540));
    }

    @Test
    public void test066() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test066");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 1079, (java.lang.Integer) 679827);
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-198118), (java.lang.Integer) 674850);
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) 303119, (java.lang.Integer) (-1360503017));
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) (-1351735770), (java.lang.Integer) 1593176728);
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-920851168), (java.lang.Integer) 793643030);
        java.lang.Integer int32 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1158485560, (java.lang.Integer) 1267506124);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-678748) + "'", int17.equals((-678748)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-872968) + "'", int20.equals((-872968)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-144182695) + "'", int23.equals((-144182695)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 241440958 + "'", int26.equals(241440958));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 1336238272 + "'", int29.equals(1336238272));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + (-2122212192) + "'", int32.equals((-2122212192)));
    }

    @Test
    public void test067() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test067");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass6 = operacionesMatematicas0.getClass();
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 688, (java.lang.Integer) (-924510));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 1427973632);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1739459091, (java.lang.Integer) 24);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-485640239), (java.lang.Integer) (-895373708));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) (-3030), (java.lang.Integer) 0);
        java.lang.Integer int24 = operacionesMatematicas0.dividir((java.lang.Integer) (-2045332130), (java.lang.Integer) 16402688);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1427973632) + "'", int12.equals((-1427973632)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1202654776) + "'", int15.equals((-1202654776)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1381013947) + "'", int18.equals((-1381013947)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-3030) + "'", int21.equals((-3030)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-124) + "'", int24.equals((-124)));
    }

    @Test
    public void test068() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test068");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1455213478), (java.lang.Integer) 1402914690);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-689109), (java.lang.Integer) (-1641019694));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-1231212116), (java.lang.Integer) 140304664);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-1352758348) + "'", int9.equals((-1352758348)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1973887674) + "'", int12.equals((-1973887674)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1090907452) + "'", int15.equals((-1090907452)));
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test069() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test069");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) 688);
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) 2308796, (java.lang.Integer) (-1708435290));
        java.lang.Class<?> wildcardClass21 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 12 + "'", int13.equals(12));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 222612 + "'", int17.equals(222612));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 1710744086 + "'", int20.equals(1710744086));
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test070() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test070");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.dividir((java.lang.Integer) (-202979700), (java.lang.Integer) 722435795);
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-1896184000), (java.lang.Integer) (-35511));
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 12772743, (java.lang.Integer) 0);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) (-1596376474), (java.lang.Integer) 118);
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1338190249, (java.lang.Integer) (-2114056976));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1896219511) + "'", int15.equals((-1896219511)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 12772743 + "'", int18.equals(12772743));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1596376356) + "'", int21.equals((-1596376356)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 1805187696 + "'", int24.equals(1805187696));
    }

    @Test
    public void test071() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test071");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-203658532), (java.lang.Integer) 797);
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1362965102, (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) (-1393809024), (java.lang.Integer) (-1077114774));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-203657735) + "'", int18.equals((-203657735)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1 + "'", int25.equals(1));
    }

    @Test
    public void test072() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test072");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 2118627465, (java.lang.Integer) 1549861004);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 975962112, (java.lang.Integer) (-1347492371));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 568766461 + "'", int12.equals(568766461));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-371530259) + "'", int15.equals((-371530259)));
    }

    @Test
    public void test073() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test073");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 679827);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-86407950), (java.lang.Integer) 419736786);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-485640240), (java.lang.Integer) 677084);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 199, (java.lang.Integer) 569427227);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 145112, (java.lang.Integer) (-343065842));
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 65, (java.lang.Integer) (-141165408));
        java.lang.Integer int24 = operacionesMatematicas0.dividir((java.lang.Integer) 1567919351, (java.lang.Integer) (-1440498544));
        java.lang.Integer int27 = operacionesMatematicas0.restar((java.lang.Integer) 2337, (java.lang.Integer) 151006688);
        java.lang.Class<?> wildcardClass28 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass29 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-679726) + "'", int6.equals((-679726)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-835045696) + "'", int12.equals((-835045696)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-569427028) + "'", int15.equals((-569427028)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 343210954 + "'", int18.equals(343210954));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-141165343) + "'", int21.equals((-141165343)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1) + "'", int24.equals((-1)));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + (-151004351) + "'", int27.equals((-151004351)));
        org.junit.Assert.assertNotNull(wildcardClass28);
        org.junit.Assert.assertNotNull(wildcardClass29);
    }

    @Test
    public void test074() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test074");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-882132), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) (-679114), (java.lang.Integer) 100);
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1144325196, (java.lang.Integer) (-1327827264));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-1480217496) + "'", int20.equals((-1480217496)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-679014) + "'", int23.equals((-679014)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 43794688 + "'", int26.equals(43794688));
    }

    @Test
    public void test075() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test075");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 473676, (java.lang.Integer) 222612);
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 200183, (java.lang.Integer) 117);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) (-1429679800));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass15 = operacionesMatematicas0.getClass();
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 578855629, (java.lang.Integer) 462121394);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 696288 + "'", int7.equals(696288));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 1710 + "'", int10.equals(1710));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1429679800 + "'", int13.equals(1429679800));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertNotNull(wildcardClass15);
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1 + "'", int18.equals(1));
    }

    @Test
    public void test076() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test076");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) 10376, (java.lang.Integer) (-1646083254));
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 529300, (java.lang.Integer) 1230060877);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1105568374), (java.lang.Integer) 868546752);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.dividir((java.lang.Integer) 1593176728, (java.lang.Integer) (-1438112546));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 1424762756 + "'", int10.equals(1424762756));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 973854592 + "'", int13.equals(973854592));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-1) + "'", int17.equals((-1)));
    }

    @Test
    public void test077() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test077");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 473676, (java.lang.Integer) (-2030));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-292), (java.lang.Integer) 2108502328);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-86797590), (java.lang.Integer) 674850);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 225533, (java.lang.Integer) 0);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 1199317608, (java.lang.Integer) 1980250204);
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) 173085920, (java.lang.Integer) (-305));
        java.lang.Integer int31 = operacionesMatematicas0.dividir((java.lang.Integer) (-1338190428), (java.lang.Integer) 1199317608);
        java.lang.Integer int34 = operacionesMatematicas0.dividir((java.lang.Integer) 7809, (java.lang.Integer) (-203662590));
        java.lang.Class<?> wildcardClass35 = operacionesMatematicas0.getClass();
        try {
            java.lang.Integer int38 = operacionesMatematicas0.dividir((java.lang.Integer) (-431597798), (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: null");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-961562280) + "'", int12.equals((-961562280)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-589628652) + "'", int19.equals((-589628652)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 225533 + "'", int22.equals(225533));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-1115399484) + "'", int25.equals((-1115399484)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-1251598048) + "'", int28.equals((-1251598048)));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-1) + "'", int31.equals((-1)));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 0 + "'", int34.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass35);
    }

    @Test
    public void test078() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test078");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 688, (java.lang.Integer) 990);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1636528032, (java.lang.Integer) (-6451));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 100004, (java.lang.Integer) 2026904576);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 1728039552, (java.lang.Integer) 38327);
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1317355602));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1678 + "'", int9.equals(1678));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1636534483 + "'", int12.equals(1636534483));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-2026804572) + "'", int15.equals((-2026804572)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 45086 + "'", int18.equals(45086));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
    }

    @Test
    public void test079() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test079");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-203657735), (java.lang.Integer) (-98975152));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-501776), (java.lang.Integer) (-903099228));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-173593983), (java.lang.Integer) 170247311);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-318552785), (java.lang.Integer) 0);
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) 1844748256, (java.lang.Integer) (-653273492));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-104682583) + "'", int9.equals((-104682583)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 902597452 + "'", int12.equals(902597452));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1) + "'", int15.equals((-1)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-2) + "'", int21.equals((-2)));
    }

    @Test
    public void test080() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test080");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 674850, (java.lang.Integer) (-203658532));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-1979747001), (java.lang.Integer) 109406);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) 1270901478);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1979637595) + "'", int13.equals((-1979637595)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
    }

    @Test
    public void test081() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test081");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 202);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) (-2030));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 134282741, (java.lang.Integer) 453522102);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 200184, (java.lang.Integer) (-91808));
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) (-153723026), (java.lang.Integer) (-342852204));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1 + "'", int15.equals(1));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 677084 + "'", int18.equals(677084));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-319239361) + "'", int22.equals((-319239361)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-1198623488) + "'", int25.equals((-1198623488)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 189129178 + "'", int28.equals(189129178));
    }

    @Test
    public void test082() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test082");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-203658532), (java.lang.Integer) 797);
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-203658612), (java.lang.Integer) 902932);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 173598797, (java.lang.Integer) (-1581516212));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-203657735) + "'", int18.equals((-203657735)));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-853072144) + "'", int22.equals((-853072144)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 41887964 + "'", int25.equals(41887964));
    }

    @Test
    public void test083() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test083");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-28275), (java.lang.Integer) 200184);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-1896219511), (java.lang.Integer) (-669335781));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-1591033656), (java.lang.Integer) 902597452);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 967005286, (java.lang.Integer) 259575483);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1729412004 + "'", int12.equals(1729412004));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-1) + "'", int16.equals((-1)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 3 + "'", int19.equals(3));
    }

    @Test
    public void test084() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test084");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) (-203654655));
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) (-203654655), (java.lang.Integer) 1401238079);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 203654756 + "'", int18.equals(203654756));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
    }

    @Test
    public void test085() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test085");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2, (java.lang.Integer) (-1));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) 338956);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-300960), (java.lang.Integer) 2679600);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 298, (java.lang.Integer) (-28474));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 902932, (java.lang.Integer) 1739459091);
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) (-485636182), (java.lang.Integer) (-563780412));
        java.lang.Integer int31 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-25), (java.lang.Integer) 445124859);
        java.lang.Class<?> wildcardClass32 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass33 = operacionesMatematicas0.getClass();
        java.lang.Integer int36 = operacionesMatematicas0.dividir((java.lang.Integer) (-351062519), (java.lang.Integer) 1424846586);
        java.lang.Class<?> wildcardClass37 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-2) + "'", int13.equals((-2)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1023986076 + "'", int16.equals(1023986076));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1001435648 + "'", int19.equals(1001435648));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-28176) + "'", int22.equals((-28176)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1740362023 + "'", int25.equals(1740362023));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 1756780413 + "'", int31.equals(1756780413));
        org.junit.Assert.assertNotNull(wildcardClass32);
        org.junit.Assert.assertNotNull(wildcardClass33);
        org.junit.Assert.assertTrue("'" + int36 + "' != '" + 0 + "'", int36.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass37);
    }

    @Test
    public void test086() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test086");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.sumar((java.lang.Integer) (-2030), (java.lang.Integer) (-674366));
        java.lang.Class<?> wildcardClass9 = operacionesMatematicas0.getClass();
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 200282, (java.lang.Integer) (-136543588));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-4060), (java.lang.Integer) (-181965));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + (-676396) + "'", int8.equals((-676396)));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1166118184) + "'", int12.equals((-1166118184)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
    }

    @Test
    public void test087() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test087");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 2354, (java.lang.Integer) (-403));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-1491), (java.lang.Integer) 1570059);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) (-1936223520), (java.lang.Integer) 1558090488);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 2026904576, (java.lang.Integer) 766898176);
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 40325680, (java.lang.Integer) 313922379);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-5) + "'", int12.equals((-5)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1) + "'", int19.equals((-1)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1501164544) + "'", int22.equals((-1501164544)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
    }

    @Test
    public void test088() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test088");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-198), (java.lang.Integer) 198);
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) (-6), (java.lang.Integer) 0);
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1772181088, (java.lang.Integer) 1835170974);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass18);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-6) + "'", int21.equals((-6)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 1210699584 + "'", int24.equals(1210699584));
    }

    @Test
    public void test089() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test089");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-204), (java.lang.Integer) 12);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 797, (java.lang.Integer) (-201378));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-853072144), (java.lang.Integer) (-358));
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-1732880844), (java.lang.Integer) (-1226993776));
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) 1109137284);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-17) + "'", int6.equals((-17)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-853071786) + "'", int13.equals((-853071786)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-505887068) + "'", int16.equals((-505887068)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 0 + "'", int20.equals(0));
    }

    @Test
    public void test090() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test090");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1419118905), (java.lang.Integer) (-1243099180));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 676395, (java.lang.Integer) (-1429679577));
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) (-250078738), (java.lang.Integer) (-1008633599));
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1853597184), (java.lang.Integer) (-706438656));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 47990220 + "'", int12.equals(47990220));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-970719232) + "'", int21.equals((-970719232)));
    }

    @Test
    public void test091() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test091");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 146, (java.lang.Integer) 990);
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 200184, (java.lang.Integer) (-2030));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-136443584), (java.lang.Integer) 198);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 1106533862, (java.lang.Integer) (-98));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 775906878, (java.lang.Integer) (-973376920));
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) (-670008196), (java.lang.Integer) (-1815850602));
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-198822382), (java.lang.Integer) (-203678316));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-844) + "'", int7.equals((-844)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-98) + "'", int10.equals((-98)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-689109) + "'", int13.equals((-689109)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1106533960 + "'", int16.equals(1106533960));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1749283798 + "'", int19.equals(1749283798));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1145842406 + "'", int22.equals(1145842406));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 128928872 + "'", int25.equals(128928872));
    }

    @Test
    public void test092() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test092");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 99, (java.lang.Integer) (-25));
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-169435336), (java.lang.Integer) 191);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 164521512, (java.lang.Integer) 680609675);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203881138, (java.lang.Integer) 2024853078);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1698415552, (java.lang.Integer) 1103767598);
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) (-169435345), (java.lang.Integer) (-110265462));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-3) + "'", int6.equals((-3)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-169435527) + "'", int9.equals((-169435527)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1374777416) + "'", int12.equals((-1374777416)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1026782156 + "'", int15.equals(1026782156));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1127011200) + "'", int18.equals((-1127011200)));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-59169883) + "'", int22.equals((-59169883)));
    }

    @Test
    public void test093() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test093");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 2002502299, (java.lang.Integer) 668453649);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 104684937, (java.lang.Integer) 1582669994);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1624011348) + "'", int13.equals((-1624011348)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1687354931 + "'", int16.equals(1687354931));
    }

    @Test
    public void test094() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test094");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99990, (java.lang.Integer) 14);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-678074), (java.lang.Integer) 3451);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-674366));
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1948415000), (java.lang.Integer) (-1536561213));
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) (-1996), (java.lang.Integer) 613025747);
        java.lang.Integer int24 = operacionesMatematicas0.restar((java.lang.Integer) (-396031516), (java.lang.Integer) 1728782720);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 100004 + "'", int9.equals(100004));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1954933922 + "'", int12.equals(1954933922));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1636247992 + "'", int18.equals(1636247992));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-2124814236) + "'", int24.equals((-2124814236)));
    }

    @Test
    public void test095() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test095");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-6351532), (java.lang.Integer) (-906878093));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 2017749937, (java.lang.Integer) (-480400));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-1231595012), (java.lang.Integer) (-1560406496));
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 995, (java.lang.Integer) (-230006161));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2018230337 + "'", int10.equals(2018230337));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1502965788 + "'", int13.equals(1502965788));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-230005166) + "'", int16.equals((-230005166)));
    }

    @Test
    public void test096() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test096");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-337227660), (java.lang.Integer) (-2962));
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-2131));
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-317895457), (java.lang.Integer) 0);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 1040387076, (java.lang.Integer) (-915911584));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) 148862956, (java.lang.Integer) 469736324);
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2092799141, (java.lang.Integer) 318033681);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 113851 + "'", int10.equals(113851));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-317895457) + "'", int16.equals((-317895457)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 124475492 + "'", int19.equals(124475492));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-320873368) + "'", int23.equals((-320873368)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-619951627) + "'", int26.equals((-619951627)));
    }

    @Test
    public void test097() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test097");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1913));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-1566625664), (java.lang.Integer) (-1566625664));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 499871, (java.lang.Integer) (-300960));
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-74681), (java.lang.Integer) (-202979700));
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) (-985707399), (java.lang.Integer) (-292));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 117 + "'", int9.equals(117));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1 + "'", int12.equals(1));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 198911 + "'", int15.equals(198911));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-203054381) + "'", int18.equals((-203054381)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-985707691) + "'", int21.equals((-985707691)));
    }

    @Test
    public void test098() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test098");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1089, (java.lang.Integer) (-1027036680));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 23816997, (java.lang.Integer) 222308);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166111, (java.lang.Integer) 1023985958);
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Integer int29 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-970066961));
        java.lang.Class<?> wildcardClass30 = operacionesMatematicas0.getClass();
        java.lang.Integer int33 = operacionesMatematicas0.sumar((java.lang.Integer) (-1643462808), (java.lang.Integer) 491241074);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1751447560) + "'", int19.equals((-1751447560)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-985706892) + "'", int22.equals((-985706892)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1741645850 + "'", int25.equals(1741645850));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 0 + "'", int29.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass30);
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + (-1152221734) + "'", int33.equals((-1152221734)));
    }

    @Test
    public void test099() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test099");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 679827);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-86407950), (java.lang.Integer) 419736786);
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-28176), (java.lang.Integer) (-237));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-679726) + "'", int6.equals((-679726)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 118 + "'", int12.equals(118));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test100() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test100");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 374697400, (java.lang.Integer) 473676);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 329, (java.lang.Integer) (-144));
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 1751438428, (java.lang.Integer) (-150310400));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-47376), (java.lang.Integer) 99692);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 1178020841, (java.lang.Integer) (-656785849));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) (-953364480), (java.lang.Integer) 2055113347);
        java.lang.Integer int31 = operacionesMatematicas0.sumar((java.lang.Integer) 1145842406, (java.lang.Integer) 223235);
        java.lang.Integer int34 = operacionesMatematicas0.sumar((java.lang.Integer) (-374696609), (java.lang.Integer) 1787);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 791 + "'", int13.equals(791));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-47376) + "'", int16.equals((-47376)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-11) + "'", int19.equals((-11)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 521234992 + "'", int25.equals(521234992));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 1146065641 + "'", int31.equals(1146065641));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + (-374694822) + "'", int34.equals((-374694822)));
    }

    @Test
    public void test101() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test101");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-91808), (java.lang.Integer) 386);
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 135279661, (java.lang.Integer) (-40337365));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1918723088), (java.lang.Integer) 1567906720);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1643596931, (java.lang.Integer) 1651769703);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-237) + "'", int9.equals((-237)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-3) + "'", int12.equals((-3)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 735036928 + "'", int15.equals(735036928));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 858278837 + "'", int18.equals(858278837));
    }

    @Test
    public void test102() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test102");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 138125136, (java.lang.Integer) 223403);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.restar((java.lang.Integer) (-391), (java.lang.Integer) 847782221);
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-1468325642), (java.lang.Integer) 6823529);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 364805792, (java.lang.Integer) (-981890556));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 137901733 + "'", int7.equals(137901733));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-847782612) + "'", int11.equals((-847782612)));
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1461502113) + "'", int15.equals((-1461502113)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-617084764) + "'", int18.equals((-617084764)));
    }

    @Test
    public void test103() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test103");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) (-1));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 2030, (java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.dividir((java.lang.Integer) 1374, (java.lang.Integer) 359256);
        java.lang.Integer int17 = operacionesMatematicas0.dividir((java.lang.Integer) 1400704484, (java.lang.Integer) (-518736683));
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) 173904523, (java.lang.Integer) (-890108686));
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) 952659857, (java.lang.Integer) (-32545458));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2029 + "'", int10.equals(2029));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14.equals(0));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-2) + "'", int17.equals((-2)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 1064013209 + "'", int20.equals(1064013209));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 920114399 + "'", int23.equals(920114399));
    }

    @Test
    public void test104() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test104");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 1678);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 242847, (java.lang.Integer) 674850);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.multiplicar((java.lang.Integer) 570175776, (java.lang.Integer) 1420242393);
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) 676193, (java.lang.Integer) (-1225373186));
        java.lang.Class<?> wildcardClass21 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 166122 + "'", int10.equals(166122));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-1354218464) + "'", int17.equals((-1354218464)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 1226049379 + "'", int20.equals(1226049379));
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test105() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test105");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-265436054), (java.lang.Integer) 521275659);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
    }

    @Test
    public void test106() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test106");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) 14);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 227358, (java.lang.Integer) 223);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 166122, (java.lang.Integer) (-11));
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 200232, (java.lang.Integer) (-181982338));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) (-485640240));
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 572102258, (java.lang.Integer) (-220855052));
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) 134051334, (java.lang.Integer) 201029);
        java.lang.Integer int31 = operacionesMatematicas0.dividir((java.lang.Integer) 880885566, (java.lang.Integer) 43690);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 675040 + "'", int10.equals(675040));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227581 + "'", int13.equals(227581));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 166111 + "'", int16.equals(166111));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-181782106) + "'", int19.equals((-181782106)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-485640240) + "'", int22.equals((-485640240)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1846095016 + "'", int25.equals(1846095016));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 133850305 + "'", int28.equals(133850305));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 20162 + "'", int31.equals(20162));
    }

    @Test
    public void test107() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test107");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-2030), (java.lang.Integer) 677084);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 438482334, (java.lang.Integer) 44540629);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-203654655), (java.lang.Integer) 678545);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 1727250696, (java.lang.Integer) 886029184);
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-127644456), (java.lang.Integer) 313922379);
        java.lang.Integer int31 = operacionesMatematicas0.sumar((java.lang.Integer) (-1918641081), (java.lang.Integer) 181674816);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-679114) + "'", int16.equals((-679114)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1809913974 + "'", int19.equals(1809913974));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-300) + "'", int22.equals((-300)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-1681687416) + "'", int25.equals((-1681687416)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 145132360 + "'", int28.equals(145132360));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-1736966265) + "'", int31.equals((-1736966265)));
    }

    @Test
    public void test108() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test108");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.sumar((java.lang.Integer) 200183, (java.lang.Integer) (-26796));
        java.lang.Integer int11 = operacionesMatematicas0.restar((java.lang.Integer) 1954931892, (java.lang.Integer) 302);
        java.lang.Integer int14 = operacionesMatematicas0.dividir((java.lang.Integer) 680607504, (java.lang.Integer) (-853072144));
        java.lang.Integer int17 = operacionesMatematicas0.dividir((java.lang.Integer) 12715818, (java.lang.Integer) 2010926408);
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-3260), (java.lang.Integer) 396342713);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 173387 + "'", int8.equals(173387));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 1954931590 + "'", int11.equals(1954931590));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14.equals(0));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 707911716 + "'", int20.equals(707911716));
    }

    @Test
    public void test109() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test109");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 2354, (java.lang.Integer) (-403));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-203289362), (java.lang.Integer) 676395);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 1741645850, (java.lang.Integer) 34527);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 654, (java.lang.Integer) 1592512321);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1848937749), (java.lang.Integer) 402135200);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-5) + "'", int12.equals((-5)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-530028550) + "'", int16.equals((-530028550)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 50443 + "'", int19.equals(50443));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1592512975 + "'", int22.equals(1592512975));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-1567237408) + "'", int25.equals((-1567237408)));
    }

    @Test
    public void test110() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test110");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-2031), (java.lang.Integer) (-100));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-679014), (java.lang.Integer) 28345);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 2108473854);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-344077));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 65, (java.lang.Integer) 1346166);
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) (-23417576));
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-248222832), (java.lang.Integer) 90483430);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-2131) + "'", int10.equals((-2131)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-650669) + "'", int13.equals((-650669)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-2108473854) + "'", int16.equals((-2108473854)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1346231 + "'", int22.equals(1346231));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 23417576 + "'", int25.equals(23417576));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 1489388384 + "'", int28.equals(1489388384));
    }

    @Test
    public void test111() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test111");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-507), (java.lang.Integer) 200871);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 1341, (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) 127651484, (java.lang.Integer) 1593177030);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-201378) + "'", int16.equals((-201378)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1341 + "'", int19.equals(1341));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-1465525546) + "'", int23.equals((-1465525546)));
    }

    @Test
    public void test112() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test112");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 925359, (java.lang.Integer) 3451);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-388878032), (java.lang.Integer) (-138799294));
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 431, (java.lang.Integer) (-1660354439));
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) (-674366), (java.lang.Integer) 343210954);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 268 + "'", int12.equals(268));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-250078738) + "'", int15.equals((-250078738)));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1660354008) + "'", int19.equals((-1660354008)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-343885320) + "'", int22.equals((-343885320)));
    }

    @Test
    public void test113() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test113");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 1678);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 242847, (java.lang.Integer) 674850);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 720, (java.lang.Integer) 1160575744);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 166122 + "'", int10.equals(166122));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
    }

    @Test
    public void test114() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test114");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-204), (java.lang.Integer) 12);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 797, (java.lang.Integer) (-201378));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.dividir((java.lang.Integer) 3233, (java.lang.Integer) 1024014076);
        java.lang.Integer int17 = operacionesMatematicas0.dividir((java.lang.Integer) (-684291464), (java.lang.Integer) 1784911650);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) (-618713097));
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) (-899864171), (java.lang.Integer) 802573192);
        java.lang.Integer int26 = operacionesMatematicas0.restar((java.lang.Integer) (-422447288), (java.lang.Integer) 1130915870);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-17) + "'", int6.equals((-17)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14.equals(0));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-618713097) + "'", int20.equals((-618713097)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-1702437363) + "'", int23.equals((-1702437363)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-1553363158) + "'", int26.equals((-1553363158)));
    }

    @Test
    public void test115() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test115");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-26796), (java.lang.Integer) (-2131));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-674366), (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 790454420, (java.lang.Integer) (-9));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) (-1435555388), (java.lang.Integer) 144768295);
        java.lang.Integer int24 = operacionesMatematicas0.dividir((java.lang.Integer) (-2047244044), (java.lang.Integer) 1739462365);
        java.lang.Class<?> wildcardClass25 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-28927) + "'", int12.equals((-28927)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-133524468) + "'", int15.equals((-133524468)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1475844812 + "'", int18.equals(1475844812));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1580323683) + "'", int21.equals((-1580323683)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1) + "'", int24.equals((-1)));
        org.junit.Assert.assertNotNull(wildcardClass25);
    }

    @Test
    public void test116() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test116");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 1079, (java.lang.Integer) 679827);
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-198118), (java.lang.Integer) 674850);
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-882132), (java.lang.Integer) (-593933344));
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) 791, (java.lang.Integer) 1269613928);
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-320873368), (java.lang.Integer) (-563780412));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-678748) + "'", int17.equals((-678748)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-872968) + "'", int20.equals((-872968)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 1728039552 + "'", int23.equals(1728039552));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-757730216) + "'", int26.equals((-757730216)));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-226140256) + "'", int29.equals((-226140256)));
    }

    @Test
    public void test117() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test117");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-203657735), (java.lang.Integer) (-98975152));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1079, (java.lang.Integer) 222612);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 99692, (java.lang.Integer) 203177900);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 589628875, (java.lang.Integer) (-1614279171));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 878490046, (java.lang.Integer) (-1112639231));
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-104682583) + "'", int9.equals((-104682583)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-221533) + "'", int12.equals((-221533)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-203078208) + "'", int15.equals((-203078208)));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1024650296) + "'", int19.equals((-1024650296)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 428048318 + "'", int22.equals(428048318));
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test118() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test118");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 1678);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2000, (java.lang.Integer) (-28474));
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-300943), (java.lang.Integer) (-2));
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 166122 + "'", int10.equals(166122));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-56948000) + "'", int13.equals((-56948000)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-300941) + "'", int16.equals((-300941)));
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test119() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test119");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.sumar((java.lang.Integer) 200183, (java.lang.Integer) (-26796));
        java.lang.Class<?> wildcardClass9 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 173387 + "'", int8.equals(173387));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test120() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test120");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-882132), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 676495, (java.lang.Integer) 2030);
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        java.lang.Integer int27 = operacionesMatematicas0.sumar((java.lang.Integer) (-1338190428), (java.lang.Integer) (-139407012));
        java.lang.Class<?> wildcardClass28 = operacionesMatematicas0.getClass();
        java.lang.Integer int31 = operacionesMatematicas0.restar((java.lang.Integer) 677574316, (java.lang.Integer) 1355465799);
        java.lang.Integer int34 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-3354), (java.lang.Integer) (-917));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-1480217496) + "'", int20.equals((-1480217496)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 333 + "'", int23.equals(333));
        org.junit.Assert.assertNotNull(wildcardClass24);
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + (-1477597440) + "'", int27.equals((-1477597440)));
        org.junit.Assert.assertNotNull(wildcardClass28);
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-677891483) + "'", int31.equals((-677891483)));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 3075618 + "'", int34.equals(3075618));
    }

    @Test
    public void test121() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test121");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1913));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-300960), (java.lang.Integer) (-17));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 863229490, (java.lang.Integer) (-6435));
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) (-736276107), (java.lang.Integer) (-598595603));
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1457005236, (java.lang.Integer) 98977889);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 117 + "'", int9.equals(117));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-300943) + "'", int12.equals((-300943)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-134145) + "'", int15.equals((-134145)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1 + "'", int18.equals(1));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1667221044 + "'", int21.equals(1667221044));
    }

    @Test
    public void test122() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test122");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 202);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) (-2030));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) (-1751447560), (java.lang.Integer) (-9430));
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1829857832, (java.lang.Integer) (-34162));
        java.lang.Integer int28 = operacionesMatematicas0.sumar((java.lang.Integer) 280056335, (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1 + "'", int15.equals(1));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 677084 + "'", int18.equals(677084));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1751438130) + "'", int21.equals((-1751438130)));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1645736496 + "'", int25.equals(1645736496));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 280056335 + "'", int28.equals(280056335));
    }

    @Test
    public void test123() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test123");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 679827);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-86407950), (java.lang.Integer) 419736786);
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-28176), (java.lang.Integer) (-237));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 1070300805, (java.lang.Integer) (-1992455588));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-679726) + "'", int6.equals((-679726)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 118 + "'", int12.equals(118));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-922154783) + "'", int16.equals((-922154783)));
    }

    @Test
    public void test124() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test124");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 1079, (java.lang.Integer) 679827);
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) 5050, (java.lang.Integer) (-1566625664));
        java.lang.Class<?> wildcardClass21 = operacionesMatematicas0.getClass();
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) (-28), (java.lang.Integer) 10495029);
        java.lang.Integer int27 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2066490878, (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-678748) + "'", int17.equals((-678748)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-129843968) + "'", int20.equals((-129843968)));
        org.junit.Assert.assertNotNull(wildcardClass21);
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 10495001 + "'", int24.equals(10495001));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 0 + "'", int27.equals(0));
    }

    @Test
    public void test125() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test125");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 473676, (java.lang.Integer) (-2030));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-292), (java.lang.Integer) 2108502328);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-86797590), (java.lang.Integer) 674850);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 225533, (java.lang.Integer) 0);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 1199317608, (java.lang.Integer) 1980250204);
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass27 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass28 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-961562280) + "'", int12.equals((-961562280)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-589628652) + "'", int19.equals((-589628652)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 225533 + "'", int22.equals(225533));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-1115399484) + "'", int25.equals((-1115399484)));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertNotNull(wildcardClass27);
        org.junit.Assert.assertNotNull(wildcardClass28);
    }

    @Test
    public void test126() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test126");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.restar((java.lang.Integer) 153262720, (java.lang.Integer) (-507));
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 1362967132, (java.lang.Integer) 153263227);
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-9351), (java.lang.Integer) (-887059769));
        java.lang.Class<?> wildcardClass21 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 153263227 + "'", int14.equals(153263227));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 1209703905 + "'", int17.equals(1209703905));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 1314051343 + "'", int20.equals(1314051343));
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test127() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test127");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) 101);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 28053, (java.lang.Integer) 849);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-86407950), (java.lang.Integer) (-2027625964));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1266663156), (java.lang.Integer) (-57810812));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2 + "'", int6.equals(2));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 23816997 + "'", int10.equals(23816997));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-2114033914) + "'", int13.equals((-2114033914)));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-290977232) + "'", int17.equals((-290977232)));
    }

    @Test
    public void test128() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test128");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-203658532), (java.lang.Integer) 797);
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-203658612), (java.lang.Integer) 902932);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 118, (java.lang.Integer) 243122812);
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) 473676, (java.lang.Integer) 676401);
        java.lang.Integer int31 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 961940492);
        java.lang.Integer int34 = operacionesMatematicas0.dividir((java.lang.Integer) 1002800788, (java.lang.Integer) (-422745004));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-203657735) + "'", int18.equals((-203657735)));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-853072144) + "'", int22.equals((-853072144)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 243122930 + "'", int25.equals(243122930));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-1727627124) + "'", int28.equals((-1727627124)));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-235635084) + "'", int31.equals((-235635084)));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + (-2) + "'", int34.equals((-2)));
    }

    @Test
    public void test129() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test129");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 703051, (java.lang.Integer) 151554579);
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 1431676816);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) (-682380));
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 656783575, (java.lang.Integer) 1713833426);
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 1133057318, (java.lang.Integer) 1749283798);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-150851528) + "'", int9.equals((-150851528)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 682380 + "'", int15.equals(682380));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1924350295) + "'", int18.equals((-1924350295)));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-616226480) + "'", int22.equals((-616226480)));
    }

    @Test
    public void test130() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test130");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-198), (java.lang.Integer) 198);
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-245718859), (java.lang.Integer) 701863);
        java.lang.Class<?> wildcardClass21 = operacionesMatematicas0.getClass();
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) 578855629, (java.lang.Integer) 1772926587);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-246420722) + "'", int20.equals((-246420722)));
        org.junit.Assert.assertNotNull(wildcardClass21);
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 2118645375 + "'", int24.equals(2118645375));
    }

    @Test
    public void test131() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test131");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 2354, (java.lang.Integer) (-403));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 1988598756, (java.lang.Integer) 6295);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 673023, (java.lang.Integer) (-679014));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-299613002), (java.lang.Integer) (-1251598048));
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) 2055113347, (java.lang.Integer) 244290);
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) 1237445058, (java.lang.Integer) 950403110);
        java.lang.Integer int31 = operacionesMatematicas0.dividir((java.lang.Integer) 870373812, (java.lang.Integer) 158761111);
        java.lang.Integer int34 = operacionesMatematicas0.sumar((java.lang.Integer) 2142216169, (java.lang.Integer) 376883144);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-5) + "'", int12.equals((-5)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 315901 + "'", int16.equals(315901));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 2145914048 + "'", int22.equals(2145914048));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 2054869057 + "'", int25.equals(2054869057));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 1 + "'", int28.equals(1));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 5 + "'", int31.equals(5));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + (-1775867983) + "'", int34.equals((-1775867983)));
    }

    @Test
    public void test132() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test132");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2, (java.lang.Integer) 99);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 200871, (java.lang.Integer) 688);
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) 669356581, (java.lang.Integer) 1001435648);
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 198 + "'", int15.equals(198));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 200183 + "'", int18.equals(200183));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test133() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test133");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-674366));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-1027036680), (java.lang.Integer) (-28927));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 834392107, (java.lang.Integer) (-343065842));
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1427973632, (java.lang.Integer) 442626564);
        java.lang.Integer int31 = operacionesMatematicas0.restar((java.lang.Integer) (-166100), (java.lang.Integer) (-952340332));
        java.lang.Integer int34 = operacionesMatematicas0.restar((java.lang.Integer) 65, (java.lang.Integer) (-313922314));
        java.lang.Class<?> wildcardClass35 = operacionesMatematicas0.getClass();
        java.lang.Integer int38 = operacionesMatematicas0.sumar((java.lang.Integer) (-835045696), (java.lang.Integer) 0);
        java.lang.Integer int41 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1855525776, (java.lang.Integer) (-824465252));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-203658532) + "'", int19.equals((-203658532)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 35504 + "'", int22.equals(35504));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-2) + "'", int25.equals((-2)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 1214289920 + "'", int28.equals(1214289920));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 952174232 + "'", int31.equals(952174232));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 313922379 + "'", int34.equals(313922379));
        org.junit.Assert.assertNotNull(wildcardClass35);
        org.junit.Assert.assertTrue("'" + int38 + "' != '" + (-835045696) + "'", int38.equals((-835045696)));
        org.junit.Assert.assertTrue("'" + int41 + "' != '" + 1984850880 + "'", int41.equals(1984850880));
    }

    @Test
    public void test134() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test134");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-204), (java.lang.Integer) 12);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 797, (java.lang.Integer) (-201378));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 3021, (java.lang.Integer) 200183);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 1593176491, (java.lang.Integer) 50443);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1954108080), (java.lang.Integer) (-606522767));
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) 1554530409, (java.lang.Integer) 1963548160);
        java.lang.Integer int24 = operacionesMatematicas0.dividir((java.lang.Integer) (-56947941), (java.lang.Integer) (-1098306876));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-17) + "'", int6.equals((-17)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1593226934 + "'", int15.equals(1593226934));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1025253456 + "'", int18.equals(1025253456));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 0 + "'", int24.equals(0));
    }

    @Test
    public void test135() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test135");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 2, (java.lang.Integer) 1023986076);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) (-1846318208), (java.lang.Integer) 203);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1846318005) + "'", int21.equals((-1846318005)));
    }

    @Test
    public void test136() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test136");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-203657735), (java.lang.Integer) (-98975152));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-872882432), (java.lang.Integer) 1374);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-203), (java.lang.Integer) 3233);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) (-223), (java.lang.Integer) 203658830);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 242847, (java.lang.Integer) (-1988598960));
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) (-26796), (java.lang.Integer) (-655230575));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-104682583) + "'", int9.equals((-104682583)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-635285) + "'", int12.equals((-635285)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1988356113) + "'", int21.equals((-1988356113)));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 0 + "'", int26.equals(0));
    }

    @Test
    public void test137() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test137");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99990, (java.lang.Integer) 14);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-78757731), (java.lang.Integer) (-527912576));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-1379066004), (java.lang.Integer) (-203078208));
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 47359032, (java.lang.Integer) 967314944);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) (-5857712), (java.lang.Integer) (-994291725));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 100004 + "'", int9.equals(100004));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-606670307) + "'", int12.equals((-606670307)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1175987796) + "'", int15.equals((-1175987796)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1000149437) + "'", int21.equals((-1000149437)));
    }

    @Test
    public void test138() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test138");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) (-292));
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) (-5), (java.lang.Integer) (-679014));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 1372443108, (java.lang.Integer) (-2113952996));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-882132) + "'", int16.equals((-882132)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
    }

    @Test
    public void test139() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test139");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 473676, (java.lang.Integer) (-2030));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 20300, (java.lang.Integer) 338956);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 3021, (java.lang.Integer) 3274);
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1101, (java.lang.Integer) (-86631468));
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 753280484, (java.lang.Integer) (-1880238365));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-961562280) + "'", int12.equals((-961562280)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 359256 + "'", int15.equals(359256));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 6295 + "'", int18.equals(6295));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-891965756) + "'", int21.equals((-891965756)));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-1126957881) + "'", int25.equals((-1126957881)));
    }

    @Test
    public void test140() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test140");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-203657735), (java.lang.Integer) (-98975152));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-872882432), (java.lang.Integer) 1374);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 1903640797, (java.lang.Integer) (-1599576402));
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 415391778, (java.lang.Integer) (-1637500208));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-104682583) + "'", int9.equals((-104682583)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-635285) + "'", int12.equals((-635285)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-791750097) + "'", int15.equals((-791750097)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 2052891986 + "'", int18.equals(2052891986));
    }

    @Test
    public void test141() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test141");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-100), (java.lang.Integer) (-2233));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-594099455), (java.lang.Integer) 99990);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1426890059, (java.lang.Integer) 532610570);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223300 + "'", int6.equals(223300));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-311834474) + "'", int9.equals((-311834474)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1455911186) + "'", int12.equals((-1455911186)));
    }

    @Test
    public void test142() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test142");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 101);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) (-411258247), (java.lang.Integer) (-889371287));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 1601067775, (java.lang.Integer) (-949939300));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 101 + "'", int4.equals(101));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 478113040 + "'", int7.equals(478113040));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-1743960221) + "'", int10.equals((-1743960221)));
    }

    @Test
    public void test143() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test143");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-203658532), (java.lang.Integer) 797);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 138125136, (java.lang.Integer) (-198));
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) 3021, (java.lang.Integer) (-607257));
        java.lang.Integer int28 = operacionesMatematicas0.sumar((java.lang.Integer) 476950759, (java.lang.Integer) 0);
        java.lang.Integer int31 = operacionesMatematicas0.restar((java.lang.Integer) (-127769093), (java.lang.Integer) 436681114);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-203657735) + "'", int18.equals((-203657735)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 138124938 + "'", int21.equals(138124938));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 610278 + "'", int25.equals(610278));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 476950759 + "'", int28.equals(476950759));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-564450207) + "'", int31.equals((-564450207)));
    }

    @Test
    public void test144() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test144");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 1678, (java.lang.Integer) (-304));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 44540629, (java.lang.Integer) (-833949417));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 674196, (java.lang.Integer) (-203658612));
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-1008434688), (java.lang.Integer) 1003415040);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1374 + "'", int9.equals(1374));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 878490046 + "'", int12.equals(878490046));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-12090128) + "'", int15.equals((-12090128)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-5019648) + "'", int18.equals((-5019648)));
    }

    @Test
    public void test145() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test145");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-223), (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-86408168), (java.lang.Integer) (-1480217496));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 2108473854, (java.lang.Integer) 99990);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-650669), (java.lang.Integer) 44906012);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 326559702, (java.lang.Integer) (-770326209));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) 223403, (java.lang.Integer) 1737910004);
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) (-2033424993), (java.lang.Integer) 55);
        java.lang.Integer int27 = operacionesMatematicas0.multiplicar((java.lang.Integer) 30106984, (java.lang.Integer) 814305812);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223 + "'", int6.equals(223));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-1566625664) + "'", int9.equals((-1566625664)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2108373864 + "'", int12.equals(2108373864));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-45556681) + "'", int15.equals((-45556681)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1096885911 + "'", int18.equals(1096885911));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1737686601) + "'", int21.equals((-1737686601)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-2033424938) + "'", int24.equals((-2033424938)));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 252132384 + "'", int27.equals(252132384));
    }

    @Test
    public void test146() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test146");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-2233));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-676396), (java.lang.Integer) 100);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-223), (java.lang.Integer) (-198));
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 14, (java.lang.Integer) 12);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 223300, (java.lang.Integer) (-86631468));
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) 44906012, (java.lang.Integer) (-365383));
        java.lang.Integer int27 = operacionesMatematicas0.sumar((java.lang.Integer) 1567906726, (java.lang.Integer) (-6));
        java.lang.Integer int30 = operacionesMatematicas0.sumar((java.lang.Integer) (-203435127), (java.lang.Integer) (-680500222));
        java.lang.Integer int33 = operacionesMatematicas0.dividir((java.lang.Integer) 1492597250, (java.lang.Integer) (-855370178));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-674366) + "'", int9.equals((-674366)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-6763) + "'", int12.equals((-6763)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-25) + "'", int15.equals((-25)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 168 + "'", int18.equals(168));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-86408168) + "'", int21.equals((-86408168)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 44540629 + "'", int24.equals(44540629));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 1567906720 + "'", int27.equals(1567906720));
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + (-883935349) + "'", int30.equals((-883935349)));
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + (-1) + "'", int33.equals((-1)));
    }

    @Test
    public void test147() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test147");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 674850, (java.lang.Integer) (-203658532));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 9120400, (java.lang.Integer) 8260879);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-454172969), (java.lang.Integer) (-844));
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 13135, (java.lang.Integer) 23816997);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 1574326088, (java.lang.Integer) 644054574);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 859521 + "'", int13.equals(859521));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1069896492 + "'", int16.equals(1069896492));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 930271514 + "'", int22.equals(930271514));
    }

    @Test
    public void test148() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test148");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) 14);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 227358, (java.lang.Integer) 223);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-702061), (java.lang.Integer) (-7812));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 680607504, (java.lang.Integer) 1089);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) 972267014);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 1278130677);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 675040 + "'", int10.equals(675040));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227581 + "'", int13.equals(227581));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-694249) + "'", int16.equals((-694249)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 680606415 + "'", int19.equals(680606415));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1278130677 + "'", int25.equals(1278130677));
    }

    @Test
    public void test149() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test149");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 100004, (java.lang.Integer) 0);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 3021, (java.lang.Integer) 100);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) (-197795));
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) 44906216, (java.lang.Integer) (-1796254192));
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 100004 + "'", int12.equals(100004));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 30 + "'", int15.equals(30));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-44108285) + "'", int18.equals((-44108285)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test150() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test150");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.restar((java.lang.Integer) 153262720, (java.lang.Integer) (-507));
        java.lang.Integer int17 = operacionesMatematicas0.multiplicar((java.lang.Integer) 467271424, (java.lang.Integer) (-4538682));
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-1734233278), (java.lang.Integer) 703040);
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-320292228), (java.lang.Integer) (-34149));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 153263227 + "'", int14.equals(153263227));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-1680000512) + "'", int17.equals((-1680000512)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-1734936318) + "'", int20.equals((-1734936318)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-1622408940) + "'", int23.equals((-1622408940)));
    }

    @Test
    public void test151() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test151");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-98975152), (java.lang.Integer) (-300960));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-676296), (java.lang.Integer) 2108473854);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 907263012, (java.lang.Integer) 1788694010);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 408714520, (java.lang.Integer) (-105355726));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1963548160 + "'", int9.equals(1963548160));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2107797558 + "'", int12.equals(2107797558));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1599010274) + "'", int15.equals((-1599010274)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 514070246 + "'", int18.equals(514070246));
    }

    @Test
    public void test152() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test152");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) (-1));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) (-2233), (java.lang.Integer) 797);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-403), (java.lang.Integer) 1701552590);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 1853597184, (java.lang.Integer) (-1885225800));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 2354, (java.lang.Integer) 311282688);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 30770291, (java.lang.Integer) 1710744086);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-3030) + "'", int10.equals((-3030)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1701552187 + "'", int13.equals(1701552187));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-31628616) + "'", int16.equals((-31628616)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-311280334) + "'", int19.equals((-311280334)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1741514377 + "'", int22.equals(1741514377));
    }

    @Test
    public void test153() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test153");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) 14);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.restar((java.lang.Integer) (-1827), (java.lang.Integer) (-694249));
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) 1346166, (java.lang.Integer) 651935);
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-1475834436), (java.lang.Integer) (-2004757748));
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) (-1195483520), (java.lang.Integer) (-169432274));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 675040 + "'", int10.equals(675040));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 692422 + "'", int14.equals(692422));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 1998101 + "'", int17.equals(1998101));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 528923312 + "'", int20.equals(528923312));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-1026051246) + "'", int23.equals((-1026051246)));
    }

    @Test
    public void test154() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test154");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 925359, (java.lang.Integer) 3451);
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 268 + "'", int12.equals(268));
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test155() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test155");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) (-203654655));
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 164521512, (java.lang.Integer) 10);
        java.lang.Integer int24 = operacionesMatematicas0.restar((java.lang.Integer) 646664509, (java.lang.Integer) (-1427015890));
        java.lang.Integer int27 = operacionesMatematicas0.multiplicar((java.lang.Integer) 23503605, (java.lang.Integer) (-1556073023));
        java.lang.Class<?> wildcardClass28 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass29 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 203654756 + "'", int18.equals(203654756));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 164521522 + "'", int21.equals(164521522));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 2073680399 + "'", int24.equals(2073680399));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 173904821 + "'", int27.equals(173904821));
        org.junit.Assert.assertNotNull(wildcardClass28);
        org.junit.Assert.assertNotNull(wildcardClass29);
    }

    @Test
    public void test156() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test156");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 166122, (java.lang.Integer) 166122);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) (-747018902), (java.lang.Integer) 166122);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-310346151), (java.lang.Integer) 2112062089);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-747185024) + "'", int10.equals((-747185024)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-673722463) + "'", int13.equals((-673722463)));
    }

    @Test
    public void test157() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test157");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2), (java.lang.Integer) 2030);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-44906012), (java.lang.Integer) 0);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1202654776), (java.lang.Integer) 1673185024);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) (-200282), (java.lang.Integer) (-288798043));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-4060) + "'", int12.equals((-4060)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-44906012) + "'", int15.equals((-44906012)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-280856576) + "'", int18.equals((-280856576)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-288998325) + "'", int21.equals((-288998325)));
    }

    @Test
    public void test158() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test158");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 99, (java.lang.Integer) (-676396));
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.sumar((java.lang.Integer) 1133064081, (java.lang.Integer) (-6763));
        java.lang.Integer int14 = operacionesMatematicas0.sumar((java.lang.Integer) 1796254192, (java.lang.Integer) (-622179770));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 676495 + "'", int7.equals(676495));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 1133057318 + "'", int11.equals(1133057318));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 1174074422 + "'", int14.equals(1174074422));
    }

    @Test
    public void test159() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test159");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-198), (java.lang.Integer) 198);
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) (-6), (java.lang.Integer) 0);
        java.lang.Integer int24 = operacionesMatematicas0.dividir((java.lang.Integer) (-1959325679), (java.lang.Integer) 87173248);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass18);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-6) + "'", int21.equals((-6)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-22) + "'", int24.equals((-22)));
    }

    @Test
    public void test160() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test160");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) 688);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) (-2031), (java.lang.Integer) 223300);
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2117069372, (java.lang.Integer) (-3241260));
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        java.lang.Integer int27 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1556252511), (java.lang.Integer) 833202705);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 12 + "'", int13.equals(12));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 222612 + "'", int17.equals(222612));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 221269 + "'", int20.equals(221269));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-1808117328) + "'", int23.equals((-1808117328)));
        org.junit.Assert.assertNotNull(wildcardClass24);
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 477686961 + "'", int27.equals(477686961));
    }

    @Test
    public void test161() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test161");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.sumar((java.lang.Integer) (-2030), (java.lang.Integer) (-674366));
        java.lang.Class<?> wildcardClass9 = operacionesMatematicas0.getClass();
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-86631468), (java.lang.Integer) 166122);
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1856303486));
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1686524160), (java.lang.Integer) (-889371287));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + (-676396) + "'", int8.equals((-676396)));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-86797590) + "'", int12.equals((-86797590)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-2079078656) + "'", int19.equals((-2079078656)));
    }

    @Test
    public void test162() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test162");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-198), (java.lang.Integer) 198);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-98990));
        java.lang.Class<?> wildcardClass21 = operacionesMatematicas0.getClass();
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) 28345, (java.lang.Integer) (-44902128));
        java.lang.Integer int27 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1896184000), (java.lang.Integer) (-343058591));
        java.lang.Integer int30 = operacionesMatematicas0.restar((java.lang.Integer) 100004, (java.lang.Integer) (-589654622));
        java.lang.Integer int33 = operacionesMatematicas0.sumar((java.lang.Integer) (-1321351936), (java.lang.Integer) 161877677);
        java.lang.Integer int36 = operacionesMatematicas0.sumar((java.lang.Integer) 10376, (java.lang.Integer) 161618560);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-98890) + "'", int20.equals((-98890)));
        org.junit.Assert.assertNotNull(wildcardClass21);
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1440498544) + "'", int24.equals((-1440498544)));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + (-1893466304) + "'", int27.equals((-1893466304)));
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + 589754626 + "'", int30.equals(589754626));
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + (-1159474259) + "'", int33.equals((-1159474259)));
        org.junit.Assert.assertTrue("'" + int36 + "' != '" + 161628936 + "'", int36.equals(161628936));
    }

    @Test
    public void test163() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test163");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) 223300);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-676396), (java.lang.Integer) 1678);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) 99990);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 1888660802, (java.lang.Integer) (-835045696));
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2030) + "'", int9.equals((-2030)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2679600 + "'", int13.equals(2679600));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-678074) + "'", int16.equals((-678074)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-98990) + "'", int19.equals((-98990)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1571260798) + "'", int22.equals((-1571260798)));
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test164() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test164");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-203658532), (java.lang.Integer) 797);
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1362965102, (java.lang.Integer) 0);
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) 1593226934, (java.lang.Integer) 1431676816);
        java.lang.Integer int27 = operacionesMatematicas0.sumar((java.lang.Integer) 651935, (java.lang.Integer) (-675020));
        java.lang.Integer int30 = operacionesMatematicas0.dividir((java.lang.Integer) (-207537300), (java.lang.Integer) (-317731454));
        java.lang.Class<?> wildcardClass31 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-203657735) + "'", int18.equals((-203657735)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1270063546) + "'", int24.equals((-1270063546)));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + (-23085) + "'", int27.equals((-23085)));
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + 0 + "'", int30.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass31);
    }

    @Test
    public void test165() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test165");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) 14);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 227358, (java.lang.Integer) 223);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-702061), (java.lang.Integer) (-7812));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-198), (java.lang.Integer) (-3260));
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) (-407224469), (java.lang.Integer) 0);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 675040 + "'", int10.equals(675040));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227581 + "'", int13.equals(227581));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-694249) + "'", int16.equals((-694249)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 3062 + "'", int19.equals(3062));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-407224469) + "'", int22.equals((-407224469)));
    }

    @Test
    public void test166() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest8.test166");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99990, (java.lang.Integer) 14);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-201378), (java.lang.Integer) (-203657735));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 1195056035, (java.lang.Integer) 2136956016);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-990242366), (java.lang.Integer) (-203054381));
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 606206594, (java.lang.Integer) (-45125428));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 100004 + "'", int9.equals(100004));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-203859113) + "'", int12.equals((-203859113)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-941899981) + "'", int15.equals((-941899981)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1193296747) + "'", int18.equals((-1193296747)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 561081166 + "'", int21.equals(561081166));
    }
}

