import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegressionTest2 {

    public static boolean debug = false;

    @Test
    public void test001() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test001");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-223), (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-4060), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-202979700), (java.lang.Integer) 386);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1913), (java.lang.Integer) (-203658612));
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 1025515084, (java.lang.Integer) 134082361);
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1255479873, (java.lang.Integer) 2354);
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223 + "'", int6.equals(223));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2) + "'", int9.equals((-2)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-202979314) + "'", int12.equals((-202979314)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1243099180) + "'", int15.equals((-1243099180)));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 1159597445 + "'", int20.equals(1159597445));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 462121394 + "'", int23.equals(462121394));
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test002() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test002");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) (-1));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 2030, (java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1304999083));
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) 1592563465, (java.lang.Integer) 132);
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2029 + "'", int10.equals(2029));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14.equals(0));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 1592563597 + "'", int17.equals(1592563597));
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test003() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test003");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 677082, (java.lang.Integer) 797);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) 221269);
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) (-320292228), (java.lang.Integer) 30770291);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 849 + "'", int22.equals(849));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 668453649 + "'", int25.equals(668453649));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-351062519) + "'", int28.equals((-351062519)));
    }

    @Test
    public void test004() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test004");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2000, (java.lang.Integer) 6559757);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 234612112 + "'", int9.equals(234612112));
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test005() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test005");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) 0);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-507), (java.lang.Integer) 100004);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-110265462), (java.lang.Integer) (-3030));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-501776), (java.lang.Integer) (-2017286592));
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2 + "'", int6.equals(2));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-50702028) + "'", int9.equals((-50702028)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-903099228) + "'", int12.equals((-903099228)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 989568000 + "'", int16.equals(989568000));
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test006() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test006");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 101);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) (-674366), (java.lang.Integer) 3020);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 223, (java.lang.Integer) (-589628652));
        try {
            java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 101 + "'", int4.equals(101));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-223) + "'", int7.equals((-223)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 589628875 + "'", int10.equals(589628875));
    }

    @Test
    public void test007() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test007");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-882132), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 676495, (java.lang.Integer) 2030);
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass25 = operacionesMatematicas0.getClass();
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) (-23417874), (java.lang.Integer) 1980253234);
        java.lang.Integer int31 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-300943), (java.lang.Integer) 961940784);
        java.lang.Integer int34 = operacionesMatematicas0.restar((java.lang.Integer) 1929826016, (java.lang.Integer) (-139423347));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-1480217496) + "'", int20.equals((-1480217496)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 333 + "'", int23.equals(333));
        org.junit.Assert.assertNotNull(wildcardClass24);
        org.junit.Assert.assertNotNull(wildcardClass25);
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 40325680 + "'", int31.equals(40325680));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 2069249363 + "'", int34.equals(2069249363));
    }

    @Test
    public void test008() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test008");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 2030, (java.lang.Integer) 688);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 204, (java.lang.Integer) 0);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 199, (java.lang.Integer) 703040);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 199825, (java.lang.Integer) 155085916);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2111569680, (java.lang.Integer) 1771579329);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2 + "'", int6.equals(2));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 204 + "'", int9.equals(204));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 139904960 + "'", int12.equals(139904960));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 155285741 + "'", int15.equals(155285741));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1748417296 + "'", int18.equals(1748417296));
    }

    @Test
    public void test009() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test009");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 298, (java.lang.Integer) 221269);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) 2010869584, (java.lang.Integer) 1198151702);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-220971) + "'", int13.equals((-220971)));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-1085946010) + "'", int17.equals((-1085946010)));
    }

    @Test
    public void test010() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test010");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 674850, (java.lang.Integer) (-203658532));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 9120400, (java.lang.Integer) 8260879);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-103), (java.lang.Integer) 0);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-827264024), (java.lang.Integer) (-365383));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 859521 + "'", int13.equals(859521));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-826898641) + "'", int19.equals((-826898641)));
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test011() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test011");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 676495, (java.lang.Integer) (-9));
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2108473854), (java.lang.Integer) 99);
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-101594), (java.lang.Integer) (-1556252358));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 676504 + "'", int17.equals(676504));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 1714485958 + "'", int20.equals(1714485958));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-434041700) + "'", int23.equals((-434041700)));
    }

    @Test
    public void test012() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test012");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-26796), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) (-6763), (java.lang.Integer) (-4060));
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) 1542, (java.lang.Integer) 2108473854);
        java.lang.Integer int29 = operacionesMatematicas0.restar((java.lang.Integer) (-3162), (java.lang.Integer) (-903099228));
        java.lang.Integer int32 = operacionesMatematicas0.restar((java.lang.Integer) 669356581, (java.lang.Integer) (-392600492));
        java.lang.Integer int35 = operacionesMatematicas0.sumar((java.lang.Integer) 2145914048, (java.lang.Integer) (-1656482186));
        java.lang.Integer int38 = operacionesMatematicas0.dividir((java.lang.Integer) (-3460), (java.lang.Integer) (-1611354393));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-28474) + "'", int20.equals((-28474)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 1 + "'", int23.equals(1));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 2108475396 + "'", int26.equals(2108475396));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 903096066 + "'", int29.equals(903096066));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 1061957073 + "'", int32.equals(1061957073));
        org.junit.Assert.assertTrue("'" + int35 + "' != '" + 489431862 + "'", int35.equals(489431862));
        org.junit.Assert.assertTrue("'" + int38 + "' != '" + 0 + "'", int38.equals(0));
    }

    @Test
    public void test013() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test013");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-882132), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 676495, (java.lang.Integer) 2030);
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        java.lang.Integer int27 = operacionesMatematicas0.sumar((java.lang.Integer) (-1338190428), (java.lang.Integer) (-139407012));
        java.lang.Class<?> wildcardClass28 = operacionesMatematicas0.getClass();
        java.lang.Integer int31 = operacionesMatematicas0.restar((java.lang.Integer) 677574316, (java.lang.Integer) 1355465799);
        java.lang.Integer int34 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1984679224), (java.lang.Integer) (-28176));
        java.lang.Integer int37 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2022570152, (java.lang.Integer) 1471);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-1480217496) + "'", int20.equals((-1480217496)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 333 + "'", int23.equals(333));
        org.junit.Assert.assertNotNull(wildcardClass24);
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + (-1477597440) + "'", int27.equals((-1477597440)));
        org.junit.Assert.assertNotNull(wildcardClass28);
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-677891483) + "'", int31.equals((-677891483)));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + (-152378496) + "'", int34.equals((-152378496)));
        org.junit.Assert.assertTrue("'" + int37 + "' != '" + (-1211642536) + "'", int37.equals((-1211642536)));
    }

    @Test
    public void test014() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test014");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-98975152), (java.lang.Integer) (-300960));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 204, (java.lang.Integer) 0);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-2034), (java.lang.Integer) 676401);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) (-1045), (java.lang.Integer) 1072089982);
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) 680607504, (java.lang.Integer) 2117069372);
        java.lang.Integer int24 = operacionesMatematicas0.dividir((java.lang.Integer) (-6349702), (java.lang.Integer) (-448050134));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1963548160 + "'", int9.equals(1963548160));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 204 + "'", int12.equals(204));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-678435) + "'", int15.equals((-678435)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1180735552) + "'", int21.equals((-1180735552)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 0 + "'", int24.equals(0));
    }

    @Test
    public void test015() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test015");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) (-26796));
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) (-1566625664), (java.lang.Integer) 0);
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1794580014, (java.lang.Integer) (-1546235464));
        java.lang.Integer int31 = operacionesMatematicas0.restar((java.lang.Integer) 568878, (java.lang.Integer) 1554530409);
        java.lang.Integer int34 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1418013643), (java.lang.Integer) (-678074));
        java.lang.Integer int37 = operacionesMatematicas0.dividir((java.lang.Integer) (-1946510055), (java.lang.Integer) (-1266096146));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-86631468) + "'", int22.equals((-86631468)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-1566625664) + "'", int25.equals((-1566625664)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-1768014064) + "'", int28.equals((-1768014064)));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-1553961531) + "'", int31.equals((-1553961531)));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + (-440559234) + "'", int34.equals((-440559234)));
        org.junit.Assert.assertTrue("'" + int37 + "' != '" + 1 + "'", int37.equals(1));
    }

    @Test
    public void test016() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test016");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) (-2452019), (java.lang.Integer) 591647436);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 673023, (java.lang.Integer) (-5));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 1678, (java.lang.Integer) (-1988598960));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) 1115055585);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 1024014076, (java.lang.Integer) 1355141328);
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 201427718, (java.lang.Integer) 1262016360);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-594099455) + "'", int6.equals((-594099455)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-134604) + "'", int9.equals((-134604)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1915811892) + "'", int18.equals((-1915811892)));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
    }

    @Test
    public void test017() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test017");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 1678, (java.lang.Integer) (-304));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 14, (java.lang.Integer) (-676594));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1374 + "'", int9.equals(1374));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test018() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test018");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 10, (java.lang.Integer) 302);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-676594), (java.lang.Integer) 298);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) (-2125425896), (java.lang.Integer) (-848979310));
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) 677084);
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) (-1862925468), (java.lang.Integer) 907263012);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 3020 + "'", int15.equals(3020));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-676296) + "'", int18.equals((-676296)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1320562090 + "'", int21.equals(1320562090));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 1524778816 + "'", int28.equals(1524778816));
    }

    @Test
    public void test019() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test019");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 677082, (java.lang.Integer) 797);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-86797590), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) 990, (java.lang.Integer) 203177900);
        java.lang.Integer int32 = operacionesMatematicas0.sumar((java.lang.Integer) (-202979314), (java.lang.Integer) 2010360317);
        java.lang.Integer int35 = operacionesMatematicas0.dividir((java.lang.Integer) (-1338190225), (java.lang.Integer) 886390221);
        java.lang.Integer int38 = operacionesMatematicas0.restar((java.lang.Integer) (-86407950), (java.lang.Integer) 692422);
        java.lang.Integer int41 = operacionesMatematicas0.restar((java.lang.Integer) (-292), (java.lang.Integer) 1678385021);
        java.lang.Integer int44 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-998194388), (java.lang.Integer) (-485638320));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 849 + "'", int22.equals(849));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-717341912) + "'", int29.equals((-717341912)));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 1807381003 + "'", int32.equals(1807381003));
        org.junit.Assert.assertTrue("'" + int35 + "' != '" + (-1) + "'", int35.equals((-1)));
        org.junit.Assert.assertTrue("'" + int38 + "' != '" + (-87100372) + "'", int38.equals((-87100372)));
        org.junit.Assert.assertTrue("'" + int41 + "' != '" + (-1678385313) + "'", int41.equals((-1678385313)));
        org.junit.Assert.assertTrue("'" + int44 + "' != '" + 1729548736 + "'", int44.equals(1729548736));
    }

    @Test
    public void test020() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test020");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 677082, (java.lang.Integer) 797);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-86797590), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) 990, (java.lang.Integer) 203177900);
        java.lang.Class<?> wildcardClass30 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 849 + "'", int22.equals(849));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-717341912) + "'", int29.equals((-717341912)));
        org.junit.Assert.assertNotNull(wildcardClass30);
    }

    @Test
    public void test021() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test021");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 198, (java.lang.Integer) (-6763));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 28053, (java.lang.Integer) (-28927));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 606945792, (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 606945792 + "'", int16.equals(606945792));
    }

    @Test
    public void test022() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test022");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 101);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 797, (java.lang.Integer) (-890108686));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) (-202979700), (java.lang.Integer) 1024014076);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-755450597), (java.lang.Integer) (-1865866569));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 101 + "'", int4.equals(101));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-747018902) + "'", int7.equals((-747018902)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-1226993776) + "'", int10.equals((-1226993776)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
    }

    @Test
    public void test023() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test023");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 1678, (java.lang.Integer) (-304));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1875127044, (java.lang.Integer) 1983457309);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1374 + "'", int9.equals(1374));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-108330265) + "'", int12.equals((-108330265)));
    }

    @Test
    public void test024() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test024");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) 14);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 227358, (java.lang.Integer) 223);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-2108473854), (java.lang.Integer) (-1427973632));
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) (-747018902), (java.lang.Integer) 1350225);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) (-2091737468), (java.lang.Integer) (-1304798801));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) (-219080448), (java.lang.Integer) (-1174645568));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 675040 + "'", int10.equals(675040));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227581 + "'", int13.equals(227581));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-680500222) + "'", int16.equals((-680500222)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-553) + "'", int19.equals((-553)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 898431027 + "'", int22.equals(898431027));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
    }

    @Test
    public void test025() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test025");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.sumar((java.lang.Integer) (-1868612537), (java.lang.Integer) (-412981212));
        java.lang.Integer int11 = operacionesMatematicas0.restar((java.lang.Integer) 110594088, (java.lang.Integer) 0);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 2013373547 + "'", int8.equals(2013373547));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 110594088 + "'", int11.equals(110594088));
    }

    @Test
    public void test026() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test026");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 2354, (java.lang.Integer) (-403));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 1983457309, (java.lang.Integer) (-104553516));
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-496153144), (java.lang.Integer) 1471202152);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-2099619127), (java.lang.Integer) 241172932);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-5) + "'", int12.equals((-5)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1878903793 + "'", int15.equals(1878903793));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 975049008 + "'", int19.equals(975049008));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-8) + "'", int22.equals((-8)));
    }

    @Test
    public void test027() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test027");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) (-198), (java.lang.Integer) (-453522300));
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) 129067, (java.lang.Integer) (-104682583));
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) 1304999083);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 453522102 + "'", int23.equals(453522102));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-104553516) + "'", int26.equals((-104553516)));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 0 + "'", int29.equals(0));
    }

    @Test
    public void test028() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test028");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) 223300);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 1567919351, (java.lang.Integer) (-337227660));
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1701552422, (java.lang.Integer) 1899059129);
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) 31, (java.lang.Integer) 99990);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2030) + "'", int9.equals((-2030)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2679600 + "'", int13.equals(2679600));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 1905147011 + "'", int17.equals(1905147011));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 2094095990 + "'", int20.equals(2094095990));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 3099690 + "'", int23.equals(3099690));
    }

    @Test
    public void test029() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test029");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass6 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-203761580), (java.lang.Integer) 1427973632);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 902597452, (java.lang.Integer) 847782221);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1592563597, (java.lang.Integer) 3020);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 1224212052 + "'", int10.equals(1224212052));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1750379673 + "'", int13.equals(1750379673));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-821308580) + "'", int16.equals((-821308580)));
    }

    @Test
    public void test030() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test030");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-237), (java.lang.Integer) 101);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-3160), (java.lang.Integer) (-506915));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 765962910, (java.lang.Integer) (-1708435290));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-23937) + "'", int16.equals((-23937)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 503755 + "'", int19.equals(503755));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-942472380) + "'", int22.equals((-942472380)));
    }

    @Test
    public void test031() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test031");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-4060), (java.lang.Integer) (-203658530));
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-941899981), (java.lang.Integer) (-104682583));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1087328256));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203662590) + "'", int6.equals((-203662590)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-837217398) + "'", int9.equals((-837217398)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
    }

    @Test
    public void test032() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test032");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) 2030, (java.lang.Integer) 688);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 138125136, (java.lang.Integer) (-453522300));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1933331090, (java.lang.Integer) (-230063104));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7.equals(2));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 591647436 + "'", int10.equals(591647436));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-8) + "'", int13.equals((-8)));
    }

    @Test
    public void test033() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test033");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-204), (java.lang.Integer) 12);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-90), (java.lang.Integer) 2063594048);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1554530409, (java.lang.Integer) (-4378));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-17) + "'", int6.equals((-17)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 2063593958 + "'", int9.equals(2063593958));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1789033558 + "'", int12.equals(1789033558));
    }

    @Test
    public void test034() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test034");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 677082, (java.lang.Integer) 797);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-86797590), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) 990, (java.lang.Integer) 203177900);
        java.lang.Integer int32 = operacionesMatematicas0.sumar((java.lang.Integer) (-202979314), (java.lang.Integer) 2010360317);
        java.lang.Integer int35 = operacionesMatematicas0.dividir((java.lang.Integer) (-1338190225), (java.lang.Integer) 886390221);
        java.lang.Integer int38 = operacionesMatematicas0.sumar((java.lang.Integer) 781422197, (java.lang.Integer) (-1566668296));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 849 + "'", int22.equals(849));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-717341912) + "'", int29.equals((-717341912)));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 1807381003 + "'", int32.equals(1807381003));
        org.junit.Assert.assertTrue("'" + int35 + "' != '" + (-1) + "'", int35.equals((-1)));
        org.junit.Assert.assertTrue("'" + int38 + "' != '" + (-785246099) + "'", int38.equals((-785246099)));
    }

    @Test
    public void test035() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test035");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-507), (java.lang.Integer) 200871);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 1951528724, (java.lang.Integer) 29684000);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-201378) + "'", int16.equals((-201378)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 65 + "'", int19.equals(65));
    }

    @Test
    public void test036() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test036");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-563084124));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 1968086742, (java.lang.Integer) (-1208538464));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-169433527), (java.lang.Integer) 5);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1) + "'", int12.equals((-1)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-169433522) + "'", int15.equals((-169433522)));
    }

    @Test
    public void test037() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test037");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 199, (java.lang.Integer) 302);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-593933344), (java.lang.Integer) 1352162704);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 758229360 + "'", int13.equals(758229360));
    }

    @Test
    public void test038() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test038");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-507), (java.lang.Integer) 200871);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-201378) + "'", int16.equals((-201378)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test039() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test039");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-223), (java.lang.Integer) (-1429679577));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2233), (java.lang.Integer) (-635285));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 23, (java.lang.Integer) 1791717132);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1429679800) + "'", int13.equals((-1429679800)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1418591405 + "'", int16.equals(1418591405));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1791717109) + "'", int19.equals((-1791717109)));
    }

    @Test
    public void test040() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test040");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) (-26796));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) (-3));
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) (-678748));
        java.lang.Integer int31 = operacionesMatematicas0.sumar((java.lang.Integer) 696288, (java.lang.Integer) (-681446));
        java.lang.Integer int34 = operacionesMatematicas0.restar((java.lang.Integer) (-372389960), (java.lang.Integer) (-1953902356));
        java.lang.Integer int37 = operacionesMatematicas0.dividir((java.lang.Integer) 1107075312, (java.lang.Integer) 1346231);
        java.lang.Integer int40 = operacionesMatematicas0.sumar((java.lang.Integer) 7809, (java.lang.Integer) 532602761);
        java.lang.Integer int43 = operacionesMatematicas0.dividir((java.lang.Integer) (-221508004), (java.lang.Integer) (-222612));
        java.lang.Integer int46 = operacionesMatematicas0.restar((java.lang.Integer) (-27186), (java.lang.Integer) 844551780);
        java.lang.Integer int49 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1358028), (java.lang.Integer) 1646120973);
        java.lang.Integer int52 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1360503017), (java.lang.Integer) (-1));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-86631468) + "'", int22.equals((-86631468)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-103) + "'", int25.equals((-103)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 678545 + "'", int28.equals(678545));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 14842 + "'", int31.equals(14842));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 1581512396 + "'", int34.equals(1581512396));
        org.junit.Assert.assertTrue("'" + int37 + "' != '" + 822 + "'", int37.equals(822));
        org.junit.Assert.assertTrue("'" + int40 + "' != '" + 532610570 + "'", int40.equals(532610570));
        org.junit.Assert.assertTrue("'" + int43 + "' != '" + 995 + "'", int43.equals(995));
        org.junit.Assert.assertTrue("'" + int46 + "' != '" + (-844578966) + "'", int46.equals((-844578966)));
        org.junit.Assert.assertTrue("'" + int49 + "' != '" + 565239204 + "'", int49.equals(565239204));
        org.junit.Assert.assertTrue("'" + int52 + "' != '" + 1360503017 + "'", int52.equals(1360503017));
    }

    @Test
    public void test041() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test041");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) (-674376), (java.lang.Integer) 166122);
        java.lang.Integer int24 = operacionesMatematicas0.dividir((java.lang.Integer) (-1362728792), (java.lang.Integer) 691719053);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertNotNull(wildcardClass18);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-840498) + "'", int21.equals((-840498)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1) + "'", int24.equals((-1)));
    }

    @Test
    public void test042() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test042");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1913));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-300960), (java.lang.Integer) (-17));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 1195056035, (java.lang.Integer) 1310);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-618713097), (java.lang.Integer) 10068892);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) (-1768014064));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 117 + "'", int9.equals(117));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-300943) + "'", int12.equals((-300943)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1195057345 + "'", int15.equals(1195057345));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-608644205) + "'", int19.equals((-608644205)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1768014064 + "'", int22.equals(1768014064));
    }

    @Test
    public void test043() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test043");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 202);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) (-2030));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) (-87100372), (java.lang.Integer) 2118110076);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1 + "'", int15.equals(1));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 677084 + "'", int18.equals(677084));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 2031009704 + "'", int22.equals(2031009704));
    }

    @Test
    public void test044() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test044");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 10, (java.lang.Integer) 302);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-676594), (java.lang.Integer) 298);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) (-2125425896), (java.lang.Integer) (-848979310));
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) (-1477849338), (java.lang.Integer) (-2091737468));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) 644427234, (java.lang.Integer) (-904417));
        java.lang.Integer int31 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-747185024), (java.lang.Integer) (-835045696));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 3020 + "'", int15.equals(3020));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-676296) + "'", int18.equals((-676296)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1320562090 + "'", int21.equals(1320562090));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-712) + "'", int28.equals((-712)));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-1599578112) + "'", int31.equals((-1599578112)));
    }

    @Test
    public void test045() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test045");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 1079, (java.lang.Integer) 679827);
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) 3274, (java.lang.Integer) 333);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) (-593256750), (java.lang.Integer) 961562281);
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        java.lang.Integer int27 = operacionesMatematicas0.restar((java.lang.Integer) (-139407012), (java.lang.Integer) 16335);
        java.lang.Class<?> wildcardClass28 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-678748) + "'", int17.equals((-678748)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 2941 + "'", int20.equals(2941));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 0 + "'", int23.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass24);
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + (-139423347) + "'", int27.equals((-139423347)));
        org.junit.Assert.assertNotNull(wildcardClass28);
    }

    @Test
    public void test046() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test046");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2, (java.lang.Integer) (-1));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) 338956);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 1738534581, (java.lang.Integer) 222612);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) (-674366), (java.lang.Integer) 138124938);
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) 129067, (java.lang.Integer) (-485640240));
        java.lang.Integer int28 = operacionesMatematicas0.sumar((java.lang.Integer) 227358, (java.lang.Integer) (-129843968));
        java.lang.Integer int31 = operacionesMatematicas0.sumar((java.lang.Integer) 1806993207, (java.lang.Integer) (-1680870648));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-2) + "'", int13.equals((-2)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1023986076 + "'", int16.equals(1023986076));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 7809 + "'", int19.equals(7809));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-138799304) + "'", int22.equals((-138799304)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 485769307 + "'", int25.equals(485769307));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-129616610) + "'", int28.equals((-129616610)));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 126122559 + "'", int31.equals(126122559));
    }

    @Test
    public void test047() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test047");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) (-200282), (java.lang.Integer) (-702061));
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-203859113), (java.lang.Integer) 953704);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 340526759, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-1355330679), (java.lang.Integer) (-1196138299));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-966916520) + "'", int10.equals((-966916520)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1 + "'", int16.equals(1));
    }

    @Test
    public void test048() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test048");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-203657735), (java.lang.Integer) (-98975152));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-872882432), (java.lang.Integer) 1374);
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) (-202978621), (java.lang.Integer) (-747018902));
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2010197328, (java.lang.Integer) 166);
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        try {
            java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) (-30), (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: null");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-104682583) + "'", int9.equals((-104682583)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-635285) + "'", int12.equals((-635285)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-949997523) + "'", int16.equals((-949997523)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2010197494 + "'", int19.equals(2010197494));
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test049() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test049");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-203658532), (java.lang.Integer) 797);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 138125136, (java.lang.Integer) (-198));
        java.lang.Integer int24 = operacionesMatematicas0.restar((java.lang.Integer) 459121771, (java.lang.Integer) (-4755287));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-203657735) + "'", int18.equals((-203657735)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 138124938 + "'", int21.equals(138124938));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 463877058 + "'", int24.equals(463877058));
    }

    @Test
    public void test050() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test050");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) (-1));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 2030, (java.lang.Integer) 1);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-952340332), (java.lang.Integer) 2029);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-1059362566), (java.lang.Integer) 1377181793);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 2108530673, (java.lang.Integer) (-1419118905));
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) (-691436), (java.lang.Integer) (-563084124));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2029 + "'", int10.equals(2029));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-952342361) + "'", int13.equals((-952342361)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1) + "'", int19.equals((-1)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 562392688 + "'", int22.equals(562392688));
    }

    @Test
    public void test051() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test051");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-98975152), (java.lang.Integer) (-300960));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1566890908), (java.lang.Integer) (-1171096232));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-1869099668), (java.lang.Integer) 1125242523);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1963548160 + "'", int9.equals(1963548160));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1772181088 + "'", int12.equals(1772181088));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1) + "'", int15.equals((-1)));
    }

    @Test
    public void test052() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test052");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.restar((java.lang.Integer) (-3160), (java.lang.Integer) (-203662590));
        java.lang.Integer int11 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1913), (java.lang.Integer) 191);
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-2125259776), (java.lang.Integer) (-2010186930));
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 1592563465, (java.lang.Integer) 1954931892);
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 1686302893, (java.lang.Integer) 2122583520);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 203659430 + "'", int8.equals(203659430));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-365383) + "'", int11.equals((-365383)));
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 159520590 + "'", int15.equals(159520590));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
    }

    @Test
    public void test053() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test053");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-223), (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-4060), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-202979700), (java.lang.Integer) 386);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1913), (java.lang.Integer) (-203658612));
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 1025515084, (java.lang.Integer) 134082361);
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1255479873, (java.lang.Integer) 2354);
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1599347695), (java.lang.Integer) 1310);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223 + "'", int6.equals(223));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2) + "'", int9.equals((-2)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-202979314) + "'", int12.equals((-202979314)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1243099180) + "'", int15.equals((-1243099180)));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 1159597445 + "'", int20.equals(1159597445));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 462121394 + "'", int23.equals(462121394));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 798559998 + "'", int26.equals(798559998));
    }

    @Test
    public void test054() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test054");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass6 = operacionesMatematicas0.getClass();
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 198, (java.lang.Integer) 83);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 16434 + "'", int9.equals(16434));
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test055() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test055");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-2031), (java.lang.Integer) (-100));
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-391), (java.lang.Integer) 191);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-2131) + "'", int10.equals((-2131)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-74681) + "'", int13.equals((-74681)));
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test056() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test056");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 1678, (java.lang.Integer) (-304));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 44540629, (java.lang.Integer) (-833949417));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 8, (java.lang.Integer) 1475499481);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1374 + "'", int9.equals(1374));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 878490046 + "'", int12.equals(878490046));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-1475499473) + "'", int16.equals((-1475499473)));
    }

    @Test
    public void test057() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test057");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.dividir((java.lang.Integer) (-25), (java.lang.Integer) (-23417874));
        java.lang.Integer int17 = operacionesMatematicas0.dividir((java.lang.Integer) 1346231, (java.lang.Integer) (-202980456));
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) (-134840), (java.lang.Integer) (-1377181541));
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) 3021, (java.lang.Integer) 667236676);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14.equals(0));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 0 + "'", int20.equals(0));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-667233655) + "'", int23.equals((-667233655)));
    }

    @Test
    public void test058() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test058");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 2354, (java.lang.Integer) (-403));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-277440624), (java.lang.Integer) (-594099455));
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) (-729019393), (java.lang.Integer) 372534577);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-5) + "'", int12.equals((-5)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-750172272) + "'", int17.equals((-750172272)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-356484816) + "'", int20.equals((-356484816)));
    }

    @Test
    public void test059() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test059");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) 2030, (java.lang.Integer) 688);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) (-2233));
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.sumar((java.lang.Integer) 298, (java.lang.Integer) 0);
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) 853185637, (java.lang.Integer) 1);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 151006688, (java.lang.Integer) (-1815878059));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7.equals(2));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 225533 + "'", int10.equals(225533));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 298 + "'", int14.equals(298));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 853185638 + "'", int17.equals(853185638));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-1664871371) + "'", int20.equals((-1664871371)));
    }

    @Test
    public void test060() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test060");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 679827);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-86407950), (java.lang.Integer) 419736786);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-485640240), (java.lang.Integer) 677084);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 199, (java.lang.Integer) 569427227);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 145112, (java.lang.Integer) (-343065842));
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 65, (java.lang.Integer) (-141165408));
        java.lang.Integer int24 = operacionesMatematicas0.dividir((java.lang.Integer) 1567919351, (java.lang.Integer) (-1440498544));
        java.lang.Integer int27 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-47712228), (java.lang.Integer) (-1230162471));
        java.lang.Integer int30 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-4372312), (java.lang.Integer) (-2548));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-679726) + "'", int6.equals((-679726)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-835045696) + "'", int12.equals((-835045696)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-569427028) + "'", int15.equals((-569427028)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 343210954 + "'", int18.equals(343210954));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-141165343) + "'", int21.equals((-141165343)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1) + "'", int24.equals((-1)));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 1881873340 + "'", int27.equals(1881873340));
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + (-1744250912) + "'", int30.equals((-1744250912)));
    }

    @Test
    public void test061() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test061");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 2354, (java.lang.Integer) (-403));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 1988598756, (java.lang.Integer) 6295);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 137901733, (java.lang.Integer) 1761955219);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) (-4060), (java.lang.Integer) 1981811896);
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-5) + "'", int12.equals((-5)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 315901 + "'", int16.equals(315901));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1592787391 + "'", int19.equals(1592787391));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1981807836 + "'", int22.equals(1981807836));
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test062() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test062");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) (-872882432), (java.lang.Integer) (-6763));
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) 30107083, (java.lang.Integer) (-1566668296));
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        java.lang.Integer int27 = operacionesMatematicas0.restar((java.lang.Integer) (-2006112533), (java.lang.Integer) 263);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 129067 + "'", int20.equals(129067));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-1536561213) + "'", int23.equals((-1536561213)));
        org.junit.Assert.assertNotNull(wildcardClass24);
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + (-2006112796) + "'", int27.equals((-2006112796)));
    }

    @Test
    public void test063() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test063");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 674850, (java.lang.Integer) (-203658532));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 338956, (java.lang.Integer) (-3030));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1796254192), (java.lang.Integer) (-138799304));
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 791133635, (java.lang.Integer) (-674376));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1980658657, (java.lang.Integer) 326559702);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1027036680) + "'", int12.equals((-1027036680)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 213271424 + "'", int15.equals(213271424));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 791808011 + "'", int18.equals(791808011));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-681908970) + "'", int22.equals((-681908970)));
    }

    @Test
    public void test064() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test064");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) (-3), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.sumar((java.lang.Integer) 703051, (java.lang.Integer) 222308);
        java.lang.Integer int14 = operacionesMatematicas0.restar((java.lang.Integer) (-203654655), (java.lang.Integer) (-895373708));
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) (-319241832), (java.lang.Integer) (-220530924));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-3) + "'", int7.equals((-3)));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 925359 + "'", int11.equals(925359));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 691719053 + "'", int14.equals(691719053));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-98710908) + "'", int17.equals((-98710908)));
    }

    @Test
    public void test065() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test065");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 1023986076, (java.lang.Integer) 28000);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1729412004, (java.lang.Integer) (-342852211));
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 216150938, (java.lang.Integer) (-606522767));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 1024014076 + "'", int7.equals(1024014076));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-28676780) + "'", int10.equals((-28676780)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 822673705 + "'", int13.equals(822673705));
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test066() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test066");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99990, (java.lang.Integer) 14);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-201378), (java.lang.Integer) (-203657735));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-241953962), (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 100004 + "'", int9.equals(100004));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-203859113) + "'", int12.equals((-203859113)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-241953962) + "'", int15.equals((-241953962)));
    }

    @Test
    public void test067() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test067");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 374697400, (java.lang.Integer) 473676);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 44906012);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 3233, (java.lang.Integer) 1771576096);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 222612);
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) (-312058070), (java.lang.Integer) (-1362290048));
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) 1933397394, (java.lang.Integer) 680609675);
        java.lang.Integer int31 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1180745982), (java.lang.Integer) (-78427168));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 791 + "'", int13.equals(791));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-44906012) + "'", int16.equals((-44906012)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1771579329 + "'", int19.equals(1771579329));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-222612) + "'", int22.equals((-222612)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 1252787719 + "'", int28.equals(1252787719));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 1987229632 + "'", int31.equals(1987229632));
    }

    @Test
    public void test068() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test068");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2000, (java.lang.Integer) (-50702028));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-105358747), (java.lang.Integer) (-1402661432));
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-610278), (java.lang.Integer) (-373730));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-1119229976) + "'", int16.equals((-1119229976)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-984008) + "'", int19.equals((-984008)));
    }

    @Test
    public void test069() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test069");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 101);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 2029, (java.lang.Integer) 2029);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) 99692);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-28176), (java.lang.Integer) 315901);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 9812853, (java.lang.Integer) 342371593);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 101 + "'", int4.equals(101));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 4058 + "'", int7.equals(4058));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 30106984 + "'", int10.equals(30106984));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-344077) + "'", int13.equals((-344077)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
    }

    @Test
    public void test070() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test070");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 2354, (java.lang.Integer) (-403));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-203289362), (java.lang.Integer) 676395);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1023986076, (java.lang.Integer) (-203658612));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1062496806), (java.lang.Integer) 44477096);
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) (-1680870648), (java.lang.Integer) 44772992);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-5) + "'", int12.equals((-5)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-530028550) + "'", int16.equals((-530028550)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1515772240 + "'", int19.equals(1515772240));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1098455824 + "'", int22.equals(1098455824));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-1725643640) + "'", int25.equals((-1725643640)));
    }

    @Test
    public void test071() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test071");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 99, (java.lang.Integer) (-676396));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 200282);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-678526), (java.lang.Integer) (-1546915));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 676495 + "'", int7.equals(676495));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-200282) + "'", int10.equals((-200282)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test072() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test072");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 223403, (java.lang.Integer) (-203658530));
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203658830, (java.lang.Integer) 1351444);
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) 1702393088, (java.lang.Integer) (-358));
        java.lang.Class<?> wildcardClass27 = operacionesMatematicas0.getClass();
        java.lang.Integer int30 = operacionesMatematicas0.dividir((java.lang.Integer) (-288288441), (java.lang.Integer) 1069896492);
        java.lang.Integer int33 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) (-1304798801));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-203435127) + "'", int20.equals((-203435127)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-885379048) + "'", int23.equals((-885379048)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-4755287) + "'", int26.equals((-4755287)));
        org.junit.Assert.assertNotNull(wildcardClass27);
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + 0 + "'", int30.equals(0));
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + 0 + "'", int33.equals(0));
    }

    @Test
    public void test073() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test073");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-198), (java.lang.Integer) 198);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-98990));
        java.lang.Class<?> wildcardClass21 = operacionesMatematicas0.getClass();
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) 28345, (java.lang.Integer) (-44902128));
        java.lang.Integer int27 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1896184000), (java.lang.Integer) (-343058591));
        java.lang.Integer int30 = operacionesMatematicas0.restar((java.lang.Integer) 100004, (java.lang.Integer) (-589654622));
        java.lang.Integer int33 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1617522543, (java.lang.Integer) (-152649640));
        java.lang.Class<?> wildcardClass34 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-98890) + "'", int20.equals((-98890)));
        org.junit.Assert.assertNotNull(wildcardClass21);
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1440498544) + "'", int24.equals((-1440498544)));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + (-1893466304) + "'", int27.equals((-1893466304)));
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + 589754626 + "'", int30.equals(589754626));
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + (-7631320) + "'", int33.equals((-7631320)));
        org.junit.Assert.assertNotNull(wildcardClass34);
    }

    @Test
    public void test074() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test074");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1089, (java.lang.Integer) (-1027036680));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 23816997, (java.lang.Integer) 222308);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166111, (java.lang.Integer) 1023985958);
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) 953704, (java.lang.Integer) (-319241594));
        java.lang.Integer int31 = operacionesMatematicas0.restar((java.lang.Integer) 1919324571, (java.lang.Integer) 13135);
        java.lang.Integer int34 = operacionesMatematicas0.sumar((java.lang.Integer) 591647436, (java.lang.Integer) (-104682583));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1751447560) + "'", int19.equals((-1751447560)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-985706892) + "'", int22.equals((-985706892)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1741645850 + "'", int25.equals(1741645850));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 1919311436 + "'", int31.equals(1919311436));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 486964853 + "'", int34.equals(486964853));
    }

    @Test
    public void test075() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test075");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 990, (java.lang.Integer) (-2031));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-17), (java.lang.Integer) 243122930);
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) 216150945, (java.lang.Integer) 961562281);
        java.lang.Integer int29 = operacionesMatematicas0.sumar((java.lang.Integer) (-1362728792), (java.lang.Integer) 245049039);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 3021 + "'", int19.equals(3021));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 161877486 + "'", int22.equals(161877486));
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 1455561545 + "'", int26.equals(1455561545));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-1117679753) + "'", int29.equals((-1117679753)));
    }

    @Test
    public void test076() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test076");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 3233, (java.lang.Integer) (-676594));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-955382367), (java.lang.Integer) 2115813051);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 679827 + "'", int9.equals(679827));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1223771878 + "'", int12.equals(1223771878));
    }

    @Test
    public void test077() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test077");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) 14);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 227358, (java.lang.Integer) 223);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-2108473854), (java.lang.Integer) (-1427973632));
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) (-747018902), (java.lang.Integer) 1350225);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-275423242), (java.lang.Integer) (-857390285));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 675040 + "'", int10.equals(675040));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227581 + "'", int13.equals(227581));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-680500222) + "'", int16.equals((-680500222)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-553) + "'", int19.equals((-553)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
    }

    @Test
    public void test078() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test078");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-98975152), (java.lang.Integer) (-300960));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 200871, (java.lang.Integer) (-702061));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203177900, (java.lang.Integer) 6295);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) (-202980456), (java.lang.Integer) (-1203252856));
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) 654, (java.lang.Integer) (-26796));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1963548160 + "'", int9.equals(1963548160));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 902932 + "'", int12.equals(902932));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-895373708) + "'", int15.equals((-895373708)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-17524584) + "'", int21.equals((-17524584)));
    }

    @Test
    public void test079() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test079");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 374697400, (java.lang.Integer) 473676);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 329, (java.lang.Integer) (-144));
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) 318557266, (java.lang.Integer) (-1571260798));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 791 + "'", int13.equals(791));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-47376) + "'", int16.equals((-47376)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 0 + "'", int20.equals(0));
    }

    @Test
    public void test080() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test080");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 4058, (java.lang.Integer) 223300);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-203656745), (java.lang.Integer) 680607504);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-337228749), (java.lang.Integer) (-1964430707));
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1133064081, (java.lang.Integer) 753280484);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 1928704779, (java.lang.Integer) (-198118));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 227358 + "'", int10.equals(227358));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 476950759 + "'", int13.equals(476950759));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1627201958 + "'", int16.equals(1627201958));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1571532252) + "'", int19.equals((-1571532252)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1928902897 + "'", int22.equals(1928902897));
    }

    @Test
    public void test081() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test081");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2354, (java.lang.Integer) 2679600);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-98990), (java.lang.Integer) (-4060));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-98), (java.lang.Integer) (-134082459));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) (-1045), (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 24 + "'", int16.equals(24));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 134082361 + "'", int19.equals(134082361));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-1045) + "'", int23.equals((-1045)));
    }

    @Test
    public void test082() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test082");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 101);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-650669), (java.lang.Integer) 28000);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) 2026904576, (java.lang.Integer) (-1351061346));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-304) + "'", int6.equals((-304)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-23) + "'", int9.equals((-23)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 675843230 + "'", int12.equals(675843230));
    }

    @Test
    public void test083() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test083");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-176982808), (java.lang.Integer) (-849161664));
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) 1889283349, (java.lang.Integer) (-202980456));
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) 201427718, (java.lang.Integer) 12704);
        java.lang.Class<?> wildcardClass21 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + (-1684829696) + "'", int14.equals((-1684829696)));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 1686302893 + "'", int17.equals(1686302893));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 15855 + "'", int20.equals(15855));
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test084() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test084");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 202);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) (-2030));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 985806408, (java.lang.Integer) (-909024281));
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1926456393), (java.lang.Integer) (-569414969));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1 + "'", int15.equals(1));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 677084 + "'", int18.equals(677084));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1) + "'", int22.equals((-1)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 132108609 + "'", int25.equals(132108609));
    }

    @Test
    public void test085() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test085");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) 2030, (java.lang.Integer) 688);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) (-2233));
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.sumar((java.lang.Integer) 298, (java.lang.Integer) 0);
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 44907013, (java.lang.Integer) (-2452019));
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) (-343058591));
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) 107, (java.lang.Integer) 970067068);
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) 166122, (java.lang.Integer) (-98));
        java.lang.Class<?> wildcardClass27 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7.equals(2));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 225533 + "'", int10.equals(225533));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 298 + "'", int14.equals(298));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 47359032 + "'", int17.equals(47359032));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-343058591) + "'", int20.equals((-343058591)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-970066961) + "'", int23.equals((-970066961)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-1695) + "'", int26.equals((-1695)));
        org.junit.Assert.assertNotNull(wildcardClass27);
    }

    @Test
    public void test086() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test086");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) 223300);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-676396), (java.lang.Integer) 1678);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) 99990);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 1554526000, (java.lang.Integer) 1214037568);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-608580408), (java.lang.Integer) (-505887068));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) 952590536, (java.lang.Integer) 1868537856);
        java.lang.Integer int31 = operacionesMatematicas0.sumar((java.lang.Integer) 337227660, (java.lang.Integer) (-42632));
        java.lang.Integer int34 = operacionesMatematicas0.sumar((java.lang.Integer) 51025500, (java.lang.Integer) 1134989380);
        java.lang.Integer int37 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1187063552, (java.lang.Integer) 1566625664);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2030) + "'", int9.equals((-2030)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2679600 + "'", int13.equals(2679600));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-678074) + "'", int16.equals((-678074)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-98990) + "'", int19.equals((-98990)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 340488432 + "'", int22.equals(340488432));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-1114467476) + "'", int25.equals((-1114467476)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 337185028 + "'", int31.equals(337185028));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 1186014880 + "'", int34.equals(1186014880));
        org.junit.Assert.assertTrue("'" + int37 + "' != '" + 790396928 + "'", int37.equals(790396928));
    }

    @Test
    public void test087() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test087");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-882132), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 676495, (java.lang.Integer) 2030);
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        java.lang.Integer int27 = operacionesMatematicas0.dividir((java.lang.Integer) (-606522767), (java.lang.Integer) (-793575866));
        java.lang.Integer int30 = operacionesMatematicas0.sumar((java.lang.Integer) 1654953096, (java.lang.Integer) (-975620096));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-1480217496) + "'", int20.equals((-1480217496)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 333 + "'", int23.equals(333));
        org.junit.Assert.assertNotNull(wildcardClass24);
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 0 + "'", int27.equals(0));
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + 679333000 + "'", int30.equals(679333000));
    }

    @Test
    public void test088() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test088");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-674366));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-1027036680), (java.lang.Integer) (-28927));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 834392107, (java.lang.Integer) (-343065842));
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1427973632, (java.lang.Integer) 442626564);
        java.lang.Integer int31 = operacionesMatematicas0.restar((java.lang.Integer) (-166100), (java.lang.Integer) (-952340332));
        java.lang.Class<?> wildcardClass32 = operacionesMatematicas0.getClass();
        java.lang.Integer int35 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) 20653);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-203658532) + "'", int19.equals((-203658532)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 35504 + "'", int22.equals(35504));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-2) + "'", int25.equals((-2)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 1214289920 + "'", int28.equals(1214289920));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 952174232 + "'", int31.equals(952174232));
        org.junit.Assert.assertNotNull(wildcardClass32);
        org.junit.Assert.assertTrue("'" + int35 + "' != '" + 0 + "'", int35.equals(0));
    }

    @Test
    public void test089() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test089");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.sumar((java.lang.Integer) 200183, (java.lang.Integer) (-26796));
        java.lang.Integer int11 = operacionesMatematicas0.restar((java.lang.Integer) 1954931892, (java.lang.Integer) 302);
        java.lang.Integer int14 = operacionesMatematicas0.dividir((java.lang.Integer) 113569156, (java.lang.Integer) 58802970);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 173387 + "'", int8.equals(173387));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 1954931590 + "'", int11.equals(1954931590));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 1 + "'", int14.equals(1));
    }

    @Test
    public void test090() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test090");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 4058, (java.lang.Integer) 223300);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-203656745), (java.lang.Integer) 680607504);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-200282), (java.lang.Integer) 3451);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 227358 + "'", int10.equals(227358));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 476950759 + "'", int13.equals(476950759));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-58) + "'", int16.equals((-58)));
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test091() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test091");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-25));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-589654622), (java.lang.Integer) 376169062);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) (-164003), (java.lang.Integer) (-801458764));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-213485560) + "'", int13.equals((-213485560)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-801622767) + "'", int16.equals((-801622767)));
    }

    @Test
    public void test092() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test092");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) 14);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 227358, (java.lang.Integer) 223);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-702061), (java.lang.Integer) (-7812));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 680607504, (java.lang.Integer) 1089);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 1114404549, (java.lang.Integer) 1968816313);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 675040 + "'", int10.equals(675040));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227581 + "'", int13.equals(227581));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-694249) + "'", int16.equals((-694249)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 680606415 + "'", int19.equals(680606415));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-854411764) + "'", int22.equals((-854411764)));
    }

    @Test
    public void test093() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test093");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 173085920, (java.lang.Integer) (-157900046));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 302, (java.lang.Integer) (-203658530));
        try {
            java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-483168480), (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: null");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-1) + "'", int9.equals((-1)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
    }

    @Test
    public void test094() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test094");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) 14);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 227358, (java.lang.Integer) 223);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-2108473854), (java.lang.Integer) (-1427973632));
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-30), (java.lang.Integer) (-505887068));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 794, (java.lang.Integer) (-1377839807));
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 675040 + "'", int10.equals(675040));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227581 + "'", int13.equals(227581));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-680500222) + "'", int16.equals((-680500222)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-2003257144) + "'", int19.equals((-2003257144)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test095() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test095");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-26796), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-3260));
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-945000448), (java.lang.Integer) 2107797558);
        java.lang.Integer int29 = operacionesMatematicas0.restar((java.lang.Integer) (-203656852), (java.lang.Integer) (-507));
        java.lang.Integer int32 = operacionesMatematicas0.restar((java.lang.Integer) (-973376920), (java.lang.Integer) 669356571);
        java.lang.Class<?> wildcardClass33 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass34 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-28474) + "'", int20.equals((-28474)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-3160) + "'", int23.equals((-3160)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 1868537856 + "'", int26.equals(1868537856));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-203656345) + "'", int29.equals((-203656345)));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + (-1642733491) + "'", int32.equals((-1642733491)));
        org.junit.Assert.assertNotNull(wildcardClass33);
        org.junit.Assert.assertNotNull(wildcardClass34);
    }

    @Test
    public void test096() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test096");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) (-26796));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) (-56940188), (java.lang.Integer) (-1352758348));
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1253233916), (java.lang.Integer) 1788694010);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-86631468) + "'", int22.equals((-86631468)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 1287665128 + "'", int28.equals(1287665128));
    }

    @Test
    public void test097() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test097");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) (-1));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 2030, (java.lang.Integer) 1);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 2010926408, (java.lang.Integer) (-220971));
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) (-1325426942), (java.lang.Integer) 1803047790);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2029 + "'", int10.equals(2029));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2010705437 + "'", int13.equals(2010705437));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 477620848 + "'", int16.equals(477620848));
    }

    @Test
    public void test098() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test098");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 198, (java.lang.Integer) (-6763));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1704551204, (java.lang.Integer) (-30));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-740361595), (java.lang.Integer) (-203658727));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1704551234 + "'", int12.equals(1704551234));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-536702868) + "'", int15.equals((-536702868)));
    }

    @Test
    public void test099() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test099");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-203658532), (java.lang.Integer) 797);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 138125136, (java.lang.Integer) (-198));
        java.lang.Integer int24 = operacionesMatematicas0.dividir((java.lang.Integer) 674196, (java.lang.Integer) (-203662590));
        java.lang.Integer int27 = operacionesMatematicas0.dividir((java.lang.Integer) (-19), (java.lang.Integer) 138124047);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-203657735) + "'", int18.equals((-203657735)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 138124938 + "'", int21.equals(138124938));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 0 + "'", int24.equals(0));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 0 + "'", int27.equals(0));
    }

    @Test
    public void test100() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test100");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 100004, (java.lang.Integer) 0);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-17), (java.lang.Integer) (-403));
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-202979700), (java.lang.Integer) 1954931590);
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) (-926090540), (java.lang.Integer) (-868904844));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 100004 + "'", int12.equals(100004));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 386 + "'", int15.equals(386));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 2137056006 + "'", int18.equals(2137056006));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-57185696) + "'", int21.equals((-57185696)));
    }

    @Test
    public void test101() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test101");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-2031), (java.lang.Integer) (-100));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-679014), (java.lang.Integer) 28345);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 2108473854);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-344077));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 65, (java.lang.Integer) 1346166);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 2024853078, (java.lang.Integer) 438482334);
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) 2464, (java.lang.Integer) (-1093161598));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-2131) + "'", int10.equals((-2131)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-650669) + "'", int13.equals((-650669)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-2108473854) + "'", int16.equals((-2108473854)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1346231 + "'", int22.equals(1346231));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-1831631884) + "'", int25.equals((-1831631884)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 1093164062 + "'", int28.equals(1093164062));
    }

    @Test
    public void test102() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test102");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) 688);
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) (-594099455), (java.lang.Integer) (-5600));
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) (-9120288), (java.lang.Integer) (-2164));
        java.lang.Integer int27 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203544205, (java.lang.Integer) 0);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 12 + "'", int13.equals(12));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 222612 + "'", int17.equals(222612));
        org.junit.Assert.assertNotNull(wildcardClass18);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-594105055) + "'", int21.equals((-594105055)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-9122452) + "'", int24.equals((-9122452)));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 0 + "'", int27.equals(0));
    }

    @Test
    public void test103() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test103");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-223), (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-86408168), (java.lang.Integer) (-1480217496));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2679600, (java.lang.Integer) (-34162));
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) 1106533862);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223 + "'", int6.equals(223));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-1566625664) + "'", int9.equals((-1566625664)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1346181984) + "'", int13.equals((-1346181984)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
    }

    @Test
    public void test104() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test104");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-2030), (java.lang.Integer) 677084);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 134282741, (java.lang.Integer) 218);
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 679827, (java.lang.Integer) 44906216);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 242847, (java.lang.Integer) 0);
        java.lang.Integer int28 = operacionesMatematicas0.sumar((java.lang.Integer) 635601112, (java.lang.Integer) 2135641200);
        java.lang.Integer int31 = operacionesMatematicas0.dividir((java.lang.Integer) 1500655044, (java.lang.Integer) 3451);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-679114) + "'", int16.equals((-679114)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-791133534) + "'", int19.equals((-791133534)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-169435336) + "'", int22.equals((-169435336)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-1523724984) + "'", int28.equals((-1523724984)));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 434846 + "'", int31.equals(434846));
    }

    @Test
    public void test105() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test105");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) (-26796));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 28000, (java.lang.Integer) (-676594));
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) 3, (java.lang.Integer) (-2789518));
        java.lang.Integer int31 = operacionesMatematicas0.sumar((java.lang.Integer) 589628875, (java.lang.Integer) (-1059362566));
        java.lang.Class<?> wildcardClass32 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-86631468) + "'", int22.equals((-86631468)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 2789521 + "'", int28.equals(2789521));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-469733691) + "'", int31.equals((-469733691)));
        org.junit.Assert.assertNotNull(wildcardClass32);
    }

    @Test
    public void test106() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test106");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 203);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) 166111, (java.lang.Integer) (-872882432));
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2118109232, (java.lang.Integer) 35504);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 117, (java.lang.Integer) (-140916264));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + (-204) + "'", int4.equals((-204)));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 767787264 + "'", int10.equals(767787264));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
    }

    @Test
    public void test107() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test107");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) 14);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1024014076, (java.lang.Integer) (-204));
        java.lang.Class<?> wildcardClass15 = operacionesMatematicas0.getClass();
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 716838140, (java.lang.Integer) (-1953902356));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 675040 + "'", int10.equals(675040));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 1554526000 + "'", int14.equals(1554526000));
        org.junit.Assert.assertNotNull(wildcardClass15);
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1552672688) + "'", int18.equals((-1552672688)));
    }

    @Test
    public void test108() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test108");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.dividir((java.lang.Integer) (-25), (java.lang.Integer) (-23417874));
        java.lang.Integer int17 = operacionesMatematicas0.dividir((java.lang.Integer) 1346231, (java.lang.Integer) (-202980456));
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) 223403);
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) 532602758, (java.lang.Integer) 1861586035);
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1614295506));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14.equals(0));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 0 + "'", int20.equals(0));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-1328983277) + "'", int23.equals((-1328983277)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 0 + "'", int26.equals(0));
    }

    @Test
    public void test109() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test109");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 674850, (java.lang.Integer) (-203658532));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 338956, (java.lang.Integer) (-3030));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1796254192), (java.lang.Integer) (-138799304));
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 791133635, (java.lang.Integer) (-674376));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1027036680) + "'", int12.equals((-1027036680)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 213271424 + "'", int15.equals(213271424));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 791808011 + "'", int18.equals(791808011));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test110() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test110");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-2030), (java.lang.Integer) 677084);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 134282741, (java.lang.Integer) 218);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-337227660), (java.lang.Integer) 218);
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 218, (java.lang.Integer) 200871);
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Integer int29 = operacionesMatematicas0.sumar((java.lang.Integer) (-1033487401), (java.lang.Integer) (-674424));
        java.lang.Integer int32 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1739459091, (java.lang.Integer) 794);
        java.lang.Integer int35 = operacionesMatematicas0.dividir((java.lang.Integer) (-1681687416), (java.lang.Integer) (-1896219511));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-679114) + "'", int16.equals((-679114)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-791133534) + "'", int19.equals((-791133534)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1546915) + "'", int22.equals((-1546915)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-1034161825) + "'", int29.equals((-1034161825)));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + (-1848951058) + "'", int32.equals((-1848951058)));
        org.junit.Assert.assertTrue("'" + int35 + "' != '" + 0 + "'", int35.equals(0));
    }

    @Test
    public void test111() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test111");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-507), (java.lang.Integer) 200871);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-3030), (java.lang.Integer) 1980253234);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 44906216, (java.lang.Integer) 797);
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) (-480618), (java.lang.Integer) 342371593);
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass27 = operacionesMatematicas0.getClass();
        java.lang.Integer int30 = operacionesMatematicas0.sumar((java.lang.Integer) 105842, (java.lang.Integer) (-181673475));
        java.lang.Integer int33 = operacionesMatematicas0.sumar((java.lang.Integer) (-520011968), (java.lang.Integer) 58802970);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-201378) + "'", int16.equals((-201378)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1980250204 + "'", int19.equals(1980250204));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 44907013 + "'", int22.equals(44907013));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-342852211) + "'", int25.equals((-342852211)));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertNotNull(wildcardClass27);
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + (-181567633) + "'", int30.equals((-181567633)));
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + (-461208998) + "'", int33.equals((-461208998)));
    }

    @Test
    public void test112() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test112");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 679827);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-86407950), (java.lang.Integer) 419736786);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1612015640, (java.lang.Integer) (-1931532588));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-679726) + "'", int6.equals((-679726)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1944730656) + "'", int12.equals((-1944730656)));
    }

    @Test
    public void test113() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test113");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166122, (java.lang.Integer) (-2030));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-203658532), (java.lang.Integer) 1566625664);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 199 + "'", int6.equals(199));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-337227660) + "'", int9.equals((-337227660)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1362967132 + "'", int13.equals(1362967132));
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test114() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test114");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) (-292));
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 6450721, (java.lang.Integer) (-1988598960));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 40548, (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-882132) + "'", int16.equals((-882132)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2010197328 + "'", int19.equals(2010197328));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 40548 + "'", int22.equals(40548));
    }

    @Test
    public void test115() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test115");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 100004, (java.lang.Integer) 0);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-17), (java.lang.Integer) (-403));
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2114056976), (java.lang.Integer) 99);
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1651067840, (java.lang.Integer) (-2003257144));
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 100004 + "'", int12.equals(100004));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 386 + "'", int15.equals(386));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1161756880 + "'", int18.equals(1161756880));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1967837696) + "'", int21.equals((-1967837696)));
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test116() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test116");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1089, (java.lang.Integer) (-1027036680));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 23816997, (java.lang.Integer) 222308);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166111, (java.lang.Integer) 1023985958);
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass27 = operacionesMatematicas0.getClass();
        java.lang.Integer int30 = operacionesMatematicas0.restar((java.lang.Integer) 1262016360, (java.lang.Integer) (-985706892));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1751447560) + "'", int19.equals((-1751447560)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-985706892) + "'", int22.equals((-985706892)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1741645850 + "'", int25.equals(1741645850));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertNotNull(wildcardClass27);
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + (-2047244044) + "'", int30.equals((-2047244044)));
    }

    @Test
    public void test117() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test117");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-204), (java.lang.Integer) 12);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1546915), (java.lang.Integer) 38790450);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-890108686), (java.lang.Integer) 99);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-17) + "'", int6.equals((-17)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-40337365) + "'", int9.equals((-40337365)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2073553302 + "'", int13.equals(2073553302));
    }

    @Test
    public void test118() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test118");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) (-26796));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) (-3));
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) (-678748));
        java.lang.Integer int31 = operacionesMatematicas0.sumar((java.lang.Integer) 696288, (java.lang.Integer) (-681446));
        java.lang.Integer int34 = operacionesMatematicas0.restar((java.lang.Integer) 844551780, (java.lang.Integer) 1346166);
        java.lang.Integer int37 = operacionesMatematicas0.sumar((java.lang.Integer) (-203566734), (java.lang.Integer) (-138322199));
        java.lang.Integer int40 = operacionesMatematicas0.restar((java.lang.Integer) (-844578966), (java.lang.Integer) (-6091349));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-86631468) + "'", int22.equals((-86631468)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-103) + "'", int25.equals((-103)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 678545 + "'", int28.equals(678545));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 14842 + "'", int31.equals(14842));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 843205614 + "'", int34.equals(843205614));
        org.junit.Assert.assertTrue("'" + int37 + "' != '" + (-341888933) + "'", int37.equals((-341888933)));
        org.junit.Assert.assertTrue("'" + int40 + "' != '" + (-838487617) + "'", int40.equals((-838487617)));
    }

    @Test
    public void test119() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test119");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 4058, (java.lang.Integer) 223300);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-1080482768), (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 227358 + "'", int10.equals(227358));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1080482768) + "'", int13.equals((-1080482768)));
    }

    @Test
    public void test120() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test120");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-304), (java.lang.Integer) 12);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) (-674366), (java.lang.Integer) 10);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 6559757, (java.lang.Integer) (-204));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1980273266), (java.lang.Integer) (-1420242393));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-292) + "'", int7.equals((-292)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-674376) + "'", int10.equals((-674376)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1338190428) + "'", int13.equals((-1338190428)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 2137009314 + "'", int16.equals(2137009314));
    }

    @Test
    public void test121() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test121");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 679827, (java.lang.Integer) 129067);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-203658532), (java.lang.Integer) (-1174185152));
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) (-1751438130), (java.lang.Integer) (-278495984));
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 1662521780, (java.lang.Integer) 1728039552);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 808894 + "'", int10.equals(808894));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-2029934114) + "'", int16.equals((-2029934114)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-904405964) + "'", int19.equals((-904405964)));
    }

    @Test
    public void test122() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test122");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 473676, (java.lang.Integer) (-2030));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 20300, (java.lang.Integer) 338956);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1513001358), (java.lang.Integer) (-318557503));
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) (-2125425896), (java.lang.Integer) 27084870);
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) 318557266, (java.lang.Integer) 854019829);
        java.lang.Class<?> wildcardClass25 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-961562280) + "'", int12.equals((-961562280)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 359256 + "'", int15.equals(359256));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1077114894) + "'", int18.equals((-1077114894)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-78) + "'", int21.equals((-78)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 1172577095 + "'", int24.equals(1172577095));
        org.junit.Assert.assertNotNull(wildcardClass25);
    }

    @Test
    public void test123() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test123");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 100004, (java.lang.Integer) 0);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1562037730), (java.lang.Integer) 59959391);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2053788032, (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 100004 + "'", int12.equals(100004));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 395626786 + "'", int15.equals(395626786));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
    }

    @Test
    public void test124() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test124");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 814305812, (java.lang.Integer) 923260234);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 24360, (java.lang.Integer) (-1757505839));
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-678074), (java.lang.Integer) (-28275));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-108954422) + "'", int12.equals((-108954422)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1757481479) + "'", int15.equals((-1757481479)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-706349) + "'", int18.equals((-706349)));
    }

    @Test
    public void test125() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test125");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-882132), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) (-679114), (java.lang.Integer) 100);
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) 153262720, (java.lang.Integer) 23816997);
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1751438428, (java.lang.Integer) 6450721);
        java.lang.Class<?> wildcardClass30 = operacionesMatematicas0.getClass();
        java.lang.Integer int33 = operacionesMatematicas0.restar((java.lang.Integer) 480721350, (java.lang.Integer) (-231829672));
        java.lang.Integer int36 = operacionesMatematicas0.sumar((java.lang.Integer) (-342852211), (java.lang.Integer) 7);
        java.lang.Integer int39 = operacionesMatematicas0.sumar((java.lang.Integer) (-527912576), (java.lang.Integer) 1159597445);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-1480217496) + "'", int20.equals((-1480217496)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-679014) + "'", int23.equals((-679014)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 6 + "'", int26.equals(6));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 326559708 + "'", int29.equals(326559708));
        org.junit.Assert.assertNotNull(wildcardClass30);
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + 712551022 + "'", int33.equals(712551022));
        org.junit.Assert.assertTrue("'" + int36 + "' != '" + (-342852204) + "'", int36.equals((-342852204)));
        org.junit.Assert.assertTrue("'" + int39 + "' != '" + 631684869 + "'", int39.equals(631684869));
    }

    @Test
    public void test126() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test126");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 374697400, (java.lang.Integer) 473676);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-109415642), (java.lang.Integer) (-1090027520));
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-673143), (java.lang.Integer) (-153263374));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 791 + "'", int13.equals(791));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1242052734) + "'", int19.equals((-1242052734)));
    }

    @Test
    public void test127() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test127");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.sumar((java.lang.Integer) (-2030), (java.lang.Integer) (-674366));
        java.lang.Class<?> wildcardClass9 = operacionesMatematicas0.getClass();
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-86631468), (java.lang.Integer) 166122);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1566668296, (java.lang.Integer) (-1149207272));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + (-676396) + "'", int8.equals((-676396)));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-86797590) + "'", int12.equals((-86797590)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 868546752 + "'", int15.equals(868546752));
    }

    @Test
    public void test128() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test128");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2354, (java.lang.Integer) 2679600);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-98990), (java.lang.Integer) (-4060));
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) 2354, (java.lang.Integer) (-1090027520));
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2062891546, (java.lang.Integer) (-1944184848));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 24 + "'", int16.equals(24));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 0 + "'", int20.equals(0));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 1428868704 + "'", int23.equals(1428868704));
    }

    @Test
    public void test129() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test129");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-203658532), (java.lang.Integer) 797);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 138125136, (java.lang.Integer) (-198));
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) 2029);
        java.lang.Integer int27 = operacionesMatematicas0.multiplicar((java.lang.Integer) 42226412, (java.lang.Integer) 1592563465);
        java.lang.Integer int30 = operacionesMatematicas0.restar((java.lang.Integer) (-9351), (java.lang.Integer) 1809913974);
        java.lang.Integer int33 = operacionesMatematicas0.restar((java.lang.Integer) 203204, (java.lang.Integer) 1240110195);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-203657735) + "'", int18.equals((-203657735)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 138124938 + "'", int21.equals(138124938));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 6559757 + "'", int24.equals(6559757));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 1025515084 + "'", int27.equals(1025515084));
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + (-1809923325) + "'", int30.equals((-1809923325)));
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + (-1239906991) + "'", int33.equals((-1239906991)));
    }

    @Test
    public void test130() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test130");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-304), (java.lang.Integer) 12);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-91798), (java.lang.Integer) (-679726));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-847782612), (java.lang.Integer) (-485640339));
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-1945867648), (java.lang.Integer) 2010711732);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-292) + "'", int7.equals((-292)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-2027022092) + "'", int10.equals((-2027022092)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1 + "'", int13.equals(1));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 338387916 + "'", int16.equals(338387916));
    }

    @Test
    public void test131() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test131");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 473676, (java.lang.Integer) (-2030));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-292), (java.lang.Integer) 2108502328);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 134282741, (java.lang.Integer) (-1001435549));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2010706583, (java.lang.Integer) (-458350076));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-961562280) + "'", int12.equals((-961562280)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-867152808) + "'", int18.equals((-867152808)));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 238934108 + "'", int22.equals(238934108));
    }

    @Test
    public void test132() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test132");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 99990, (java.lang.Integer) 298);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-1868330036), (java.lang.Integer) (-45556681));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) 527278124);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 99692 + "'", int9.equals(99692));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1913886717) + "'", int13.equals((-1913886717)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
    }

    @Test
    public void test133() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test133");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-28275), (java.lang.Integer) 200184);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-1896219511), (java.lang.Integer) (-669335781));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 6823529, (java.lang.Integer) (-1243099180));
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 21210, (java.lang.Integer) 494);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1729412004 + "'", int12.equals(1729412004));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1249922709 + "'", int15.equals(1249922709));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 42 + "'", int18.equals(42));
    }

    @Test
    public void test134() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test134");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-304), (java.lang.Integer) 12);
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 532602761, (java.lang.Integer) 880885566);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-844), (java.lang.Integer) 51025500);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-292) + "'", int7.equals((-292)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
    }

    @Test
    public void test135() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test135");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.dividir((java.lang.Integer) (-689499353), (java.lang.Integer) (-1566625664));
        java.lang.Integer int11 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1346531, (java.lang.Integer) 0);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8.equals(0));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11.equals(0));
    }

    @Test
    public void test136() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test136");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) 2030, (java.lang.Integer) 688);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) (-2233));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-203656745), (java.lang.Integer) 2000);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-610278), (java.lang.Integer) 0);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-56948000), (java.lang.Integer) (-593256750));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-844), (java.lang.Integer) (-1000630274));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7.equals(2));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 225533 + "'", int10.equals(225533));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-203654745) + "'", int13.equals((-203654745)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1418013632 + "'", int19.equals(1418013632));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1576606056) + "'", int22.equals((-1576606056)));
    }

    @Test
    public void test137() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test137");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-100), (java.lang.Integer) (-2233));
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 223, (java.lang.Integer) (-2131));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-207537300), (java.lang.Integer) (-1634558791));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 343210954, (java.lang.Integer) 4738865);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2679600, (java.lang.Integer) (-729019393));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223300 + "'", int6.equals(223300));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 2354 + "'", int9.equals(2354));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 347949819 + "'", int15.equals(347949819));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-390243120) + "'", int18.equals((-390243120)));
    }

    @Test
    public void test138() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test138");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 138125136, (java.lang.Integer) 223403);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203654756, (java.lang.Integer) (-846460796));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-985707399), (java.lang.Integer) 2256);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-29247463), (java.lang.Integer) (-12511));
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 318557266, (java.lang.Integer) 86235969);
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-2058714520));
        try {
            java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) (-1253233916), (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: null");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 137901733 + "'", int7.equals(137901733));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-248222832) + "'", int10.equals((-248222832)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-985705143) + "'", int13.equals((-985705143)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 2337 + "'", int16.equals(2337));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1641019694) + "'", int19.equals((-1641019694)));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 0 + "'", int23.equals(0));
    }

    @Test
    public void test139() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test139");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-2030), (java.lang.Integer) 677084);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 134282741, (java.lang.Integer) 218);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-337227660), (java.lang.Integer) 218);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 902932, (java.lang.Integer) (-2108473854));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) (-2030), (java.lang.Integer) 1379243856);
        java.lang.Integer int31 = operacionesMatematicas0.sumar((java.lang.Integer) 408719360, (java.lang.Integer) 886390221);
        java.lang.Class<?> wildcardClass32 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-679114) + "'", int16.equals((-679114)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-791133534) + "'", int19.equals((-791133534)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1546915) + "'", int22.equals((-1546915)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 164521512 + "'", int25.equals(164521512));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 1295109581 + "'", int31.equals(1295109581));
        org.junit.Assert.assertNotNull(wildcardClass32);
    }

    @Test
    public void test140() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test140");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-9430), (java.lang.Integer) (-1695));
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1088946496), (java.lang.Integer) 2073680399);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 5 + "'", int10.equals(5));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 571960640 + "'", int13.equals(571960640));
    }

    @Test
    public void test141() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test141");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-2030), (java.lang.Integer) 677084);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 134282741, (java.lang.Integer) 218);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-337227660), (java.lang.Integer) 218);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 1771576096, (java.lang.Integer) (-203656745));
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) 681653544, (java.lang.Integer) 4079228);
        java.lang.Integer int31 = operacionesMatematicas0.dividir((java.lang.Integer) (-4), (java.lang.Integer) 462121394);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-679114) + "'", int16.equals((-679114)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-791133534) + "'", int19.equals((-791133534)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1546915) + "'", int22.equals((-1546915)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1567919351 + "'", int25.equals(1567919351));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 677574316 + "'", int28.equals(677574316));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 0 + "'", int31.equals(0));
    }

    @Test
    public void test142() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test142");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) 14);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 227358, (java.lang.Integer) 223);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-702061), (java.lang.Integer) (-7812));
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 675040 + "'", int10.equals(675040));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227581 + "'", int13.equals(227581));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-694249) + "'", int16.equals((-694249)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test143() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test143");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 676495, (java.lang.Integer) (-9));
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) 2679600, (java.lang.Integer) 204);
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-680500222), (java.lang.Integer) (-853072144));
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 0);
        java.lang.Integer int29 = operacionesMatematicas0.dividir((java.lang.Integer) 13135, (java.lang.Integer) 1577950580);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 676504 + "'", int17.equals(676504));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 13135 + "'", int20.equals(13135));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 2122583520 + "'", int23.equals(2122583520));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 0 + "'", int26.equals(0));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 0 + "'", int29.equals(0));
    }

    @Test
    public void test144() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test144");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) 0);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 959378576, (java.lang.Integer) (-618713097));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 29684000, (java.lang.Integer) 1954931892);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2 + "'", int6.equals(2));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-1734056208) + "'", int9.equals((-1734056208)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1925247892) + "'", int12.equals((-1925247892)));
    }

    @Test
    public void test145() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test145");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 1678);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) 797);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-343058591), (java.lang.Integer) 961940492);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-1418013643), (java.lang.Integer) (-1856436792));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 166122 + "'", int10.equals(166122));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-1304999083) + "'", int16.equals((-1304999083)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 438423149 + "'", int19.equals(438423149));
    }

    @Test
    public void test146() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test146");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-223), (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-86408168), (java.lang.Integer) (-1480217496));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 161877486, (java.lang.Integer) 1427973632);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-1346181984), (java.lang.Integer) 6823529);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        try {
            java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) (-1508354034), (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: null");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223 + "'", int6.equals(223));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-1566625664) + "'", int9.equals((-1566625664)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1266096146) + "'", int13.equals((-1266096146)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-197) + "'", int16.equals((-197)));
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test147() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test147");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-507), (java.lang.Integer) 200871);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-3030), (java.lang.Integer) 1980253234);
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 153262720, (java.lang.Integer) (-100));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 2107797558, (java.lang.Integer) (-44906012));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) (-391), (java.lang.Integer) 10495029);
        java.lang.Integer int31 = operacionesMatematicas0.restar((java.lang.Integer) (-278495984), (java.lang.Integer) (-922956032));
        java.lang.Class<?> wildcardClass32 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-201378) + "'", int16.equals((-201378)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1980250204 + "'", int19.equals(1980250204));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1853597184 + "'", int22.equals(1853597184));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 2062891546 + "'", int25.equals(2062891546));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 644460048 + "'", int31.equals(644460048));
        org.junit.Assert.assertNotNull(wildcardClass32);
    }

    @Test
    public void test148() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test148");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 674850, (java.lang.Integer) (-203658532));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 338956, (java.lang.Integer) (-3030));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2037583576, (java.lang.Integer) (-1853597184));
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) (-904405964), (java.lang.Integer) 314459147);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1027036680) + "'", int12.equals((-1027036680)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1922560000 + "'", int15.equals(1922560000));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-2) + "'", int19.equals((-2)));
    }

    @Test
    public void test149() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test149");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 302, (java.lang.Integer) 675054);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.sumar((java.lang.Integer) 1105768, (java.lang.Integer) 243122930);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-674752) + "'", int10.equals((-674752)));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 244228698 + "'", int14.equals(244228698));
    }

    @Test
    public void test150() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test150");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-2031), (java.lang.Integer) (-100));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-679014), (java.lang.Integer) 28345);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-304), (java.lang.Integer) (-98975152));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-2125259776), (java.lang.Integer) 166120);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) (-105355726), (java.lang.Integer) 3021);
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) (-197), (java.lang.Integer) 139904960);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-2131) + "'", int10.equals((-2131)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-650669) + "'", int13.equals((-650669)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 98974848 + "'", int16.equals(98974848));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-2125425896) + "'", int19.equals((-2125425896)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-105358747) + "'", int22.equals((-105358747)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-139905157) + "'", int25.equals((-139905157)));
    }

    @Test
    public void test151() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test151");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 2354, (java.lang.Integer) (-403));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 1983457309, (java.lang.Integer) (-104553516));
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) (-202979465));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-5) + "'", int12.equals((-5)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1878903793 + "'", int15.equals(1878903793));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 202979465 + "'", int18.equals(202979465));
    }

    @Test
    public void test152() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test152");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 1079, (java.lang.Integer) 679827);
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) 5050, (java.lang.Integer) (-1566625664));
        java.lang.Class<?> wildcardClass21 = operacionesMatematicas0.getClass();
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) 886390221, (java.lang.Integer) (-1794902748));
        java.lang.Integer int27 = operacionesMatematicas0.restar((java.lang.Integer) (-1642274501), (java.lang.Integer) 1829857832);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-678748) + "'", int17.equals((-678748)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-129843968) + "'", int20.equals((-129843968)));
        org.junit.Assert.assertNotNull(wildcardClass21);
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1937186348) + "'", int24.equals((-1937186348)));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 822834963 + "'", int27.equals(822834963));
    }

    @Test
    public void test153() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test153");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-98975152), (java.lang.Integer) (-300960));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 418510030, (java.lang.Integer) (-25));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 343210954, (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1963548160 + "'", int9.equals(1963548160));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 418510005 + "'", int13.equals(418510005));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
    }

    @Test
    public void test154() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test154");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-304));
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 191, (java.lang.Integer) 137901733);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 2010197328, (java.lang.Integer) (-133524468));
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-593256750), (java.lang.Integer) (-1429679800));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) 767787264, (java.lang.Integer) 38144331);
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 569427227 + "'", int13.equals(569427227));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-15) + "'", int16.equals((-15)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-2022936550) + "'", int19.equals((-2022936550)));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 729642933 + "'", int23.equals(729642933));
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test155() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test155");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 677082, (java.lang.Integer) 797);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-86797590), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) 990, (java.lang.Integer) 203177900);
        java.lang.Integer int32 = operacionesMatematicas0.sumar((java.lang.Integer) (-202979314), (java.lang.Integer) 2010360317);
        java.lang.Integer int35 = operacionesMatematicas0.dividir((java.lang.Integer) (-1338190225), (java.lang.Integer) 886390221);
        java.lang.Integer int38 = operacionesMatematicas0.restar((java.lang.Integer) (-86407950), (java.lang.Integer) 692422);
        java.lang.Class<?> wildcardClass39 = operacionesMatematicas0.getClass();
        java.lang.Integer int42 = operacionesMatematicas0.dividir((java.lang.Integer) (-6351532), (java.lang.Integer) (-810610436));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 849 + "'", int22.equals(849));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-717341912) + "'", int29.equals((-717341912)));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 1807381003 + "'", int32.equals(1807381003));
        org.junit.Assert.assertTrue("'" + int35 + "' != '" + (-1) + "'", int35.equals((-1)));
        org.junit.Assert.assertTrue("'" + int38 + "' != '" + (-87100372) + "'", int38.equals((-87100372)));
        org.junit.Assert.assertNotNull(wildcardClass39);
        org.junit.Assert.assertTrue("'" + int42 + "' != '" + 0 + "'", int42.equals(0));
    }

    @Test
    public void test156() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test156");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 166122, (java.lang.Integer) 166122);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.dividir((java.lang.Integer) 151554579, (java.lang.Integer) 313922379);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11.equals(0));
    }

    @Test
    public void test157() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test157");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.sumar((java.lang.Integer) (-2030), (java.lang.Integer) (-674366));
        java.lang.Integer int11 = operacionesMatematicas0.dividir((java.lang.Integer) 6559757, (java.lang.Integer) 99692);
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 14, (java.lang.Integer) 1025253456);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) (-1868612537), (java.lang.Integer) (-278495984));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + (-676396) + "'", int8.equals((-676396)));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 65 + "'", int11.equals(65));
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1025253470 + "'", int15.equals(1025253470));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 6 + "'", int18.equals(6));
    }

    @Test
    public void test158() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test158");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 473676, (java.lang.Integer) 222612);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.restar((java.lang.Integer) (-56948000), (java.lang.Integer) (-7812));
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 696288 + "'", int7.equals(696288));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-56940188) + "'", int11.equals((-56940188)));
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test159() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test159");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-676594), (java.lang.Integer) 3451);
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-810610436), (java.lang.Integer) (-1868612537));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 12 + "'", int13.equals(12));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-673143) + "'", int17.equals((-673143)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 1058002101 + "'", int20.equals(1058002101));
    }

    @Test
    public void test160() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test160");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-674366));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-1027036680), (java.lang.Integer) (-28927));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 834392107, (java.lang.Integer) (-343065842));
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1427973632, (java.lang.Integer) 442626564);
        java.lang.Integer int31 = operacionesMatematicas0.restar((java.lang.Integer) (-166100), (java.lang.Integer) (-952340332));
        java.lang.Class<?> wildcardClass32 = operacionesMatematicas0.getClass();
        java.lang.Integer int35 = operacionesMatematicas0.sumar((java.lang.Integer) 668453649, (java.lang.Integer) 44772992);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-203658532) + "'", int19.equals((-203658532)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 35504 + "'", int22.equals(35504));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-2) + "'", int25.equals((-2)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 1214289920 + "'", int28.equals(1214289920));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 952174232 + "'", int31.equals(952174232));
        org.junit.Assert.assertNotNull(wildcardClass32);
        org.junit.Assert.assertTrue("'" + int35 + "' != '" + 713226641 + "'", int35.equals(713226641));
    }

    @Test
    public void test161() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test161");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99990, (java.lang.Integer) 14);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-201378), (java.lang.Integer) (-203657735));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 1195056035, (java.lang.Integer) 2136956016);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-990242366), (java.lang.Integer) (-203054381));
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) 1615957977);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 100004 + "'", int9.equals(100004));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-203859113) + "'", int12.equals((-203859113)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-941899981) + "'", int15.equals((-941899981)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1193296747) + "'", int18.equals((-1193296747)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
    }

    @Test
    public void test162() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test162");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 14, (java.lang.Integer) (-204));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1034161825), (java.lang.Integer) 100);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 834392107);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 239705415, (java.lang.Integer) 241172932);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 218 + "'", int9.equals(218));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-336967396) + "'", int12.equals((-336967396)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 480878347 + "'", int18.equals(480878347));
    }

    @Test
    public void test163() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test163");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.dividir((java.lang.Integer) (-202979700), (java.lang.Integer) 722435795);
        java.lang.Integer int14 = operacionesMatematicas0.dividir((java.lang.Integer) (-59), (java.lang.Integer) 1599953920);
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) 698720868);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11.equals(0));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14.equals(0));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-698045814) + "'", int17.equals((-698045814)));
    }

    @Test
    public void test164() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test164");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-2030), (java.lang.Integer) 677084);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 134282741, (java.lang.Integer) 218);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-337227660), (java.lang.Integer) 218);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 902932, (java.lang.Integer) (-2108473854));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) (-2030), (java.lang.Integer) 1379243856);
        java.lang.Integer int31 = operacionesMatematicas0.sumar((java.lang.Integer) 408719360, (java.lang.Integer) 886390221);
        java.lang.Integer int34 = operacionesMatematicas0.dividir((java.lang.Integer) (-1818794811), (java.lang.Integer) 318557266);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-679114) + "'", int16.equals((-679114)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-791133534) + "'", int19.equals((-791133534)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1546915) + "'", int22.equals((-1546915)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 164521512 + "'", int25.equals(164521512));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 1295109581 + "'", int31.equals(1295109581));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + (-5) + "'", int34.equals((-5)));
    }

    @Test
    public void test165() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test165");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 1023986076, (java.lang.Integer) 28000);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 1024014076 + "'", int7.equals(1024014076));
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test166() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test166");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) (-292));
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 6450721, (java.lang.Integer) (-1988598960));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 28345, (java.lang.Integer) 359256);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-136759878), (java.lang.Integer) 13135);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-882132) + "'", int16.equals((-882132)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2010197328 + "'", int19.equals(2010197328));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1593176728 + "'", int22.equals(1593176728));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-136746743) + "'", int25.equals((-136746743)));
    }

    @Test
    public void test167() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test167");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 227581, (java.lang.Integer) 200257);
        java.lang.Integer int26 = operacionesMatematicas0.restar((java.lang.Integer) (-1737142630), (java.lang.Integer) (-887497368));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 1 + "'", int23.equals(1));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-849645262) + "'", int26.equals((-849645262)));
    }

    @Test
    public void test168() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test168");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) (-1));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 696288, (java.lang.Integer) (-6763));
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 2108023758, (java.lang.Integer) (-207537300));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 703051 + "'", int10.equals(703051));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1979406238) + "'", int13.equals((-1979406238)));
    }

    @Test
    public void test169() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test169");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 814305812, (java.lang.Integer) 923260234);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 24360, (java.lang.Integer) (-1757505839));
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 6, (java.lang.Integer) (-2017286592));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-108954422) + "'", int12.equals((-108954422)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1757481479) + "'", int15.equals((-1757481479)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 781182336 + "'", int18.equals(781182336));
    }

    @Test
    public void test170() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test170");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-223), (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-86408168), (java.lang.Integer) (-1480217496));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 2108473854, (java.lang.Integer) 99990);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-650669), (java.lang.Integer) 44906012);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 326559702, (java.lang.Integer) (-770326209));
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 153188039, (java.lang.Integer) (-985930295));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223 + "'", int6.equals(223));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-1566625664) + "'", int9.equals((-1566625664)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2108373864 + "'", int12.equals(2108373864));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-45556681) + "'", int15.equals((-45556681)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1096885911 + "'", int18.equals(1096885911));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-832742256) + "'", int21.equals((-832742256)));
    }

    @Test
    public void test171() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test171");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) (-872882432), (java.lang.Integer) (-6763));
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) 30107083, (java.lang.Integer) (-1566668296));
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) 425815, (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 129067 + "'", int20.equals(129067));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-1536561213) + "'", int23.equals((-1536561213)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 425815 + "'", int26.equals(425815));
    }

    @Test
    public void test172() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test172");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-2233));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 849, (java.lang.Integer) 925359);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-872882432), (java.lang.Integer) (-1468325642));
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-589656652), (java.lang.Integer) (-625983112));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 753280484, (java.lang.Integer) 680609675);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-674366) + "'", int9.equals((-674366)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-924510) + "'", int12.equals((-924510)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1215639764) + "'", int19.equals((-1215639764)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1268567756 + "'", int22.equals(1268567756));
    }

    @Test
    public void test173() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test173");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-98975152), (java.lang.Integer) (-300960));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-676296), (java.lang.Integer) 2108473854);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 21210, (java.lang.Integer) (-1933340520));
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 674030, (java.lang.Integer) 1545561088);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1963548160 + "'", int9.equals(1963548160));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2107797558 + "'", int12.equals(2107797558));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1933319310) + "'", int15.equals((-1933319310)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
    }

    @Test
    public void test174() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test174");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-2233));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 1167531498, (java.lang.Integer) (-304));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-674366) + "'", int9.equals((-674366)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-3840564) + "'", int12.equals((-3840564)));
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test175() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test175");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 100004, (java.lang.Integer) 0);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-17), (java.lang.Integer) (-403));
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 14, (java.lang.Integer) 2000);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 1678, (java.lang.Integer) (-203658530));
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-202979314), (java.lang.Integer) 1372443167);
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) (-565899369));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 100004 + "'", int12.equals(100004));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 386 + "'", int15.equals(386));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 28000 + "'", int18.equals(28000));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-203656852) + "'", int21.equals((-203656852)));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 2051257778 + "'", int25.equals(2051257778));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 565899369 + "'", int28.equals(565899369));
    }

    @Test
    public void test176() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test176");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-2031), (java.lang.Integer) (-100));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-679014), (java.lang.Integer) 28345);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 2108473854);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-344077));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass21 = operacionesMatematicas0.getClass();
        java.lang.Integer int24 = operacionesMatematicas0.restar((java.lang.Integer) (-949939300), (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-2131) + "'", int10.equals((-2131)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-650669) + "'", int13.equals((-650669)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-2108473854) + "'", int16.equals((-2108473854)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertNotNull(wildcardClass21);
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-949939300) + "'", int24.equals((-949939300)));
    }

    @Test
    public void test177() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test177");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 1079, (java.lang.Integer) 679827);
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) 273941315, (java.lang.Integer) (-29683782));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-678748) + "'", int17.equals((-678748)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 303625097 + "'", int20.equals(303625097));
    }

    @Test
    public void test178() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test178");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.sumar((java.lang.Integer) (-2030), (java.lang.Integer) (-674366));
        java.lang.Class<?> wildcardClass9 = operacionesMatematicas0.getClass();
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 961940492, (java.lang.Integer) (-1468950954));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-110177015), (java.lang.Integer) 426796764);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-153327714), (java.lang.Integer) (-1475834436));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + (-676396) + "'", int8.equals((-676396)));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-549252088) + "'", int12.equals((-549252088)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 316619749 + "'", int15.equals(316619749));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1679942136) + "'", int18.equals((-1679942136)));
    }

    @Test
    public void test179() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test179");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 677082, (java.lang.Integer) 797);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-86797590), (java.lang.Integer) 0);
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) 675054, (java.lang.Integer) 1963548060);
        java.lang.Integer int31 = operacionesMatematicas0.restar((java.lang.Integer) 200183, (java.lang.Integer) 4738865);
        java.lang.Integer int34 = operacionesMatematicas0.restar((java.lang.Integer) (-682294930), (java.lang.Integer) 0);
        java.lang.Integer int37 = operacionesMatematicas0.dividir((java.lang.Integer) (-826225304), (java.lang.Integer) (-1311414660));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 849 + "'", int22.equals(849));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 1050105608 + "'", int28.equals(1050105608));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-4538682) + "'", int31.equals((-4538682)));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + (-682294930) + "'", int34.equals((-682294930)));
        org.junit.Assert.assertTrue("'" + int37 + "' != '" + 0 + "'", int37.equals(0));
    }

    @Test
    public void test180() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test180");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) (-26796));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 28000, (java.lang.Integer) (-676594));
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) 3, (java.lang.Integer) (-2789518));
        java.lang.Integer int31 = operacionesMatematicas0.sumar((java.lang.Integer) 589628875, (java.lang.Integer) (-1059362566));
        java.lang.Integer int34 = operacionesMatematicas0.dividir((java.lang.Integer) (-219080448), (java.lang.Integer) (-1198851069));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-86631468) + "'", int22.equals((-86631468)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 2789521 + "'", int28.equals(2789521));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-469733691) + "'", int31.equals((-469733691)));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 0 + "'", int34.equals(0));
    }

    @Test
    public void test181() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test181");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 990, (java.lang.Integer) 302);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 7458814, (java.lang.Integer) (-676296));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) 1710, (java.lang.Integer) 168);
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) (-86797590), (java.lang.Integer) (-1646083254));
        java.lang.Integer int27 = operacionesMatematicas0.dividir((java.lang.Integer) (-1827), (java.lang.Integer) (-17));
        java.lang.Integer int30 = operacionesMatematicas0.restar((java.lang.Integer) (-1614297536), (java.lang.Integer) 705204090);
        java.lang.Class<?> wildcardClass31 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 688 + "'", int15.equals(688));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-11) + "'", int18.equals((-11)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1542 + "'", int21.equals(1542));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1732880844) + "'", int24.equals((-1732880844)));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 107 + "'", int27.equals(107));
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + 1975465670 + "'", int30.equals(1975465670));
        org.junit.Assert.assertNotNull(wildcardClass31);
    }

    @Test
    public void test182() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test182");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2354, (java.lang.Integer) 2679600);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-98990), (java.lang.Integer) (-4060));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-313922314), (java.lang.Integer) 222308);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 2010926408, (java.lang.Integer) (-951117697));
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) 0);
        java.lang.Integer int29 = operacionesMatematicas0.restar((java.lang.Integer) (-1228275729), (java.lang.Integer) 275362136);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 24 + "'", int16.equals(24));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-314144622) + "'", int19.equals((-314144622)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1059808711 + "'", int22.equals(1059808711));
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 0 + "'", int26.equals(0));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-1503637865) + "'", int29.equals((-1503637865)));
    }

    @Test
    public void test183() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test183");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-223), (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-4060), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-343065842), (java.lang.Integer) 138124938);
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) (-3712), (java.lang.Integer) 1979743289);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223 + "'", int6.equals(223));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2) + "'", int9.equals((-2)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-204940904) + "'", int12.equals((-204940904)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-1979747001) + "'", int17.equals((-1979747001)));
    }

    @Test
    public void test184() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test184");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-100), (java.lang.Integer) (-2233));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 675054, (java.lang.Integer) (-201378));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-2099619127), (java.lang.Integer) (-680500222));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 1728039552, (java.lang.Integer) 0);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 170247311, (java.lang.Integer) (-45125428));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223300 + "'", int6.equals(223300));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 473676 + "'", int9.equals(473676));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1419118905) + "'", int12.equals((-1419118905)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1728039552 + "'", int15.equals(1728039552));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 215372739 + "'", int18.equals(215372739));
    }

    @Test
    public void test185() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test185");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass4 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.sumar((java.lang.Integer) 668453649, (java.lang.Integer) 1771576096);
        java.lang.Integer int11 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-5043200), (java.lang.Integer) (-485640339));
        java.lang.Integer int14 = operacionesMatematicas0.dividir((java.lang.Integer) (-337185028), (java.lang.Integer) (-118991840));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + (-1854937551) + "'", int8.equals((-1854937551)));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 2026904576 + "'", int11.equals(2026904576));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 2 + "'", int14.equals(2));
    }

    @Test
    public void test186() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test186");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 679827);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-86407950), (java.lang.Integer) 419736786);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-485640240), (java.lang.Integer) 677084);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 199, (java.lang.Integer) 569427227);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 145112, (java.lang.Integer) (-343065842));
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 65, (java.lang.Integer) (-141165408));
        java.lang.Integer int24 = operacionesMatematicas0.dividir((java.lang.Integer) 1567919351, (java.lang.Integer) (-1440498544));
        java.lang.Integer int27 = operacionesMatematicas0.dividir((java.lang.Integer) 438482334, (java.lang.Integer) 3);
        java.lang.Integer int30 = operacionesMatematicas0.multiplicar((java.lang.Integer) 701863, (java.lang.Integer) (-955375139));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-679726) + "'", int6.equals((-679726)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-835045696) + "'", int12.equals((-835045696)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-569427028) + "'", int15.equals((-569427028)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 343210954 + "'", int18.equals(343210954));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-141165343) + "'", int21.equals((-141165343)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1) + "'", int24.equals((-1)));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 146160778 + "'", int27.equals(146160778));
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + 717969451 + "'", int30.equals(717969451));
    }

    @Test
    public void test187() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test187");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 473676, (java.lang.Integer) (-2030));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-138799294), (java.lang.Integer) (-1033487401));
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 137901045, (java.lang.Integer) (-778127));
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1556252358), (java.lang.Integer) (-374696206));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-961562280) + "'", int12.equals((-961562280)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 894688107 + "'", int15.equals(894688107));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 936480549 + "'", int18.equals(936480549));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1616341804) + "'", int21.equals((-1616341804)));
    }

    @Test
    public void test188() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test188");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-203657735), (java.lang.Integer) (-98975152));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1079, (java.lang.Integer) 222612);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 961940784, (java.lang.Integer) 314525);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-104682583) + "'", int9.equals((-104682583)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-221533) + "'", int12.equals((-221533)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 962255309 + "'", int15.equals(962255309));
    }

    @Test
    public void test189() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test189");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 199, (java.lang.Integer) 302);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-58), (java.lang.Integer) (-674366));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2010926408, (java.lang.Integer) 1352434);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 1963548045, (java.lang.Integer) 200282);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-480400), (java.lang.Integer) (-164003));
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1885225800), (java.lang.Integer) 688);
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) 591647436, (java.lang.Integer) (-8230961));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-674424) + "'", int13.equals((-674424)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-1060594160) + "'", int16.equals((-1060594160)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1963748327 + "'", int19.equals(1963748327));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 2 + "'", int22.equals(2));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 44772992 + "'", int25.equals(44772992));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 222267124 + "'", int28.equals(222267124));
    }

    @Test
    public void test190() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test190");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) (-26796));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 28000, (java.lang.Integer) (-676594));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-674424));
        java.lang.Class<?> wildcardClass29 = operacionesMatematicas0.getClass();
        java.lang.Integer int32 = operacionesMatematicas0.restar((java.lang.Integer) 1963548060, (java.lang.Integer) 44907013);
        java.lang.Class<?> wildcardClass33 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-86631468) + "'", int22.equals((-86631468)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass29);
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 1918641047 + "'", int32.equals(1918641047));
        org.junit.Assert.assertNotNull(wildcardClass33);
    }

    @Test
    public void test191() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test191");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 473676, (java.lang.Integer) 222612);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.dividir((java.lang.Integer) 775906878, (java.lang.Integer) 1983457309);
        java.lang.Integer int14 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1971875568, (java.lang.Integer) 268);
        java.lang.Integer int17 = operacionesMatematicas0.dividir((java.lang.Integer) 166, (java.lang.Integer) 811017156);
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 696288 + "'", int7.equals(696288));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11.equals(0));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 181674816 + "'", int14.equals(181674816));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test192() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test192");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 14, (java.lang.Integer) (-204));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1034161825), (java.lang.Integer) 100);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-43315734), (java.lang.Integer) (-794241824));
        try {
            java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) (-1193296747), (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: null");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 218 + "'", int9.equals(218));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-336967396) + "'", int12.equals((-336967396)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
    }

    @Test
    public void test193() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test193");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.sumar((java.lang.Integer) (-2030), (java.lang.Integer) (-674366));
        java.lang.Class<?> wildcardClass9 = operacionesMatematicas0.getClass();
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-319239361), (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-895373708), (java.lang.Integer) 990);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 83, (java.lang.Integer) 137901733);
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223403, (java.lang.Integer) (-1126066292));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + (-676396) + "'", int8.equals((-676396)));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-319241594) + "'", int12.equals((-319241594)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-904417) + "'", int15.equals((-904417)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1439058049) + "'", int18.equals((-1439058049)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1763370364) + "'", int21.equals((-1763370364)));
    }

    @Test
    public void test194() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test194");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-203657735), (java.lang.Integer) (-98975152));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1079, (java.lang.Integer) 222612);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 99692, (java.lang.Integer) 203177900);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-59233708), (java.lang.Integer) 151006688);
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) 6863566, (java.lang.Integer) 9120300);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-104682583) + "'", int9.equals((-104682583)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-221533) + "'", int12.equals((-221533)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-203078208) + "'", int15.equals((-203078208)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 91772980 + "'", int18.equals(91772980));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
    }

    @Test
    public void test195() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test195");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) 14);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 227358, (java.lang.Integer) 223);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-702061), (java.lang.Integer) (-7812));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 680607504, (java.lang.Integer) 1089);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) 972267014);
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        try {
            java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) 1771579329, (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: null");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 675040 + "'", int10.equals(675040));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227581 + "'", int13.equals(227581));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-694249) + "'", int16.equals((-694249)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 680606415 + "'", int19.equals(680606415));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test196() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test196");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 473676, (java.lang.Integer) (-2030));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-292), (java.lang.Integer) 2108502328);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-86797590), (java.lang.Integer) 674850);
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) (-136543588), (java.lang.Integer) 1439577904);
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) 1006273629, (java.lang.Integer) (-2114033914));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-961562280) + "'", int12.equals((-961562280)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-589628652) + "'", int19.equals((-589628652)));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 1303034316 + "'", int23.equals(1303034316));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 0 + "'", int26.equals(0));
    }

    @Test
    public void test197() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test197");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-100), (java.lang.Integer) (-2233));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-599940), (java.lang.Integer) (-203078208));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1224212052, (java.lang.Integer) (-454172969));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-1796353182), (java.lang.Integer) (-1115399484));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223300 + "'", int6.equals(223300));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-203678148) + "'", int9.equals((-203678148)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1678385021 + "'", int12.equals(1678385021));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1383214630 + "'", int15.equals(1383214630));
    }

    @Test
    public void test198() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test198");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) (-505887068));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2030) + "'", int9.equals((-2030)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1775677520) + "'", int12.equals((-1775677520)));
    }

    @Test
    public void test199() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test199");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-2233), (java.lang.Integer) 199);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1072115060), (java.lang.Integer) 1244115421);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-11) + "'", int10.equals((-11)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1974169892) + "'", int13.equals((-1974169892)));
    }

    @Test
    public void test200() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test200");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2000);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-11), (java.lang.Integer) (-678074));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-391), (java.lang.Integer) 975452);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-311834474), (java.lang.Integer) (-222612));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) (-114017579), (java.lang.Integer) (-1180745982));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 7458814 + "'", int12.equals(7458814));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-312057086) + "'", int18.equals((-312057086)));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1066728403 + "'", int22.equals(1066728403));
    }

    @Test
    public void test201() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test201");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 374697400, (java.lang.Integer) 473676);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 44906012);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 3233, (java.lang.Integer) 1771576096);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) (-667400994), (java.lang.Integer) 28000);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 791 + "'", int13.equals(791));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-44906012) + "'", int16.equals((-44906012)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1771579329 + "'", int19.equals(1771579329));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-667372994) + "'", int22.equals((-667372994)));
    }

    @Test
    public void test202() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test202");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-237), (java.lang.Integer) 101);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-3160), (java.lang.Integer) (-506915));
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 173387, (java.lang.Integer) (-33139200));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-23937) + "'", int16.equals((-23937)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 503755 + "'", int19.equals(503755));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 33312587 + "'", int22.equals(33312587));
    }

    @Test
    public void test203() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test203");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2000, (java.lang.Integer) (-50702028));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-105358747), (java.lang.Integer) (-1402661432));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-1017), (java.lang.Integer) (-1627618704));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-1119229976) + "'", int16.equals((-1119229976)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1627617687 + "'", int19.equals(1627617687));
    }

    @Test
    public void test204() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test204");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 202);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) (-2030));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 791, (java.lang.Integer) 677082);
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) 44906216, (java.lang.Integer) (-204));
        java.lang.Integer int29 = operacionesMatematicas0.dividir((java.lang.Integer) (-203435127), (java.lang.Integer) (-203437481));
        java.lang.Integer int32 = operacionesMatematicas0.restar((java.lang.Integer) 21210, (java.lang.Integer) 203658830);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1 + "'", int15.equals(1));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 677084 + "'", int18.equals(677084));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 44906012 + "'", int26.equals(44906012));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 0 + "'", int29.equals(0));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + (-203637620) + "'", int32.equals((-203637620)));
    }

    @Test
    public void test205() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test205");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 677082, (java.lang.Integer) 797);
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) 302, (java.lang.Integer) 0);
        java.lang.Integer int28 = operacionesMatematicas0.sumar((java.lang.Integer) (-458350076), (java.lang.Integer) (-1734233278));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 849 + "'", int22.equals(849));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 302 + "'", int25.equals(302));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 2102383942 + "'", int28.equals(2102383942));
    }

    @Test
    public void test206() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test206");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 374697400, (java.lang.Integer) 473676);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 329, (java.lang.Integer) (-144));
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-1513750879), (java.lang.Integer) 243122930);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 791 + "'", int13.equals(791));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-47376) + "'", int16.equals((-47376)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1270627949) + "'", int19.equals((-1270627949)));
    }

    @Test
    public void test207() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test207");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 218, (java.lang.Integer) 3233);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-970067068), (java.lang.Integer) 920644544);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) (-85417856));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 3451 + "'", int9.equals(3451));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-922956032) + "'", int12.equals((-922956032)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 85417856 + "'", int15.equals(85417856));
    }

    @Test
    public void test208() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test208");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-2233), (java.lang.Integer) 199);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 227358, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 28000, (java.lang.Integer) 675040);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 223403, (java.lang.Integer) 30107083);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 302, (java.lang.Integer) (-678748));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 905316668, (java.lang.Integer) 675054);
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1034161825), (java.lang.Integer) (-3241260));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-11) + "'", int10.equals((-11)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227358 + "'", int13.equals(227358));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 703040 + "'", int16.equals(703040));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1341 + "'", int25.equals(1341));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 1605572780 + "'", int28.equals(1605572780));
    }

    @Test
    public void test209() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test209");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-204940904), (java.lang.Integer) (-678074));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 7458814, (java.lang.Integer) 227581);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-127444272), (java.lang.Integer) 0);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) (-42632), (java.lang.Integer) (-742442848));
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) (-1035598570), (java.lang.Integer) 2971);
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) 813585152, (java.lang.Integer) 713226641);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1431676816 + "'", int9.equals(1431676816));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 972267014 + "'", int12.equals(972267014));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-127444272) + "'", int15.equals((-127444272)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-348569) + "'", int21.equals((-348569)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 2085094144 + "'", int24.equals(2085094144));
    }

    @Test
    public void test210() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test210");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 674850, (java.lang.Integer) (-203658532));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 338956, (java.lang.Integer) (-3030));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2233), (java.lang.Integer) (-1027036680));
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-1427466717), (java.lang.Integer) (-975620096));
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2066210485), (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1027036680) + "'", int12.equals((-1027036680)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-139629624) + "'", int15.equals((-139629624)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1891880483 + "'", int18.equals(1891880483));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
    }

    @Test
    public void test211() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test211");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-98975152), (java.lang.Integer) (-676594));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 1624322327, (java.lang.Integer) 200184);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 146 + "'", int12.equals(146));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 8114 + "'", int15.equals(8114));
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test212() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test212");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-100), (java.lang.Integer) (-2233));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1337112768));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-217172785), (java.lang.Integer) (-1985));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223300 + "'", int6.equals(223300));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 109406 + "'", int12.equals(109406));
    }

    @Test
    public void test213() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test213");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-304));
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 191, (java.lang.Integer) 137901733);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1646267980), (java.lang.Integer) 84409818);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 569427227 + "'", int13.equals(569427227));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-495012024) + "'", int16.equals((-495012024)));
    }

    @Test
    public void test214() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test214");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) (-139407012), (java.lang.Integer) 203658830);
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) 342371593, (java.lang.Integer) (-221533));
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 614351822, (java.lang.Integer) (-139407012));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-343065842) + "'", int17.equals((-343065842)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 342593126 + "'", int20.equals(342593126));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-4) + "'", int23.equals((-4)));
    }

    @Test
    public void test215() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test215");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-674366));
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 342593126, (java.lang.Integer) (-337185028));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-203658532) + "'", int19.equals((-203658532)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 679778154 + "'", int22.equals(679778154));
    }

    @Test
    public void test216() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test216");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) (-3), (java.lang.Integer) 0);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 1593226934, (java.lang.Integer) (-1338190225));
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 315901, (java.lang.Integer) (-973393354));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-3) + "'", int7.equals((-3)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-1363550137) + "'", int10.equals((-1363550137)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-2045332130) + "'", int13.equals((-2045332130)));
    }

    @Test
    public void test217() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test217");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) 688);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) (-2031), (java.lang.Integer) 223300);
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) 191, (java.lang.Integer) 3451);
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) (-86797590), (java.lang.Integer) (-1072091024));
        java.lang.Class<?> wildcardClass27 = operacionesMatematicas0.getClass();
        java.lang.Integer int30 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1737910004, (java.lang.Integer) 1325219516);
        java.lang.Integer int33 = operacionesMatematicas0.restar((java.lang.Integer) (-101594), (java.lang.Integer) (-1230162471));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 12 + "'", int13.equals(12));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 222612 + "'", int17.equals(222612));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 221269 + "'", int20.equals(221269));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-3260) + "'", int23.equals((-3260)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 0 + "'", int26.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass27);
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + (-1769557200) + "'", int30.equals((-1769557200)));
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + 1230060877 + "'", int33.equals(1230060877));
    }

    @Test
    public void test218() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test218");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 198, (java.lang.Integer) (-3030));
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 1115055585, (java.lang.Integer) (-311834474));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-129843968), (java.lang.Integer) (-136413280));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) (-1985326896), (java.lang.Integer) 1310);
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) (-29513), (java.lang.Integer) 849);
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-599940) + "'", int13.equals((-599940)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1426890059 + "'", int16.equals(1426890059));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 6569312 + "'", int19.equals(6569312));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1985325586) + "'", int22.equals((-1985325586)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-34) + "'", int25.equals((-34)));
        org.junit.Assert.assertNotNull(wildcardClass26);
    }

    @Test
    public void test219() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test219");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 473676, (java.lang.Integer) (-2030));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-292), (java.lang.Integer) 2108502328);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 1029847456, (java.lang.Integer) (-505));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-961562280) + "'", int12.equals((-961562280)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1029846951 + "'", int18.equals(1029846951));
    }

    @Test
    public void test220() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test220");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-98975152), (java.lang.Integer) (-676594));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 1624322327, (java.lang.Integer) 200184);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 1771579329, (java.lang.Integer) 1710);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 23816997, (java.lang.Integer) (-1667413622));
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-246420722), (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 146 + "'", int12.equals(146));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 8114 + "'", int15.equals(8114));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1771581039 + "'", int18.equals(1771581039));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1643596625) + "'", int21.equals((-1643596625)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 0 + "'", int24.equals(0));
    }

    @Test
    public void test221() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test221");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-2233));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-676396), (java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 24, (java.lang.Integer) 677084);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) 419736786);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-1796353182), (java.lang.Integer) 29745240);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-674366) + "'", int9.equals((-674366)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-6763) + "'", int12.equals((-6763)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-60) + "'", int22.equals((-60)));
    }

    @Test
    public void test222() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test222");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-98975152), (java.lang.Integer) (-300960));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 200871, (java.lang.Integer) (-702061));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 1566625664, (java.lang.Integer) 203654756);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 1346531, (java.lang.Integer) (-530028550));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1963548160 + "'", int9.equals(1963548160));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 902932 + "'", int12.equals(902932));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1770280420 + "'", int15.equals(1770280420));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-528682019) + "'", int18.equals((-528682019)));
    }

    @Test
    public void test223() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test223");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 473676, (java.lang.Integer) (-2030));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-292), (java.lang.Integer) 2108502328);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-86797590), (java.lang.Integer) 674850);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 225533, (java.lang.Integer) 0);
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) 1089, (java.lang.Integer) (-1072088893));
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Integer int29 = operacionesMatematicas0.dividir((java.lang.Integer) (-1465414824), (java.lang.Integer) (-455475856));
        java.lang.Integer int32 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-8), (java.lang.Integer) 1349710685);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-961562280) + "'", int12.equals((-961562280)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-589628652) + "'", int19.equals((-589628652)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 225533 + "'", int22.equals(225533));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1072089982 + "'", int25.equals(1072089982));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 3 + "'", int29.equals(3));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 2087216408 + "'", int32.equals(2087216408));
    }

    @Test
    public void test224() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test224");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 674850, (java.lang.Integer) (-203658532));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 338956, (java.lang.Integer) (-3030));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2233), (java.lang.Integer) (-1027036680));
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-678748), (java.lang.Integer) (-198118));
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-203859113), (java.lang.Integer) (-9430));
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) (-92868540), (java.lang.Integer) (-1019770691));
        java.lang.Integer int27 = operacionesMatematicas0.dividir((java.lang.Integer) (-619745648), (java.lang.Integer) 99990);
        java.lang.Integer int30 = operacionesMatematicas0.dividir((java.lang.Integer) (-1600748477), (java.lang.Integer) (-2093124336));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1027036680) + "'", int12.equals((-1027036680)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-139629624) + "'", int15.equals((-139629624)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-480630) + "'", int18.equals((-480630)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1753913018) + "'", int21.equals((-1753913018)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1112639231) + "'", int24.equals((-1112639231)));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + (-6198) + "'", int27.equals((-6198)));
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + 0 + "'", int30.equals(0));
    }

    @Test
    public void test225() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test225");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 101);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-650669), (java.lang.Integer) 28000);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-304) + "'", int6.equals((-304)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-23) + "'", int9.equals((-23)));
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test226() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test226");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-204940904), (java.lang.Integer) (-678074));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-680500222), (java.lang.Integer) 453522102);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 638678568);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 200282, (java.lang.Integer) (-129616610));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1431676816 + "'", int9.equals(1431676816));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1937735020 + "'", int12.equals(1937735020));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-638678568) + "'", int15.equals((-638678568)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-129416328) + "'", int18.equals((-129416328)));
    }

    @Test
    public void test227() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test227");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 1079, (java.lang.Integer) 679827);
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-198118), (java.lang.Integer) 674850);
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) 166120, (java.lang.Integer) 200282);
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        java.lang.Integer int27 = operacionesMatematicas0.dividir((java.lang.Integer) 1079, (java.lang.Integer) 241172932);
        java.lang.Integer int30 = operacionesMatematicas0.restar((java.lang.Integer) 88707587, (java.lang.Integer) (-223));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-678748) + "'", int17.equals((-678748)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-872968) + "'", int20.equals((-872968)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-34162) + "'", int23.equals((-34162)));
        org.junit.Assert.assertNotNull(wildcardClass24);
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 0 + "'", int27.equals(0));
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + 88707810 + "'", int30.equals(88707810));
    }

    @Test
    public void test228() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test228");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 677082, (java.lang.Integer) 797);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-86797590), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) 990, (java.lang.Integer) 203177900);
        java.lang.Integer int32 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99990, (java.lang.Integer) 2074781511);
        java.lang.Integer int35 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1704563908, (java.lang.Integer) 0);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 849 + "'", int22.equals(849));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-717341912) + "'", int29.equals((-717341912)));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 1892953498 + "'", int32.equals(1892953498));
        org.junit.Assert.assertTrue("'" + int35 + "' != '" + 0 + "'", int35.equals(0));
    }

    @Test
    public void test229() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test229");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 791, (java.lang.Integer) 222612);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-28474), (java.lang.Integer) 2108502328);
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-453522300), (java.lang.Integer) (-1988598960));
        java.lang.Integer int31 = operacionesMatematicas0.dividir((java.lang.Integer) (-485636182), (java.lang.Integer) (-1915811892));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 223403 + "'", int22.equals(223403));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 2108473854 + "'", int25.equals(2108473854));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-479791808) + "'", int28.equals((-479791808)));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 0 + "'", int31.equals(0));
    }

    @Test
    public void test230() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test230");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-223), (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-4060), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-202979700), (java.lang.Integer) 386);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1913), (java.lang.Integer) (-203658612));
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 21210, (java.lang.Integer) 1567906720);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223 + "'", int6.equals(223));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2) + "'", int9.equals((-2)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-202979314) + "'", int12.equals((-202979314)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1243099180) + "'", int15.equals((-1243099180)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
    }

    @Test
    public void test231() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test231");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2000);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-11), (java.lang.Integer) (-678074));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 10, (java.lang.Integer) (-91808));
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-320292228), (java.lang.Integer) (-673143));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1027036680));
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 7458814 + "'", int12.equals(7458814));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-91798) + "'", int15.equals((-91798)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-319619085) + "'", int18.equals((-319619085)));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test232() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test232");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) (-2452019), (java.lang.Integer) 591647436);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 972267014, (java.lang.Integer) 153721199);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-1620334347), (java.lang.Integer) (-22));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-594099455) + "'", int6.equals((-594099455)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 6 + "'", int9.equals(6));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1620334369) + "'", int12.equals((-1620334369)));
    }

    @Test
    public void test233() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test233");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-100), (java.lang.Integer) (-2233));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1337112768));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-789783992), (java.lang.Integer) (-868904844));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223300 + "'", int6.equals(223300));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1996712096 + "'", int13.equals(1996712096));
    }

    @Test
    public void test234() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test234");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 99, (java.lang.Integer) (-676396));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 200282);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-678526), (java.lang.Integer) (-1546915));
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) (-1226993776), (java.lang.Integer) 0);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 198911, (java.lang.Integer) 499871);
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1753913018), (java.lang.Integer) 38327);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 676495 + "'", int7.equals(676495));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-200282) + "'", int10.equals((-200282)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-1226993776) + "'", int16.equals((-1226993776)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1691091190) + "'", int22.equals((-1691091190)));
    }

    @Test
    public void test235() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test235");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 1678);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-204), (java.lang.Integer) (-669335781));
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) 2679600);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-28275), (java.lang.Integer) (-847810496));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) (-1545028408), (java.lang.Integer) 1863220498);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 166122 + "'", int10.equals(166122));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 669335577 + "'", int13.equals(669335577));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 847782221 + "'", int19.equals(847782221));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 318192090 + "'", int22.equals(318192090));
    }

    @Test
    public void test236() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test236");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2000);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-11), (java.lang.Integer) (-678074));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-391), (java.lang.Integer) 975452);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 438482334, (java.lang.Integer) (-702061));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) 110594088, (java.lang.Integer) (-1377181541));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 7458814 + "'", int12.equals(7458814));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 437780273 + "'", int18.equals(437780273));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1487775629 + "'", int21.equals(1487775629));
    }

    @Test
    public void test237() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test237");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-203657735), (java.lang.Integer) (-98975152));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-872882432), (java.lang.Integer) 1374);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-203), (java.lang.Integer) 3233);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) (-223), (java.lang.Integer) 203658830);
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) (-2114033914), (java.lang.Integer) 898431027);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-104682583) + "'", int9.equals((-104682583)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-635285) + "'", int12.equals((-635285)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1282502355 + "'", int21.equals(1282502355));
    }

    @Test
    public void test238() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test238");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2, (java.lang.Integer) (-1));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) 338956);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-300960), (java.lang.Integer) 2679600);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 298, (java.lang.Integer) (-28474));
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203658830, (java.lang.Integer) (-702086));
        java.lang.Integer int29 = operacionesMatematicas0.dividir((java.lang.Integer) 88707587, (java.lang.Integer) (-550455336));
        java.lang.Class<?> wildcardClass30 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-2) + "'", int13.equals((-2)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1023986076 + "'", int16.equals(1023986076));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1001435648 + "'", int19.equals(1001435648));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-28176) + "'", int22.equals((-28176)));
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 2037899052 + "'", int26.equals(2037899052));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 0 + "'", int29.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass30);
    }

    @Test
    public void test239() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test239");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 100004, (java.lang.Integer) 0);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 3021, (java.lang.Integer) 100);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 1242635283, (java.lang.Integer) (-204));
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) (-17524584), (java.lang.Integer) 527278124);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 100004 + "'", int12.equals(100004));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 30 + "'", int15.equals(30));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-6091349) + "'", int18.equals((-6091349)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 509753540 + "'", int21.equals(509753540));
    }

    @Test
    public void test240() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test240");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-204940904), (java.lang.Integer) (-678074));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-680500222), (java.lang.Integer) 453522102);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-203567463));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1431676816 + "'", int9.equals(1431676816));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1937735020 + "'", int12.equals(1937735020));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
    }

    @Test
    public void test241() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test241");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-676594), (java.lang.Integer) 1678);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-1), (java.lang.Integer) 3233);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-165904), (java.lang.Integer) 6295);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 42226412, (java.lang.Integer) (-43315734));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) (-313922314), (java.lang.Integer) 2108502328);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 168, (java.lang.Integer) (-1831631884));
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-403) + "'", int10.equals((-403)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-172199) + "'", int16.equals((-172199)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-372389960) + "'", int19.equals((-372389960)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1794580014 + "'", int22.equals(1794580014));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1523488800 + "'", int25.equals(1523488800));
        org.junit.Assert.assertNotNull(wildcardClass26);
    }

    @Test
    public void test242() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test242");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 166111, (java.lang.Integer) (-594099455));
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-1338190428), (java.lang.Integer) (-203));
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 153262720, (java.lang.Integer) (-74681));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 99692);
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-593933344) + "'", int13.equals((-593933344)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-1338190225) + "'", int16.equals((-1338190225)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 153188039 + "'", int19.equals(153188039));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 10068892 + "'", int22.equals(10068892));
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test243() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test243");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 1678);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-204), (java.lang.Integer) (-669335781));
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 529010916, (java.lang.Integer) 87104372);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 166122 + "'", int10.equals(166122));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 669335577 + "'", int13.equals(669335577));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 616115288 + "'", int16.equals(616115288));
    }

    @Test
    public void test244() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test244");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-203), (java.lang.Integer) 1000);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 19735044, (java.lang.Integer) (-337227660));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 4058, (java.lang.Integer) 2109130613);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-317492616) + "'", int13.equals((-317492616)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-1017793374) + "'", int16.equals((-1017793374)));
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test245() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test245");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-198), (java.lang.Integer) 198);
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-245718859), (java.lang.Integer) 701863);
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1125633776, (java.lang.Integer) 753394741);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-246420722) + "'", int20.equals((-246420722)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 315332528 + "'", int23.equals(315332528));
    }

    @Test
    public void test246() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test246");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-98890), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test247() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test247");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-2233));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) 1980253234, (java.lang.Integer) 30107083);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 198, (java.lang.Integer) 1199317608);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 791133635, (java.lang.Integer) 1144325196);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-674366) + "'", int9.equals((-674366)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2010360317 + "'", int12.equals(2010360317));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1199317410) + "'", int15.equals((-1199317410)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
    }

    @Test
    public void test248() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test248");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1050661100, (java.lang.Integer) 1985);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2030) + "'", int9.equals((-2030)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 529300 + "'", int13.equals(529300));
    }

    @Test
    public void test249() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test249");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-100), (java.lang.Integer) (-2233));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 675054, (java.lang.Integer) (-201378));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223300 + "'", int6.equals(223300));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 473676 + "'", int9.equals(473676));
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test250() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test250");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.sumar((java.lang.Integer) 200183, (java.lang.Integer) (-26796));
        java.lang.Integer int11 = operacionesMatematicas0.restar((java.lang.Integer) 1954931892, (java.lang.Integer) 302);
        java.lang.Integer int14 = operacionesMatematicas0.restar((java.lang.Integer) 24, (java.lang.Integer) 2062891546);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 173387 + "'", int8.equals(173387));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 1954931590 + "'", int11.equals(1954931590));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + (-2062891522) + "'", int14.equals((-2062891522)));
    }

    @Test
    public void test251() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test251");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-2233));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-676396), (java.lang.Integer) 100);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-223), (java.lang.Integer) (-198));
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 14, (java.lang.Integer) 12);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 223, (java.lang.Integer) 0);
        java.lang.Integer int24 = operacionesMatematicas0.dividir((java.lang.Integer) (-907044204), (java.lang.Integer) (-1988598960));
        java.lang.Integer int27 = operacionesMatematicas0.sumar((java.lang.Integer) 859521, (java.lang.Integer) (-824465252));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-674366) + "'", int9.equals((-674366)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-6763) + "'", int12.equals((-6763)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-25) + "'", int15.equals((-25)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 168 + "'", int18.equals(168));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 223 + "'", int21.equals(223));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 0 + "'", int24.equals(0));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + (-823605731) + "'", int27.equals((-823605731)));
    }

    @Test
    public void test252() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test252");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) 0);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3020, (java.lang.Integer) 3020);
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-26796));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 206108356, (java.lang.Integer) 765962910);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-231829672));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2 + "'", int6.equals(2));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 9120400 + "'", int9.equals(9120400));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
    }

    @Test
    public void test253() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test253");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 703051, (java.lang.Integer) 191);
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 298, (java.lang.Integer) (-3260));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 1220739868, (java.lang.Integer) (-840498));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 134282741 + "'", int7.equals(134282741));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-2962) + "'", int10.equals((-2962)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1219899370 + "'", int13.equals(1219899370));
    }

    @Test
    public void test254() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test254");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-28275), (java.lang.Integer) 200184);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-1896219511), (java.lang.Integer) (-669335781));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 568901, (java.lang.Integer) (-1734056208));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1729412004 + "'", int12.equals(1729412004));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1734625109 + "'", int15.equals(1734625109));
    }

    @Test
    public void test255() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test255");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-29513), (java.lang.Integer) 1175508908);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
    }

    @Test
    public void test256() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test256");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 1352162704);
        java.lang.Integer int14 = operacionesMatematicas0.sumar((java.lang.Integer) (-1646083254), (java.lang.Integer) (-1646083254));
        java.lang.Integer int17 = operacionesMatematicas0.dividir((java.lang.Integer) (-890108686), (java.lang.Integer) (-679014));
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-844), (java.lang.Integer) 691719053);
        java.lang.Class<?> wildcardClass21 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-388878032) + "'", int11.equals((-388878032)));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 1002800788 + "'", int14.equals(1002800788));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 1310 + "'", int17.equals(1310));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-691719897) + "'", int20.equals((-691719897)));
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test257() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test257");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-204), (java.lang.Integer) 12);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 797, (java.lang.Integer) (-201378));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 3021, (java.lang.Integer) 200183);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 99692, (java.lang.Integer) 203427);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 44907013, (java.lang.Integer) (-681446));
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) 4, (java.lang.Integer) (-431597798));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-17) + "'", int6.equals((-17)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 303119 + "'", int15.equals(303119));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-65) + "'", int18.equals((-65)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1726391192) + "'", int21.equals((-1726391192)));
    }

    @Test
    public void test258() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test258");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 298, (java.lang.Integer) (-203658532));
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2941, (java.lang.Integer) 985806408);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 137901733, (java.lang.Integer) 688);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 203658830 + "'", int7.equals(203658830));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 153721128 + "'", int10.equals(153721128));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 137901045 + "'", int13.equals(137901045));
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test259() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test259");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 674850, (java.lang.Integer) (-203658532));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 9120400, (java.lang.Integer) 8260879);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-454172969), (java.lang.Integer) (-844));
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 13135, (java.lang.Integer) 23816997);
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 859521 + "'", int13.equals(859521));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1069896492 + "'", int16.equals(1069896492));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test260() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test260");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 1678);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-204), (java.lang.Integer) (-669335781));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 953704, (java.lang.Integer) (-1358028));
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-91798), (java.lang.Integer) (-1882945794));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 166122 + "'", int10.equals(166122));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 669335577 + "'", int13.equals(669335577));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1923387680 + "'", int16.equals(1923387680));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-300829908) + "'", int19.equals((-300829908)));
    }

    @Test
    public void test261() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test261");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.sumar((java.lang.Integer) (-2030), (java.lang.Integer) (-674366));
        java.lang.Class<?> wildcardClass9 = operacionesMatematicas0.getClass();
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-86631468), (java.lang.Integer) 166122);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-319241594), (java.lang.Integer) 0);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 527278124);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + (-676396) + "'", int8.equals((-676396)));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-86797590) + "'", int12.equals((-86797590)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-527278124) + "'", int18.equals((-527278124)));
    }

    @Test
    public void test262() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test262");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 1079, (java.lang.Integer) 679827);
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-198118), (java.lang.Integer) 674850);
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) 303119, (java.lang.Integer) (-1360503017));
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) (-1351735770), (java.lang.Integer) 1593176728);
        java.lang.Integer int29 = operacionesMatematicas0.dividir((java.lang.Integer) (-674366), (java.lang.Integer) 27084870);
        java.lang.Integer int32 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1152048556, (java.lang.Integer) 303119);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-678748) + "'", int17.equals((-678748)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-872968) + "'", int20.equals((-872968)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-144182695) + "'", int23.equals((-144182695)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 241440958 + "'", int26.equals(241440958));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 0 + "'", int29.equals(0));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 1195277588 + "'", int32.equals(1195277588));
    }

    @Test
    public void test263() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test263");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass6 = operacionesMatematicas0.getClass();
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 688, (java.lang.Integer) (-924510));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 1710, (java.lang.Integer) 6823529);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-907041850), (java.lang.Integer) 0);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1737686601), (java.lang.Integer) (-1925247892));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 6825239 + "'", int13.equals(6825239));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-907041850) + "'", int16.equals((-907041850)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1592111820) + "'", int19.equals((-1592111820)));
    }

    @Test
    public void test264() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test264");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-2031), (java.lang.Integer) (-100));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-679014), (java.lang.Integer) 28345);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 33312587, (java.lang.Integer) (-155));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-2131) + "'", int10.equals((-2131)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-650669) + "'", int13.equals((-650669)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-214919) + "'", int16.equals((-214919)));
    }

    @Test
    public void test265() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test265");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-3), (java.lang.Integer) (-9));
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-23417576), (java.lang.Integer) 2002502299);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 240844168 + "'", int19.equals(240844168));
    }

    @Test
    public void test266() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test266");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 1973854425, (java.lang.Integer) 1968086742);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 134284782, (java.lang.Integer) 1740362023);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2 + "'", int6.equals(2));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 5767683 + "'", int10.equals(5767683));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
    }

    @Test
    public void test267() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test267");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 14, (java.lang.Integer) (-3260));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 173387, (java.lang.Integer) (-802065));
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 677089, (java.lang.Integer) 4530202);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3274 + "'", int12.equals(3274));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 975452 + "'", int15.equals(975452));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 743292634 + "'", int19.equals(743292634));
    }

    @Test
    public void test268() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test268");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-2548), (java.lang.Integer) 7809);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-169), (java.lang.Integer) 1702393088);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 58376960 + "'", int16.equals(58376960));
    }

    @Test
    public void test269() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test269");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-100), (java.lang.Integer) (-2233));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 675054, (java.lang.Integer) (-201378));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-2099619127), (java.lang.Integer) (-680500222));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 1728039552, (java.lang.Integer) 0);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-50382572), (java.lang.Integer) (-593256750));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223300 + "'", int6.equals(223300));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 473676 + "'", int9.equals(473676));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1419118905) + "'", int12.equals((-1419118905)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1728039552 + "'", int15.equals(1728039552));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-643639322) + "'", int18.equals((-643639322)));
    }

    @Test
    public void test270() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test270");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-3), (java.lang.Integer) 203658830);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-689499353), (java.lang.Integer) 338956);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 134284771, (java.lang.Integer) (-139407012));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-480400), (java.lang.Integer) (-1174185152));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-32545458), (java.lang.Integer) 408714520);
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) (-902763319), (java.lang.Integer) 0);
        try {
            java.lang.Integer int31 = operacionesMatematicas0.dividir((java.lang.Integer) (-6), (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: null");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-2034) + "'", int16.equals((-2034)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-952340332) + "'", int19.equals((-952340332)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-982799360) + "'", int22.equals((-982799360)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 376169062 + "'", int25.equals(376169062));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-902763319) + "'", int28.equals((-902763319)));
    }

    @Test
    public void test271() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test271");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 146, (java.lang.Integer) 990);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1017), (java.lang.Integer) (-11));
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-458350076), (java.lang.Integer) (-569427028));
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-114017579), (java.lang.Integer) 1582669994);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 1592563465, (java.lang.Integer) 2311052);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-844) + "'", int7.equals((-844)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 11187 + "'", int10.equals(11187));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 111076952 + "'", int13.equals(111076952));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-1696687573) + "'", int16.equals((-1696687573)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1594874517 + "'", int19.equals(1594874517));
    }

    @Test
    public void test272() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test272");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-26796), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) (-6763), (java.lang.Integer) (-4060));
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) (-28474), (java.lang.Integer) 199);
        java.lang.Integer int29 = operacionesMatematicas0.sumar((java.lang.Integer) (-319237330), (java.lang.Integer) (-86797590));
        java.lang.Integer int32 = operacionesMatematicas0.restar((java.lang.Integer) 173387, (java.lang.Integer) 2010360317);
        java.lang.Integer int35 = operacionesMatematicas0.restar((java.lang.Integer) (-501776), (java.lang.Integer) (-181982338));
        java.lang.Integer int38 = operacionesMatematicas0.multiplicar((java.lang.Integer) 182961501, (java.lang.Integer) (-758700656));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-28474) + "'", int20.equals((-28474)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 1 + "'", int23.equals(1));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-28275) + "'", int26.equals((-28275)));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-406034920) + "'", int29.equals((-406034920)));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + (-2010186930) + "'", int32.equals((-2010186930)));
        org.junit.Assert.assertTrue("'" + int35 + "' != '" + 181480562 + "'", int35.equals(181480562));
        org.junit.Assert.assertTrue("'" + int38 + "' != '" + 1462793552 + "'", int38.equals(1462793552));
    }

    @Test
    public void test273() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test273");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) (-1));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 696288, (java.lang.Integer) (-6763));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) 820922168);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) (-288288441), (java.lang.Integer) (-203078208));
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 703051 + "'", int10.equals(703051));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-491366649) + "'", int16.equals((-491366649)));
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test274() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test274");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-2030), (java.lang.Integer) 677084);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 134282741, (java.lang.Integer) 218);
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 679827, (java.lang.Integer) 44906216);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 242847, (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Integer int29 = operacionesMatematicas0.restar((java.lang.Integer) (-1937186348), (java.lang.Integer) 1476845647);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-679114) + "'", int16.equals((-679114)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-791133534) + "'", int19.equals((-791133534)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-169435336) + "'", int22.equals((-169435336)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 880935301 + "'", int29.equals(880935301));
    }

    @Test
    public void test275() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test275");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 10, (java.lang.Integer) 302);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1750448650), (java.lang.Integer) 0);
        java.lang.Integer int24 = operacionesMatematicas0.restar((java.lang.Integer) (-844578966), (java.lang.Integer) (-2030504282));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 3020 + "'", int15.equals(3020));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1) + "'", int18.equals((-1)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 1185925316 + "'", int24.equals(1185925316));
    }

    @Test
    public void test276() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test276");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 44906012, (java.lang.Integer) 2679600);
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 84409818, (java.lang.Integer) (-1985325586));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 42226412 + "'", int7.equals(42226412));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
    }

    @Test
    public void test277() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test277");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2354, (java.lang.Integer) 2679600);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-98990), (java.lang.Integer) (-4060));
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) (-203761580), (java.lang.Integer) 44540629);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 268, (java.lang.Integer) 129067);
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) 610278, (java.lang.Integer) 198586);
        java.lang.Integer int29 = operacionesMatematicas0.sumar((java.lang.Integer) 1996158842, (java.lang.Integer) 1427973632);
        java.lang.Integer int32 = operacionesMatematicas0.sumar((java.lang.Integer) 342593126, (java.lang.Integer) (-141165408));
        java.lang.Integer int35 = operacionesMatematicas0.restar((java.lang.Integer) (-1264536494), (java.lang.Integer) 155085916);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 24 + "'", int16.equals(24));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-4) + "'", int20.equals((-4)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 0 + "'", int23.equals(0));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 808864 + "'", int26.equals(808864));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-870834822) + "'", int29.equals((-870834822)));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 201427718 + "'", int32.equals(201427718));
        org.junit.Assert.assertTrue("'" + int35 + "' != '" + (-1419622410) + "'", int35.equals((-1419622410)));
    }

    @Test
    public void test278() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test278");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-676594), (java.lang.Integer) 1678);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-1), (java.lang.Integer) 3233);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-165904), (java.lang.Integer) 6295);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 42226412, (java.lang.Integer) (-43315734));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) (-313922314), (java.lang.Integer) 2108502328);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1371794486, (java.lang.Integer) 880012598);
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) (-1744315875), (java.lang.Integer) (-1518064724));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-403) + "'", int10.equals((-403)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-172199) + "'", int16.equals((-172199)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-372389960) + "'", int19.equals((-372389960)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1794580014 + "'", int22.equals(1794580014));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-1253846684) + "'", int25.equals((-1253846684)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-226251151) + "'", int28.equals((-226251151)));
    }

    @Test
    public void test279() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test279");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 679827, (java.lang.Integer) 129067);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 654, (java.lang.Integer) (-241953962));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 808894 + "'", int10.equals(808894));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
    }

    @Test
    public void test280() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test280");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-304), (java.lang.Integer) 12);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 902932, (java.lang.Integer) 199);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 151210502, (java.lang.Integer) 6);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) (-674376), (java.lang.Integer) (-1546235464));
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-153723026), (java.lang.Integer) (-86408168));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-292) + "'", int7.equals((-292)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 179683468 + "'", int10.equals(179683468));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 907263012 + "'", int13.equals(907263012));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 1545561088 + "'", int17.equals(1545561088));
        org.junit.Assert.assertNotNull(wildcardClass18);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-41178544) + "'", int21.equals((-41178544)));
    }

    @Test
    public void test281() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test281");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2, (java.lang.Integer) (-1));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) 338956);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) 340488432, (java.lang.Integer) 134082361);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-2) + "'", int13.equals((-2)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1023986076 + "'", int16.equals(1023986076));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 206406071 + "'", int20.equals(206406071));
    }

    @Test
    public void test282() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test282");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 198, (java.lang.Integer) (-3030));
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-2548), (java.lang.Integer) 532602758);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-319619085), (java.lang.Integer) (-1477849338));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-569427028), (java.lang.Integer) (-1708435290));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) (-485640240), (java.lang.Integer) (-1440498544));
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) 402135200, (java.lang.Integer) (-1117679753));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-599940) + "'", int13.equals((-599940)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1797468423) + "'", int19.equals((-1797468423)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1203252856) + "'", int22.equals((-1203252856)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 1519814953 + "'", int28.equals(1519814953));
    }

    @Test
    public void test283() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test283");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 202);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) (-2030));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) (-453522300), (java.lang.Integer) 44906216);
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) 959378576, (java.lang.Integer) 1216786422);
        java.lang.Integer int27 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203153963, (java.lang.Integer) 418510005);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1 + "'", int15.equals(1));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 677084 + "'", int18.equals(677084));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-498428516) + "'", int21.equals((-498428516)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-2118802298) + "'", int24.equals((-2118802298)));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 365093991 + "'", int27.equals(365093991));
    }

    @Test
    public void test284() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test284");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 2354, (java.lang.Integer) (-403));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) (-28474));
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-979644056), (java.lang.Integer) (-985705143));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) (-844346128), (java.lang.Integer) 1110802624);
        java.lang.Integer int24 = operacionesMatematicas0.restar((java.lang.Integer) (-655230575), (java.lang.Integer) 201427718);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-5) + "'", int12.equals((-5)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-6349702) + "'", int15.equals((-6349702)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 6061087 + "'", int18.equals(6061087));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1955148752) + "'", int21.equals((-1955148752)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-856658293) + "'", int24.equals((-856658293)));
    }

    @Test
    public void test285() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test285");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 199);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1734056208), (java.lang.Integer) 204);
        java.lang.Integer int17 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1567906726, (java.lang.Integer) (-882132));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 298 + "'", int9.equals(298));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + (-1560148160) + "'", int14.equals((-1560148160)));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-967623544) + "'", int17.equals((-967623544)));
    }

    @Test
    public void test286() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test286");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 1678);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-28176), (java.lang.Integer) (-507));
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 525, (java.lang.Integer) 1554526000);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-2452019), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass21 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 166122 + "'", int10.equals(166122));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 55 + "'", int13.equals(55));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1554526525 + "'", int16.equals(1554526525));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-2452019) + "'", int19.equals((-2452019)));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test287() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test287");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 138125136, (java.lang.Integer) 223403);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass9 = operacionesMatematicas0.getClass();
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-1761916892), (java.lang.Integer) (-1954108080));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-747018902), (java.lang.Integer) (-496153144));
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 137901733 + "'", int7.equals(137901733));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-250865758) + "'", int15.equals((-250865758)));
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test288() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test288");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 473676, (java.lang.Integer) 222612);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.restar((java.lang.Integer) (-56948000), (java.lang.Integer) (-7812));
        java.lang.Integer int14 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) 1558090488);
        java.lang.Integer int17 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-484966043), (java.lang.Integer) 767787264);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 696288 + "'", int7.equals(696288));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-56940188) + "'", int11.equals((-56940188)));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14.equals(0));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-375200512) + "'", int17.equals((-375200512)));
    }

    @Test
    public void test289() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test289");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-100), (java.lang.Integer) (-2233));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1337112768));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-1168106582), (java.lang.Integer) (-406896600));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223300 + "'", int6.equals(223300));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2 + "'", int12.equals(2));
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test290() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test290");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 1352162704);
        java.lang.Integer int14 = operacionesMatematicas0.sumar((java.lang.Integer) (-1646083254), (java.lang.Integer) (-1646083254));
        java.lang.Integer int17 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1420242393, (java.lang.Integer) (-1));
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) 1502123315, (java.lang.Integer) 43381528);
        java.lang.Class<?> wildcardClass21 = operacionesMatematicas0.getClass();
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-151145729), (java.lang.Integer) 0);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-388878032) + "'", int11.equals((-388878032)));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 1002800788 + "'", int14.equals(1002800788));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-1420242393) + "'", int17.equals((-1420242393)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 1458741787 + "'", int20.equals(1458741787));
        org.junit.Assert.assertNotNull(wildcardClass21);
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 0 + "'", int24.equals(0));
    }

    @Test
    public void test291() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test291");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-26796), (java.lang.Integer) (-2131));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-674366), (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-678748), (java.lang.Integer) (-678748));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) 359256, (java.lang.Integer) (-202806058));
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) (-982799360), (java.lang.Integer) 2059941074);
        java.lang.Class<?> wildcardClass25 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-28927) + "'", int12.equals((-28927)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-133524468) + "'", int15.equals((-133524468)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 203165314 + "'", int21.equals(203165314));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 1077141714 + "'", int24.equals(1077141714));
        org.junit.Assert.assertNotNull(wildcardClass25);
        org.junit.Assert.assertNotNull(wildcardClass26);
    }

    @Test
    public void test292() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test292");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-204), (java.lang.Integer) 12);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 797, (java.lang.Integer) (-201378));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.dividir((java.lang.Integer) 3233, (java.lang.Integer) 1024014076);
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 775906878, (java.lang.Integer) 164521512);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) (-846460796), (java.lang.Integer) (-1183));
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 1523488800, (java.lang.Integer) 314459147);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-17) + "'", int6.equals((-17)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14.equals(0));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 611385366 + "'", int17.equals(611385366));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-846461979) + "'", int20.equals((-846461979)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 4 + "'", int23.equals(4));
    }

    @Test
    public void test293() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test293");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) 203);
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 791, (java.lang.Integer) (-198118));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 907263012, (java.lang.Integer) (-1882945794));
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 831667596, (java.lang.Integer) 6823429);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 797 + "'", int7.equals(797));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 824844167 + "'", int16.equals(824844167));
    }

    @Test
    public void test294() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test294");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-100), (java.lang.Integer) (-2233));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 245557226, (java.lang.Integer) (-138322199));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223300 + "'", int6.equals(223300));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-1) + "'", int9.equals((-1)));
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test295() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test295");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) (-1));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) (-2233), (java.lang.Integer) 797);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-3030) + "'", int10.equals((-3030)));
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test296() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test296");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2, (java.lang.Integer) (-1));
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) (-3353), (java.lang.Integer) 191);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 2010869584);
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 1663016120);
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-2) + "'", int13.equals((-2)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-3162) + "'", int16.equals((-3162)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2010869584 + "'", int19.equals(2010869584));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-1663016120) + "'", int23.equals((-1663016120)));
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test297() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test297");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1349700, (java.lang.Integer) 1045334094);
        java.lang.Integer int14 = operacionesMatematicas0.sumar((java.lang.Integer) 2037583576, (java.lang.Integer) (-36253604));
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 1278130677, (java.lang.Integer) 1569639104);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-740129608) + "'", int11.equals((-740129608)));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 2001329972 + "'", int14.equals(2001329972));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-291508427) + "'", int17.equals((-291508427)));
    }

    @Test
    public void test298() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test298");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) (-3), (java.lang.Integer) 0);
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 203);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-3) + "'", int7.equals((-3)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 203 + "'", int10.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test299() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test299");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-203657735), (java.lang.Integer) (-98975152));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 868432494, (java.lang.Integer) (-337227660));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1750448650), (java.lang.Integer) 3021);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 2135641200, (java.lang.Integer) 2115103491);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-104682583) + "'", int9.equals((-104682583)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-2) + "'", int12.equals((-2)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1000630274) + "'", int15.equals((-1000630274)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 20537709 + "'", int18.equals(20537709));
    }

    @Test
    public void test300() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test300");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 374697400, (java.lang.Integer) 473676);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 44906012);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 3233, (java.lang.Integer) 1771576096);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 222612);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2037583576, (java.lang.Integer) (-1695));
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) 7809, (java.lang.Integer) (-26404274));
        java.lang.Integer int31 = operacionesMatematicas0.dividir((java.lang.Integer) (-844), (java.lang.Integer) (-28927));
        java.lang.Class<?> wildcardClass32 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 791 + "'", int13.equals(791));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-44906012) + "'", int16.equals((-44906012)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1771579329 + "'", int19.equals(1771579329));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-222612) + "'", int22.equals((-222612)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-550455336) + "'", int25.equals((-550455336)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-32545458) + "'", int28.equals((-32545458)));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 0 + "'", int31.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass32);
    }

    @Test
    public void test301() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test301");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 473676, (java.lang.Integer) (-2030));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 20300, (java.lang.Integer) 338956);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 3021, (java.lang.Integer) 3274);
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) 4051882, (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-961562280) + "'", int12.equals((-961562280)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 359256 + "'", int15.equals(359256));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 6295 + "'", int18.equals(6295));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
    }

    @Test
    public void test302() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test302");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-204), (java.lang.Integer) 12);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 797, (java.lang.Integer) (-201378));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 3021, (java.lang.Integer) 200183);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 1593176491, (java.lang.Integer) 50443);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1954108080), (java.lang.Integer) (-606522767));
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) (-127644456), (java.lang.Integer) (-150851528));
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) (-1463775408), (java.lang.Integer) 675040);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-17) + "'", int6.equals((-17)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1593226934 + "'", int15.equals(1593226934));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1025253456 + "'", int18.equals(1025253456));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-278495984) + "'", int21.equals((-278495984)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1463100368) + "'", int24.equals((-1463100368)));
    }

    @Test
    public void test303() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test303");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 674850, (java.lang.Integer) (-203658532));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 338956, (java.lang.Integer) (-3030));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2233), (java.lang.Integer) (-1027036680));
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-678748), (java.lang.Integer) (-198118));
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) (-14473), (java.lang.Integer) (-141378));
        java.lang.Integer int24 = operacionesMatematicas0.restar((java.lang.Integer) (-129616610), (java.lang.Integer) 1601067675);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1027036680) + "'", int12.equals((-1027036680)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-139629624) + "'", int15.equals((-139629624)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-480630) + "'", int18.equals((-480630)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1730684285) + "'", int24.equals((-1730684285)));
    }

    @Test
    public void test304() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test304");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 101);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) (-674366), (java.lang.Integer) 3020);
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 200183);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-25548148), (java.lang.Integer) 644460048);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1471, (java.lang.Integer) (-655230575));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 101 + "'", int4.equals(101));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-223) + "'", int7.equals((-223)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 200184 + "'", int10.equals(200184));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-670008196) + "'", int13.equals((-670008196)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-1771501521) + "'", int16.equals((-1771501521)));
    }

    @Test
    public void test305() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test305");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 473676, (java.lang.Integer) 222612);
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 200183, (java.lang.Integer) 117);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) (-1429679800));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass15 = operacionesMatematicas0.getClass();
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-6451), (java.lang.Integer) (-338281594));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 696288 + "'", int7.equals(696288));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 1710 + "'", int10.equals(1710));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1429679800 + "'", int13.equals(1429679800));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertNotNull(wildcardClass15);
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 338275143 + "'", int18.equals(338275143));
    }

    @Test
    public void test306() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test306");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 44906012, (java.lang.Integer) 2679600);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-223), (java.lang.Integer) (-28927));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 2108502328, (java.lang.Integer) 28345);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-479791808), (java.lang.Integer) (-1514116262));
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 42226412 + "'", int7.equals(42226412));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 6450721 + "'", int10.equals(6450721));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2108530673 + "'", int13.equals(2108530673));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-1993908070) + "'", int17.equals((-1993908070)));
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test307() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test307");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-204940904), (java.lang.Integer) (-678074));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-680500222), (java.lang.Integer) 453522102);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 638678568);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-35511), (java.lang.Integer) 316619749);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1431676816 + "'", int9.equals(1431676816));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1937735020 + "'", int12.equals(1937735020));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-638678568) + "'", int15.equals((-638678568)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 740474189 + "'", int18.equals(740474189));
    }

    @Test
    public void test308() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test308");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-2233), (java.lang.Integer) 199);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 227358, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 28000, (java.lang.Integer) 675040);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 218, (java.lang.Integer) (-98));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 677082, (java.lang.Integer) 589628875);
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) 438482334, (java.lang.Integer) (-237));
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) (-9430), (java.lang.Integer) (-1933340520));
        java.lang.Class<?> wildcardClass29 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass30 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-11) + "'", int10.equals((-11)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227358 + "'", int13.equals(227358));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 703040 + "'", int16.equals(703040));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 120 + "'", int19.equals(120));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 438482571 + "'", int25.equals(438482571));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 1933331090 + "'", int28.equals(1933331090));
        org.junit.Assert.assertNotNull(wildcardClass29);
        org.junit.Assert.assertNotNull(wildcardClass30);
    }

    @Test
    public void test309() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test309");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) (-311834474), (java.lang.Integer) (-673460));
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-312507934) + "'", int22.equals((-312507934)));
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test310() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test310");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99990, (java.lang.Integer) 14);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-201378), (java.lang.Integer) (-203657735));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 1195056035, (java.lang.Integer) 2136956016);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-994773116), (java.lang.Integer) (-133524468));
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) 37, (java.lang.Integer) (-679114));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 100004 + "'", int9.equals(100004));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-203859113) + "'", int12.equals((-203859113)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-941899981) + "'", int15.equals((-941899981)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1656482352 + "'", int18.equals(1656482352));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-25127218) + "'", int21.equals((-25127218)));
    }

    @Test
    public void test311() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test311");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) 0);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-507), (java.lang.Integer) 100004);
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-1913), (java.lang.Integer) 29684000);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-1427973632), (java.lang.Integer) 11187);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1806993207, (java.lang.Integer) 790454420);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2 + "'", int6.equals(2));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-50702028) + "'", int9.equals((-50702028)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1427984819) + "'", int15.equals((-1427984819)));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1632488396 + "'", int19.equals(1632488396));
    }

    @Test
    public void test312() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test312");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.dividir((java.lang.Integer) 198, (java.lang.Integer) 2989800);
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) 2085094144, (java.lang.Integer) (-682380));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass18);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 2085776524 + "'", int21.equals(2085776524));
    }

    @Test
    public void test313() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test313");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-676594), (java.lang.Integer) 3451);
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-477630), (java.lang.Integer) (-203678316));
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) 570175776, (java.lang.Integer) 419736786);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 12 + "'", int13.equals(12));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-673143) + "'", int17.equals((-673143)));
        org.junit.Assert.assertNotNull(wildcardClass18);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1864816680 + "'", int21.equals(1864816680));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 912378944 + "'", int24.equals(912378944));
    }

    @Test
    public void test314() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test314");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1349700, (java.lang.Integer) 1045334094);
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-740129608) + "'", int11.equals((-740129608)));
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test315() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test315");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 677082, (java.lang.Integer) 797);
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) 798559998, (java.lang.Integer) (-71));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 849 + "'", int22.equals(849));
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-11247323) + "'", int26.equals((-11247323)));
    }

    @Test
    public void test316() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test316");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-98975152), (java.lang.Integer) (-676594));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 117, (java.lang.Integer) 1);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-201378), (java.lang.Integer) (-3260));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) 1023986076, (java.lang.Integer) 118);
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) 679827, (java.lang.Integer) (-141707));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) (-43316026), (java.lang.Integer) 532602758);
        java.lang.Integer int31 = operacionesMatematicas0.restar((java.lang.Integer) 1217752351, (java.lang.Integer) (-9122452));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 146 + "'", int12.equals(146));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 118 + "'", int15.equals(118));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-198118) + "'", int18.equals((-198118)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1023985958 + "'", int21.equals(1023985958));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 821534 + "'", int25.equals(821534));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 1226874803 + "'", int31.equals(1226874803));
    }

    @Test
    public void test317() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test317");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 101);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 797, (java.lang.Integer) (-890108686));
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-702086), (java.lang.Integer) 1954931892);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-56204610), (java.lang.Integer) (-1477851346));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 101 + "'", int4.equals(101));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-747018902) + "'", int7.equals((-747018902)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 1954229806 + "'", int10.equals(1954229806));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1421646736 + "'", int13.equals(1421646736));
    }

    @Test
    public void test318() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test318");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-507), (java.lang.Integer) 200871);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-3030), (java.lang.Integer) 1980253234);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 44906216, (java.lang.Integer) 797);
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) (-853072144), (java.lang.Integer) (-873146058));
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1869407804), (java.lang.Integer) 698720868);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-201378) + "'", int16.equals((-201378)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1980250204 + "'", int19.equals(1980250204));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 44907013 + "'", int22.equals(44907013));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 20073914 + "'", int25.equals(20073914));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-1894246256) + "'", int28.equals((-1894246256)));
    }

    @Test
    public void test319() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test319");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) (-3), (java.lang.Integer) 0);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 791133635, (java.lang.Integer) 0);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 1710, (java.lang.Integer) (-1599578112));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-3) + "'", int7.equals((-3)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 791133635 + "'", int10.equals(791133635));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1599576402) + "'", int13.equals((-1599576402)));
    }

    @Test
    public void test320() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test320");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) 0);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-507), (java.lang.Integer) 100004);
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 1963548160, (java.lang.Integer) 23417576);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 1636534483, (java.lang.Integer) 337185028);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2 + "'", int6.equals(2));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-50702028) + "'", int9.equals((-50702028)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 83 + "'", int12.equals(83));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1973719511 + "'", int15.equals(1973719511));
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test321() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test321");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 166111, (java.lang.Integer) (-594099455));
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-1338190428), (java.lang.Integer) (-203));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-91808), (java.lang.Integer) (-44568904));
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 776603166, (java.lang.Integer) 9812853);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-593933344) + "'", int13.equals((-593933344)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-1338190225) + "'", int16.equals((-1338190225)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 44477096 + "'", int19.equals(44477096));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 766790313 + "'", int22.equals(766790313));
    }

    @Test
    public void test322() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test322");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2, (java.lang.Integer) (-1));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) 338956);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-300960), (java.lang.Integer) 2679600);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 298, (java.lang.Integer) (-28474));
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        java.lang.Integer int27 = operacionesMatematicas0.dividir((java.lang.Integer) 817658232, (java.lang.Integer) 1130388972);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-2) + "'", int13.equals((-2)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1023986076 + "'", int16.equals(1023986076));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1001435648 + "'", int19.equals(1001435648));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-28176) + "'", int22.equals((-28176)));
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertNotNull(wildcardClass24);
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 0 + "'", int27.equals(0));
    }

    @Test
    public void test323() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test323");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-203), (java.lang.Integer) 1000);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 19735044, (java.lang.Integer) (-337227660));
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 200282, (java.lang.Integer) (-955917192));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-317492616) + "'", int13.equals((-317492616)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 956117474 + "'", int16.equals(956117474));
    }

    @Test
    public void test324() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test324");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) 1853597184);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-480400), (java.lang.Integer) 1624322327);
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-747018927), (java.lang.Integer) (-563826261));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 0 + "'", int4.equals(0));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-1902651632) + "'", int7.equals((-1902651632)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 1 + "'", int10.equals(1));
    }

    @Test
    public void test325() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test325");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.sumar((java.lang.Integer) (-2030), (java.lang.Integer) (-674366));
        java.lang.Class<?> wildcardClass9 = operacionesMatematicas0.getClass();
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-319239361), (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-26796), (java.lang.Integer) (-678748));
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2989800, (java.lang.Integer) (-844));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 1980273266);
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-202979314), (java.lang.Integer) (-98965));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + (-676396) + "'", int8.equals((-676396)));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-319241594) + "'", int12.equals((-319241594)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1771576096 + "'", int18.equals(1771576096));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1980273266) + "'", int21.equals((-1980273266)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 285766618 + "'", int24.equals(285766618));
    }

    @Test
    public void test326() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test326");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 10, (java.lang.Integer) 302);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-676594), (java.lang.Integer) 298);
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) (-1720148840), (java.lang.Integer) (-891965756));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 3020 + "'", int15.equals(3020));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-676296) + "'", int18.equals((-676296)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1 + "'", int21.equals(1));
    }

    @Test
    public void test327() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test327");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 9992694, (java.lang.Integer) (-1115561068));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-1105568374) + "'", int9.equals((-1105568374)));
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test328() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test328");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 677082, (java.lang.Integer) 797);
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) (-1378461349), (java.lang.Integer) (-1988598960));
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-8230984));
        java.lang.Class<?> wildcardClass30 = operacionesMatematicas0.getClass();
        java.lang.Integer int33 = operacionesMatematicas0.sumar((java.lang.Integer) 1420242393, (java.lang.Integer) 2083402166);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 849 + "'", int22.equals(849));
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 0 + "'", int26.equals(0));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 0 + "'", int29.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass30);
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + (-791322737) + "'", int33.equals((-791322737)));
    }

    @Test
    public void test329() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test329");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-100), (java.lang.Integer) (-2233));
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 223, (java.lang.Integer) (-2131));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-607257));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223300 + "'", int6.equals(223300));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 2354 + "'", int9.equals(2354));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
    }

    @Test
    public void test330() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test330");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2, (java.lang.Integer) 1000);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-907044204), (java.lang.Integer) 166111);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-480618), (java.lang.Integer) (-674366));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-152649640), (java.lang.Integer) (-678074));
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 1904969904, (java.lang.Integer) (-1744250912));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2000 + "'", int6.equals(2000));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-906878093) + "'", int9.equals((-906878093)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1989890988 + "'", int12.equals(1989890988));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-153327714) + "'", int15.equals((-153327714)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 160718992 + "'", int18.equals(160718992));
    }

    @Test
    public void test331() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test331");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 1079, (java.lang.Integer) 679827);
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) 3274, (java.lang.Integer) 333);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) (-593256750), (java.lang.Integer) 961562281);
        java.lang.Integer int26 = operacionesMatematicas0.restar((java.lang.Integer) (-86631468), (java.lang.Integer) 1980273266);
        java.lang.Integer int29 = operacionesMatematicas0.dividir((java.lang.Integer) 41385914, (java.lang.Integer) 2679600);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-678748) + "'", int17.equals((-678748)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 2941 + "'", int20.equals(2941));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 0 + "'", int23.equals(0));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-2066904734) + "'", int26.equals((-2066904734)));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 15 + "'", int29.equals(15));
    }

    @Test
    public void test332() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test332");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass6 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 12, (java.lang.Integer) 9120300);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-9120288) + "'", int10.equals((-9120288)));
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test333() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test333");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-3), (java.lang.Integer) 203658830);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-689499353), (java.lang.Integer) 338956);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 134284771, (java.lang.Integer) (-139407012));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-987197735), (java.lang.Integer) (-1963548160));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 1889283349, (java.lang.Integer) 1209703905);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-2034) + "'", int16.equals((-2034)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-952340332) + "'", int19.equals((-952340332)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1 + "'", int25.equals(1));
    }

    @Test
    public void test334() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test334");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166122, (java.lang.Integer) (-2030));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 203659430, (java.lang.Integer) 1954931892);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 2989800, (java.lang.Integer) (-50702028));
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) (-203761580), (java.lang.Integer) 200257);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 1617522543, (java.lang.Integer) 2037583576);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1402661432), (java.lang.Integer) (-979644056));
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass27 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 199 + "'", int6.equals(199));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-337227660) + "'", int9.equals((-337227660)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-47712228) + "'", int16.equals((-47712228)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1017) + "'", int19.equals((-1017)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-1477369536) + "'", int25.equals((-1477369536)));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertNotNull(wildcardClass27);
    }

    @Test
    public void test335() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test335");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-2233), (java.lang.Integer) 199);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 227358, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 28000, (java.lang.Integer) 675040);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-134082459), (java.lang.Integer) 6731);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) (-138799304), (java.lang.Integer) 10);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 681653544, (java.lang.Integer) (-98));
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Integer int29 = operacionesMatematicas0.restar((java.lang.Integer) (-1507971691), (java.lang.Integer) (-9351));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-11) + "'", int10.equals((-11)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227358 + "'", int13.equals(227358));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 703040 + "'", int16.equals(703040));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-565899369) + "'", int19.equals((-565899369)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-138799294) + "'", int22.equals((-138799294)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1917429424 + "'", int25.equals(1917429424));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-1507962340) + "'", int29.equals((-1507962340)));
    }

    @Test
    public void test336() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test336");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-98975152), (java.lang.Integer) (-300960));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 30, (java.lang.Integer) (-1928299760));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-1507971691), (java.lang.Integer) 767787264);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1963548160 + "'", int9.equals(1963548160));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1928299790 + "'", int12.equals(1928299790));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-740184427) + "'", int15.equals((-740184427)));
    }

    @Test
    public void test337() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test337");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1089, (java.lang.Integer) (-1027036680));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 23816997, (java.lang.Integer) 222308);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166111, (java.lang.Integer) 1023985958);
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) (-11), (java.lang.Integer) 1418013632);
        java.lang.Integer int31 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) (-676594));
        java.lang.Integer int34 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1483608740), (java.lang.Integer) (-985706892));
        java.lang.Integer int37 = operacionesMatematicas0.sumar((java.lang.Integer) 8960, (java.lang.Integer) (-1882945794));
        java.lang.Integer int40 = operacionesMatematicas0.restar((java.lang.Integer) 2005911435, (java.lang.Integer) 1364569856);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1751447560) + "'", int19.equals((-1751447560)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-985706892) + "'", int22.equals((-985706892)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1741645850 + "'", int25.equals(1741645850));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-1418013643) + "'", int28.equals((-1418013643)));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-676594) + "'", int31.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + (-1709861456) + "'", int34.equals((-1709861456)));
        org.junit.Assert.assertTrue("'" + int37 + "' != '" + (-1882936834) + "'", int37.equals((-1882936834)));
        org.junit.Assert.assertTrue("'" + int40 + "' != '" + 641341579 + "'", int40.equals(641341579));
    }

    @Test
    public void test338() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test338");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 1352162704);
        java.lang.Integer int14 = operacionesMatematicas0.sumar((java.lang.Integer) (-1646083254), (java.lang.Integer) (-1646083254));
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 102968, (java.lang.Integer) (-680500222));
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-689499353), (java.lang.Integer) 1502123315);
        java.lang.Class<?> wildcardClass21 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-388878032) + "'", int11.equals((-388878032)));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 1002800788 + "'", int14.equals(1002800788));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 680603190 + "'", int17.equals(680603190));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 166101957 + "'", int20.equals(166101957));
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test339() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test339");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-203658532), (java.lang.Integer) 797);
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1362965102, (java.lang.Integer) 0);
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) 2108475396, (java.lang.Integer) 1497177416);
        java.lang.Class<?> wildcardClass25 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-203657735) + "'", int18.equals((-203657735)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-689314484) + "'", int24.equals((-689314484)));
        org.junit.Assert.assertNotNull(wildcardClass25);
    }

    @Test
    public void test340() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test340");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) (-872882432), (java.lang.Integer) (-6763));
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1641), (java.lang.Integer) 200184);
        java.lang.Integer int26 = operacionesMatematicas0.restar((java.lang.Integer) (-674366), (java.lang.Integer) 989568000);
        java.lang.Integer int29 = operacionesMatematicas0.dividir((java.lang.Integer) 42226412, (java.lang.Integer) 2058400712);
        java.lang.Class<?> wildcardClass30 = operacionesMatematicas0.getClass();
        java.lang.Integer int33 = operacionesMatematicas0.restar((java.lang.Integer) 1524453132, (java.lang.Integer) 880935301);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 129067 + "'", int20.equals(129067));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-328501944) + "'", int23.equals((-328501944)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-990242366) + "'", int26.equals((-990242366)));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 0 + "'", int29.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass30);
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + 643517831 + "'", int33.equals(643517831));
    }

    @Test
    public void test341() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test341");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 101);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 1024014076, (java.lang.Integer) 41385914);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1796254192), (java.lang.Integer) (-1273057980));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-304) + "'", int6.equals((-304)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 24 + "'", int9.equals(24));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-523196212) + "'", int12.equals((-523196212)));
    }

    @Test
    public void test342() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test342");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass6 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-203761580), (java.lang.Integer) 1427973632);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.dividir((java.lang.Integer) 20653, (java.lang.Integer) (-202979314));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 1224212052 + "'", int10.equals(1224212052));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14.equals(0));
    }

    @Test
    public void test343() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test343");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) (-292));
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 674850, (java.lang.Integer) 3274);
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) 7, (java.lang.Integer) (-17));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-882132) + "'", int16.equals((-882132)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 206 + "'", int19.equals(206));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-10) + "'", int23.equals((-10)));
    }

    @Test
    public void test344() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test344");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) 101);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 28053, (java.lang.Integer) 849);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-86407950), (java.lang.Integer) (-2027625964));
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 1704551204, (java.lang.Integer) 1346531);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-336967396), (java.lang.Integer) 747407281);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2 + "'", int6.equals(2));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 23816997 + "'", int10.equals(23816997));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-2114033914) + "'", int13.equals((-2114033914)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1705897735 + "'", int16.equals(1705897735));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 410439885 + "'", int19.equals(410439885));
    }

    @Test
    public void test345() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test345");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 99, (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1195703928, (java.lang.Integer) (-203437481));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-3) + "'", int6.equals((-3)));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 1002777288 + "'", int10.equals(1002777288));
    }

    @Test
    public void test346() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test346");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 302, (java.lang.Integer) 675054);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.multiplicar((java.lang.Integer) 863229758, (java.lang.Integer) (-1419118905));
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 216150945, (java.lang.Integer) 7);
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1830249920, (java.lang.Integer) (-1377840312));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-674752) + "'", int10.equals((-674752)));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 38790450 + "'", int14.equals(38790450));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 216150938 + "'", int17.equals(216150938));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 1504828928 + "'", int20.equals(1504828928));
    }

    @Test
    public void test347() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test347");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 101);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 797, (java.lang.Integer) (-890108686));
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 91982416);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 101 + "'", int4.equals(101));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-747018902) + "'", int7.equals((-747018902)));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-91982416) + "'", int11.equals((-91982416)));
    }

    @Test
    public void test348() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test348");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 688, (java.lang.Integer) 990);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 793643030, (java.lang.Integer) 1983457309);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1678 + "'", int9.equals(1678));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1189814279) + "'", int12.equals((-1189814279)));
    }

    @Test
    public void test349() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test349");
        java.lang.Object obj0 = new java.lang.Object();
        java.lang.Class<?> wildcardClass1 = obj0.getClass();
        java.lang.Class<?> wildcardClass2 = obj0.getClass();
        java.lang.Class<?> wildcardClass3 = obj0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertNotNull(wildcardClass2);
        org.junit.Assert.assertNotNull(wildcardClass3);
    }

    @Test
    public void test350() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test350");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 473676, (java.lang.Integer) 222612);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.sumar((java.lang.Integer) 1193221951, (java.lang.Integer) (-1963548160));
        java.lang.Integer int14 = operacionesMatematicas0.multiplicar((java.lang.Integer) 281731133, (java.lang.Integer) (-2131278928));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 696288 + "'", int7.equals(696288));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-770326209) + "'", int11.equals((-770326209)));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 2131042544 + "'", int14.equals(2131042544));
    }

    @Test
    public void test351() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test351");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 473676, (java.lang.Integer) (-2030));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-292), (java.lang.Integer) 2108502328);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-86797590), (java.lang.Integer) 674850);
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 135279661, (java.lang.Integer) 199);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) 134282741);
        java.lang.Integer int28 = operacionesMatematicas0.sumar((java.lang.Integer) 527278124, (java.lang.Integer) 153262720);
        java.lang.Integer int31 = operacionesMatematicas0.restar((java.lang.Integer) 1315789908, (java.lang.Integer) (-1060091956));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-961562280) + "'", int12.equals((-961562280)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-589628652) + "'", int19.equals((-589628652)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1150848763 + "'", int22.equals(1150848763));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 134284771 + "'", int25.equals(134284771));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 680540844 + "'", int28.equals(680540844));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-1919085432) + "'", int31.equals((-1919085432)));
    }

    @Test
    public void test352() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test352");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-203657735), (java.lang.Integer) (-98975152));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 98976810, (java.lang.Integer) (-203658530));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-104682583) + "'", int9.equals((-104682583)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 302635340 + "'", int12.equals(302635340));
    }

    @Test
    public void test353() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test353");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 1079, (java.lang.Integer) 679827);
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) 3274, (java.lang.Integer) 333);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) (-593256750), (java.lang.Integer) 961562281);
        java.lang.Integer int26 = operacionesMatematicas0.restar((java.lang.Integer) (-1536561213), (java.lang.Integer) (-98));
        java.lang.Class<?> wildcardClass27 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-678748) + "'", int17.equals((-678748)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 2941 + "'", int20.equals(2941));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 0 + "'", int23.equals(0));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-1536561115) + "'", int26.equals((-1536561115)));
        org.junit.Assert.assertNotNull(wildcardClass27);
    }

    @Test
    public void test354() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test354");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 674850, (java.lang.Integer) (-203658532));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 338956, (java.lang.Integer) (-3030));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 1569639104, (java.lang.Integer) (-1198623488));
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-86408168), (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1027036680) + "'", int12.equals((-1027036680)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 371015616 + "'", int16.equals(371015616));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
    }

    @Test
    public void test355() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test355");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) 14);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 227358, (java.lang.Integer) 223);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-702061), (java.lang.Integer) (-7812));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 680607504, (java.lang.Integer) 1089);
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) 17564979);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 675040 + "'", int10.equals(675040));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227581 + "'", int13.equals(227581));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-694249) + "'", int16.equals((-694249)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 680606415 + "'", int19.equals(680606415));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 0 + "'", int23.equals(0));
    }

    @Test
    public void test356() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test356");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-674366));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-1027036680), (java.lang.Integer) (-28927));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 834392107, (java.lang.Integer) (-343065842));
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1427973632, (java.lang.Integer) 442626564);
        java.lang.Integer int31 = operacionesMatematicas0.restar((java.lang.Integer) (-166100), (java.lang.Integer) (-952340332));
        java.lang.Integer int34 = operacionesMatematicas0.restar((java.lang.Integer) 65, (java.lang.Integer) (-313922314));
        java.lang.Integer int37 = operacionesMatematicas0.restar((java.lang.Integer) 2022570152, (java.lang.Integer) (-1654132654));
        java.lang.Integer int40 = operacionesMatematicas0.restar((java.lang.Integer) 614351822, (java.lang.Integer) (-1627618704));
        java.lang.Integer int43 = operacionesMatematicas0.restar((java.lang.Integer) 2087982714, (java.lang.Integer) 668453655);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-203658532) + "'", int19.equals((-203658532)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 35504 + "'", int22.equals(35504));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-2) + "'", int25.equals((-2)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 1214289920 + "'", int28.equals(1214289920));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 952174232 + "'", int31.equals(952174232));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 313922379 + "'", int34.equals(313922379));
        org.junit.Assert.assertTrue("'" + int37 + "' != '" + (-618264490) + "'", int37.equals((-618264490)));
        org.junit.Assert.assertTrue("'" + int40 + "' != '" + (-2052996770) + "'", int40.equals((-2052996770)));
        org.junit.Assert.assertTrue("'" + int43 + "' != '" + 1419529059 + "'", int43.equals(1419529059));
    }

    @Test
    public void test357() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test357");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-674366));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 153262720, (java.lang.Integer) 86442695);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 533113047, (java.lang.Integer) (-143));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-203658532) + "'", int19.equals((-203658532)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 239705415 + "'", int22.equals(239705415));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1074245607 + "'", int25.equals(1074245607));
    }

    @Test
    public void test358() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test358");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 1352162704);
        java.lang.Integer int14 = operacionesMatematicas0.sumar((java.lang.Integer) (-1646083254), (java.lang.Integer) (-1646083254));
        java.lang.Integer int17 = operacionesMatematicas0.dividir((java.lang.Integer) (-890108686), (java.lang.Integer) (-679014));
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-844), (java.lang.Integer) 691719053);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 1727250696, (java.lang.Integer) 283089185);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-388878032) + "'", int11.equals((-388878032)));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 1002800788 + "'", int14.equals(1002800788));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 1310 + "'", int17.equals(1310));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-691719897) + "'", int20.equals((-691719897)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 6 + "'", int23.equals(6));
    }

    @Test
    public void test359() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test359");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-2031), (java.lang.Integer) (-100));
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-391), (java.lang.Integer) 191);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 894688107, (java.lang.Integer) 0);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-2131) + "'", int10.equals((-2131)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-74681) + "'", int13.equals((-74681)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 894688107 + "'", int16.equals(894688107));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
    }

    @Test
    public void test360() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test360");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-2233));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-676396), (java.lang.Integer) 100);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-223), (java.lang.Integer) (-198));
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 14, (java.lang.Integer) 12);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 223, (java.lang.Integer) 0);
        java.lang.Integer int24 = operacionesMatematicas0.dividir((java.lang.Integer) (-907044204), (java.lang.Integer) (-1988598960));
        java.lang.Integer int27 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-8230961), (java.lang.Integer) (-91798));
        java.lang.Integer int30 = operacionesMatematicas0.multiplicar((java.lang.Integer) 374697459, (java.lang.Integer) (-480618));
        java.lang.Integer int33 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1855525776, (java.lang.Integer) 1569639104);
        java.lang.Class<?> wildcardClass34 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-674366) + "'", int9.equals((-674366)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-6763) + "'", int12.equals((-6763)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-25) + "'", int15.equals((-25)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 168 + "'", int18.equals(168));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 223 + "'", int21.equals(223));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 0 + "'", int24.equals(0));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + (-328486218) + "'", int27.equals((-328486218)));
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + 1635371618 + "'", int30.equals(1635371618));
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + (-434222080) + "'", int33.equals((-434222080)));
        org.junit.Assert.assertNotNull(wildcardClass34);
    }

    @Test
    public void test361() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test361");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-98975152), (java.lang.Integer) (-300960));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 418510030, (java.lang.Integer) (-25));
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 996650796, (java.lang.Integer) 1737910004);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1963548160 + "'", int9.equals(1963548160));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 418510005 + "'", int13.equals(418510005));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-1560406496) + "'", int16.equals((-1560406496)));
    }

    @Test
    public void test362() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test362");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-2233));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) 1980253234, (java.lang.Integer) 30107083);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 100004, (java.lang.Integer) 161877486);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 532602761, (java.lang.Integer) (-1815854480));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 126122559, (java.lang.Integer) 1492597250);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-674366) + "'", int9.equals((-674366)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2010360317 + "'", int12.equals(2010360317));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1946510055) + "'", int18.equals((-1946510055)));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1383843202) + "'", int22.equals((-1383843202)));
    }

    @Test
    public void test363() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test363");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 374697400, (java.lang.Integer) 473676);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 44906012);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) (-86631468), (java.lang.Integer) 2);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 1771576096, (java.lang.Integer) 1144325196);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1680870648), (java.lang.Integer) 1029846951);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 791 + "'", int13.equals(791));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-44906012) + "'", int16.equals((-44906012)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-43315734) + "'", int19.equals((-43315734)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1379066004) + "'", int22.equals((-1379066004)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1122910776 + "'", int25.equals(1122910776));
    }

    @Test
    public void test364() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test364");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 200183);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) (-2283703));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 200282 + "'", int9.equals(200282));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2283703 + "'", int13.equals(2283703));
    }

    @Test
    public void test365() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test365");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-2233), (java.lang.Integer) 199);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 227358, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 28000, (java.lang.Integer) 675040);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 218, (java.lang.Integer) (-98));
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) (-27871872), (java.lang.Integer) 1161756880);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-11) + "'", int10.equals((-11)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227358 + "'", int13.equals(227358));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 703040 + "'", int16.equals(703040));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 120 + "'", int19.equals(120));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1189628752) + "'", int22.equals((-1189628752)));
    }

    @Test
    public void test366() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test366");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 218, (java.lang.Integer) 3233);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-970067068), (java.lang.Integer) 920644544);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 1678385021, (java.lang.Integer) 200232);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 906810492, (java.lang.Integer) 1968286926);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 3451 + "'", int9.equals(3451));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-922956032) + "'", int12.equals((-922956032)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 8382 + "'", int15.equals(8382));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 72502216 + "'", int18.equals(72502216));
    }

    @Test
    public void test367() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test367");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 302, (java.lang.Integer) 675054);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 24, (java.lang.Integer) 168);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 28000, (java.lang.Integer) 98974848);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-674752), (java.lang.Integer) (-157900046));
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 1980250204, (java.lang.Integer) 822834963);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-674752) + "'", int10.equals((-674752)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-144) + "'", int13.equals((-144)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 157225294 + "'", int19.equals(157225294));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1157415241 + "'", int22.equals(1157415241));
    }

    @Test
    public void test368() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test368");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 101);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) (-674366), (java.lang.Integer) 3020);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 223, (java.lang.Integer) (-589628652));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 2055113347, (java.lang.Integer) 1536832996);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 101 + "'", int4.equals(101));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-223) + "'", int7.equals((-223)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 589628875 + "'", int10.equals(589628875));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-703020953) + "'", int13.equals((-703020953)));
    }

    @Test
    public void test369() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test369");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-304), (java.lang.Integer) 12);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) (-2789518), (java.lang.Integer) 1426890059);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-655230575), (java.lang.Integer) (-949939300));
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1476845172, (java.lang.Integer) 1721190272);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-292) + "'", int7.equals((-292)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-1429679577) + "'", int10.equals((-1429679577)));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1877661532 + "'", int15.equals(1877661532));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1502438912 + "'", int18.equals(1502438912));
    }

    @Test
    public void test370() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test370");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) (-3), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.sumar((java.lang.Integer) 703051, (java.lang.Integer) 222308);
        java.lang.Integer int14 = operacionesMatematicas0.dividir((java.lang.Integer) 1741645850, (java.lang.Integer) 86235969);
        java.lang.Integer int17 = operacionesMatematicas0.multiplicar((java.lang.Integer) 877006832, (java.lang.Integer) (-203657736));
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 342593126, (java.lang.Integer) (-1565024324));
        java.lang.Class<?> wildcardClass21 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-3) + "'", int7.equals((-3)));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 925359 + "'", int11.equals(925359));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 20 + "'", int14.equals(20));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-85417856) + "'", int17.equals((-85417856)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-1222431198) + "'", int20.equals((-1222431198)));
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test371() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test371");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass6 = operacionesMatematicas0.getClass();
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 688, (java.lang.Integer) (-924510));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-678074), (java.lang.Integer) (-138799304));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-108954422), (java.lang.Integer) 166122);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-139477378) + "'", int12.equals((-139477378)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-109120544) + "'", int15.equals((-109120544)));
    }

    @Test
    public void test372() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test372");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-91808), (java.lang.Integer) 386);
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 135279661, (java.lang.Integer) (-40337365));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-847782612), (java.lang.Integer) 0);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 1256826432, (java.lang.Integer) (-608580408));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-237) + "'", int9.equals((-237)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-3) + "'", int12.equals((-3)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-847782612) + "'", int16.equals((-847782612)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 648246024 + "'", int19.equals(648246024));
    }

    @Test
    public void test373() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test373");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-105358747), (java.lang.Integer) 0);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) (-1220739868));
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) 1321880, (java.lang.Integer) (-680500222));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-105358747) + "'", int19.equals((-105358747)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1220739868 + "'", int22.equals(1220739868));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 681822102 + "'", int25.equals(681822102));
    }

    @Test
    public void test374() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test374");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-203658532), (java.lang.Integer) 797);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 138125136, (java.lang.Integer) (-198));
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) 2029);
        java.lang.Integer int27 = operacionesMatematicas0.restar((java.lang.Integer) 1150848763, (java.lang.Integer) (-1270063546));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-203657735) + "'", int18.equals((-203657735)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 138124938 + "'", int21.equals(138124938));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 6559757 + "'", int24.equals(6559757));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + (-1874054987) + "'", int27.equals((-1874054987)));
    }

    @Test
    public void test375() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test375");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.sumar((java.lang.Integer) 243122930, (java.lang.Integer) (-848979310));
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-11127039), (java.lang.Integer) 1029847456);
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) (-1565024324), (java.lang.Integer) (-892129759));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + (-605856380) + "'", int14.equals((-605856380)));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 1018720417 + "'", int17.equals(1018720417));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 1 + "'", int20.equals(1));
    }

    @Test
    public void test376() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test376");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-2233));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) 1980253234, (java.lang.Integer) 30107083);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 100004, (java.lang.Integer) 161877486);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 244290, (java.lang.Integer) (-328501944));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-674366) + "'", int9.equals((-674366)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2010360317 + "'", int12.equals(2010360317));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1724026000 + "'", int18.equals(1724026000));
    }

    @Test
    public void test377() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test377");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 679827);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-679726) + "'", int6.equals((-679726)));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertNotNull(wildcardClass8);
    }

    @Test
    public void test378() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test378");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.sumar((java.lang.Integer) (-1868612537), (java.lang.Integer) (-412981212));
        java.lang.Integer int11 = operacionesMatematicas0.dividir((java.lang.Integer) (-173593983), (java.lang.Integer) 133452995);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 2013373547 + "'", int8.equals(2013373547));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-1) + "'", int11.equals((-1)));
    }

    @Test
    public void test379() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test379");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) (-26796));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) (-3));
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) 677084, (java.lang.Integer) 204);
        java.lang.Integer int31 = operacionesMatematicas0.restar((java.lang.Integer) (-984), (java.lang.Integer) 34527);
        java.lang.Integer int34 = operacionesMatematicas0.sumar((java.lang.Integer) 1214037568, (java.lang.Integer) 1195703928);
        java.lang.Integer int37 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3241260, (java.lang.Integer) (-42632));
        java.lang.Integer int40 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1560406496));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-86631468) + "'", int22.equals((-86631468)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-103) + "'", int25.equals((-103)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 138125136 + "'", int28.equals(138125136));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-35511) + "'", int31.equals((-35511)));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + (-1885225800) + "'", int34.equals((-1885225800)));
        org.junit.Assert.assertTrue("'" + int37 + "' != '" + (-742442848) + "'", int37.equals((-742442848)));
        org.junit.Assert.assertTrue("'" + int40 + "' != '" + 0 + "'", int40.equals(0));
    }

    @Test
    public void test380() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test380");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) (-2034), (java.lang.Integer) (-1196138299));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertNotNull(wildcardClass18);
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1196136265 + "'", int22.equals(1196136265));
    }

    @Test
    public void test381() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test381");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-673143), (java.lang.Integer) (-100));
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-203658530), (java.lang.Integer) (-1072091024));
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 223403, (java.lang.Integer) (-747185024));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 1617474503, (java.lang.Integer) (-703020953));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 6731 + "'", int10.equals(6731));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 868432494 + "'", int13.equals(868432494));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 747408427 + "'", int16.equals(747408427));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1974471840) + "'", int19.equals((-1974471840)));
    }

    @Test
    public void test382() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test382");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 674850, (java.lang.Integer) (-203658532));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) (-103));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-1865816759), (java.lang.Integer) 1076637360);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-9) + "'", int12.equals((-9)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1352513177 + "'", int15.equals(1352513177));
    }

    @Test
    public void test383() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test383");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-198), (java.lang.Integer) 198);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-98990));
        java.lang.Class<?> wildcardClass21 = operacionesMatematicas0.getClass();
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) (-589628652), (java.lang.Integer) (-1530152438));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-98890) + "'", int20.equals((-98890)));
        org.junit.Assert.assertNotNull(wildcardClass21);
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-2119781090) + "'", int24.equals((-2119781090)));
    }

    @Test
    public void test384() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test384");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-2031), (java.lang.Integer) (-100));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-679014), (java.lang.Integer) 28345);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 2108473854);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-344077));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 65, (java.lang.Integer) 1346166);
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) 972267014, (java.lang.Integer) (-169435345));
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) 1105768, (java.lang.Integer) 1360503017);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-2131) + "'", int10.equals((-2131)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-650669) + "'", int13.equals((-650669)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-2108473854) + "'", int16.equals((-2108473854)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1346231 + "'", int22.equals(1346231));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1141702359 + "'", int25.equals(1141702359));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-1359397249) + "'", int28.equals((-1359397249)));
    }

    @Test
    public void test385() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test385");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.sumar((java.lang.Integer) (-241069610), (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-241069610) + "'", int11.equals((-241069610)));
    }

    @Test
    public void test386() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test386");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 10, (java.lang.Integer) 302);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-844), (java.lang.Integer) 675040);
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) (-485640240), (java.lang.Integer) 99);
        java.lang.Integer int24 = operacionesMatematicas0.restar((java.lang.Integer) (-1427973632), (java.lang.Integer) (-506915));
        java.lang.Integer int27 = operacionesMatematicas0.dividir((java.lang.Integer) 1592787391, (java.lang.Integer) 1988598756);
        java.lang.Integer int30 = operacionesMatematicas0.restar((java.lang.Integer) 157227294, (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 3020 + "'", int15.equals(3020));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 674196 + "'", int18.equals(674196));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-485640339) + "'", int21.equals((-485640339)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1427466717) + "'", int24.equals((-1427466717)));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 0 + "'", int27.equals(0));
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + 157227294 + "'", int30.equals(157227294));
    }

    @Test
    public void test387() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test387");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-98975152), (java.lang.Integer) (-676594));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 117, (java.lang.Integer) 1);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-201378), (java.lang.Integer) (-3260));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 698720868, (java.lang.Integer) (-6451));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 1002777288);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 146 + "'", int12.equals(146));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 118 + "'", int15.equals(118));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-198118) + "'", int18.equals((-198118)));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-2027625964) + "'", int22.equals((-2027625964)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1002777288 + "'", int25.equals(1002777288));
    }

    @Test
    public void test388() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test388");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166122, (java.lang.Integer) (-2030));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 202, (java.lang.Integer) (-1546915));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-109415642), (java.lang.Integer) (-909226455));
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) 1352089752, (java.lang.Integer) (-409642758));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 199 + "'", int6.equals(199));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-337227660) + "'", int9.equals((-337227660)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-1035598570) + "'", int17.equals((-1035598570)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-3) + "'", int20.equals((-3)));
    }

    @Test
    public void test389() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test389");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-28275), (java.lang.Integer) 200184);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-1896219511), (java.lang.Integer) (-669335781));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 418510030, (java.lang.Integer) 523299800);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1729412004 + "'", int12.equals(1729412004));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 8366032 + "'", int15.equals(8366032));
    }

    @Test
    public void test390() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test390");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-985129904), (java.lang.Integer) (-299613002));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 714849216, (java.lang.Integer) 1923387680);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3 + "'", int12.equals(3));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1208538464) + "'", int15.equals((-1208538464)));
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test391() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test391");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 473676, (java.lang.Integer) (-2030));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 20300, (java.lang.Integer) 338956);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 3021, (java.lang.Integer) 3274);
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-43315734), (java.lang.Integer) (-1001435549));
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-961562280) + "'", int12.equals((-961562280)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 359256 + "'", int15.equals(359256));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 6295 + "'", int18.equals(6295));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 299478398 + "'", int21.equals(299478398));
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test392() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test392");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1913));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-28275), (java.lang.Integer) (-98975152));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 688, (java.lang.Integer) 2029);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-139473728), (java.lang.Integer) 9812897);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) (-1709861456), (java.lang.Integer) (-139423347));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 117 + "'", int9.equals(117));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1796254192) + "'", int12.equals((-1796254192)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1541407936 + "'", int19.equals(1541407936));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1570438109) + "'", int22.equals((-1570438109)));
    }

    @Test
    public void test393() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test393");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.restar((java.lang.Integer) (-3160), (java.lang.Integer) (-203662590));
        java.lang.Integer int11 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1913), (java.lang.Integer) 191);
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-1732880844), (java.lang.Integer) 1352434);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-855249787), (java.lang.Integer) (-245251504));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 203659430 + "'", int8.equals(203659430));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-365383) + "'", int11.equals((-365383)));
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1734233278) + "'", int15.equals((-1734233278)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-609998283) + "'", int18.equals((-609998283)));
    }

    @Test
    public void test394() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test394");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-223), (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-86408168), (java.lang.Integer) (-1480217496));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2679600, (java.lang.Integer) (-34162));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1199317608, (java.lang.Integer) (-1162179335));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223 + "'", int6.equals(223));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-1566625664) + "'", int9.equals((-1566625664)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1346181984) + "'", int13.equals((-1346181984)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-98674904) + "'", int16.equals((-98674904)));
    }

    @Test
    public void test395() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test395");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2), (java.lang.Integer) 2030);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-221533), (java.lang.Integer) 0);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1351444, (java.lang.Integer) (-28275));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-4060) + "'", int12.equals((-4060)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-221533) + "'", int15.equals((-221533)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 442626564 + "'", int18.equals(442626564));
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test396() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test396");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) (-134082459), (java.lang.Integer) 30106984);
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) (-1796353170), (java.lang.Integer) 1636528032);
        java.lang.Class<?> wildcardClass27 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-4) + "'", int23.equals((-4)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-159825138) + "'", int26.equals((-159825138)));
        org.junit.Assert.assertNotNull(wildcardClass27);
    }

    @Test
    public void test397() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test397");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-203658532), (java.lang.Integer) 797);
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) (-5600), (java.lang.Integer) (-2233));
        java.lang.Integer int24 = operacionesMatematicas0.restar((java.lang.Integer) 2679600, (java.lang.Integer) (-1896184000));
        java.lang.Integer int27 = operacionesMatematicas0.restar((java.lang.Integer) (-682176), (java.lang.Integer) 0);
        java.lang.Integer int30 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1304, (java.lang.Integer) (-203682216));
        java.lang.Integer int33 = operacionesMatematicas0.dividir((java.lang.Integer) (-1438112546), (java.lang.Integer) (-177082801));
        java.lang.Integer int36 = operacionesMatematicas0.sumar((java.lang.Integer) (-1937186348), (java.lang.Integer) (-245718859));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-203657735) + "'", int18.equals((-203657735)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 2 + "'", int21.equals(2));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 1898863600 + "'", int24.equals(1898863600));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + (-682176) + "'", int27.equals((-682176)));
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + 686362688 + "'", int30.equals(686362688));
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + 8 + "'", int33.equals(8));
        org.junit.Assert.assertTrue("'" + int36 + "' != '" + 2112062089 + "'", int36.equals(2112062089));
    }

    @Test
    public void test398() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test398");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 10495029, (java.lang.Integer) (-202979700));
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-56947941), (java.lang.Integer) 914381443);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 171100924 + "'", int13.equals(171100924));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-971329384) + "'", int16.equals((-971329384)));
    }

    @Test
    public void test399() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test399");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-98975152), (java.lang.Integer) (-300960));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 200871, (java.lang.Integer) (-702061));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203177900, (java.lang.Integer) 6295);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) (-202980456), (java.lang.Integer) (-1203252856));
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) (-844), (java.lang.Integer) (-1561128462));
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1963548160 + "'", int9.equals(1963548160));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 902932 + "'", int12.equals(902932));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-895373708) + "'", int15.equals((-895373708)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test400() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test400");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) 2030, (java.lang.Integer) 688);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) (-2233));
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.dividir((java.lang.Integer) (-153263374), (java.lang.Integer) (-1263368348));
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 959382027, (java.lang.Integer) 1355465799);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7.equals(2));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 225533 + "'", int10.equals(225533));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14.equals(0));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-396083772) + "'", int17.equals((-396083772)));
    }

    @Test
    public void test401() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test401");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) (-26796));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) (-3));
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) 677084, (java.lang.Integer) 204);
        java.lang.Integer int31 = operacionesMatematicas0.restar((java.lang.Integer) (-984), (java.lang.Integer) 34527);
        java.lang.Class<?> wildcardClass32 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass33 = operacionesMatematicas0.getClass();
        java.lang.Integer int36 = operacionesMatematicas0.dividir((java.lang.Integer) 273834434, (java.lang.Integer) 200282);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-86631468) + "'", int22.equals((-86631468)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-103) + "'", int25.equals((-103)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 138125136 + "'", int28.equals(138125136));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-35511) + "'", int31.equals((-35511)));
        org.junit.Assert.assertNotNull(wildcardClass32);
        org.junit.Assert.assertNotNull(wildcardClass33);
        org.junit.Assert.assertTrue("'" + int36 + "' != '" + 1367 + "'", int36.equals(1367));
    }

    @Test
    public void test402() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test402");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-337227660), (java.lang.Integer) (-2962));
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-2131));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 39396980, (java.lang.Integer) (-1072091024));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 113851 + "'", int10.equals(113851));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 2139787968 + "'", int16.equals(2139787968));
    }

    @Test
    public void test403() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test403");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 101);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) (-674366), (java.lang.Integer) 3020);
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 200183);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-310476446), (java.lang.Integer) 0);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 101 + "'", int4.equals(101));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-223) + "'", int7.equals((-223)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 200184 + "'", int10.equals(200184));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14.equals(0));
    }

    @Test
    public void test404() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test404");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166122, (java.lang.Integer) (-2030));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 202, (java.lang.Integer) (-1546915));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-1270063546), (java.lang.Integer) (-5600));
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 2135641200, (java.lang.Integer) (-892130051));
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) 173359620, (java.lang.Integer) 42226412);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 199 + "'", int6.equals(199));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-337227660) + "'", int9.equals((-337227660)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-1270069146) + "'", int17.equals((-1270069146)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 1243511149 + "'", int20.equals(1243511149));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-405920848) + "'", int23.equals((-405920848)));
    }

    @Test
    public void test405() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test405");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-203657735), (java.lang.Integer) (-98975152));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-872882432), (java.lang.Integer) 1374);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-203), (java.lang.Integer) 3233);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-453522300), (java.lang.Integer) 2108475396);
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) 186, (java.lang.Integer) 1346166);
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-104682583) + "'", int9.equals((-104682583)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-635285) + "'", int12.equals((-635285)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1654953096 + "'", int18.equals(1654953096));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test406() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test406");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) (-26796));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) (-3));
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) (-678748));
        java.lang.Integer int31 = operacionesMatematicas0.sumar((java.lang.Integer) 696288, (java.lang.Integer) (-681446));
        java.lang.Integer int34 = operacionesMatematicas0.restar((java.lang.Integer) (-372389960), (java.lang.Integer) (-1953902356));
        java.lang.Integer int37 = operacionesMatematicas0.dividir((java.lang.Integer) 1107075312, (java.lang.Integer) 1346231);
        java.lang.Integer int40 = operacionesMatematicas0.sumar((java.lang.Integer) 7809, (java.lang.Integer) 532602761);
        java.lang.Integer int43 = operacionesMatematicas0.dividir((java.lang.Integer) (-221508004), (java.lang.Integer) (-222612));
        java.lang.Integer int46 = operacionesMatematicas0.restar((java.lang.Integer) (-27186), (java.lang.Integer) 844551780);
        java.lang.Integer int49 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-13), (java.lang.Integer) 58376960);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-86631468) + "'", int22.equals((-86631468)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-103) + "'", int25.equals((-103)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 678545 + "'", int28.equals(678545));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 14842 + "'", int31.equals(14842));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 1581512396 + "'", int34.equals(1581512396));
        org.junit.Assert.assertTrue("'" + int37 + "' != '" + 822 + "'", int37.equals(822));
        org.junit.Assert.assertTrue("'" + int40 + "' != '" + 532610570 + "'", int40.equals(532610570));
        org.junit.Assert.assertTrue("'" + int43 + "' != '" + 995 + "'", int43.equals(995));
        org.junit.Assert.assertTrue("'" + int46 + "' != '" + (-844578966) + "'", int46.equals((-844578966)));
        org.junit.Assert.assertTrue("'" + int49 + "' != '" + (-758900480) + "'", int49.equals((-758900480)));
    }

    @Test
    public void test407() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test407");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 100004, (java.lang.Integer) 222308);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 218, (java.lang.Integer) 166122);
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-165904) + "'", int12.equals((-165904)));
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test408() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test408");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 473676, (java.lang.Integer) (-2030));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-292), (java.lang.Integer) 2108502328);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-86797590), (java.lang.Integer) 674850);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 225533, (java.lang.Integer) 0);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 1199317608, (java.lang.Integer) 1980250204);
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) 868214330, (java.lang.Integer) (-233883));
        java.lang.Integer int31 = operacionesMatematicas0.restar((java.lang.Integer) (-1808117328), (java.lang.Integer) 2139858193);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-961562280) + "'", int12.equals((-961562280)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-589628652) + "'", int19.equals((-589628652)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 225533 + "'", int22.equals(225533));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-1115399484) + "'", int25.equals((-1115399484)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-3712) + "'", int28.equals((-3712)));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 346991775 + "'", int31.equals(346991775));
    }

    @Test
    public void test409() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test409");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-223), (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-86408168), (java.lang.Integer) (-1480217496));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-336954577), (java.lang.Integer) (-181782106));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 1829024444, (java.lang.Integer) (-1011630517));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223 + "'", int6.equals(223));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-1566625664) + "'", int9.equals((-1566625664)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-518736683) + "'", int12.equals((-518736683)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1) + "'", int15.equals((-1)));
    }

    @Test
    public void test410() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test410");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 674850, (java.lang.Integer) (-203658532));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 338956, (java.lang.Integer) (-3030));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 1569639104, (java.lang.Integer) (-1198623488));
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 677084, (java.lang.Integer) (-967623544));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1027036680) + "'", int12.equals((-1027036680)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 371015616 + "'", int16.equals(371015616));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
    }

    @Test
    public void test411() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test411");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 679827);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-203658532), (java.lang.Integer) (-589628652));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-202979314), (java.lang.Integer) 203177900);
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1110802624, (java.lang.Integer) 1240110195);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-679726) + "'", int6.equals((-679726)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 961940784 + "'", int9.equals(961940784));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 198586 + "'", int12.equals(198586));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 469227072 + "'", int17.equals(469227072));
    }

    @Test
    public void test412() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test412");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-2233), (java.lang.Integer) 199);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 227358, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 28000, (java.lang.Integer) 675040);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 218, (java.lang.Integer) (-98));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 677082, (java.lang.Integer) 589628875);
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) (-56), (java.lang.Integer) 337185028);
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) (-1115350710), (java.lang.Integer) 1937735020);
        java.lang.Integer int31 = operacionesMatematicas0.sumar((java.lang.Integer) (-1500976826), (java.lang.Integer) 794901606);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-11) + "'", int10.equals((-11)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227358 + "'", int13.equals(227358));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 703040 + "'", int16.equals(703040));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 120 + "'", int19.equals(120));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-337185084) + "'", int25.equals((-337185084)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 1241881566 + "'", int28.equals(1241881566));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-706075220) + "'", int31.equals((-706075220)));
    }

    @Test
    public void test413() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test413");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test414() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test414");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-26796), (java.lang.Integer) (-2131));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-674366), (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-678748), (java.lang.Integer) (-678748));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) (-11127039), (java.lang.Integer) 1701552590);
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-507), (java.lang.Integer) 7);
        java.lang.Class<?> wildcardClass25 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-28927) + "'", int12.equals((-28927)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-133524468) + "'", int15.equals((-133524468)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1712679629) + "'", int21.equals((-1712679629)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-3549) + "'", int24.equals((-3549)));
        org.junit.Assert.assertNotNull(wildcardClass25);
    }

    @Test
    public void test415() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test415");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-203658532), (java.lang.Integer) 797);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 138125136, (java.lang.Integer) (-198));
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) 2029);
        java.lang.Class<?> wildcardClass25 = operacionesMatematicas0.getClass();
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) 541237670, (java.lang.Integer) (-2052996770));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-203657735) + "'", int18.equals((-203657735)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 138124938 + "'", int21.equals(138124938));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 6559757 + "'", int24.equals(6559757));
        org.junit.Assert.assertNotNull(wildcardClass25);
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-1700732856) + "'", int28.equals((-1700732856)));
    }

    @Test
    public void test416() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test416");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-26796), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) (-6763), (java.lang.Integer) (-4060));
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) (-28474), (java.lang.Integer) 199);
        java.lang.Integer int29 = operacionesMatematicas0.sumar((java.lang.Integer) (-319237330), (java.lang.Integer) (-86797590));
        java.lang.Integer int32 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1566890908, (java.lang.Integer) 0);
        java.lang.Integer int35 = operacionesMatematicas0.restar((java.lang.Integer) 4051228, (java.lang.Integer) (-654));
        java.lang.Integer int38 = operacionesMatematicas0.dividir((java.lang.Integer) (-65), (java.lang.Integer) 578855629);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-28474) + "'", int20.equals((-28474)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 1 + "'", int23.equals(1));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-28275) + "'", int26.equals((-28275)));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-406034920) + "'", int29.equals((-406034920)));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 0 + "'", int32.equals(0));
        org.junit.Assert.assertTrue("'" + int35 + "' != '" + 4051882 + "'", int35.equals(4051882));
        org.junit.Assert.assertTrue("'" + int38 + "' != '" + 0 + "'", int38.equals(0));
    }

    @Test
    public void test417() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test417");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 679827, (java.lang.Integer) 129067);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-203658532), (java.lang.Integer) (-1174185152));
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 1971875568, (java.lang.Integer) (-136759878));
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 436681114, (java.lang.Integer) 1278130677);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 808894 + "'", int10.equals(808894));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 2108635446 + "'", int16.equals(2108635446));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
    }

    @Test
    public void test418() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test418");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-304), (java.lang.Integer) 12);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 902932, (java.lang.Integer) 199);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2989800, (java.lang.Integer) (-203657735));
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203658612), (java.lang.Integer) (-453522300));
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2679600, (java.lang.Integer) (-4));
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) (-1791717109), (java.lang.Integer) 72502216);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-292) + "'", int7.equals((-292)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 179683468 + "'", int10.equals(179683468));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 249863688 + "'", int16.equals(249863688));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2679596 + "'", int19.equals(2679596));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1864219325) + "'", int22.equals((-1864219325)));
    }

    @Test
    public void test419() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test419");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) (-292));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-694249), (java.lang.Integer) (-343065842));
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) (-970067068));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 195080, (java.lang.Integer) 202);
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) 1270901478, (java.lang.Integer) 669356581);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-882132) + "'", int16.equals((-882132)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 342371593 + "'", int19.equals(342371593));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 970067068 + "'", int22.equals(970067068));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 195282 + "'", int25.equals(195282));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 601544897 + "'", int28.equals(601544897));
    }

    @Test
    public void test420() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test420");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1419118905), (java.lang.Integer) (-1243099180));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1556073023), (java.lang.Integer) 952174232);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 47990220 + "'", int12.equals(47990220));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 769880216 + "'", int15.equals(769880216));
    }

    @Test
    public void test421() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test421");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2000);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-11), (java.lang.Integer) (-678074));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1623620464, (java.lang.Integer) (-480618));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 7458814 + "'", int12.equals(7458814));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1497058400) + "'", int15.equals((-1497058400)));
    }

    @Test
    public void test422() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test422");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 100004, (java.lang.Integer) 0);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 3021, (java.lang.Integer) 100);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) (-197795));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 200184, (java.lang.Integer) 84409818);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-1183), (java.lang.Integer) (-28676780));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) (-28676780), (java.lang.Integer) (-127541488));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 100004 + "'", int12.equals(100004));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 30 + "'", int15.equals(30));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-44108285) + "'", int18.equals((-44108285)));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-28677963) + "'", int25.equals((-28677963)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
    }

    @Test
    public void test423() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test423");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 791, (java.lang.Integer) 222612);
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) (-1358028), (java.lang.Integer) (-203289362));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 223403 + "'", int22.equals(223403));
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 0 + "'", int26.equals(0));
    }

    @Test
    public void test424() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test424");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) (-203654655));
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) (-169), (java.lang.Integer) 1006273798);
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 203654756 + "'", int18.equals(203654756));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1006273629 + "'", int21.equals(1006273629));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test425() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test425");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) 14);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-1979406238), (java.lang.Integer) (-350481344));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 675040 + "'", int10.equals(675040));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1965079714 + "'", int13.equals(1965079714));
    }

    @Test
    public void test426() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test426");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 1963548160, (java.lang.Integer) 100);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 1476845647, (java.lang.Integer) 426562025);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 1963548060 + "'", int10.equals(1963548060));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1903407672 + "'", int13.equals(1903407672));
    }

    @Test
    public void test427() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test427");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.dividir((java.lang.Integer) 198, (java.lang.Integer) 2989800);
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) 701863, (java.lang.Integer) (-203658530));
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) 129067, (java.lang.Integer) 654);
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 419736786 + "'", int20.equals(419736786));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 84409818 + "'", int23.equals(84409818));
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test428() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test428");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-2031), (java.lang.Integer) (-100));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-679014), (java.lang.Integer) 28345);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 2108473854);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-344077));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 65, (java.lang.Integer) 1346166);
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) (-23417576));
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) 3241260, (java.lang.Integer) (-674571));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-2131) + "'", int10.equals((-2131)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-650669) + "'", int13.equals((-650669)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-2108473854) + "'", int16.equals((-2108473854)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1346231 + "'", int22.equals(1346231));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 23417576 + "'", int25.equals(23417576));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 3915831 + "'", int28.equals(3915831));
    }

    @Test
    public void test429() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test429");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 99, (java.lang.Integer) (-676396));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-982799360), (java.lang.Integer) (-6451));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 676495 + "'", int7.equals(676495));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 152348 + "'", int10.equals(152348));
    }

    @Test
    public void test430() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test430");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 473676, (java.lang.Integer) 222612);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.restar((java.lang.Integer) (-157900046), (java.lang.Integer) 44906012);
        java.lang.Integer int14 = operacionesMatematicas0.sumar((java.lang.Integer) 791, (java.lang.Integer) 1772925796);
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 1898863600, (java.lang.Integer) 216149571);
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1383214630, (java.lang.Integer) 565899369);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 696288 + "'", int7.equals(696288));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-202806058) + "'", int11.equals((-202806058)));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 1772926587 + "'", int14.equals(1772926587));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 1682714029 + "'", int17.equals(1682714029));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 1236569494 + "'", int20.equals(1236569494));
    }

    @Test
    public void test431() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test431");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-304), (java.lang.Integer) 12);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) (-2789518), (java.lang.Integer) 1426890059);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 9120400, (java.lang.Integer) 20);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-292) + "'", int7.equals((-292)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-1429679577) + "'", int10.equals((-1429679577)));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 456020 + "'", int15.equals(456020));
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test432() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test432");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 302, (java.lang.Integer) 675054);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1918641047, (java.lang.Integer) 131302456);
        java.lang.Integer int17 = operacionesMatematicas0.dividir((java.lang.Integer) 1985458364, (java.lang.Integer) (-821417174));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-674752) + "'", int10.equals((-674752)));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + (-50944760) + "'", int14.equals((-50944760)));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-2) + "'", int17.equals((-2)));
    }

    @Test
    public void test433() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test433");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 2030, (java.lang.Integer) 688);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1085946010), (java.lang.Integer) (-824464727));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2 + "'", int6.equals(2));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-261481283) + "'", int9.equals((-261481283)));
    }

    @Test
    public void test434() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test434");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 166111, (java.lang.Integer) (-594099455));
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) (-396083772), (java.lang.Integer) (-9122452));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-593933344) + "'", int13.equals((-593933344)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-405206224) + "'", int16.equals((-405206224)));
    }

    @Test
    public void test435() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test435");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2030), (java.lang.Integer) 99990);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1355331012, (java.lang.Integer) 1971875568);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-854220070), (java.lang.Integer) (-2063594965));
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1198151702, (java.lang.Integer) 438482334);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-202979700) + "'", int9.equals((-202979700)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 852880276 + "'", int20.equals(852880276));
    }

    @Test
    public void test436() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test436");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-198), (java.lang.Integer) 198);
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) (-1379066004), (java.lang.Integer) (-23));
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 1889283349, (java.lang.Integer) (-3));
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        java.lang.Integer int27 = operacionesMatematicas0.restar((java.lang.Integer) (-458350076), (java.lang.Integer) (-1611354393));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 59959391 + "'", int20.equals(59959391));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-629761116) + "'", int23.equals((-629761116)));
        org.junit.Assert.assertNotNull(wildcardClass24);
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 1153004317 + "'", int27.equals(1153004317));
    }

    @Test
    public void test437() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test437");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 218, (java.lang.Integer) 3233);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-970067068), (java.lang.Integer) 920644544);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-1744250912), (java.lang.Integer) (-1809923325));
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-98965), (java.lang.Integer) 797559558);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 3451 + "'", int9.equals(3451));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-922956032) + "'", int12.equals((-922956032)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 740793059 + "'", int15.equals(740793059));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-797658523) + "'", int18.equals((-797658523)));
    }

    @Test
    public void test438() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test438");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 677082, (java.lang.Integer) 797);
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) (-1378461349), (java.lang.Integer) (-1988598960));
        java.lang.Integer int29 = operacionesMatematicas0.restar((java.lang.Integer) 1, (java.lang.Integer) (-961562280));
        java.lang.Integer int32 = operacionesMatematicas0.restar((java.lang.Integer) 29686803, (java.lang.Integer) (-302635422));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 849 + "'", int22.equals(849));
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 0 + "'", int26.equals(0));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 961562281 + "'", int29.equals(961562281));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 332322225 + "'", int32.equals(332322225));
    }

    @Test
    public void test439() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test439");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 669356581, (java.lang.Integer) (-5));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-945000448), (java.lang.Integer) (-203656745));
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-203859113));
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-7812), (java.lang.Integer) 1096885911);
        java.lang.Integer int31 = operacionesMatematicas0.dividir((java.lang.Integer) 365093991, (java.lang.Integer) (-203289362));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 669356576 + "'", int19.equals(669356576));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 255987712 + "'", int22.equals(255987712));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-412981212) + "'", int28.equals((-412981212)));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-1) + "'", int31.equals((-1)));
    }

    @Test
    public void test440() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test440");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-198), (java.lang.Integer) 198);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-98990));
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2117069372, (java.lang.Integer) (-4060));
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-917), (java.lang.Integer) (-1988598960));
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) 277200146, (java.lang.Integer) 1106533960);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-98890) + "'", int20.equals((-98890)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-1072091024) + "'", int23.equals((-1072091024)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-1815854480) + "'", int26.equals((-1815854480)));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-2010003184) + "'", int29.equals((-2010003184)));
    }

    @Test
    public void test441() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test441");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1023986076, (java.lang.Integer) (-100));
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) 9120300, (java.lang.Integer) (-203656345));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 680607504 + "'", int20.equals(680607504));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-1111508044) + "'", int23.equals((-1111508044)));
    }

    @Test
    public void test442() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test442");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) (-139407012), (java.lang.Integer) 203658830);
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) 118, (java.lang.Integer) 1143182576);
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) (-5541169), (java.lang.Integer) 8960);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-343065842) + "'", int17.equals((-343065842)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 0 + "'", int20.equals(0));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-5532209) + "'", int23.equals((-5532209)));
    }

    @Test
    public void test443() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test443");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-204), (java.lang.Integer) 12);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 797, (java.lang.Integer) (-201378));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 2111569680, (java.lang.Integer) (-328501944));
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 311282688);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-17) + "'", int6.equals((-17)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-1854895672) + "'", int16.equals((-1854895672)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 311282688 + "'", int19.equals(311282688));
    }

    @Test
    public void test444() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test444");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-673143), (java.lang.Integer) (-100));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2117069372, (java.lang.Integer) 1996158842);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-30573510), (java.lang.Integer) (-1796353170));
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 6731 + "'", int10.equals(6731));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1 + "'", int13.equals(1));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertNotNull(wildcardClass18);
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test445() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test445");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 677082, (java.lang.Integer) 797);
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) (-6), (java.lang.Integer) 2354);
        java.lang.Integer int29 = operacionesMatematicas0.sumar((java.lang.Integer) 43690, (java.lang.Integer) 712551022);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 849 + "'", int22.equals(849));
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 2348 + "'", int26.equals(2348));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 712594712 + "'", int29.equals(712594712));
    }

    @Test
    public void test446() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test446");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 2354, (java.lang.Integer) (-403));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) (-28474));
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) (-56948000));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-5) + "'", int12.equals((-5)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-6349702) + "'", int15.equals((-6349702)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-56948000) + "'", int18.equals((-56948000)));
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test447() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test447");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-26796), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) (-6763), (java.lang.Integer) (-4060));
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) 1542, (java.lang.Integer) 2108473854);
        java.lang.Integer int29 = operacionesMatematicas0.restar((java.lang.Integer) (-3162), (java.lang.Integer) (-903099228));
        java.lang.Integer int32 = operacionesMatematicas0.restar((java.lang.Integer) 669356581, (java.lang.Integer) (-392600492));
        java.lang.Integer int35 = operacionesMatematicas0.sumar((java.lang.Integer) 2145914048, (java.lang.Integer) (-1656482186));
        java.lang.Integer int38 = operacionesMatematicas0.multiplicar((java.lang.Integer) 199825, (java.lang.Integer) (-902663315));
        java.lang.Class<?> wildcardClass39 = operacionesMatematicas0.getClass();
        java.lang.Integer int42 = operacionesMatematicas0.dividir((java.lang.Integer) 834595461, (java.lang.Integer) 654);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-28474) + "'", int20.equals((-28474)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 1 + "'", int23.equals(1));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 2108475396 + "'", int26.equals(2108475396));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 903096066 + "'", int29.equals(903096066));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 1061957073 + "'", int32.equals(1061957073));
        org.junit.Assert.assertTrue("'" + int35 + "' != '" + 489431862 + "'", int35.equals(489431862));
        org.junit.Assert.assertTrue("'" + int38 + "' != '" + 1044610237 + "'", int38.equals(1044610237));
        org.junit.Assert.assertNotNull(wildcardClass39);
        org.junit.Assert.assertTrue("'" + int42 + "' != '" + 1276139 + "'", int42.equals(1276139));
    }

    @Test
    public void test448() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test448");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-304));
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 191, (java.lang.Integer) 137901733);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 2010197328, (java.lang.Integer) (-133524468));
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-593256750), (java.lang.Integer) (-1429679800));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) 767787264, (java.lang.Integer) 38144331);
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) (-134082459), (java.lang.Integer) (-373730));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 569427227 + "'", int13.equals(569427227));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-15) + "'", int16.equals((-15)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-2022936550) + "'", int19.equals((-2022936550)));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 729642933 + "'", int23.equals(729642933));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 358 + "'", int26.equals(358));
    }

    @Test
    public void test449() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test449");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166122, (java.lang.Integer) (-2030));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.dividir((java.lang.Integer) (-1691091190), (java.lang.Integer) (-785246099));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 199 + "'", int6.equals(199));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-337227660) + "'", int9.equals((-337227660)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 2 + "'", int14.equals(2));
    }

    @Test
    public void test450() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test450");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 990, (java.lang.Integer) 302);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 7458814, (java.lang.Integer) (-676296));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) 1710, (java.lang.Integer) 168);
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) (-86797590), (java.lang.Integer) (-1646083254));
        java.lang.Integer int27 = operacionesMatematicas0.dividir((java.lang.Integer) (-1827), (java.lang.Integer) (-17));
        java.lang.Integer int30 = operacionesMatematicas0.restar((java.lang.Integer) (-1614297536), (java.lang.Integer) 705204090);
        java.lang.Integer int33 = operacionesMatematicas0.restar((java.lang.Integer) (-203435127), (java.lang.Integer) 961562281);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 688 + "'", int15.equals(688));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-11) + "'", int18.equals((-11)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1542 + "'", int21.equals(1542));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1732880844) + "'", int24.equals((-1732880844)));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 107 + "'", int27.equals(107));
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + 1975465670 + "'", int30.equals(1975465670));
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + (-1164997408) + "'", int33.equals((-1164997408)));
    }

    @Test
    public void test451() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test451");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2, (java.lang.Integer) (-1));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) 338956);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-300960), (java.lang.Integer) 2679600);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 298, (java.lang.Integer) (-28474));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 902932, (java.lang.Integer) 1739459091);
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) (-485636182), (java.lang.Integer) (-563780412));
        java.lang.Integer int31 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-25), (java.lang.Integer) 445124859);
        java.lang.Class<?> wildcardClass32 = operacionesMatematicas0.getClass();
        java.lang.Integer int35 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-952342361), (java.lang.Integer) (-12090128));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-2) + "'", int13.equals((-2)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1023986076 + "'", int16.equals(1023986076));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1001435648 + "'", int19.equals(1001435648));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-28176) + "'", int22.equals((-28176)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1740362023 + "'", int25.equals(1740362023));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 1756780413 + "'", int31.equals(1756780413));
        org.junit.Assert.assertNotNull(wildcardClass32);
        org.junit.Assert.assertTrue("'" + int35 + "' != '" + 1307130000 + "'", int35.equals(1307130000));
    }

    @Test
    public void test452() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test452");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-91808), (java.lang.Integer) 386);
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 135279661, (java.lang.Integer) (-40337365));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-2108473854), (java.lang.Integer) 1833676529);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-237) + "'", int9.equals((-237)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-3) + "'", int12.equals((-3)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-1) + "'", int16.equals((-1)));
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test453() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test453");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 990, (java.lang.Integer) 302);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 7458814, (java.lang.Integer) (-676296));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) 1710, (java.lang.Integer) 168);
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) (-86797590), (java.lang.Integer) (-1646083254));
        java.lang.Integer int27 = operacionesMatematicas0.dividir((java.lang.Integer) (-1827), (java.lang.Integer) (-17));
        java.lang.Integer int30 = operacionesMatematicas0.sumar((java.lang.Integer) 791133635, (java.lang.Integer) 102968);
        java.lang.Integer int33 = operacionesMatematicas0.sumar((java.lang.Integer) 30, (java.lang.Integer) 183914880);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 688 + "'", int15.equals(688));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-11) + "'", int18.equals((-11)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1542 + "'", int21.equals(1542));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1732880844) + "'", int24.equals((-1732880844)));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 107 + "'", int27.equals(107));
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + 791236603 + "'", int30.equals(791236603));
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + 183914910 + "'", int33.equals(183914910));
    }

    @Test
    public void test454() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test454");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-203657735), (java.lang.Integer) (-98975152));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1079, (java.lang.Integer) 222612);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 99692, (java.lang.Integer) 203177900);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 1003415040, (java.lang.Integer) (-6198));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-104682583) + "'", int9.equals((-104682583)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-221533) + "'", int12.equals((-221533)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-203078208) + "'", int15.equals((-203078208)));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1003408842 + "'", int19.equals(1003408842));
    }

    @Test
    public void test455() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test455");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 1089, (java.lang.Integer) 10);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-304));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) (-1351735770), (java.lang.Integer) (-674424));
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) (-1448119822), (java.lang.Integer) (-103));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1079 + "'", int16.equals(1079));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-91808) + "'", int19.equals((-91808)));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-1351061346) + "'", int23.equals((-1351061346)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 14059415 + "'", int26.equals(14059415));
    }

    @Test
    public void test456() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test456");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 199);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1734056208), (java.lang.Integer) 204);
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-1072115060), (java.lang.Integer) (-2474));
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1346531, (java.lang.Integer) (-299613002));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 298 + "'", int9.equals(298));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + (-1560148160) + "'", int14.equals((-1560148160)));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-1072117534) + "'", int17.equals((-1072117534)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 967819106 + "'", int20.equals(967819106));
    }

    @Test
    public void test457() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test457");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1913));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-121741867), (java.lang.Integer) 1106533862);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-169435345), (java.lang.Integer) (-4538682));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 117 + "'", int9.equals(117));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1228275729) + "'", int13.equals((-1228275729)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 37 + "'", int16.equals(37));
    }

    @Test
    public void test458() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test458");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 101);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 2029, (java.lang.Integer) 2029);
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 6559757, (java.lang.Integer) 1442258560);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-673143), (java.lang.Integer) 790457339);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 473676, (java.lang.Integer) 996650801);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-684291464), (java.lang.Integer) 722435795);
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) 1352162704, (java.lang.Integer) 1939697086);
        java.lang.Integer int26 = operacionesMatematicas0.restar((java.lang.Integer) 332322225, (java.lang.Integer) 3571264);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 101 + "'", int4.equals(101));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 4058 + "'", int7.equals(4058));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 1448818317 + "'", int10.equals(1448818317));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 789784196 + "'", int13.equals(789784196));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-996177125) + "'", int16.equals((-996177125)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 38144331 + "'", int19.equals(38144331));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-587534382) + "'", int23.equals((-587534382)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 328750961 + "'", int26.equals(328750961));
    }

    @Test
    public void test459() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test459");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-98975152), (java.lang.Integer) (-676594));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 1624322327, (java.lang.Integer) 200184);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 222308, (java.lang.Integer) 203658830);
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1338190428), (java.lang.Integer) 1249922709);
        java.lang.Integer int24 = operacionesMatematicas0.dividir((java.lang.Integer) (-1311414660), (java.lang.Integer) (-706075220));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 146 + "'", int12.equals(146));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 8114 + "'", int15.equals(8114));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 203881138 + "'", int18.equals(203881138));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1451249780 + "'", int21.equals(1451249780));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 1 + "'", int24.equals(1));
    }

    @Test
    public void test460() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test460");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 1089, (java.lang.Integer) 10);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-304));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass21 = operacionesMatematicas0.getClass();
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-747018902), (java.lang.Integer) 240844168);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1079 + "'", int16.equals(1079));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-91808) + "'", int19.equals((-91808)));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertNotNull(wildcardClass21);
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-990141872) + "'", int24.equals((-990141872)));
    }

    @Test
    public void test461() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test461");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 1352162704);
        java.lang.Integer int14 = operacionesMatematicas0.sumar((java.lang.Integer) (-1646083254), (java.lang.Integer) (-1646083254));
        java.lang.Class<?> wildcardClass15 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-127644456), (java.lang.Integer) 102968);
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass21 = operacionesMatematicas0.getClass();
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-458350076), (java.lang.Integer) (-315691537));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-388878032) + "'", int11.equals((-388878032)));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 1002800788 + "'", int14.equals(1002800788));
        org.junit.Assert.assertNotNull(wildcardClass15);
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-127541488) + "'", int19.equals((-127541488)));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertNotNull(wildcardClass21);
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-2067302980) + "'", int24.equals((-2067302980)));
    }

    @Test
    public void test462() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test462");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-2030), (java.lang.Integer) 677084);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 1362965102, (java.lang.Integer) (-589656652));
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 2029, (java.lang.Integer) 44);
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 1195057345, (java.lang.Integer) 354278770);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-679114) + "'", int16.equals((-679114)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 773308450 + "'", int19.equals(773308450));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1985 + "'", int22.equals(1985));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 3 + "'", int25.equals(3));
    }

    @Test
    public void test463() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test463");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 1678);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 242847, (java.lang.Integer) 674850);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 216149571, (java.lang.Integer) 179685178);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 166122 + "'", int10.equals(166122));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 36464393 + "'", int16.equals(36464393));
    }

    @Test
    public void test464() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test464");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.dividir((java.lang.Integer) (-25), (java.lang.Integer) (-23417874));
        java.lang.Integer int17 = operacionesMatematicas0.dividir((java.lang.Integer) 1346231, (java.lang.Integer) (-202980456));
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) (-1174185152), (java.lang.Integer) (-203654655));
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) 1006273798, (java.lang.Integer) (-118991840));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14.equals(0));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-1377839807) + "'", int20.equals((-1377839807)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 887281958 + "'", int23.equals(887281958));
    }

    @Test
    public void test465() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test465");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 676495, (java.lang.Integer) (-9));
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) 2679600, (java.lang.Integer) 204);
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-680500222), (java.lang.Integer) (-853072144));
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1501164544), (java.lang.Integer) 1);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 676504 + "'", int17.equals(676504));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 13135 + "'", int20.equals(13135));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 2122583520 + "'", int23.equals(2122583520));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-1501164544) + "'", int26.equals((-1501164544)));
    }

    @Test
    public void test466() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test466");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-1439058049), (java.lang.Integer) 669356571);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 2069249363, (java.lang.Integer) 93545);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2) + "'", int9.equals((-2)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2069155818 + "'", int12.equals(2069155818));
    }

    @Test
    public void test467() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test467");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.dividir((java.lang.Integer) 198, (java.lang.Integer) 2989800);
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) 798559998, (java.lang.Integer) (-373730));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass18);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1435555388) + "'", int21.equals((-1435555388)));
    }

    @Test
    public void test468() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test468");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) 203);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass9 = operacionesMatematicas0.getClass();
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-361282912), (java.lang.Integer) (-594099455));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-36253604), (java.lang.Integer) 1349710685);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 797 + "'", int7.equals(797));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-955382367) + "'", int12.equals((-955382367)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1479998100) + "'", int15.equals((-1479998100)));
    }

    @Test
    public void test469() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test469");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-2030), (java.lang.Integer) 677084);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 134282741, (java.lang.Integer) 218);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-337227660), (java.lang.Integer) 218);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 1771576096, (java.lang.Integer) (-203656745));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) 1402914690, (java.lang.Integer) (-203678148));
        java.lang.Integer int31 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-140916264), (java.lang.Integer) 1829857832);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-679114) + "'", int16.equals((-679114)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-791133534) + "'", int19.equals((-791133534)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1546915) + "'", int22.equals((-1546915)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1567919351 + "'", int25.equals(1567919351));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-6) + "'", int28.equals((-6)));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-1124927040) + "'", int31.equals((-1124927040)));
    }

    @Test
    public void test470() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test470");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 138125136, (java.lang.Integer) 223403);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203654756, (java.lang.Integer) (-846460796));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-985707399), (java.lang.Integer) 2256);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-1077768), (java.lang.Integer) (-141165343));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 137901733 + "'", int7.equals(137901733));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-248222832) + "'", int10.equals((-248222832)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-985705143) + "'", int13.equals((-985705143)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 140087575 + "'", int16.equals(140087575));
    }

    @Test
    public void test471() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test471");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-138799304), (java.lang.Integer) (-674424));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-86631468));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-139473728) + "'", int6.equals((-139473728)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test472() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test472");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2, (java.lang.Integer) 1000);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 1312932620, (java.lang.Integer) (-1222714618));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2000 + "'", int6.equals(2000));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 90218002 + "'", int9.equals(90218002));
    }

    @Test
    public void test473() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test473");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-203658532), (java.lang.Integer) 797);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 138125136, (java.lang.Integer) (-198));
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-104682583), (java.lang.Integer) (-673143));
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) (-58), (java.lang.Integer) 668453649);
        java.lang.Class<?> wildcardClass29 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-203657735) + "'", int18.equals((-203657735)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 138124938 + "'", int21.equals(138124938));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-105355726) + "'", int25.equals((-105355726)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-668453707) + "'", int28.equals((-668453707)));
        org.junit.Assert.assertNotNull(wildcardClass29);
    }

    @Test
    public void test474() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test474");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166122, (java.lang.Integer) (-2030));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 146, (java.lang.Integer) (-292));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 3451, (java.lang.Integer) 959378576);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1226585631), (java.lang.Integer) 1568598365);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 199 + "'", int6.equals(199));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-337227660) + "'", int9.equals((-337227660)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-42632) + "'", int12.equals((-42632)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 959382027 + "'", int15.equals(959382027));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-583262275) + "'", int18.equals((-583262275)));
    }

    @Test
    public void test475() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test475");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) (-26796));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 28000, (java.lang.Integer) (-676594));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-674424));
        java.lang.Class<?> wildcardClass29 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass30 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass31 = operacionesMatematicas0.getClass();
        java.lang.Integer int34 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 9812897);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-86631468) + "'", int22.equals((-86631468)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass29);
        org.junit.Assert.assertNotNull(wildcardClass30);
        org.junit.Assert.assertNotNull(wildcardClass31);
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + (-9135813) + "'", int34.equals((-9135813)));
    }

    @Test
    public void test476() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test476");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.dividir((java.lang.Integer) (-202979700), (java.lang.Integer) 722435795);
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-507), (java.lang.Integer) 13135);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-98484026), (java.lang.Integer) (-562612214));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-661096240) + "'", int18.equals((-661096240)));
    }

    @Test
    public void test477() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test477");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2), (java.lang.Integer) 2030);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-391), (java.lang.Integer) (-872968));
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) (-1700198433), (java.lang.Integer) (-136759878));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-4060) + "'", int12.equals((-4060)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 12 + "'", int18.equals(12));
    }

    @Test
    public void test478() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test478");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) (-1));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 696288, (java.lang.Integer) (-6763));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-1865866569), (java.lang.Integer) 151006688);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) (-827264024), (java.lang.Integer) (-2327));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 703051 + "'", int10.equals(703051));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1714859881) + "'", int13.equals((-1714859881)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-827266351) + "'", int16.equals((-827266351)));
    }

    @Test
    public void test479() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test479");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-344077), (java.lang.Integer) 791808011);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
    }

    @Test
    public void test480() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test480");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) 223300);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-676396), (java.lang.Integer) 1678);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) 99990);
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2030) + "'", int9.equals((-2030)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2679600 + "'", int13.equals(2679600));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-678074) + "'", int16.equals((-678074)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-98990) + "'", int19.equals((-98990)));
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test481() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test481");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-2031), (java.lang.Integer) (-100));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-679014), (java.lang.Integer) 28345);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 2108473854);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-344077));
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 1875127044, (java.lang.Integer) (-88705877));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 863195596, (java.lang.Integer) (-23417576));
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-2131) + "'", int10.equals((-2131)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-650669) + "'", int13.equals((-650669)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-2108473854) + "'", int16.equals((-2108473854)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1963832921 + "'", int22.equals(1963832921));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-36) + "'", int25.equals((-36)));
        org.junit.Assert.assertNotNull(wildcardClass26);
    }

    @Test
    public void test482() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test482");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-203658532), (java.lang.Integer) 797);
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) 1738534581, (java.lang.Integer) 1728039552);
        java.lang.Integer int24 = operacionesMatematicas0.dividir((java.lang.Integer) (-177138076), (java.lang.Integer) (-868904844));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-203657735) + "'", int18.equals((-203657735)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 10495029 + "'", int21.equals(10495029));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 0 + "'", int24.equals(0));
    }

    @Test
    public void test483() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test483");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 679827);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-203658532), (java.lang.Integer) (-589628652));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-202979314), (java.lang.Integer) 203177900);
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.dividir((java.lang.Integer) (-202978621), (java.lang.Integer) 712594712);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-679726) + "'", int6.equals((-679726)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 961940784 + "'", int9.equals(961940784));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 198586 + "'", int12.equals(198586));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
    }

    @Test
    public void test484() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test484");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-237), (java.lang.Integer) 1593176728);
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) 138125136, (java.lang.Integer) (-569427028));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1593176491 + "'", int19.equals(1593176491));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-76121664) + "'", int23.equals((-76121664)));
    }

    @Test
    public void test485() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test485");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-507), (java.lang.Integer) 200871);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 28053, (java.lang.Integer) (-1709861456));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2027625964), (java.lang.Integer) 5767683);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-201378) + "'", int16.equals((-201378)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-548663440) + "'", int19.equals((-548663440)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1777152452) + "'", int22.equals((-1777152452)));
    }

    @Test
    public void test486() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test486");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass9 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertNotNull(wildcardClass9);
    }

    @Test
    public void test487() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test487");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 1678, (java.lang.Integer) (-304));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 14, (java.lang.Integer) (-676594));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-2030), (java.lang.Integer) 98974848);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-319619085), (java.lang.Integer) 28053);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) (-550455336));
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-203859113), (java.lang.Integer) (-242352736));
        java.lang.Integer int27 = operacionesMatematicas0.sumar((java.lang.Integer) (-220530924), (java.lang.Integer) (-372389960));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1374 + "'", int9.equals(1374));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1617522543 + "'", int18.equals(1617522543));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-550455336) + "'", int21.equals((-550455336)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 1829310816 + "'", int24.equals(1829310816));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + (-592920884) + "'", int27.equals((-592920884)));
    }

    @Test
    public void test488() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test488");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) 688);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) (-2031), (java.lang.Integer) 223300);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 29686803, (java.lang.Integer) (-480618));
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1218812475), (java.lang.Integer) (-221533));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 12 + "'", int13.equals(12));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 222612 + "'", int17.equals(222612));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 221269 + "'", int20.equals(221269));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-61) + "'", int23.equals((-61)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-230006161) + "'", int26.equals((-230006161)));
    }

    @Test
    public void test489() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test489");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) 101);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 28053, (java.lang.Integer) 849);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.restar((java.lang.Integer) 374697400, (java.lang.Integer) (-59));
        java.lang.Integer int17 = operacionesMatematicas0.dividir((java.lang.Integer) 1701718238, (java.lang.Integer) (-1215639764));
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) 245557226, (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2 + "'", int6.equals(2));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 23816997 + "'", int10.equals(23816997));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 374697459 + "'", int14.equals(374697459));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-1) + "'", int17.equals((-1)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 0 + "'", int20.equals(0));
    }

    @Test
    public void test490() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test490");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-98975152), (java.lang.Integer) (-676594));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-702061), (java.lang.Integer) 100004);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 218, (java.lang.Integer) (-86408168));
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) 200183, (java.lang.Integer) 315901);
        java.lang.Integer int24 = operacionesMatematicas0.restar((java.lang.Integer) 6450721, (java.lang.Integer) 1711856);
        java.lang.Class<?> wildcardClass25 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 146 + "'", int12.equals(146));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-802065) + "'", int15.equals((-802065)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-86407950) + "'", int18.equals((-86407950)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 4738865 + "'", int24.equals(4738865));
        org.junit.Assert.assertNotNull(wildcardClass25);
    }

    @Test
    public void test491() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test491");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 302, (java.lang.Integer) 675054);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 24, (java.lang.Integer) 168);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 1424762756, (java.lang.Integer) 0);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-674752) + "'", int10.equals((-674752)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-144) + "'", int13.equals((-144)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1424762756 + "'", int16.equals(1424762756));
    }

    @Test
    public void test492() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test492");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2, (java.lang.Integer) 1000);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-907044204), (java.lang.Integer) 166111);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-480618), (java.lang.Integer) (-674366));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-152649640), (java.lang.Integer) (-678074));
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-1460280832), (java.lang.Integer) 135279661);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2000 + "'", int6.equals(2000));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-906878093) + "'", int9.equals((-906878093)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1989890988 + "'", int12.equals(1989890988));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-153327714) + "'", int15.equals((-153327714)));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1325001171) + "'", int19.equals((-1325001171)));
    }

    @Test
    public void test493() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test493");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 2354, (java.lang.Integer) (-403));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-203289362), (java.lang.Integer) 676395);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 1741645850, (java.lang.Integer) 34527);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 1651769703, (java.lang.Integer) 2092799141);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 78757851, (java.lang.Integer) (-320292228));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-5) + "'", int12.equals((-5)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-530028550) + "'", int16.equals((-530028550)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 50443 + "'", int19.equals(50443));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-550398452) + "'", int22.equals((-550398452)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-241534377) + "'", int25.equals((-241534377)));
    }

    @Test
    public void test494() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test494");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) (-872882432), (java.lang.Integer) (-6763));
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) 30107083, (java.lang.Integer) (-1566668296));
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        java.lang.Integer int27 = operacionesMatematicas0.multiplicar((java.lang.Integer) 6823529, (java.lang.Integer) (-13));
        java.lang.Integer int30 = operacionesMatematicas0.dividir((java.lang.Integer) 29745240, (java.lang.Integer) 278031330);
        java.lang.Integer int33 = operacionesMatematicas0.dividir((java.lang.Integer) 1615957977, (java.lang.Integer) 199092912);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 129067 + "'", int20.equals(129067));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-1536561213) + "'", int23.equals((-1536561213)));
        org.junit.Assert.assertNotNull(wildcardClass24);
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + (-88705877) + "'", int27.equals((-88705877)));
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + 0 + "'", int30.equals(0));
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + 8 + "'", int33.equals(8));
    }

    @Test
    public void test495() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test495");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 674850, (java.lang.Integer) (-203658532));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 338956, (java.lang.Integer) (-3030));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2233), (java.lang.Integer) (-1027036680));
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-678748), (java.lang.Integer) (-198118));
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-203859113), (java.lang.Integer) (-9430));
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) 3021);
        java.lang.Integer int27 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1426890059, (java.lang.Integer) 1352434);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1027036680) + "'", int12.equals((-1027036680)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-139629624) + "'", int15.equals((-139629624)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-480630) + "'", int18.equals((-480630)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1753913018) + "'", int21.equals((-1753913018)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 0 + "'", int24.equals(0));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + (-1420679450) + "'", int27.equals((-1420679450)));
    }

    @Test
    public void test496() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test496");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-507), (java.lang.Integer) 200871);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-702061), (java.lang.Integer) (-25));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 198, (java.lang.Integer) 7458814);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 675054, (java.lang.Integer) (-2031));
        java.lang.Integer int28 = operacionesMatematicas0.sumar((java.lang.Integer) (-164003), (java.lang.Integer) (-891965756));
        java.lang.Integer int31 = operacionesMatematicas0.multiplicar((java.lang.Integer) 28345, (java.lang.Integer) 23);
        java.lang.Class<?> wildcardClass32 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-201378) + "'", int16.equals((-201378)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-702086) + "'", int19.equals((-702086)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1476845172 + "'", int22.equals(1476845172));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 673023 + "'", int25.equals(673023));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-892129759) + "'", int28.equals((-892129759)));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 651935 + "'", int31.equals(651935));
        org.junit.Assert.assertNotNull(wildcardClass32);
    }

    @Test
    public void test497() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test497");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) (-872882432), (java.lang.Integer) (-6763));
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1641), (java.lang.Integer) 200184);
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        java.lang.Integer int27 = operacionesMatematicas0.dividir((java.lang.Integer) (-1896219511), (java.lang.Integer) (-1536561213));
        java.lang.Integer int30 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1762232261));
        java.lang.Integer int33 = operacionesMatematicas0.sumar((java.lang.Integer) 43315734, (java.lang.Integer) 379713536);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 129067 + "'", int20.equals(129067));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-328501944) + "'", int23.equals((-328501944)));
        org.junit.Assert.assertNotNull(wildcardClass24);
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 1 + "'", int27.equals(1));
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + 0 + "'", int30.equals(0));
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + 423029270 + "'", int33.equals(423029270));
    }

    @Test
    public void test498() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test498");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 1678, (java.lang.Integer) (-304));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-970066961), (java.lang.Integer) 680607504);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 1710, (java.lang.Integer) (-88705877));
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-2108473854), (java.lang.Integer) (-1072088893));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) 199092912, (java.lang.Integer) (-1027036680));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1374 + "'", int9.equals(1374));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 877006832 + "'", int12.equals(877006832));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 88707587 + "'", int15.equals(88707587));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1114404549 + "'", int18.equals(1114404549));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1226129592 + "'", int21.equals(1226129592));
    }

    @Test
    public void test499() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test499");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-28275), (java.lang.Integer) 200184);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-1896219511), (java.lang.Integer) (-669335781));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 157225294, (java.lang.Integer) 2000);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-267264000), (java.lang.Integer) (-203566734));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1729412004 + "'", int12.equals(1729412004));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 157227294 + "'", int15.equals(157227294));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-63697266) + "'", int18.equals((-63697266)));
    }

    @Test
    public void test500() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest2.test500");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 1023986076, (java.lang.Integer) 28000);
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-1796254192), (java.lang.Integer) (-98990));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 419736786, (java.lang.Integer) 6825239);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-970067068), (java.lang.Integer) 1163107105);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 1024014076 + "'", int7.equals(1024014076));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-1796353182) + "'", int10.equals((-1796353182)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 426562025 + "'", int13.equals(426562025));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-433500156) + "'", int16.equals((-433500156)));
        org.junit.Assert.assertNotNull(wildcardClass17);
    }
}

