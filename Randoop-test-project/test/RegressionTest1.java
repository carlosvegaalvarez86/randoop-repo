import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegressionTest1 {

    public static boolean debug = false;

    @Test
    public void test001() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test001");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-26796), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) 164521512, (java.lang.Integer) (-469733691));
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) (-71), (java.lang.Integer) 703040);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-28474) + "'", int20.equals((-28474)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-608580408) + "'", int23.equals((-608580408)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 0 + "'", int26.equals(0));
    }

    @Test
    public void test002() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test002");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) 202);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 674850, (java.lang.Integer) (-2789518));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-820), (java.lang.Integer) 674850);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 204 + "'", int6.equals(204));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 3464368 + "'", int9.equals(3464368));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 674030 + "'", int12.equals(674030));
    }

    @Test
    public void test003() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test003");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 146, (java.lang.Integer) 990);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-223), (java.lang.Integer) 3020);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 1567765512, (java.lang.Integer) (-560593138));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-844) + "'", int7.equals((-844)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-673460) + "'", int10.equals((-673460)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1007172374 + "'", int13.equals(1007172374));
    }

    @Test
    public void test004() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test004");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-304), (java.lang.Integer) 12);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) (-674366), (java.lang.Integer) 10);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 6559757, (java.lang.Integer) (-204));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 438482334, (java.lang.Integer) (-490567384));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-292) + "'", int7.equals((-292)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-674376) + "'", int10.equals((-674376)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1338190428) + "'", int13.equals((-1338190428)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-967184720) + "'", int16.equals((-967184720)));
    }

    @Test
    public void test005() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test005");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) 2030, (java.lang.Integer) 688);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 138125136, (java.lang.Integer) (-453522300));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-1304999083), (java.lang.Integer) 200282);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7.equals(2));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 591647436 + "'", int10.equals(591647436));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1304798801) + "'", int13.equals((-1304798801)));
    }

    @Test
    public void test006() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test006");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 1089, (java.lang.Integer) 10);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-304));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass21 = operacionesMatematicas0.getClass();
        java.lang.Integer int24 = operacionesMatematicas0.dividir((java.lang.Integer) (-717311), (java.lang.Integer) 1262016360);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1079 + "'", int16.equals(1079));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-91808) + "'", int19.equals((-91808)));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertNotNull(wildcardClass21);
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 0 + "'", int24.equals(0));
    }

    @Test
    public void test007() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test007");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 1678);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2000, (java.lang.Integer) (-28474));
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-961562280), (java.lang.Integer) 2679600);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 98974848, (java.lang.Integer) (-1962));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 166122 + "'", int10.equals(166122));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-56948000) + "'", int13.equals((-56948000)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-358) + "'", int16.equals((-358)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 98976810 + "'", int19.equals(98976810));
    }

    @Test
    public void test008() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test008");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2, (java.lang.Integer) (-1));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) 338956);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-300960), (java.lang.Integer) 2679600);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 298, (java.lang.Integer) (-28474));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 902932, (java.lang.Integer) 1739459091);
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) (-485636182), (java.lang.Integer) (-563780412));
        java.lang.Integer int31 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-300960), (java.lang.Integer) (-1181025384));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-2) + "'", int13.equals((-2)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1023986076 + "'", int16.equals(1023986076));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1001435648 + "'", int19.equals(1001435648));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-28176) + "'", int22.equals((-28176)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1740362023 + "'", int25.equals(1740362023));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-1503913728) + "'", int31.equals((-1503913728)));
    }

    @Test
    public void test009() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test009");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-198), (java.lang.Integer) 198);
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1750295076, (java.lang.Integer) (-56948000));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 1721190272 + "'", int20.equals(1721190272));
    }

    @Test
    public void test010() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test010");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-98975152), (java.lang.Integer) (-300960));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 204, (java.lang.Integer) 0);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 849, (java.lang.Integer) 55);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-682176), (java.lang.Integer) 204);
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1963548160 + "'", int9.equals(1963548160));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 204 + "'", int12.equals(204));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 794 + "'", int15.equals(794));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-682380) + "'", int18.equals((-682380)));
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test011() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test011");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-4060), (java.lang.Integer) (-203658530));
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-941899981), (java.lang.Integer) (-104682583));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203662590) + "'", int6.equals((-203662590)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-837217398) + "'", int9.equals((-837217398)));
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test012() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test012");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 676495, (java.lang.Integer) (-9));
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) 2679600, (java.lang.Integer) 204);
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-680500222), (java.lang.Integer) (-853072144));
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) 1466134454, (java.lang.Integer) (-821707220));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 676504 + "'", int17.equals(676504));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 13135 + "'", int20.equals(13135));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 2122583520 + "'", int23.equals(2122583520));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 644427234 + "'", int26.equals(644427234));
    }

    @Test
    public void test013() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test013");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-2030), (java.lang.Integer) 677084);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 134282741, (java.lang.Integer) 218);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-337227660), (java.lang.Integer) 218);
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 218, (java.lang.Integer) 200871);
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Integer int29 = operacionesMatematicas0.sumar((java.lang.Integer) 243122812, (java.lang.Integer) 2030);
        java.lang.Integer int32 = operacionesMatematicas0.restar((java.lang.Integer) (-1646310612), (java.lang.Integer) (-42632));
        java.lang.Integer int35 = operacionesMatematicas0.restar((java.lang.Integer) (-203289362), (java.lang.Integer) 1593177030);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-679114) + "'", int16.equals((-679114)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-791133534) + "'", int19.equals((-791133534)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1546915) + "'", int22.equals((-1546915)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 243124842 + "'", int29.equals(243124842));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + (-1646267980) + "'", int32.equals((-1646267980)));
        org.junit.Assert.assertTrue("'" + int35 + "' != '" + (-1796466392) + "'", int35.equals((-1796466392)));
    }

    @Test
    public void test014() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test014");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 100004, (java.lang.Integer) 0);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 3021, (java.lang.Integer) 100);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) (-197795));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-3353), (java.lang.Integer) (-682176));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 100004 + "'", int12.equals(100004));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 30 + "'", int15.equals(30));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-44108285) + "'", int18.equals((-44108285)));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-2007631168) + "'", int22.equals((-2007631168)));
    }

    @Test
    public void test015() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test015");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166122, (java.lang.Integer) (-2030));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 1362967132, (java.lang.Integer) (-892129759));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 199 + "'", int6.equals(199));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-337227660) + "'", int9.equals((-337227660)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1) + "'", int12.equals((-1)));
    }

    @Test
    public void test016() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test016");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-98975152), (java.lang.Integer) (-300960));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-676296), (java.lang.Integer) 2108473854);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 1195056035, (java.lang.Integer) (-1399189976));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1963548160 + "'", int9.equals(1963548160));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2107797558 + "'", int12.equals(2107797558));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
    }

    @Test
    public void test017() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test017");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 677082, (java.lang.Integer) 797);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-86797590), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1865866569), (java.lang.Integer) (-625983112));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 849 + "'", int22.equals(849));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-1545028408) + "'", int29.equals((-1545028408)));
    }

    @Test
    public void test018() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test018");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2, (java.lang.Integer) (-1));
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 30106984, (java.lang.Integer) 99);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) 1751438428, (java.lang.Integer) (-159825138));
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) (-1507971691), (java.lang.Integer) 1727250696);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-2) + "'", int13.equals((-2)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 30107083 + "'", int16.equals(30107083));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 1911263566 + "'", int20.equals(1911263566));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 1059744909 + "'", int23.equals(1059744909));
    }

    @Test
    public void test019() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test019");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) (-26796));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) (-3));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) (-3260), (java.lang.Integer) 44906012);
        java.lang.Integer int31 = operacionesMatematicas0.sumar((java.lang.Integer) 138124938, (java.lang.Integer) 20300);
        java.lang.Class<?> wildcardClass32 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-86631468) + "'", int22.equals((-86631468)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-103) + "'", int25.equals((-103)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 138145238 + "'", int31.equals(138145238));
        org.junit.Assert.assertNotNull(wildcardClass32);
    }

    @Test
    public void test020() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test020");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) 14);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 227358, (java.lang.Integer) 223);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-2108473854), (java.lang.Integer) (-1427973632));
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) (-747018902), (java.lang.Integer) 1350225);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 134284782, (java.lang.Integer) 14);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 675040 + "'", int10.equals(675040));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227581 + "'", int13.equals(227581));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-680500222) + "'", int16.equals((-680500222)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-553) + "'", int19.equals((-553)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 9591770 + "'", int22.equals(9591770));
    }

    @Test
    public void test021() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test021");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 669356581, (java.lang.Integer) (-5));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-945000448), (java.lang.Integer) (-203656745));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 200282, (java.lang.Integer) 1471);
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) (-74681));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 669356576 + "'", int19.equals(669356576));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 255987712 + "'", int22.equals(255987712));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 136 + "'", int25.equals(136));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-241443673) + "'", int28.equals((-241443673)));
    }

    @Test
    public void test022() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test022");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1913));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-1566625664), (java.lang.Integer) (-1566625664));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 499871, (java.lang.Integer) (-300960));
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-74681), (java.lang.Integer) (-202979700));
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) (-970067068), (java.lang.Integer) 1971875568);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 117 + "'", int9.equals(117));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1 + "'", int12.equals(1));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 198911 + "'", int15.equals(198911));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-203054381) + "'", int18.equals((-203054381)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
    }

    @Test
    public void test023() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test023");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.dividir((java.lang.Integer) (-202979700), (java.lang.Integer) 722435795);
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-1773969801), (java.lang.Integer) 1197940124);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-576029677) + "'", int15.equals((-576029677)));
    }

    @Test
    public void test024() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test024");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 198586, (java.lang.Integer) 252);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 198838 + "'", int13.equals(198838));
    }

    @Test
    public void test025() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test025");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 302, (java.lang.Integer) 675054);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 24, (java.lang.Integer) 168);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-674752) + "'", int10.equals((-674752)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-144) + "'", int13.equals((-144)));
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test026() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test026");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 1678, (java.lang.Integer) (-304));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-867152808), (java.lang.Integer) 961940784);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-28676780), (java.lang.Integer) 43690);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1374 + "'", int9.equals(1374));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1241932232 + "'", int15.equals(1241932232));
    }

    @Test
    public void test027() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test027");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 677082, (java.lang.Integer) 797);
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) (-203656745), (java.lang.Integer) (-747018927));
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) 1412301232, (java.lang.Integer) (-203656745));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 849 + "'", int22.equals(849));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 1615957977 + "'", int28.equals(1615957977));
    }

    @Test
    public void test028() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test028");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 990, (java.lang.Integer) 302);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 7458814, (java.lang.Integer) (-676296));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) 1710, (java.lang.Integer) 168);
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) (-86797590), (java.lang.Integer) (-1646083254));
        java.lang.Integer int27 = operacionesMatematicas0.dividir((java.lang.Integer) (-1827), (java.lang.Integer) (-17));
        java.lang.Integer int30 = operacionesMatematicas0.restar((java.lang.Integer) (-1614297536), (java.lang.Integer) 705204090);
        java.lang.Integer int33 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1546233468), (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 688 + "'", int15.equals(688));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-11) + "'", int18.equals((-11)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1542 + "'", int21.equals(1542));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1732880844) + "'", int24.equals((-1732880844)));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 107 + "'", int27.equals(107));
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + 1975465670 + "'", int30.equals(1975465670));
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + 0 + "'", int33.equals(0));
    }

    @Test
    public void test029() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test029");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-404362922), (java.lang.Integer) (-86797590));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 1696355484 + "'", int7.equals(1696355484));
    }

    @Test
    public void test030() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test030");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 669356581, (java.lang.Integer) (-5));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) (-2147357664), (java.lang.Integer) 1050105608);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-853072144), (java.lang.Integer) (-2895));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 669356576 + "'", int19.equals(669356576));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1097252056) + "'", int22.equals((-1097252056)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-853075039) + "'", int25.equals((-853075039)));
    }

    @Test
    public void test031() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test031");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 677082, (java.lang.Integer) 797);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) 221269);
        java.lang.Integer int28 = operacionesMatematicas0.sumar((java.lang.Integer) (-802065), (java.lang.Integer) 4058);
        java.lang.Integer int31 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) (-220971));
        java.lang.Integer int34 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) (-1105768));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 849 + "'", int22.equals(849));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 668453649 + "'", int25.equals(668453649));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-798007) + "'", int28.equals((-798007)));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-220971) + "'", int31.equals((-220971)));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 1105768 + "'", int34.equals(1105768));
    }

    @Test
    public void test032() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test032");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-204), (java.lang.Integer) 12);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 797, (java.lang.Integer) (-201378));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.dividir((java.lang.Integer) 3233, (java.lang.Integer) 1024014076);
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) (-1769557200), (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-17) + "'", int6.equals((-17)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14.equals(0));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-1769557200) + "'", int17.equals((-1769557200)));
    }

    @Test
    public void test033() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test033");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-203435127), (java.lang.Integer) 2354);
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-203437481) + "'", int19.equals((-203437481)));
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test034() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test034");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) (-1));
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 905316668, (java.lang.Integer) (-1753913018));
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 1199317608 + "'", int10.equals(1199317608));
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test035() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test035");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) 688);
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) (-594099455), (java.lang.Integer) (-5600));
        java.lang.Integer int24 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) 38790450);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 12 + "'", int13.equals(12));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 222612 + "'", int17.equals(222612));
        org.junit.Assert.assertNotNull(wildcardClass18);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-594105055) + "'", int21.equals((-594105055)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 0 + "'", int24.equals(0));
    }

    @Test
    public void test036() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test036");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-676594), (java.lang.Integer) 3451);
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-477630), (java.lang.Integer) (-203678316));
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-667400994), (java.lang.Integer) 326559702);
        java.lang.Integer int27 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-4755287), (java.lang.Integer) (-109387168));
        java.lang.Class<?> wildcardClass28 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 12 + "'", int13.equals(12));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-673143) + "'", int17.equals((-673143)));
        org.junit.Assert.assertNotNull(wildcardClass18);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1864816680 + "'", int21.equals(1864816680));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1115561068) + "'", int24.equals((-1115561068)));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + (-406228640) + "'", int27.equals((-406228640)));
        org.junit.Assert.assertNotNull(wildcardClass28);
    }

    @Test
    public void test037() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test037");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-203657735), (java.lang.Integer) (-98975152));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-872882432), (java.lang.Integer) 1374);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-203), (java.lang.Integer) 3233);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) (-223), (java.lang.Integer) 203658830);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 242847, (java.lang.Integer) (-1988598960));
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) (-1830), (java.lang.Integer) (-6349702));
        java.lang.Integer int27 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1963748327, (java.lang.Integer) 24);
        java.lang.Integer int30 = operacionesMatematicas0.multiplicar((java.lang.Integer) 476950759, (java.lang.Integer) (-1085863536));
        java.lang.Integer int33 = operacionesMatematicas0.sumar((java.lang.Integer) (-589628652), (java.lang.Integer) 1807381003);
        java.lang.Integer int36 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2125425692), (java.lang.Integer) 2005911435);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-104682583) + "'", int9.equals((-104682583)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-635285) + "'", int12.equals((-635285)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1988356113) + "'", int21.equals((-1988356113)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-6351532) + "'", int24.equals((-6351532)));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + (-114680408) + "'", int27.equals((-114680408)));
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + 1107075312 + "'", int30.equals(1107075312));
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + 1217752351 + "'", int33.equals(1217752351));
        org.junit.Assert.assertTrue("'" + int36 + "' != '" + 1913817292 + "'", int36.equals(1913817292));
    }

    @Test
    public void test038() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test038");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 374697400, (java.lang.Integer) 473676);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 44906012);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-1073433704), (java.lang.Integer) 86235969);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1727));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 791 + "'", int13.equals(791));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-44906012) + "'", int16.equals((-44906012)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-987197735) + "'", int19.equals((-987197735)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
    }

    @Test
    public void test039() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test039");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) 203);
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 791, (java.lang.Integer) (-198118));
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 155085916, (java.lang.Integer) 1355141328);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 408714520, (java.lang.Integer) (-1183797801));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 797 + "'", int7.equals(797));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1569639104 + "'", int13.equals(1569639104));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1592512321 + "'", int16.equals(1592512321));
    }

    @Test
    public void test040() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test040");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 674850, (java.lang.Integer) (-203658532));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 9120400, (java.lang.Integer) 8260879);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203658532), (java.lang.Integer) (-91798));
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 354278770, (java.lang.Integer) (-207537300));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 859521 + "'", int13.equals(859521));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203566734) + "'", int16.equals((-203566734)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 146741470 + "'", int19.equals(146741470));
    }

    @Test
    public void test041() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test041");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 791, (java.lang.Integer) 222612);
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) 681653544, (java.lang.Integer) 326559702);
        java.lang.Class<?> wildcardClass27 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 223403 + "'", int22.equals(223403));
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 2 + "'", int26.equals(2));
        org.junit.Assert.assertNotNull(wildcardClass27);
    }

    @Test
    public void test042() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test042");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-882132), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 676495, (java.lang.Integer) 2030);
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass25 = operacionesMatematicas0.getClass();
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) 2348, (java.lang.Integer) (-979644056));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-1480217496) + "'", int20.equals((-1480217496)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 333 + "'", int23.equals(333));
        org.junit.Assert.assertNotNull(wildcardClass24);
        org.junit.Assert.assertNotNull(wildcardClass25);
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 979646404 + "'", int28.equals(979646404));
    }

    @Test
    public void test043() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test043");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-23062), (java.lang.Integer) 1968086742);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1033577087), (java.lang.Integer) (-1355141328));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1197940124 + "'", int9.equals(1197940124));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1439577904 + "'", int12.equals(1439577904));
    }

    @Test
    public void test044() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test044");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) 101);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-44902128), (java.lang.Integer) 223300);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2 + "'", int6.equals(2));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-45125428) + "'", int9.equals((-45125428)));
    }

    @Test
    public void test045() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test045");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 203);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) 166111, (java.lang.Integer) (-872882432));
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2118109232, (java.lang.Integer) 35504);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 1241932232, (java.lang.Integer) 703051);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + (-204) + "'", int4.equals((-204)));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 767787264 + "'", int10.equals(767787264));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1242635283 + "'", int13.equals(1242635283));
    }

    @Test
    public void test046() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test046");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-2030), (java.lang.Integer) 677084);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 134282741, (java.lang.Integer) 218);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-337227660), (java.lang.Integer) 218);
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 218, (java.lang.Integer) 200871);
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Integer int29 = operacionesMatematicas0.sumar((java.lang.Integer) 243122812, (java.lang.Integer) 2030);
        java.lang.Integer int32 = operacionesMatematicas0.restar((java.lang.Integer) (-1646310612), (java.lang.Integer) (-42632));
        java.lang.Class<?> wildcardClass33 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-679114) + "'", int16.equals((-679114)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-791133534) + "'", int19.equals((-791133534)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1546915) + "'", int22.equals((-1546915)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 243124842 + "'", int29.equals(243124842));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + (-1646267980) + "'", int32.equals((-1646267980)));
        org.junit.Assert.assertNotNull(wildcardClass33);
    }

    @Test
    public void test047() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test047");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1089, (java.lang.Integer) (-1027036680));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 23816997, (java.lang.Integer) 222308);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166111, (java.lang.Integer) 1023985958);
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Integer int29 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-970066961));
        java.lang.Integer int32 = operacionesMatematicas0.dividir((java.lang.Integer) (-6794), (java.lang.Integer) (-676296));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1751447560) + "'", int19.equals((-1751447560)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-985706892) + "'", int22.equals((-985706892)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1741645850 + "'", int25.equals(1741645850));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 0 + "'", int29.equals(0));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 0 + "'", int32.equals(0));
    }

    @Test
    public void test048() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test048");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-2030), (java.lang.Integer) 677084);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 134282741, (java.lang.Integer) 218);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-337227660), (java.lang.Integer) 218);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 1771576096, (java.lang.Integer) (-203656745));
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) 681653544, (java.lang.Integer) 4079228);
        java.lang.Integer int31 = operacionesMatematicas0.sumar((java.lang.Integer) (-108954422), (java.lang.Integer) 902597452);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-679114) + "'", int16.equals((-679114)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-791133534) + "'", int19.equals((-791133534)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1546915) + "'", int22.equals((-1546915)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1567919351 + "'", int25.equals(1567919351));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 677574316 + "'", int28.equals(677574316));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 793643030 + "'", int31.equals(793643030));
    }

    @Test
    public void test049() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test049");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1089, (java.lang.Integer) (-1027036680));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 23816997, (java.lang.Integer) 222308);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166111, (java.lang.Integer) 1023985958);
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-548575792));
        java.lang.Integer int31 = operacionesMatematicas0.dividir((java.lang.Integer) (-890108686), (java.lang.Integer) 442013635);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1751447560) + "'", int19.equals((-1751447560)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-985706892) + "'", int22.equals((-985706892)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1741645850 + "'", int25.equals(1741645850));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-2) + "'", int31.equals((-2)));
    }

    @Test
    public void test050() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test050");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) (-589628652), (java.lang.Integer) (-169435336));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 1372443108, (java.lang.Integer) (-1936223520));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 415729, (java.lang.Integer) 2137056006);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 3 + "'", int19.equals(3));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-563780412) + "'", int22.equals((-563780412)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 2137471735 + "'", int25.equals(2137471735));
    }

    @Test
    public void test051() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test051");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 99990, (java.lang.Integer) 298);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-90), (java.lang.Integer) (-139473728));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 99692 + "'", int9.equals(99692));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-139473818) + "'", int12.equals((-139473818)));
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test052() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test052");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 1678);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-204), (java.lang.Integer) (-669335781));
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) 2679600);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 166122 + "'", int10.equals(166122));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 669335577 + "'", int13.equals(669335577));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test053() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test053");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-2030), (java.lang.Integer) 677084);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 438482334, (java.lang.Integer) 44540629);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 669335577, (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-679114) + "'", int16.equals((-679114)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1809913974 + "'", int19.equals(1809913974));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 669335577 + "'", int22.equals(669335577));
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test054() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test054");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1455213478), (java.lang.Integer) 1402914690);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-678526), (java.lang.Integer) 354278770);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 20, (java.lang.Integer) (-372389960));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-1352758348) + "'", int9.equals((-1352758348)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 353600244 + "'", int12.equals(353600244));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1142135392 + "'", int15.equals(1142135392));
    }

    @Test
    public void test055() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test055");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-98975152), (java.lang.Integer) (-676594));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 1624322327, (java.lang.Integer) 200184);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 1771579329, (java.lang.Integer) 1710);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 23816997, (java.lang.Integer) (-1667413622));
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) 44898775, (java.lang.Integer) (-684291464));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 146 + "'", int12.equals(146));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 8114 + "'", int15.equals(8114));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1771581039 + "'", int18.equals(1771581039));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1643596625) + "'", int21.equals((-1643596625)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-639392689) + "'", int24.equals((-639392689)));
    }

    @Test
    public void test056() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test056");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 1888659260);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) (-23), (java.lang.Integer) 2108473854);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-1402661432), (java.lang.Integer) (-1761916892));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-2108473877) + "'", int10.equals((-2108473877)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1130388972 + "'", int13.equals(1130388972));
    }

    @Test
    public void test057() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test057");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 473676, (java.lang.Integer) 222612);
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 200183, (java.lang.Integer) 117);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1701552422, (java.lang.Integer) 239705415);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 696288 + "'", int7.equals(696288));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 1710 + "'", int10.equals(1710));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 7 + "'", int13.equals(7));
    }

    @Test
    public void test058() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test058");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) 203);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1751447560), (java.lang.Integer) (-1751438130));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-824465252), (java.lang.Integer) 525);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 797 + "'", int7.equals(797));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-1085863536) + "'", int10.equals((-1085863536)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-824464727) + "'", int13.equals((-824464727)));
    }

    @Test
    public void test059() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test059");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) 688);
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) (-1035598570), (java.lang.Integer) 138124938);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 12 + "'", int13.equals(12));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 222612 + "'", int17.equals(222612));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-7) + "'", int20.equals((-7)));
    }

    @Test
    public void test060() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test060");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 374697400, (java.lang.Integer) 473676);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 44906012);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 3233, (java.lang.Integer) 1771576096);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) (-903099228), (java.lang.Integer) (-1418013643));
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1477849338), (java.lang.Integer) (-892129759));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 791 + "'", int13.equals(791));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-44906012) + "'", int16.equals((-44906012)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1771579329 + "'", int19.equals(1771579329));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1973854425 + "'", int22.equals(1973854425));
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 105432006 + "'", int26.equals(105432006));
    }

    @Test
    public void test061() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test061");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-198), (java.lang.Integer) 198);
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) (-1379066004), (java.lang.Integer) (-23));
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 1889283349, (java.lang.Integer) (-3));
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) (-517446961), (java.lang.Integer) 302);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 59959391 + "'", int20.equals(59959391));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-629761116) + "'", int23.equals((-629761116)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-1713400) + "'", int26.equals((-1713400)));
    }

    @Test
    public void test062() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test062");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 14, (java.lang.Integer) (-204));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1034161825), (java.lang.Integer) 100);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-43315734), (java.lang.Integer) (-794241824));
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 239705415, (java.lang.Integer) (-1368255616));
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-139473728), (java.lang.Integer) (-26404274));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 218 + "'", int9.equals(218));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-336967396) + "'", int12.equals((-336967396)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1827456 + "'", int21.equals(1827456));
    }

    @Test
    public void test063() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test063");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) 223300);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-676396), (java.lang.Integer) 1678);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-203654745), (java.lang.Integer) (-365383));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) (-9), (java.lang.Integer) (-169435336));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 3021, (java.lang.Integer) (-391));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) (-319619085), (java.lang.Integer) (-1033021970));
        java.lang.Integer int31 = operacionesMatematicas0.sumar((java.lang.Integer) 1355465799, (java.lang.Integer) (-1381013947));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2030) + "'", int9.equals((-2030)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2679600 + "'", int13.equals(2679600));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-678074) + "'", int16.equals((-678074)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-203289362) + "'", int19.equals((-203289362)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-169435345) + "'", int22.equals((-169435345)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-7) + "'", int25.equals((-7)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-25548148) + "'", int31.equals((-25548148)));
    }

    @Test
    public void test064() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test064");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-223), (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-86408168), (java.lang.Integer) (-1480217496));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 2108473854, (java.lang.Integer) 99990);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-650669), (java.lang.Integer) 44906012);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 326559702, (java.lang.Integer) (-770326209));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) (-78757731), (java.lang.Integer) 95730);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223 + "'", int6.equals(223));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-1566625664) + "'", int9.equals((-1566625664)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2108373864 + "'", int12.equals(2108373864));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-45556681) + "'", int15.equals((-45556681)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1096885911 + "'", int18.equals(1096885911));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-78853461) + "'", int21.equals((-78853461)));
    }

    @Test
    public void test065() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test065");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-703216327), (java.lang.Integer) 437780273);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-265436054) + "'", int9.equals((-265436054)));
    }

    @Test
    public void test066() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test066");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) (-292));
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 6450721, (java.lang.Integer) (-1988598960));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 28345, (java.lang.Integer) 359256);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 613024037, (java.lang.Integer) 16434);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-882132) + "'", int16.equals((-882132)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2010197328 + "'", int19.equals(2010197328));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1593176728 + "'", int22.equals(1593176728));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-1556252358) + "'", int25.equals((-1556252358)));
    }

    @Test
    public void test067() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test067");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) (-1));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 696288, (java.lang.Integer) (-6763));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) 820922168);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1546789220), (java.lang.Integer) 113851);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 703051 + "'", int10.equals(703051));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-1250415628) + "'", int16.equals((-1250415628)));
    }

    @Test
    public void test068() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test068");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 218, (java.lang.Integer) 3233);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 676401, (java.lang.Integer) (-129843968));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-11), (java.lang.Integer) 0);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 688, (java.lang.Integer) (-28927));
        try {
            java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) (-586816723), (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 3451 + "'", int9.equals(3451));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1196436736 + "'", int12.equals(1196436736));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-11) + "'", int15.equals((-11)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
    }

    @Test
    public void test069() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test069");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.sumar((java.lang.Integer) (-996177125), (java.lang.Integer) (-407224469));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-1403401594) + "'", int11.equals((-1403401594)));
    }

    @Test
    public void test070() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test070");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        try {
            java.lang.Integer int3 = operacionesMatematicas0.dividir((java.lang.Integer) (-241069610), (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
    }

    @Test
    public void test071() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test071");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-2233), (java.lang.Integer) 199);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 227358, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 994649, (java.lang.Integer) 1291504344);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-11) + "'", int10.equals((-11)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227358 + "'", int13.equals(227358));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-1149207272) + "'", int16.equals((-1149207272)));
    }

    @Test
    public void test072() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test072");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-91808), (java.lang.Integer) 386);
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 135279661, (java.lang.Integer) (-40337365));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 129067, (java.lang.Integer) (-200282));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-237) + "'", int9.equals((-237)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-3) + "'", int12.equals((-3)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-71215) + "'", int16.equals((-71215)));
    }

    @Test
    public void test073() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test073");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.sumar((java.lang.Integer) 12, (java.lang.Integer) (-480630));
        java.lang.Integer int11 = operacionesMatematicas0.restar((java.lang.Integer) (-319241594), (java.lang.Integer) 23816997);
        java.lang.Integer int14 = operacionesMatematicas0.sumar((java.lang.Integer) (-392600492), (java.lang.Integer) (-91798));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + (-480618) + "'", int8.equals((-480618)));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-343058591) + "'", int11.equals((-343058591)));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + (-392692290) + "'", int14.equals((-392692290)));
    }

    @Test
    public void test074() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test074");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-304), (java.lang.Integer) 12);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-91798), (java.lang.Integer) (-679726));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2002502299, (java.lang.Integer) 4051228);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-292) + "'", int7.equals((-292)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-2027022092) + "'", int10.equals((-2027022092)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 494 + "'", int13.equals(494));
    }

    @Test
    public void test075() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test075");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-304));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 765962910, (java.lang.Integer) 686730967);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1 + "'", int13.equals(1));
    }

    @Test
    public void test076() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test076");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) 101);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) (-319239361), (java.lang.Integer) (-2031));
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-1033487401), (java.lang.Integer) (-169435345));
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 644427234, (java.lang.Integer) (-1027036680));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2 + "'", int6.equals(2));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-319237330) + "'", int10.equals((-319237330)));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 6 + "'", int15.equals(6));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1671463914 + "'", int18.equals(1671463914));
    }

    @Test
    public void test077() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test077");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 2030);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 990, (java.lang.Integer) (-304));
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 1362967132, (java.lang.Integer) 2030);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2062891546, (java.lang.Integer) (-867152808));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-2233) + "'", int16.equals((-2233)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-300960) + "'", int19.equals((-300960)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1362965102 + "'", int22.equals(1362965102));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-240406288) + "'", int25.equals((-240406288)));
    }

    @Test
    public void test078() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test078");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.restar((java.lang.Integer) (-3160), (java.lang.Integer) (-203662590));
        java.lang.Integer int11 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1913), (java.lang.Integer) 191);
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-2125259776), (java.lang.Integer) (-2010186930));
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 1592563465, (java.lang.Integer) 1954931892);
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2137056006, (java.lang.Integer) (-702061));
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-505887068), (java.lang.Integer) 2058400712);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 203659430 + "'", int8.equals(203659430));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-365383) + "'", int11.equals((-365383)));
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 159520590 + "'", int15.equals(159520590));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 69014130 + "'", int21.equals(69014130));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1124082656) + "'", int24.equals((-1124082656)));
    }

    @Test
    public void test079() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test079");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 2354, (java.lang.Integer) (-403));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 1988598756, (java.lang.Integer) 6295);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 673023, (java.lang.Integer) (-679014));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-299613002), (java.lang.Integer) (-1251598048));
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) (-177082801), (java.lang.Integer) (-1455213478));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-5) + "'", int12.equals((-5)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 315901 + "'", int16.equals(315901));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 2145914048 + "'", int22.equals(2145914048));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1278130677 + "'", int25.equals(1278130677));
    }

    @Test
    public void test080() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test080");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) 2030, (java.lang.Integer) 688);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 138125136, (java.lang.Integer) (-453522300));
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-231829672), (java.lang.Integer) 1996646746);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-99), (java.lang.Integer) (-11127039));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7.equals(2));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 591647436 + "'", int10.equals(591647436));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2066490878 + "'", int13.equals(2066490878));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
    }

    @Test
    public void test081() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test081");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2, (java.lang.Integer) 99);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 200871, (java.lang.Integer) 688);
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) (-1566625664), (java.lang.Integer) 166122);
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 1954931645, (java.lang.Integer) 2022570152);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 198 + "'", int15.equals(198));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 200183 + "'", int18.equals(200183));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-9430) + "'", int21.equals((-9430)));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
    }

    @Test
    public void test082() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test082");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-2233));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-676396), (java.lang.Integer) 100);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-223), (java.lang.Integer) (-198));
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 14, (java.lang.Integer) 12);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 223, (java.lang.Integer) 0);
        java.lang.Integer int24 = operacionesMatematicas0.dividir((java.lang.Integer) 137901045, (java.lang.Integer) 35504);
        java.lang.Integer int27 = operacionesMatematicas0.sumar((java.lang.Integer) 334, (java.lang.Integer) (-613263));
        java.lang.Integer int30 = operacionesMatematicas0.dividir((java.lang.Integer) 298, (java.lang.Integer) 1059359213);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-674366) + "'", int9.equals((-674366)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-6763) + "'", int12.equals((-6763)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-25) + "'", int15.equals((-25)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 168 + "'", int18.equals(168));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 223 + "'", int21.equals(223));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 3884 + "'", int24.equals(3884));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + (-612929) + "'", int27.equals((-612929)));
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + 0 + "'", int30.equals(0));
    }

    @Test
    public void test083() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test083");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-26796), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) (-6763), (java.lang.Integer) (-4060));
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) 1542, (java.lang.Integer) 2108473854);
        java.lang.Integer int29 = operacionesMatematicas0.sumar((java.lang.Integer) 1710, (java.lang.Integer) 613024037);
        java.lang.Integer int32 = operacionesMatematicas0.restar((java.lang.Integer) (-584753), (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-28474) + "'", int20.equals((-28474)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 1 + "'", int23.equals(1));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 2108475396 + "'", int26.equals(2108475396));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 613025747 + "'", int29.equals(613025747));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + (-584753) + "'", int32.equals((-584753)));
    }

    @Test
    public void test084() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test084");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-203658532), (java.lang.Integer) 2941);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1963548160), (java.lang.Integer) 668453655);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-1959288468) + "'", int6.equals((-1959288468)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1662965481 + "'", int9.equals(1662965481));
    }

    @Test
    public void test085() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test085");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) 688);
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) (-594099455), (java.lang.Integer) (-5600));
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 12 + "'", int13.equals(12));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 222612 + "'", int17.equals(222612));
        org.junit.Assert.assertNotNull(wildcardClass18);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-594105055) + "'", int21.equals((-594105055)));
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test086() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test086");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2), (java.lang.Integer) 2030);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-44906012), (java.lang.Integer) 0);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-374696609), (java.lang.Integer) (-619745648));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) (-85417856), (java.lang.Integer) (-1338190225));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-4060) + "'", int12.equals((-4060)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-44906012) + "'", int15.equals((-44906012)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 245049039 + "'", int18.equals(245049039));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1252772369 + "'", int21.equals(1252772369));
    }

    @Test
    public void test087() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test087");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 338956, (java.lang.Integer) (-337227660));
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) 4738865, (java.lang.Integer) 86442695);
        java.lang.Integer int29 = operacionesMatematicas0.dividir((java.lang.Integer) 550098208, (java.lang.Integer) 181674816);
        java.lang.Integer int32 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-248222832), (java.lang.Integer) (-117892701));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-833949417) + "'", int26.equals((-833949417)));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 3 + "'", int29.equals(3));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + (-772698960) + "'", int32.equals((-772698960)));
    }

    @Test
    public void test088() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test088");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2), (java.lang.Integer) 2030);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-44906012), (java.lang.Integer) 0);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-98990), (java.lang.Integer) 764151000);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) (-2076676394), (java.lang.Integer) 680606415);
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2109130613, (java.lang.Integer) (-791133534));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-4060) + "'", int12.equals((-4060)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-44906012) + "'", int15.equals((-44906012)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-764249990) + "'", int18.equals((-764249990)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1396069979) + "'", int21.equals((-1396069979)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 130966026 + "'", int24.equals(130966026));
    }

    @Test
    public void test089() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test089");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 10, (java.lang.Integer) 302);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-676594), (java.lang.Integer) 298);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) (-2125425896), (java.lang.Integer) (-848979310));
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) (-1477849338), (java.lang.Integer) (-2091737468));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) 1033577350, (java.lang.Integer) 1270901478);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 3020 + "'", int15.equals(3020));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-676296) + "'", int18.equals((-676296)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1320562090 + "'", int21.equals(1320562090));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
    }

    @Test
    public void test090() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test090");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 199);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 98974848, (java.lang.Integer) 1729412004);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-50702028), (java.lang.Integer) 1161756880);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-592313823), (java.lang.Integer) 1586840422);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 298 + "'", int9.equals(298));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2115813051 + "'", int19.equals(2115813051));
    }

    @Test
    public void test091() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test091");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) 0);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-507), (java.lang.Integer) 100004);
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-1913), (java.lang.Integer) 29684000);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-1427973632), (java.lang.Integer) 11187);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-250078738), (java.lang.Integer) 1989128711);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2 + "'", int6.equals(2));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-50702028) + "'", int9.equals((-50702028)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1427984819) + "'", int15.equals((-1427984819)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1739049973 + "'", int18.equals(1739049973));
    }

    @Test
    public void test092() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test092");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2, (java.lang.Integer) (-1));
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 30106984, (java.lang.Integer) 99);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-2) + "'", int13.equals((-2)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 30107083 + "'", int16.equals(30107083));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test093() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test093");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) 223300);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 3020, (java.lang.Integer) (-2030));
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 1351444, (java.lang.Integer) 990);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 299478398, (java.lang.Integer) (-23937));
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1349700, (java.lang.Integer) (-6351532));
        java.lang.Integer int29 = operacionesMatematicas0.dividir((java.lang.Integer) (-1379066004), (java.lang.Integer) (-680500910));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2030) + "'", int9.equals((-2030)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2679600 + "'", int13.equals(2679600));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 5050 + "'", int17.equals(5050));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 1352434 + "'", int20.equals(1352434));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-12511) + "'", int23.equals((-12511)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 91982416 + "'", int26.equals(91982416));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 2 + "'", int29.equals(2));
    }

    @Test
    public void test094() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test094");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) 2030, (java.lang.Integer) 688);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) (-2233));
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.sumar((java.lang.Integer) 298, (java.lang.Integer) 0);
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 44907013, (java.lang.Integer) (-2452019));
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) (-343058591));
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) 107, (java.lang.Integer) 970067068);
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7.equals(2));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 225533 + "'", int10.equals(225533));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 298 + "'", int14.equals(298));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 47359032 + "'", int17.equals(47359032));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-343058591) + "'", int20.equals((-343058591)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-970066961) + "'", int23.equals((-970066961)));
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test095() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test095");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.sumar((java.lang.Integer) (-2030), (java.lang.Integer) (-674366));
        java.lang.Integer int11 = operacionesMatematicas0.dividir((java.lang.Integer) 6559757, (java.lang.Integer) 99692);
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 134282741, (java.lang.Integer) (-485640239));
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 1468944689, (java.lang.Integer) 0);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + (-676396) + "'", int8.equals((-676396)));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 65 + "'", int11.equals(65));
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1657436165 + "'", int15.equals(1657436165));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1468944689 + "'", int18.equals(1468944689));
    }

    @Test
    public void test096() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test096");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-26796), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-3260));
        java.lang.Integer int26 = operacionesMatematicas0.restar((java.lang.Integer) 1364569856, (java.lang.Integer) 6559757);
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1881873340, (java.lang.Integer) 673023);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-28474) + "'", int20.equals((-28474)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-3160) + "'", int23.equals((-3160)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 1358010099 + "'", int26.equals(1358010099));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 1134989380 + "'", int29.equals(1134989380));
    }

    @Test
    public void test097() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test097");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) 14);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 227358, (java.lang.Integer) 223);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-1546789220), (java.lang.Integer) 173359620);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 675040 + "'", int10.equals(675040));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227581 + "'", int13.equals(227581));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-1720148840) + "'", int16.equals((-1720148840)));
    }

    @Test
    public void test098() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test098");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 2030);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 666292770, (java.lang.Integer) (-104682583));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 16434, (java.lang.Integer) 698720868);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1701552422, (java.lang.Integer) 7326);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-2233) + "'", int16.equals((-2233)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1513001358) + "'", int19.equals((-1513001358)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 698737302 + "'", int22.equals(698737302));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1577950580 + "'", int25.equals(1577950580));
    }

    @Test
    public void test099() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test099");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-673143), (java.lang.Integer) (-100));
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.sumar((java.lang.Integer) (-6451), (java.lang.Integer) (-23062));
        java.lang.Integer int17 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-778127), (java.lang.Integer) 1143182576);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 6731 + "'", int10.equals(6731));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + (-29513) + "'", int14.equals((-29513)));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-1961706000) + "'", int17.equals((-1961706000)));
    }

    @Test
    public void test100() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test100");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-204), (java.lang.Integer) 12);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1546915), (java.lang.Integer) 38790450);
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-498428516), (java.lang.Integer) 1321880);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-17) + "'", int6.equals((-17)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-40337365) + "'", int9.equals((-40337365)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-377) + "'", int12.equals((-377)));
    }

    @Test
    public void test101() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test101");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.dividir((java.lang.Integer) 198, (java.lang.Integer) 2989800);
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) 701863, (java.lang.Integer) (-203658530));
        java.lang.Class<?> wildcardClass21 = operacionesMatematicas0.getClass();
        java.lang.Integer int24 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-169435527));
        java.lang.Integer int27 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2118109232, (java.lang.Integer) (-458350076));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 419736786 + "'", int20.equals(419736786));
        org.junit.Assert.assertNotNull(wildcardClass21);
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 0 + "'", int24.equals(0));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 1110802624 + "'", int27.equals(1110802624));
    }

    @Test
    public void test102() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test102");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1419118905), (java.lang.Integer) (-1243099180));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 676395, (java.lang.Integer) (-1429679577));
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) (-250078738), (java.lang.Integer) (-1008633599));
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) 402135200, (java.lang.Integer) 1592787391);
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 47990220 + "'", int12.equals(47990220));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test103() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test103");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-26796), (java.lang.Integer) (-2131));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-674366), (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-365383), (java.lang.Integer) (-594099455));
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-666387), (java.lang.Integer) (-1562038384));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-28927) + "'", int12.equals((-28927)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-133524468) + "'", int15.equals((-133524468)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1899059129 + "'", int18.equals(1899059129));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1906292656) + "'", int21.equals((-1906292656)));
    }

    @Test
    public void test104() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test104");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 2117069372, (java.lang.Integer) (-91798));
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 859521, (java.lang.Integer) 30106984);
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 1001435648, (java.lang.Integer) 666292770);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-23062) + "'", int19.equals((-23062)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-29247463) + "'", int22.equals((-29247463)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1 + "'", int25.equals(1));
    }

    @Test
    public void test105() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test105");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-223), (java.lang.Integer) (-1429679577));
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 134282741, (java.lang.Integer) 102968);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1072727885), (java.lang.Integer) (-98890));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 1968086948, (java.lang.Integer) (-698473020));
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1429679800) + "'", int13.equals((-1429679800)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1304 + "'", int16.equals(1304));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 663303746 + "'", int19.equals(663303746));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1269613928 + "'", int22.equals(1269613928));
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test106() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test106");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.sumar((java.lang.Integer) 243122930, (java.lang.Integer) (-848979310));
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-11127039), (java.lang.Integer) 1029847456);
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-110265462), (java.lang.Integer) 342593126);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + (-605856380) + "'", int14.equals((-605856380)));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 1018720417 + "'", int17.equals(1018720417));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-2032941828) + "'", int20.equals((-2032941828)));
    }

    @Test
    public void test107() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test107");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-26796), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) 164521512, (java.lang.Integer) (-469733691));
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-674752), (java.lang.Integer) (-485638320));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-28474) + "'", int20.equals((-28474)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-608580408) + "'", int23.equals((-608580408)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 897848320 + "'", int26.equals(897848320));
    }

    @Test
    public void test108() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test108");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-304));
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) 227581);
        java.lang.Class<?> wildcardClass15 = operacionesMatematicas0.getClass();
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-1566625664), (java.lang.Integer) 0);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass15);
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1566625664) + "'", int18.equals((-1566625664)));
    }

    @Test
    public void test109() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test109");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) (-26796));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) (-3));
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) (-678748));
        java.lang.Integer int31 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) (-134840));
        java.lang.Integer int34 = operacionesMatematicas0.multiplicar((java.lang.Integer) 155085916, (java.lang.Integer) (-29683782));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-86631468) + "'", int22.equals((-86631468)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-103) + "'", int25.equals((-103)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 678545 + "'", int28.equals(678545));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-134840) + "'", int31.equals((-134840)));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + (-1595400488) + "'", int34.equals((-1595400488)));
    }

    @Test
    public void test110() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test110");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3020, (java.lang.Integer) 990);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 680603190, (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2989800 + "'", int12.equals(2989800));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
    }

    @Test
    public void test111() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test111");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1089, (java.lang.Integer) (-1027036680));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 23816997, (java.lang.Integer) 222308);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166111, (java.lang.Integer) 1023985958);
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Integer int29 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-970066961));
        java.lang.Integer int32 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-985705143), (java.lang.Integer) (-1713400));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1751447560) + "'", int19.equals((-1751447560)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-985706892) + "'", int22.equals((-985706892)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1741645850 + "'", int25.equals(1741645850));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 0 + "'", int29.equals(0));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 1497177416 + "'", int32.equals(1497177416));
    }

    @Test
    public void test112() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test112");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 1023986076, (java.lang.Integer) 28000);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1729412004, (java.lang.Integer) (-342852211));
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 216150938, (java.lang.Integer) (-606522767));
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) (-985930295), (java.lang.Integer) 1593177030);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 1024014076 + "'", int7.equals(1024014076));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-28676780) + "'", int10.equals((-28676780)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 822673705 + "'", int13.equals(822673705));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 607246735 + "'", int16.equals(607246735));
    }

    @Test
    public void test113() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test113");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2, (java.lang.Integer) (-1));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) 338956);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 1738534581, (java.lang.Integer) 222612);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) (-674366), (java.lang.Integer) 138124938);
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) 129067, (java.lang.Integer) (-485640240));
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) 1592787391);
        java.lang.Integer int31 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-612929), (java.lang.Integer) (-680600602));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-2) + "'", int13.equals((-2)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1023986076 + "'", int16.equals(1023986076));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 7809 + "'", int19.equals(7809));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-138799304) + "'", int22.equals((-138799304)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 485769307 + "'", int25.equals(485769307));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-1737142630) + "'", int31.equals((-1737142630)));
    }

    @Test
    public void test114() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test114");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-237), (java.lang.Integer) 101);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-1346181984), (java.lang.Integer) (-28474));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 164521522, (java.lang.Integer) 1988598756);
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-23937) + "'", int16.equals((-23937)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1346210458) + "'", int19.equals((-1346210458)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-2141847018) + "'", int22.equals((-2141847018)));
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test115() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test115");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) 101);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1001435648, (java.lang.Integer) 203);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-98990), (java.lang.Integer) 25);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2 + "'", int6.equals(2));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 1427973632 + "'", int10.equals(1427973632));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-98965) + "'", int13.equals((-98965)));
    }

    @Test
    public void test116() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test116");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-2031), (java.lang.Integer) (-100));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-679014), (java.lang.Integer) 28345);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-203658612), (java.lang.Integer) 101);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 84409818, (java.lang.Integer) 29686803);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-2131) + "'", int10.equals((-2131)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-650669) + "'", int13.equals((-650669)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 905316668 + "'", int16.equals(905316668));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2 + "'", int19.equals(2));
    }

    @Test
    public void test117() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test117");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.restar((java.lang.Integer) 7458814, (java.lang.Integer) (-802065));
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 8260879 + "'", int11.equals(8260879));
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test118() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test118");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 473676, (java.lang.Integer) (-2030));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 20300, (java.lang.Integer) 338956);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 3021, (java.lang.Integer) 3274);
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1101, (java.lang.Integer) (-86631468));
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2000, (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-961562280) + "'", int12.equals((-961562280)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 359256 + "'", int15.equals(359256));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 6295 + "'", int18.equals(6295));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-891965756) + "'", int21.equals((-891965756)));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
    }

    @Test
    public void test119() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test119");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-304), (java.lang.Integer) 12);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 902932, (java.lang.Integer) 199);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 151210502, (java.lang.Integer) 6);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 698720868, (java.lang.Integer) 2010926408);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-292) + "'", int7.equals((-292)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 179683468 + "'", int10.equals(179683468));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 907263012 + "'", int13.equals(907263012));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
    }

    @Test
    public void test120() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test120");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-26796), (java.lang.Integer) (-2131));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-674366), (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-365383), (java.lang.Integer) (-594099455));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) (-33139200), (java.lang.Integer) 1072089982);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-28927) + "'", int12.equals((-28927)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-133524468) + "'", int15.equals((-133524468)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1899059129 + "'", int18.equals(1899059129));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1105229182) + "'", int22.equals((-1105229182)));
    }

    @Test
    public void test121() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test121");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) (-292));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-694249), (java.lang.Integer) (-343065842));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-593933344), (java.lang.Integer) (-388878032));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-480618), (java.lang.Integer) 218);
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2099619127), (java.lang.Integer) (-1885225800));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-882132) + "'", int16.equals((-882132)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 342371593 + "'", int19.equals(342371593));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1 + "'", int22.equals(1));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-480400) + "'", int25.equals((-480400)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-955917192) + "'", int28.equals((-955917192)));
    }

    @Test
    public void test122() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test122");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99990, (java.lang.Integer) 14);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 218, (java.lang.Integer) 203658830);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 34527, (java.lang.Integer) (-198));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1402661432), (java.lang.Integer) (-1937186348));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 100004 + "'", int9.equals(100004));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-26796) + "'", int12.equals((-26796)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-203658612) + "'", int15.equals((-203658612)));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 34329 + "'", int19.equals(34329));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1910845024) + "'", int22.equals((-1910845024)));
    }

    @Test
    public void test123() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test123");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-304));
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) 227581);
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 7458814);
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-12511));
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) (-9430), (java.lang.Integer) (-23));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14.equals(0));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 7458814 + "'", int17.equals(7458814));
        org.junit.Assert.assertNotNull(wildcardClass18);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-12411) + "'", int21.equals((-12411)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-9453) + "'", int24.equals((-9453)));
    }

    @Test
    public void test124() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test124");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) (-1));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) (-2233), (java.lang.Integer) 797);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-403), (java.lang.Integer) 1701552590);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.dividir((java.lang.Integer) 1710, (java.lang.Integer) 1045334094);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 334, (java.lang.Integer) (-309626391));
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) 29684000, (java.lang.Integer) (-789783992));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-3030) + "'", int10.equals((-3030)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1701552187 + "'", int13.equals(1701552187));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-309626057) + "'", int20.equals((-309626057)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 2117669120 + "'", int23.equals(2117669120));
    }

    @Test
    public void test125() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test125");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2354, (java.lang.Integer) 2679600);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-98990), (java.lang.Integer) (-4060));
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2022936550), (java.lang.Integer) 676504);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 24 + "'", int16.equals(24));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-2058427536) + "'", int20.equals((-2058427536)));
    }

    @Test
    public void test126() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test126");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-100), (java.lang.Integer) (-2233));
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 2679600, (java.lang.Integer) (-406034920));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-1352861920), (java.lang.Integer) 30106984);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223300 + "'", int6.equals(223300));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 408714520 + "'", int9.equals(408714520));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-44) + "'", int12.equals((-44)));
    }

    @Test
    public void test127() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test127");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-2233), (java.lang.Integer) 199);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 227358, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 28000, (java.lang.Integer) 675040);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 218, (java.lang.Integer) (-98));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 677082, (java.lang.Integer) 589628875);
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) 438482334, (java.lang.Integer) (-237));
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-915911584), (java.lang.Integer) 137901733);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-11) + "'", int10.equals((-11)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227358 + "'", int13.equals(227358));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 703040 + "'", int16.equals(703040));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 120 + "'", int19.equals(120));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 438482571 + "'", int25.equals(438482571));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-2057995808) + "'", int28.equals((-2057995808)));
    }

    @Test
    public void test128() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test128");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) (-198), (java.lang.Integer) (-453522300));
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) 129067, (java.lang.Integer) (-104682583));
        java.lang.Integer int29 = operacionesMatematicas0.dividir((java.lang.Integer) (-127644456), (java.lang.Integer) (-873156000));
        java.lang.Class<?> wildcardClass30 = operacionesMatematicas0.getClass();
        java.lang.Integer int33 = operacionesMatematicas0.dividir((java.lang.Integer) 1672679892, (java.lang.Integer) 611385366);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 453522102 + "'", int23.equals(453522102));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-104553516) + "'", int26.equals((-104553516)));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 0 + "'", int29.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass30);
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + 2 + "'", int33.equals(2));
    }

    @Test
    public void test129() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test129");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-71215), (java.lang.Integer) 44477096);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
    }

    @Test
    public void test130() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test130");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2000);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-11), (java.lang.Integer) (-678074));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 10, (java.lang.Integer) (-91808));
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-28275), (java.lang.Integer) 44540629);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) (-1761916892), (java.lang.Integer) 996650801);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 7458814 + "'", int12.equals(7458814));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-91798) + "'", int15.equals((-91798)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-44568904) + "'", int18.equals((-44568904)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-765266091) + "'", int21.equals((-765266091)));
    }

    @Test
    public void test131() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test131");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 688, (java.lang.Integer) 990);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-3), (java.lang.Integer) (-103));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1024014076, (java.lang.Integer) 200183);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-365383), (java.lang.Integer) 0);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 476950759, (java.lang.Integer) (-2106518274));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1678 + "'", int9.equals(1678));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 100 + "'", int12.equals(100));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 10672420 + "'", int15.equals(10672420));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1629567515) + "'", int21.equals((-1629567515)));
    }

    @Test
    public void test132() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test132");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 138125136, (java.lang.Integer) 223403);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass9 = operacionesMatematicas0.getClass();
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 2108502328, (java.lang.Integer) 972267014);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-46395108), (java.lang.Integer) (-826855080));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 137901733 + "'", int7.equals(137901733));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2 + "'", int12.equals(2));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
    }

    @Test
    public void test133() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test133");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 1678, (java.lang.Integer) (-304));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 44540629, (java.lang.Integer) (-833949417));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-1338190225), (java.lang.Integer) 326559708);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1374 + "'", int9.equals(1374));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 878490046 + "'", int12.equals(878490046));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1011630517) + "'", int15.equals((-1011630517)));
    }

    @Test
    public void test134() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test134");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-198), (java.lang.Integer) 198);
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-245718859), (java.lang.Integer) 701863);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 1889283349, (java.lang.Integer) 17);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-246420722) + "'", int20.equals((-246420722)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 111134314 + "'", int23.equals(111134314));
    }

    @Test
    public void test135() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test135");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 791, (java.lang.Integer) 222612);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-28474), (java.lang.Integer) 2108502328);
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) 200282, (java.lang.Integer) 134282741);
        java.lang.Integer int31 = operacionesMatematicas0.dividir((java.lang.Integer) (-1480217496), (java.lang.Integer) 1362965102);
        java.lang.Integer int34 = operacionesMatematicas0.dividir((java.lang.Integer) (-1360503017), (java.lang.Integer) 1988598756);
        java.lang.Class<?> wildcardClass35 = operacionesMatematicas0.getClass();
        java.lang.Integer int38 = operacionesMatematicas0.dividir((java.lang.Integer) 794, (java.lang.Integer) (-761787472));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 223403 + "'", int22.equals(223403));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 2108473854 + "'", int25.equals(2108473854));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-134082459) + "'", int28.equals((-134082459)));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-1) + "'", int31.equals((-1)));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 0 + "'", int34.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass35);
        org.junit.Assert.assertTrue("'" + int38 + "' != '" + 0 + "'", int38.equals(0));
    }

    @Test
    public void test136() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test136");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 155085916, (java.lang.Integer) 30106984);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1424993952) + "'", int13.equals((-1424993952)));
    }

    @Test
    public void test137() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test137");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 679827);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-203658532), (java.lang.Integer) (-589628652));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-202979314), (java.lang.Integer) 203177900);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-684291464), (java.lang.Integer) 669335577);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 98974848, (java.lang.Integer) (-134082459));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-679726) + "'", int6.equals((-679726)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 961940784 + "'", int9.equals(961940784));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 198586 + "'", int12.equals(198586));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1) + "'", int15.equals((-1)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 815652480 + "'", int18.equals(815652480));
    }

    @Test
    public void test138() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test138");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) 688);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) (-2031), (java.lang.Integer) 223300);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 2337, (java.lang.Integer) (-202979465));
        java.lang.Integer int26 = operacionesMatematicas0.restar((java.lang.Integer) 4738865, (java.lang.Integer) 0);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 12 + "'", int13.equals(12));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 222612 + "'", int17.equals(222612));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 221269 + "'", int20.equals(221269));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 0 + "'", int23.equals(0));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 4738865 + "'", int26.equals(4738865));
    }

    @Test
    public void test139() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test139");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.restar((java.lang.Integer) (-3160), (java.lang.Integer) (-203662590));
        java.lang.Integer int11 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1913), (java.lang.Integer) 191);
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-2125259776), (java.lang.Integer) (-2010186930));
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 1592563465, (java.lang.Integer) 1954931892);
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 1954931645, (java.lang.Integer) (-1060594160));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 203659430 + "'", int8.equals(203659430));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-365383) + "'", int11.equals((-365383)));
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 159520590 + "'", int15.equals(159520590));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1) + "'", int22.equals((-1)));
    }

    @Test
    public void test140() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test140");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-98), (java.lang.Integer) 1739049973);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1371794486 + "'", int13.equals(1371794486));
    }

    @Test
    public void test141() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test141");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2, (java.lang.Integer) (-1));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) 338956);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-300960), (java.lang.Integer) 2679600);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-181982338), (java.lang.Integer) (-150851528));
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-2) + "'", int13.equals((-2)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1023986076 + "'", int16.equals(1023986076));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1001435648 + "'", int19.equals(1001435648));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1 + "'", int22.equals(1));
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test142() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test142");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) 223300);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-676396), (java.lang.Integer) 1678);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-203654745), (java.lang.Integer) (-365383));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) (-676296), (java.lang.Integer) 791133635);
        try {
            java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 1636528032, (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2030) + "'", int9.equals((-2030)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2679600 + "'", int13.equals(2679600));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-678074) + "'", int16.equals((-678074)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-203289362) + "'", int19.equals((-203289362)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 790457339 + "'", int22.equals(790457339));
    }

    @Test
    public void test143() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test143");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) (-1646083254), (java.lang.Integer) 227358);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 7699977, (java.lang.Integer) (-742442848));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-1646310612) + "'", int10.equals((-1646310612)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1891089824 + "'", int13.equals(1891089824));
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test144() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test144");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1419118905), (java.lang.Integer) (-1243099180));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 676395, (java.lang.Integer) (-1429679577));
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 47990220 + "'", int12.equals(47990220));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test145() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test145");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 2354, (java.lang.Integer) (-403));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-203289362), (java.lang.Integer) 676395);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1023986076, (java.lang.Integer) (-203658612));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1062496806), (java.lang.Integer) 44477096);
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-5) + "'", int12.equals((-5)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-530028550) + "'", int16.equals((-530028550)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1515772240 + "'", int19.equals(1515772240));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1098455824 + "'", int22.equals(1098455824));
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test146() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test146");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 1678);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-204), (java.lang.Integer) (-669335781));
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) 2679600);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 588844663, (java.lang.Integer) (-110177015));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 166122 + "'", int10.equals(166122));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 669335577 + "'", int13.equals(669335577));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 699021678 + "'", int19.equals(699021678));
    }

    @Test
    public void test147() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test147");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-26796), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) (-6763), (java.lang.Integer) (-4060));
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) (-28474), (java.lang.Integer) 199);
        java.lang.Class<?> wildcardClass27 = operacionesMatematicas0.getClass();
        java.lang.Integer int30 = operacionesMatematicas0.sumar((java.lang.Integer) (-1013322), (java.lang.Integer) 195080);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-28474) + "'", int20.equals((-28474)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 1 + "'", int23.equals(1));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-28275) + "'", int26.equals((-28275)));
        org.junit.Assert.assertNotNull(wildcardClass27);
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + (-818242) + "'", int30.equals((-818242)));
    }

    @Test
    public void test148() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test148");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) 223300);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 3020, (java.lang.Integer) (-2030));
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 1351444, (java.lang.Integer) 990);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 299478398, (java.lang.Integer) (-23937));
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1349700, (java.lang.Integer) (-6351532));
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) 789784196, (java.lang.Integer) 1973854425);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2030) + "'", int9.equals((-2030)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2679600 + "'", int13.equals(2679600));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 5050 + "'", int17.equals(5050));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 1352434 + "'", int20.equals(1352434));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-12511) + "'", int23.equals((-12511)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 91982416 + "'", int26.equals(91982416));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 753280484 + "'", int29.equals(753280484));
    }

    @Test
    public void test149() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test149");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 688, (java.lang.Integer) 990);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) 218, (java.lang.Integer) (-2131));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 1771576096, (java.lang.Integer) 2108530673);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 117, (java.lang.Integer) 99990);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 4058, (java.lang.Integer) (-1556252358));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1678 + "'", int9.equals(1678));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1913) + "'", int12.equals((-1913)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-336954577) + "'", int15.equals((-336954577)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1556248300) + "'", int21.equals((-1556248300)));
    }

    @Test
    public void test150() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test150");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 473676, (java.lang.Integer) (-2030));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 20300, (java.lang.Integer) 338956);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 229388, (java.lang.Integer) (-90));
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) 541237670, (java.lang.Integer) (-237));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-961562280) + "'", int12.equals((-961562280)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 359256 + "'", int15.equals(359256));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-2548) + "'", int18.equals((-2548)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-2283703) + "'", int21.equals((-2283703)));
    }

    @Test
    public void test151() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test151");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 198, (java.lang.Integer) (-6763));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-337227660), (java.lang.Integer) 6);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-56204610) + "'", int12.equals((-56204610)));
    }

    @Test
    public void test152() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test152");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166122, (java.lang.Integer) (-2030));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 146, (java.lang.Integer) (-292));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) (-203657735));
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-204), (java.lang.Integer) (-1988598960));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 199 + "'", int6.equals(199));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-337227660) + "'", int9.equals((-337227660)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-42632) + "'", int12.equals((-42632)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-203656745) + "'", int15.equals((-203656745)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1988598756 + "'", int18.equals(1988598756));
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test153() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test153");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) 14);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 227358, (java.lang.Integer) 223);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 166122, (java.lang.Integer) (-11));
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 200232, (java.lang.Integer) (-181982338));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) (-485640240));
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) (-1625055376), (java.lang.Integer) (-43315734));
        java.lang.Integer int29 = operacionesMatematicas0.restar((java.lang.Integer) 116, (java.lang.Integer) (-87104256));
        java.lang.Integer int32 = operacionesMatematicas0.dividir((java.lang.Integer) 155085916, (java.lang.Integer) 173359620);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 675040 + "'", int10.equals(675040));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227581 + "'", int13.equals(227581));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 166111 + "'", int16.equals(166111));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-181782106) + "'", int19.equals((-181782106)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-485640240) + "'", int22.equals((-485640240)));
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 37 + "'", int26.equals(37));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 87104372 + "'", int29.equals(87104372));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 0 + "'", int32.equals(0));
    }

    @Test
    public void test154() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test154");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-26796), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) (-6763), (java.lang.Integer) (-4060));
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) (-28474), (java.lang.Integer) 199);
        java.lang.Integer int29 = operacionesMatematicas0.sumar((java.lang.Integer) (-319237330), (java.lang.Integer) (-86797590));
        java.lang.Integer int32 = operacionesMatematicas0.sumar((java.lang.Integer) (-2147357664), (java.lang.Integer) (-1620334347));
        java.lang.Integer int35 = operacionesMatematicas0.restar((java.lang.Integer) 442013635, (java.lang.Integer) 1981352906);
        java.lang.Integer int38 = operacionesMatematicas0.restar((java.lang.Integer) (-1796355519), (java.lang.Integer) 852490804);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-28474) + "'", int20.equals((-28474)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 1 + "'", int23.equals(1));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-28275) + "'", int26.equals((-28275)));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-406034920) + "'", int29.equals((-406034920)));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 527275285 + "'", int32.equals(527275285));
        org.junit.Assert.assertTrue("'" + int35 + "' != '" + (-1539339271) + "'", int35.equals((-1539339271)));
        org.junit.Assert.assertTrue("'" + int38 + "' != '" + 1646120973 + "'", int38.equals(1646120973));
    }

    @Test
    public void test155() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test155");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 100004, (java.lang.Integer) 222308);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 218, (java.lang.Integer) 166122);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 179683468);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 1787, (java.lang.Integer) 791);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 1975465670, (java.lang.Integer) 9992694);
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) (-300943), (java.lang.Integer) 607246735);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-165904) + "'", int12.equals((-165904)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-179683468) + "'", int15.equals((-179683468)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 2 + "'", int18.equals(2));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1985458364 + "'", int21.equals(1985458364));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 606945792 + "'", int24.equals(606945792));
    }

    @Test
    public void test156() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test156");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) 101);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 28053, (java.lang.Integer) 849);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-86407950), (java.lang.Integer) (-2027625964));
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 1216786422, (java.lang.Integer) 1224212052);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2 + "'", int6.equals(2));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 23816997 + "'", int10.equals(23816997));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-2114033914) + "'", int13.equals((-2114033914)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-1853968822) + "'", int16.equals((-1853968822)));
    }

    @Test
    public void test157() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test157");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-26796), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) (-6763), (java.lang.Integer) (-4060));
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) (-300943), (java.lang.Integer) 3021);
        java.lang.Integer int29 = operacionesMatematicas0.dividir((java.lang.Integer) 2010197494, (java.lang.Integer) (-359));
        java.lang.Class<?> wildcardClass30 = operacionesMatematicas0.getClass();
        java.lang.Integer int33 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-870834822), (java.lang.Integer) (-853071786));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-28474) + "'", int20.equals((-28474)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 1 + "'", int23.equals(1));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-99) + "'", int26.equals((-99)));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-5599435) + "'", int29.equals((-5599435)));
        org.junit.Assert.assertNotNull(wildcardClass30);
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + (-2075461892) + "'", int33.equals((-2075461892)));
    }

    @Test
    public void test158() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test158");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 2, (java.lang.Integer) 1023986076);
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 2354, (java.lang.Integer) (-104682583));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) (-114017579), (java.lang.Integer) 1592787391);
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1656482352, (java.lang.Integer) 161877486);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 104684937 + "'", int22.equals(104684937));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-524801888) + "'", int28.equals((-524801888)));
    }

    @Test
    public void test159() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test159");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass6 = operacionesMatematicas0.getClass();
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 688, (java.lang.Integer) (-924510));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 1427973632);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1739459091, (java.lang.Integer) 24);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-1387015424), (java.lang.Integer) (-203078208));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1427973632) + "'", int12.equals((-1427973632)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1202654776) + "'", int15.equals((-1202654776)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1183937216) + "'", int18.equals((-1183937216)));
    }

    @Test
    public void test160() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test160");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1089, (java.lang.Integer) (-1027036680));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 23816997, (java.lang.Integer) 222308);
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1751447560) + "'", int19.equals((-1751447560)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-985706892) + "'", int22.equals((-985706892)));
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test161() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test161");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2, (java.lang.Integer) 1000);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-907044204), (java.lang.Integer) 166111);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-480618), (java.lang.Integer) (-674366));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 854019829, (java.lang.Integer) 200232);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-826225304), (java.lang.Integer) (-60261300));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2000 + "'", int6.equals(2000));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-906878093) + "'", int9.equals((-906878093)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1989890988 + "'", int12.equals(1989890988));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 854220061 + "'", int15.equals(854220061));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1761912096) + "'", int18.equals((-1761912096)));
    }

    @Test
    public void test162() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test162");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test163() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test163");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 990, (java.lang.Integer) (-2031));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-17), (java.lang.Integer) 243122930);
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) 216150945, (java.lang.Integer) 961562281);
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1500976826), (java.lang.Integer) 333);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 3021 + "'", int19.equals(3021));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 161877486 + "'", int22.equals(161877486));
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 1455561545 + "'", int26.equals(1455561545));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-1609076722) + "'", int29.equals((-1609076722)));
    }

    @Test
    public void test164() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test164");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) 14);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 227358, (java.lang.Integer) 223);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-702061), (java.lang.Integer) (-7812));
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 1772926587, (java.lang.Integer) 99);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 675040 + "'", int10.equals(675040));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227581 + "'", int13.equals(227581));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-694249) + "'", int16.equals((-694249)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1772926686 + "'", int19.equals(1772926686));
    }

    @Test
    public void test165() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test165");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) 203);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) (-223), (java.lang.Integer) (-702086));
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1779233240, (java.lang.Integer) 361450384);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 797 + "'", int7.equals(797));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 701863 + "'", int10.equals(701863));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2053788032 + "'", int13.equals(2053788032));
    }

    @Test
    public void test166() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test166");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) 2030, (java.lang.Integer) 688);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) (-2233));
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.sumar((java.lang.Integer) 298, (java.lang.Integer) 0);
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 44907013, (java.lang.Integer) (-2452019));
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) (-343058591));
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) 107, (java.lang.Integer) 970067068);
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) 166122, (java.lang.Integer) (-98));
        java.lang.Integer int29 = operacionesMatematicas0.dividir((java.lang.Integer) 243122930, (java.lang.Integer) (-480630));
        java.lang.Integer int32 = operacionesMatematicas0.dividir((java.lang.Integer) (-2017286592), (java.lang.Integer) (-1352861920));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7.equals(2));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 225533 + "'", int10.equals(225533));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 298 + "'", int14.equals(298));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 47359032 + "'", int17.equals(47359032));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-343058591) + "'", int20.equals((-343058591)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-970066961) + "'", int23.equals((-970066961)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-1695) + "'", int26.equals((-1695)));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-505) + "'", int29.equals((-505)));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 1 + "'", int32.equals(1));
    }

    @Test
    public void test167() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test167");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 473676, (java.lang.Integer) (-2030));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-292), (java.lang.Integer) 2108502328);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-86797590), (java.lang.Integer) 674850);
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 135279661, (java.lang.Integer) 199);
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-961562280) + "'", int12.equals((-961562280)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-589628652) + "'", int19.equals((-589628652)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1150848763 + "'", int22.equals(1150848763));
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test168() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test168");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-674366));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-1027036680), (java.lang.Integer) (-28927));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 834392107, (java.lang.Integer) (-343065842));
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1427973632, (java.lang.Integer) 442626564);
        java.lang.Integer int31 = operacionesMatematicas0.restar((java.lang.Integer) (-166100), (java.lang.Integer) (-952340332));
        java.lang.Integer int34 = operacionesMatematicas0.restar((java.lang.Integer) 65, (java.lang.Integer) (-313922314));
        java.lang.Class<?> wildcardClass35 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass36 = operacionesMatematicas0.getClass();
        java.lang.Integer int39 = operacionesMatematicas0.sumar((java.lang.Integer) 1492597250, (java.lang.Integer) (-917));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-203658532) + "'", int19.equals((-203658532)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 35504 + "'", int22.equals(35504));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-2) + "'", int25.equals((-2)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 1214289920 + "'", int28.equals(1214289920));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 952174232 + "'", int31.equals(952174232));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 313922379 + "'", int34.equals(313922379));
        org.junit.Assert.assertNotNull(wildcardClass35);
        org.junit.Assert.assertNotNull(wildcardClass36);
        org.junit.Assert.assertTrue("'" + int39 + "' != '" + 1492596333 + "'", int39.equals(1492596333));
    }

    @Test
    public void test169() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test169");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) (-26796));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) (-3));
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) (-678748));
        java.lang.Integer int31 = operacionesMatematicas0.dividir((java.lang.Integer) 369151, (java.lang.Integer) (-1033487401));
        java.lang.Integer int34 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-606670307), (java.lang.Integer) 1079);
        java.lang.Class<?> wildcardClass35 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-86631468) + "'", int22.equals((-86631468)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-103) + "'", int25.equals((-103)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 678545 + "'", int28.equals(678545));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 0 + "'", int31.equals(0));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + (-1762232261) + "'", int34.equals((-1762232261)));
        org.junit.Assert.assertNotNull(wildcardClass35);
    }

    @Test
    public void test170() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test170");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-198), (java.lang.Integer) 198);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-98990));
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2117069372, (java.lang.Integer) (-4060));
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-917), (java.lang.Integer) (-1988598960));
        java.lang.Integer int29 = operacionesMatematicas0.sumar((java.lang.Integer) (-209727), (java.lang.Integer) (-164003));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-98890) + "'", int20.equals((-98890)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-1072091024) + "'", int23.equals((-1072091024)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-1815854480) + "'", int26.equals((-1815854480)));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-373730) + "'", int29.equals((-373730)));
    }

    @Test
    public void test171() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test171");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.restar((java.lang.Integer) 7458814, (java.lang.Integer) (-802065));
        java.lang.Integer int14 = operacionesMatematicas0.restar((java.lang.Integer) 45086, (java.lang.Integer) (-220878114));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 8260879 + "'", int11.equals(8260879));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 220923200 + "'", int14.equals(220923200));
    }

    @Test
    public void test172() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test172");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-204), (java.lang.Integer) 12);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1546915), (java.lang.Integer) 38790450);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1623565852, (java.lang.Integer) (-1088946496));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-17) + "'", int6.equals((-17)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-40337365) + "'", int9.equals((-40337365)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2140167424 + "'", int12.equals(2140167424));
    }

    @Test
    public void test173() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test173");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 677082, (java.lang.Integer) 797);
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) (-6), (java.lang.Integer) 2354);
        java.lang.Integer int29 = operacionesMatematicas0.dividir((java.lang.Integer) 681653544, (java.lang.Integer) (-882132));
        java.lang.Integer int32 = operacionesMatematicas0.sumar((java.lang.Integer) (-1198851069), (java.lang.Integer) (-78));
        java.lang.Integer int35 = operacionesMatematicas0.restar((java.lang.Integer) (-1518077235), (java.lang.Integer) (-12511));
        java.lang.Integer int38 = operacionesMatematicas0.dividir((java.lang.Integer) 1968286926, (java.lang.Integer) (-607257));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 849 + "'", int22.equals(849));
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 2348 + "'", int26.equals(2348));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-772) + "'", int29.equals((-772)));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + (-1198851147) + "'", int32.equals((-1198851147)));
        org.junit.Assert.assertTrue("'" + int35 + "' != '" + (-1518064724) + "'", int35.equals((-1518064724)));
        org.junit.Assert.assertTrue("'" + int38 + "' != '" + (-3241) + "'", int38.equals((-3241)));
    }

    @Test
    public void test174() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test174");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) (-3), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.sumar((java.lang.Integer) 703051, (java.lang.Integer) 222308);
        java.lang.Integer int14 = operacionesMatematicas0.dividir((java.lang.Integer) 1741645850, (java.lang.Integer) 86235969);
        java.lang.Class<?> wildcardClass15 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-3) + "'", int7.equals((-3)));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 925359 + "'", int11.equals(925359));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 20 + "'", int14.equals(20));
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test175() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test175");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2, (java.lang.Integer) (-1));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) 338956);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 1738534581, (java.lang.Integer) 222612);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) (-674366), (java.lang.Integer) 138124938);
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) 129067, (java.lang.Integer) (-485640240));
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Integer int29 = operacionesMatematicas0.sumar((java.lang.Integer) (-58), (java.lang.Integer) 0);
        java.lang.Integer int32 = operacionesMatematicas0.sumar((java.lang.Integer) 925442, (java.lang.Integer) (-682294930));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-2) + "'", int13.equals((-2)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1023986076 + "'", int16.equals(1023986076));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 7809 + "'", int19.equals(7809));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-138799304) + "'", int22.equals((-138799304)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 485769307 + "'", int25.equals(485769307));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-58) + "'", int29.equals((-58)));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + (-681369488) + "'", int32.equals((-681369488)));
    }

    @Test
    public void test176() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test176");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 44906012, (java.lang.Integer) 2679600);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-223), (java.lang.Integer) (-28927));
        try {
            java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1937735020, (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 42226412 + "'", int7.equals(42226412));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 6450721 + "'", int10.equals(6450721));
    }

    @Test
    public void test177() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test177");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.dividir((java.lang.Integer) 675054, (java.lang.Integer) (-674366));
        java.lang.Integer int11 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1349700, (java.lang.Integer) (-136443584));
        java.lang.Integer int14 = operacionesMatematicas0.restar((java.lang.Integer) 1888659260, (java.lang.Integer) (-1480217496));
        java.lang.Integer int17 = operacionesMatematicas0.multiplicar((java.lang.Integer) 333, (java.lang.Integer) (-203859113));
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1980658657, (java.lang.Integer) (-406034920));
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) (-1896184000), (java.lang.Integer) (-91808));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + (-1) + "'", int8.equals((-1)));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 1702393088 + "'", int11.equals(1702393088));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + (-926090540) + "'", int14.equals((-926090540)));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 834392107 + "'", int17.equals(834392107));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 835879704 + "'", int20.equals(835879704));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 20653 + "'", int23.equals(20653));
    }

    @Test
    public void test178() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test178");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2, (java.lang.Integer) (-1));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) 338956);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 1738534581, (java.lang.Integer) 222612);
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-2) + "'", int13.equals((-2)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1023986076 + "'", int16.equals(1023986076));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 7809 + "'", int19.equals(7809));
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test179() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test179");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) (-2015005451), (java.lang.Integer) 1351444);
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1101, (java.lang.Integer) 9992694);
        java.lang.Class<?> wildcardClass25 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertNotNull(wildcardClass18);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1491) + "'", int21.equals((-1491)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1882945794) + "'", int24.equals((-1882945794)));
        org.junit.Assert.assertNotNull(wildcardClass25);
    }

    @Test
    public void test180() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test180");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) (-292));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-694249), (java.lang.Integer) (-343065842));
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) (-970067068));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 677084, (java.lang.Integer) (-305));
        java.lang.Integer int28 = operacionesMatematicas0.sumar((java.lang.Integer) (-197), (java.lang.Integer) (-203658530));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-882132) + "'", int16.equals((-882132)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 342371593 + "'", int19.equals(342371593));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 970067068 + "'", int22.equals(970067068));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 676779 + "'", int25.equals(676779));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-203658727) + "'", int28.equals((-203658727)));
    }

    @Test
    public void test181() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test181");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-98975152), (java.lang.Integer) (-300960));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 676395, (java.lang.Integer) (-655230575));
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 880885566, (java.lang.Integer) (-8230961));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1963548160 + "'", int9.equals(1963548160));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 655906970 + "'", int13.equals(655906970));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 872654605 + "'", int16.equals(872654605));
    }

    @Test
    public void test182() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test182");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-2233));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 849, (java.lang.Integer) 925359);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-872882432), (java.lang.Integer) (-1468325642));
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-589656652), (java.lang.Integer) (-625983112));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 905316668, (java.lang.Integer) (-548663440));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-674366) + "'", int9.equals((-674366)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-924510) + "'", int12.equals((-924510)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1215639764) + "'", int19.equals((-1215639764)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 356653228 + "'", int22.equals(356653228));
    }

    @Test
    public void test183() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test183");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 199);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1315384639), (java.lang.Integer) (-127444272));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 298 + "'", int9.equals(298));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-907405616) + "'", int13.equals((-907405616)));
    }

    @Test
    public void test184() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test184");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-98975152), (java.lang.Integer) (-300960));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-676296), (java.lang.Integer) 2108473854);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 21210, (java.lang.Integer) (-1933340520));
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-71), (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1963548160 + "'", int9.equals(1963548160));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2107797558 + "'", int12.equals(2107797558));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1933319310) + "'", int15.equals((-1933319310)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
    }

    @Test
    public void test185() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test185");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) 688);
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) (-594099455), (java.lang.Integer) (-5600));
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) (-9120288), (java.lang.Integer) (-2164));
        java.lang.Integer int27 = operacionesMatematicas0.dividir((java.lang.Integer) 9992694, (java.lang.Integer) (-973376920));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 12 + "'", int13.equals(12));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 222612 + "'", int17.equals(222612));
        org.junit.Assert.assertNotNull(wildcardClass18);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-594105055) + "'", int21.equals((-594105055)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-9122452) + "'", int24.equals((-9122452)));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 0 + "'", int27.equals(0));
    }

    @Test
    public void test186() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test186");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass6 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertNotNull(wildcardClass7);
    }

    @Test
    public void test187() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test187");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) 688);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) (-2031), (java.lang.Integer) 223300);
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) 191, (java.lang.Integer) 3451);
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) (-86797590), (java.lang.Integer) (-1072091024));
        java.lang.Integer int29 = operacionesMatematicas0.dividir((java.lang.Integer) (-2233), (java.lang.Integer) 161877486);
        java.lang.Integer int32 = operacionesMatematicas0.dividir((java.lang.Integer) 419736786, (java.lang.Integer) (-2962));
        java.lang.Class<?> wildcardClass33 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 12 + "'", int13.equals(12));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 222612 + "'", int17.equals(222612));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 221269 + "'", int20.equals(221269));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-3260) + "'", int23.equals((-3260)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 0 + "'", int26.equals(0));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 0 + "'", int29.equals(0));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + (-141707) + "'", int32.equals((-141707)));
        org.junit.Assert.assertNotNull(wildcardClass33);
    }

    @Test
    public void test188() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test188");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 990, (java.lang.Integer) (-2031));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) (-984), (java.lang.Integer) (-312057086));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) (-1017), (java.lang.Integer) (-2027625964));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 3021 + "'", int19.equals(3021));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-312058070) + "'", int22.equals((-312058070)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
    }

    @Test
    public void test189() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test189");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 674850, (java.lang.Integer) (-203658532));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 9120400, (java.lang.Integer) 8260879);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-454172969), (java.lang.Integer) (-844));
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) (-202980456), (java.lang.Integer) (-245718859));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 859521 + "'", int13.equals(859521));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1069896492 + "'", int16.equals(1069896492));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 0 + "'", int20.equals(0));
    }

    @Test
    public void test190() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test190");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 3241260, (java.lang.Integer) 1001435648);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 134282741, (java.lang.Integer) (-203761583));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 2037894992, (java.lang.Integer) (-315420402));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-998194388) + "'", int16.equals((-998194388)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-6) + "'", int22.equals((-6)));
    }

    @Test
    public void test191() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test191");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-4755287), (java.lang.Integer) (-43315734));
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.restar((java.lang.Integer) (-1709861456), (java.lang.Integer) 2137056006);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 705204090 + "'", int10.equals(705204090));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 448049834 + "'", int14.equals(448049834));
    }

    @Test
    public void test192() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test192");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-204), (java.lang.Integer) 12);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 797, (java.lang.Integer) (-201378));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 3021, (java.lang.Integer) 200183);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-882132), (java.lang.Integer) 668453649);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-17) + "'", int6.equals((-17)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-669335781) + "'", int15.equals((-669335781)));
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test193() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test193");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-882132), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) (-679114), (java.lang.Integer) 100);
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) 153262720, (java.lang.Integer) 23816997);
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1751438428, (java.lang.Integer) 6450721);
        java.lang.Integer int32 = operacionesMatematicas0.multiplicar((java.lang.Integer) 680540844, (java.lang.Integer) (-773335248));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-1480217496) + "'", int20.equals((-1480217496)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-679014) + "'", int23.equals((-679014)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 6 + "'", int26.equals(6));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 326559708 + "'", int29.equals(326559708));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 428176448 + "'", int32.equals(428176448));
    }

    @Test
    public void test194() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test194");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) (-319241688), (java.lang.Integer) 1346231);
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) (-98990));
        java.lang.Integer int29 = operacionesMatematicas0.sumar((java.lang.Integer) 5050, (java.lang.Integer) 0);
        java.lang.Integer int32 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1807381003, (java.lang.Integer) (-138799294));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-317895457) + "'", int23.equals((-317895457)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-98990) + "'", int26.equals((-98990)));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 5050 + "'", int29.equals(5050));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + (-11639850) + "'", int32.equals((-11639850)));
    }

    @Test
    public void test195() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test195");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 2354, (java.lang.Integer) (-403));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-594105055), (java.lang.Integer) 10);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 191, (java.lang.Integer) 5050);
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-203657736), (java.lang.Integer) 2118110076);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-5) + "'", int12.equals((-5)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1646083254) + "'", int15.equals((-1646083254)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
    }

    @Test
    public void test196() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test196");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 10);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) 847782221, (java.lang.Integer) 847782221);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 83, (java.lang.Integer) 925359);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-1377839691), (java.lang.Integer) (-5600));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1000 + "'", int9.equals(1000));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1695564442 + "'", int12.equals(1695564442));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 925442 + "'", int15.equals(925442));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1377845291) + "'", int18.equals((-1377845291)));
    }

    @Test
    public void test197() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test197");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-304), (java.lang.Integer) 12);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-91798), (java.lang.Integer) (-679726));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-847782612), (java.lang.Integer) (-485640339));
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-1902651632), (java.lang.Integer) (-1507971691));
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 1919311436, (java.lang.Integer) (-994773116));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-292) + "'", int7.equals((-292)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-2027022092) + "'", int10.equals((-2027022092)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1 + "'", int13.equals(1));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1 + "'", int16.equals(1));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 924538320 + "'", int19.equals(924538320));
    }

    @Test
    public void test198() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test198");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1963548160, (java.lang.Integer) (-110177015));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 1284132352 + "'", int3.equals(1284132352));
    }

    @Test
    public void test199() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test199");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 374697400, (java.lang.Integer) 473676);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 329, (java.lang.Integer) (-144));
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 669356581, (java.lang.Integer) (-1566890908));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 3233, (java.lang.Integer) (-98));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 791 + "'", int13.equals(791));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-47376) + "'", int16.equals((-47376)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-996062604) + "'", int19.equals((-996062604)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-32) + "'", int22.equals((-32)));
    }

    @Test
    public void test200() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test200");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-26796), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-3260));
        java.lang.Integer int26 = operacionesMatematicas0.restar((java.lang.Integer) (-1536561115), (java.lang.Integer) (-344077));
        java.lang.Class<?> wildcardClass27 = operacionesMatematicas0.getClass();
        java.lang.Integer int30 = operacionesMatematicas0.restar((java.lang.Integer) (-74681), (java.lang.Integer) 428176448);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-28474) + "'", int20.equals((-28474)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-3160) + "'", int23.equals((-3160)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-1536217038) + "'", int26.equals((-1536217038)));
        org.junit.Assert.assertNotNull(wildcardClass27);
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + (-428251129) + "'", int30.equals((-428251129)));
    }

    @Test
    public void test201() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test201");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 198911, (java.lang.Integer) (-949997523));
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) (-1461502113));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 950196434 + "'", int10.equals(950196434));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1461502113 + "'", int13.equals(1461502113));
    }

    @Test
    public void test202() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test202");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 674850, (java.lang.Integer) (-203658532));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 9120400, (java.lang.Integer) 8260879);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-454172969), (java.lang.Integer) (-844));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-328486218), (java.lang.Integer) 1214037568);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 859521 + "'", int13.equals(859521));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1069896492 + "'", int16.equals(1069896492));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1542523786) + "'", int19.equals((-1542523786)));
    }

    @Test
    public void test203() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test203");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 146, (java.lang.Integer) 990);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1017), (java.lang.Integer) (-11));
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-458350076), (java.lang.Integer) (-569427028));
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-114017579), (java.lang.Integer) 1582669994);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-1034161825), (java.lang.Integer) 1024014076);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-844) + "'", int7.equals((-844)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 11187 + "'", int10.equals(11187));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 111076952 + "'", int13.equals(111076952));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-1696687573) + "'", int16.equals((-1696687573)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-2058175901) + "'", int19.equals((-2058175901)));
    }

    @Test
    public void test204() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test204");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) (-2452019), (java.lang.Integer) 591647436);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1944184848), (java.lang.Integer) (-309626057));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-594099455) + "'", int6.equals((-594099455)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-1634558791) + "'", int9.equals((-1634558791)));
    }

    @Test
    public void test205() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test205");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-2031), (java.lang.Integer) (-100));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-679014), (java.lang.Integer) 28345);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-304), (java.lang.Integer) (-98975152));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-2125259776), (java.lang.Integer) 166120);
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-2131) + "'", int10.equals((-2131)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-650669) + "'", int13.equals((-650669)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 98974848 + "'", int16.equals(98974848));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-2125425896) + "'", int19.equals((-2125425896)));
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test206() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test206");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-320292228), (java.lang.Integer) (-204));
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-676396), (java.lang.Integer) 5050);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-3), (java.lang.Integer) (-203761580));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 38790450, (java.lang.Integer) 790454420);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2030) + "'", int9.equals((-2030)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1570059 + "'", int13.equals(1570059));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-681446) + "'", int16.equals((-681446)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-203761583) + "'", int19.equals((-203761583)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1892499688 + "'", int22.equals(1892499688));
    }

    @Test
    public void test207() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test207");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 3884, (java.lang.Integer) (-1554526525));
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-138799294), (java.lang.Integer) 1349710685);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1000 + "'", int9.equals(1000));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1554530409 + "'", int13.equals(1554530409));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
    }

    @Test
    public void test208() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test208");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 473676, (java.lang.Integer) (-2030));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-292), (java.lang.Integer) 2108502328);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-86797590), (java.lang.Integer) 674850);
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1750295076, (java.lang.Integer) (-350481344));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-961562280) + "'", int12.equals((-961562280)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-589628652) + "'", int19.equals((-589628652)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1618172160 + "'", int22.equals(1618172160));
    }

    @Test
    public void test209() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test209");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 1079, (java.lang.Integer) 679827);
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) 5050, (java.lang.Integer) (-1566625664));
        java.lang.Class<?> wildcardClass21 = operacionesMatematicas0.getClass();
        java.lang.Integer int24 = operacionesMatematicas0.restar((java.lang.Integer) 1701552187, (java.lang.Integer) 1998101);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-678748) + "'", int17.equals((-678748)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-129843968) + "'", int20.equals((-129843968)));
        org.junit.Assert.assertNotNull(wildcardClass21);
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 1699554086 + "'", int24.equals(1699554086));
    }

    @Test
    public void test210() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test210");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-203657735), (java.lang.Integer) (-98975152));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 868432494, (java.lang.Integer) (-337227660));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1750448650), (java.lang.Integer) 3021);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-203437481), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-104682583) + "'", int9.equals((-104682583)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-2) + "'", int12.equals((-2)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1000630274) + "'", int15.equals((-1000630274)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-203437481) + "'", int18.equals((-203437481)));
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test211() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test211");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-204), (java.lang.Integer) 12);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 797, (java.lang.Integer) (-201378));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) (-1146), (java.lang.Integer) 747408427);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-17) + "'", int6.equals((-17)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 747407281 + "'", int16.equals(747407281));
    }

    @Test
    public void test212() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test212");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 2, (java.lang.Integer) 1023986076);
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 2354, (java.lang.Integer) (-104682583));
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 104684937 + "'", int22.equals(104684937));
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test213() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test213");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) 1089);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1545561088, (java.lang.Integer) (-108954422));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-45556681), (java.lang.Integer) 1125633776);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1228204032 + "'", int12.equals(1228204032));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1080077095 + "'", int15.equals(1080077095));
    }

    @Test
    public void test214() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test214");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 2354, (java.lang.Integer) (-403));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-1491), (java.lang.Integer) 1570059);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) (-1936223520), (java.lang.Integer) 1558090488);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 989568000, (java.lang.Integer) (-484966043));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-5) + "'", int12.equals((-5)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1) + "'", int19.equals((-1)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-2) + "'", int22.equals((-2)));
    }

    @Test
    public void test215() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test215");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 1352162704);
        java.lang.Integer int14 = operacionesMatematicas0.sumar((java.lang.Integer) (-1646083254), (java.lang.Integer) (-1646083254));
        java.lang.Integer int17 = operacionesMatematicas0.dividir((java.lang.Integer) (-890108686), (java.lang.Integer) (-679014));
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-844), (java.lang.Integer) 691719053);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 5, (java.lang.Integer) 952174232);
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-506915));
        java.lang.Integer int29 = operacionesMatematicas0.dividir((java.lang.Integer) 1710, (java.lang.Integer) (-740129608));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-388878032) + "'", int11.equals((-388878032)));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 1002800788 + "'", int14.equals(1002800788));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 1310 + "'", int17.equals(1310));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-691719897) + "'", int20.equals((-691719897)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 0 + "'", int23.equals(0));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 0 + "'", int26.equals(0));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 0 + "'", int29.equals(0));
    }

    @Test
    public void test216() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test216");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) (-1));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 2030, (java.lang.Integer) 1);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-952340332), (java.lang.Integer) 2029);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 181480562, (java.lang.Integer) 2111569680);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2029 + "'", int10.equals(2029));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-952342361) + "'", int13.equals((-952342361)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-1930089118) + "'", int16.equals((-1930089118)));
    }

    @Test
    public void test217() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test217");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 218, (java.lang.Integer) 3233);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 676401, (java.lang.Integer) (-129843968));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-11), (java.lang.Integer) 0);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 688, (java.lang.Integer) (-28927));
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) (-151004351), (java.lang.Integer) 7458814);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 3451 + "'", int9.equals(3451));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1196436736 + "'", int12.equals(1196436736));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-11) + "'", int15.equals((-11)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-143545537) + "'", int21.equals((-143545537)));
    }

    @Test
    public void test218() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test218");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-223), (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-4060), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-202979700), (java.lang.Integer) 386);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 1059744909, (java.lang.Integer) 1731557128);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223 + "'", int6.equals(223));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2) + "'", int9.equals((-2)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-202979314) + "'", int12.equals((-202979314)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-671812219) + "'", int15.equals((-671812219)));
    }

    @Test
    public void test219() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test219");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) 223300);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-676396), (java.lang.Integer) 1678);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1418591405, (java.lang.Integer) (-1338190428));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2030) + "'", int9.equals((-2030)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2679600 + "'", int13.equals(2679600));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-678074) + "'", int16.equals((-678074)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertNotNull(wildcardClass18);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1992150996 + "'", int21.equals(1992150996));
    }

    @Test
    public void test220() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test220");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-203657735), (java.lang.Integer) (-98975152));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-872882432), (java.lang.Integer) 1374);
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) (-202978621), (java.lang.Integer) (-747018902));
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2010197328, (java.lang.Integer) 166);
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) (-625983112), (java.lang.Integer) (-15));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-104682583) + "'", int9.equals((-104682583)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-635285) + "'", int12.equals((-635285)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-949997523) + "'", int16.equals((-949997523)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2010197494 + "'", int19.equals(2010197494));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-625983127) + "'", int23.equals((-625983127)));
    }

    @Test
    public void test221() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test221");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.dividir((java.lang.Integer) 1739049973, (java.lang.Integer) 1963548060);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
    }

    @Test
    public void test222() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test222");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.sumar((java.lang.Integer) 166122, (java.lang.Integer) (-2));
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) (-50702028), (java.lang.Integer) (-1170752155));
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1174185152), (java.lang.Integer) (-996375243));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 166120 + "'", int4.equals(166120));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-602266560) + "'", int10.equals((-602266560)));
    }

    @Test
    public void test223() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test223");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) (-198), (java.lang.Integer) (-453522300));
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) 28000, (java.lang.Integer) 4051228);
        java.lang.Integer int29 = operacionesMatematicas0.sumar((java.lang.Integer) (-2015005451), (java.lang.Integer) 426562025);
        java.lang.Integer int32 = operacionesMatematicas0.multiplicar((java.lang.Integer) 118, (java.lang.Integer) 878490046);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 453522102 + "'", int23.equals(453522102));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 4079228 + "'", int26.equals(4079228));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-1588443426) + "'", int29.equals((-1588443426)));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 582610324 + "'", int32.equals(582610324));
    }

    @Test
    public void test224() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test224");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.dividir((java.lang.Integer) (-25), (java.lang.Integer) (-23417874));
        java.lang.Integer int17 = operacionesMatematicas0.dividir((java.lang.Integer) 1346231, (java.lang.Integer) (-202980456));
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) (-1174185152), (java.lang.Integer) (-203654655));
        java.lang.Class<?> wildcardClass21 = operacionesMatematicas0.getClass();
        java.lang.Integer int24 = operacionesMatematicas0.restar((java.lang.Integer) 113569156, (java.lang.Integer) 191);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 0 + "'", int14.equals(0));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-1377839807) + "'", int20.equals((-1377839807)));
        org.junit.Assert.assertNotNull(wildcardClass21);
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 113568965 + "'", int24.equals(113568965));
    }

    @Test
    public void test225() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test225");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 198, (java.lang.Integer) (-3030));
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-2548), (java.lang.Integer) 532602758);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-319619085), (java.lang.Integer) (-1477849338));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-569427028), (java.lang.Integer) (-1708435290));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 1771581039, (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-599940) + "'", int13.equals((-599940)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1797468423) + "'", int19.equals((-1797468423)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1203252856) + "'", int22.equals((-1203252856)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1771581039 + "'", int25.equals(1771581039));
    }

    @Test
    public void test226() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test226");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-304));
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 191, (java.lang.Integer) 137901733);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 2010197328, (java.lang.Integer) (-133524468));
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-136543588), (java.lang.Integer) (-485636182));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-1988598960), (java.lang.Integer) 903096066);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 569427227 + "'", int13.equals(569427227));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-15) + "'", int16.equals((-15)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-622179770) + "'", int19.equals((-622179770)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-2) + "'", int22.equals((-2)));
    }

    @Test
    public void test227() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test227");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-882132), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) (-679114), (java.lang.Integer) 100);
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) 153262720, (java.lang.Integer) 23816997);
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1751438428, (java.lang.Integer) 6450721);
        java.lang.Class<?> wildcardClass30 = operacionesMatematicas0.getClass();
        java.lang.Integer int33 = operacionesMatematicas0.sumar((java.lang.Integer) (-610278), (java.lang.Integer) (-221533));
        java.lang.Class<?> wildcardClass34 = operacionesMatematicas0.getClass();
        java.lang.Integer int37 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-26796), (java.lang.Integer) (-141378));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-1480217496) + "'", int20.equals((-1480217496)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-679014) + "'", int23.equals((-679014)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 6 + "'", int26.equals(6));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 326559708 + "'", int29.equals(326559708));
        org.junit.Assert.assertNotNull(wildcardClass30);
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + (-831811) + "'", int33.equals((-831811)));
        org.junit.Assert.assertNotNull(wildcardClass34);
        org.junit.Assert.assertTrue("'" + int37 + "' != '" + (-506602408) + "'", int37.equals((-506602408)));
    }

    @Test
    public void test228() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test228");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 9992694, (java.lang.Integer) (-1115561068));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 29686803, (java.lang.Integer) 765962910);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-1105568374) + "'", int9.equals((-1105568374)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-736276107) + "'", int12.equals((-736276107)));
    }

    @Test
    public void test229() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test229");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 1079, (java.lang.Integer) 679827);
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-198118), (java.lang.Integer) 674850);
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) 166120, (java.lang.Integer) 200282);
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass25 = operacionesMatematicas0.getClass();
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) (-78), (java.lang.Integer) 1916550);
        java.lang.Class<?> wildcardClass29 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-678748) + "'", int17.equals((-678748)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-872968) + "'", int20.equals((-872968)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-34162) + "'", int23.equals((-34162)));
        org.junit.Assert.assertNotNull(wildcardClass24);
        org.junit.Assert.assertNotNull(wildcardClass25);
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-1916628) + "'", int28.equals((-1916628)));
        org.junit.Assert.assertNotNull(wildcardClass29);
    }

    @Test
    public void test230() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test230");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) (-2452019), (java.lang.Integer) 591647436);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-86408168), (java.lang.Integer) (-1427973632));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1954229806, (java.lang.Integer) 342371593);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-501776), (java.lang.Integer) 1657436165);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-594099455) + "'", int6.equals((-594099455)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-945000448) + "'", int9.equals((-945000448)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 438482334 + "'", int12.equals(438482334));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1656934389 + "'", int15.equals(1656934389));
    }

    @Test
    public void test231() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test231");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 99990, (java.lang.Integer) 298);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-90), (java.lang.Integer) (-139473728));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 38327, (java.lang.Integer) 1761955219);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1418013632, (java.lang.Integer) 1346231);
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) 1937735020, (java.lang.Integer) 2005911435);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 99692 + "'", int9.equals(99692));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-139473818) + "'", int12.equals((-139473818)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1761916892) + "'", int15.equals((-1761916892)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1614297536) + "'", int18.equals((-1614297536)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-68176415) + "'", int21.equals((-68176415)));
    }

    @Test
    public void test232() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test232");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 1963548160, (java.lang.Integer) 100);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-316838208), (java.lang.Integer) 952590536);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 1963548060 + "'", int10.equals(1963548060));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1606845952 + "'", int13.equals(1606845952));
    }

    @Test
    public void test233() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test233");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.sumar((java.lang.Integer) (-2030), (java.lang.Integer) (-674366));
        java.lang.Class<?> wildcardClass9 = operacionesMatematicas0.getClass();
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 961940492, (java.lang.Integer) (-1468950954));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-550455336), (java.lang.Integer) 669335577);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-319237330), (java.lang.Integer) 312147405);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) (-235635084), (java.lang.Integer) 1115055585);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + (-676396) + "'", int8.equals((-676396)));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-549252088) + "'", int12.equals((-549252088)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1219790913) + "'", int15.equals((-1219790913)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-241069610) + "'", int18.equals((-241069610)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 879420501 + "'", int21.equals(879420501));
    }

    @Test
    public void test234() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test234");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) 2030, (java.lang.Integer) 688);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 138125136, (java.lang.Integer) (-453522300));
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 1554526000, (java.lang.Integer) 1025515084);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7.equals(2));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 591647436 + "'", int10.equals(591647436));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 529010916 + "'", int13.equals(529010916));
    }

    @Test
    public void test235() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test235");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 14, (java.lang.Integer) (-3260));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 173387, (java.lang.Integer) (-802065));
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) (-47712228), (java.lang.Integer) 204);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 1868537856);
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) (-1377839691), (java.lang.Integer) (-221508004));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3274 + "'", int12.equals(3274));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 975452 + "'", int15.equals(975452));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-233883) + "'", int18.equals((-233883)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1868537856 + "'", int21.equals(1868537856));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1599347695) + "'", int24.equals((-1599347695)));
    }

    @Test
    public void test236() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test236");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) 14);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.restar((java.lang.Integer) (-1827), (java.lang.Integer) (-694249));
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-872968), (java.lang.Integer) 880885566);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 791131605);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 675040 + "'", int10.equals(675040));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 692422 + "'", int14.equals(692422));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 880012598 + "'", int17.equals(880012598));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 791131605 + "'", int20.equals(791131605));
    }

    @Test
    public void test237() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test237");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 2354, (java.lang.Integer) (-403));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) (-28474));
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 961562281, (java.lang.Integer) 8260879);
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 1863494656, (java.lang.Integer) (-1913886717));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-5) + "'", int12.equals((-5)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-6349702) + "'", int15.equals((-6349702)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 116 + "'", int18.equals(116));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
    }

    @Test
    public void test238() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test238");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 473676, (java.lang.Integer) 222612);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.dividir((java.lang.Integer) 775906878, (java.lang.Integer) 1983457309);
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 696288 + "'", int7.equals(696288));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 0 + "'", int11.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test239() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test239");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) (-589628652), (java.lang.Integer) (-169435336));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 275028550, (java.lang.Integer) 814305812);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 3 + "'", int19.equals(3));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 817658232 + "'", int22.equals(817658232));
    }

    @Test
    public void test240() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test240");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 1678);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) 797);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-136413280));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 166122 + "'", int10.equals(166122));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
    }

    @Test
    public void test241() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test241");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-100), (java.lang.Integer) (-2233));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 675054, (java.lang.Integer) (-201378));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-2099619127), (java.lang.Integer) (-680500222));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 1728039552, (java.lang.Integer) 0);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 2108023789, (java.lang.Integer) (-1345275750));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223300 + "'", int6.equals(223300));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 473676 + "'", int9.equals(473676));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1419118905) + "'", int12.equals((-1419118905)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1728039552 + "'", int15.equals(1728039552));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-841667757) + "'", int18.equals((-841667757)));
    }

    @Test
    public void test242() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test242");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) 14);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 227358, (java.lang.Integer) 223);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 166122, (java.lang.Integer) (-11));
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 200232, (java.lang.Integer) (-181982338));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) (-485640240));
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        java.lang.Integer int27 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1303034316, (java.lang.Integer) 0);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 675040 + "'", int10.equals(675040));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227581 + "'", int13.equals(227581));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 166111 + "'", int16.equals(166111));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-181782106) + "'", int19.equals((-181782106)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-485640240) + "'", int22.equals((-485640240)));
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertNotNull(wildcardClass24);
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 0 + "'", int27.equals(0));
    }

    @Test
    public void test243() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test243");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-2030), (java.lang.Integer) 677084);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 438482334, (java.lang.Integer) 44540629);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-203654655), (java.lang.Integer) 678545);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-141165343), (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-679114) + "'", int16.equals((-679114)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1809913974 + "'", int19.equals(1809913974));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-300) + "'", int22.equals((-300)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
    }

    @Test
    public void test244() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test244");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-2233), (java.lang.Integer) 199);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 227358, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 28000, (java.lang.Integer) 675040);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-11) + "'", int10.equals((-11)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227358 + "'", int13.equals(227358));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 703040 + "'", int16.equals(703040));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test245() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test245");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 1678, (java.lang.Integer) (-304));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 14, (java.lang.Integer) (-676594));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-2030), (java.lang.Integer) 98974848);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-319619085), (java.lang.Integer) 28053);
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) (-791236412), (java.lang.Integer) (-949997523));
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) 396033303, (java.lang.Integer) (-1862095776));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1374 + "'", int9.equals(1374));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1617522543 + "'", int18.equals(1617522543));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 158761111 + "'", int21.equals(158761111));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1466062473) + "'", int24.equals((-1466062473)));
    }

    @Test
    public void test246() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test246");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) (-589628652), (java.lang.Integer) (-169435336));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) 1500655044, (java.lang.Integer) (-1653304684));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 3 + "'", int19.equals(3));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-152649640) + "'", int23.equals((-152649640)));
    }

    @Test
    public void test247() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test247");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 374697400, (java.lang.Integer) 473676);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 44906012);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) (-86631468), (java.lang.Integer) 2);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) (-970067068), (java.lang.Integer) 203354);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 791 + "'", int13.equals(791));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-44906012) + "'", int16.equals((-44906012)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-43315734) + "'", int19.equals((-43315734)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-969863714) + "'", int22.equals((-969863714)));
    }

    @Test
    public void test248() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test248");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) 688);
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) (-202979314), (java.lang.Integer) 2108530673);
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1827), (java.lang.Integer) 808894);
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) (-342147));
        java.lang.Integer int31 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-3162), (java.lang.Integer) 1911263566);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 12 + "'", int13.equals(12));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 222612 + "'", int17.equals(222612));
        org.junit.Assert.assertNotNull(wildcardClass18);
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1983457309 + "'", int21.equals(1983457309));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-1477849338) + "'", int25.equals((-1477849338)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 342147 + "'", int28.equals(342147));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-396410220) + "'", int31.equals((-396410220)));
    }

    @Test
    public void test249() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test249");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-2233), (java.lang.Integer) 199);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 227358, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 28000, (java.lang.Integer) 675040);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 223403, (java.lang.Integer) 30107083);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 302, (java.lang.Integer) (-678748));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) (-30573510), (java.lang.Integer) (-682380));
        java.lang.Integer int28 = operacionesMatematicas0.sumar((java.lang.Integer) (-887497368), (java.lang.Integer) 30107083);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-11) + "'", int10.equals((-11)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227358 + "'", int13.equals(227358));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 703040 + "'", int16.equals(703040));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 44 + "'", int25.equals(44));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-857390285) + "'", int28.equals((-857390285)));
    }

    @Test
    public void test250() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test250");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 679827);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-801458764), (java.lang.Integer) (-764249990));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-679726) + "'", int6.equals((-679726)));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 1 + "'", int10.equals(1));
    }

    @Test
    public void test251() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test251");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 1079, (java.lang.Integer) 679827);
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) 5050, (java.lang.Integer) (-1566625664));
        java.lang.Class<?> wildcardClass21 = operacionesMatematicas0.getClass();
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) 886390221, (java.lang.Integer) (-1794902748));
        java.lang.Class<?> wildcardClass25 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-678748) + "'", int17.equals((-678748)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-129843968) + "'", int20.equals((-129843968)));
        org.junit.Assert.assertNotNull(wildcardClass21);
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1937186348) + "'", int24.equals((-1937186348)));
        org.junit.Assert.assertNotNull(wildcardClass25);
    }

    @Test
    public void test252() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test252");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-204), (java.lang.Integer) 12);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 797, (java.lang.Integer) (-201378));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) (-43315734));
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 1346531, (java.lang.Integer) 203441365);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-17) + "'", int6.equals((-17)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 43315734 + "'", int13.equals(43315734));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 204787896 + "'", int16.equals(204787896));
    }

    @Test
    public void test253() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test253");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) 2030, (java.lang.Integer) 688);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) (-2233));
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.sumar((java.lang.Integer) 298, (java.lang.Integer) 0);
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 44907013, (java.lang.Integer) (-2452019));
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) (-343058591));
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) 107, (java.lang.Integer) 970067068);
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) 166122, (java.lang.Integer) (-98));
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1196436736, (java.lang.Integer) 44898775);
        java.lang.Class<?> wildcardClass30 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 2 + "'", int7.equals(2));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 225533 + "'", int10.equals(225533));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 298 + "'", int14.equals(298));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 47359032 + "'", int17.equals(47359032));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-343058591) + "'", int20.equals((-343058591)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-970066961) + "'", int23.equals((-970066961)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-1695) + "'", int26.equals((-1695)));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 565889792 + "'", int29.equals(565889792));
        org.junit.Assert.assertNotNull(wildcardClass30);
    }

    @Test
    public void test254() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test254");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 1079, (java.lang.Integer) 679827);
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) 3274, (java.lang.Integer) 333);
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) (-203435127), (java.lang.Integer) (-1896184000));
        java.lang.Integer int26 = operacionesMatematicas0.restar((java.lang.Integer) (-608580408), (java.lang.Integer) (-1614297536));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-678748) + "'", int17.equals((-678748)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 2941 + "'", int20.equals(2941));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-2099619127) + "'", int23.equals((-2099619127)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 1005717128 + "'", int26.equals(1005717128));
    }

    @Test
    public void test255() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test255");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 679827);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-86407950), (java.lang.Integer) 419736786);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-485640240), (java.lang.Integer) 677084);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 199, (java.lang.Integer) 569427227);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) (-203658612), (java.lang.Integer) (-358));
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) (-406034920), (java.lang.Integer) 28053);
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) 1963548160, (java.lang.Integer) 2989800);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-679726) + "'", int6.equals((-679726)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-835045696) + "'", int12.equals((-835045696)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-569427028) + "'", int15.equals((-569427028)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 568878 + "'", int18.equals(568878));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-14473) + "'", int21.equals((-14473)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 1966537960 + "'", int24.equals(1966537960));
    }

    @Test
    public void test256() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test256");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-223), (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-4060), (java.lang.Integer) 2030);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 109293, (java.lang.Integer) (-2099619127));
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-1478733724), (java.lang.Integer) 1455561545);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) (-548442062), (java.lang.Integer) 669335577);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223 + "'", int6.equals(223));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2) + "'", int9.equals((-2)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-2099509834) + "'", int13.equals((-2099509834)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1360672027 + "'", int16.equals(1360672027));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
    }

    @Test
    public void test257() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test257");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-320292228), (java.lang.Integer) (-204));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-1198851069), (java.lang.Integer) 1980273266);
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) (-400013783), (java.lang.Integer) (-13));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1570059 + "'", int22.equals(1570059));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 781422197 + "'", int25.equals(781422197));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 30770291 + "'", int28.equals(30770291));
    }

    @Test
    public void test258() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test258");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 10, (java.lang.Integer) 302);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-844), (java.lang.Integer) 675040);
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) (-6451), (java.lang.Integer) (-139629624));
        java.lang.Integer int24 = operacionesMatematicas0.restar((java.lang.Integer) (-674366), (java.lang.Integer) 338956);
        java.lang.Integer int27 = operacionesMatematicas0.multiplicar((java.lang.Integer) 44907013, (java.lang.Integer) 473558740);
        java.lang.Integer int30 = operacionesMatematicas0.restar((java.lang.Integer) (-219080448), (java.lang.Integer) 427138911);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 3020 + "'", int15.equals(3020));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 674196 + "'", int18.equals(674196));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1013322) + "'", int24.equals((-1013322)));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + (-1165905372) + "'", int27.equals((-1165905372)));
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + (-646219359) + "'", int30.equals((-646219359)));
    }

    @Test
    public void test259() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test259");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 925359, (java.lang.Integer) 3451);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-388878032), (java.lang.Integer) (-138799294));
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-403), (java.lang.Integer) 849);
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) 23, (java.lang.Integer) 340488432);
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 268 + "'", int12.equals(268));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-250078738) + "'", int15.equals((-250078738)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-342147) + "'", int18.equals((-342147)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-758700656) + "'", int21.equals((-758700656)));
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test260() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test260");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-91808), (java.lang.Integer) 386);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1581512396, (java.lang.Integer) (-1377181541));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 961940492, (java.lang.Integer) (-177104011));
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-237) + "'", int9.equals((-237)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 113569156 + "'", int12.equals(113569156));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 784836481 + "'", int15.equals(784836481));
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test261() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test261");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 1678);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-28176), (java.lang.Integer) (-507));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass15 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 166122 + "'", int10.equals(166122));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 55 + "'", int13.equals(55));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertNotNull(wildcardClass15);
    }

    @Test
    public void test262() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test262");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 674850, (java.lang.Integer) (-203658532));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 338956, (java.lang.Integer) (-3030));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2037583576, (java.lang.Integer) (-1853597184));
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 298, (java.lang.Integer) (-98975152));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1027036680) + "'", int12.equals((-1027036680)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1922560000 + "'", int15.equals(1922560000));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 570175776 + "'", int18.equals(570175776));
    }

    @Test
    public void test263() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test263");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 202);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) (-2030));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 791, (java.lang.Integer) 677082);
        try {
            java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) (-44568904), (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1 + "'", int15.equals(1));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 677084 + "'", int18.equals(677084));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
    }

    @Test
    public void test264() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test264");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 3233, (java.lang.Integer) (-676594));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-635285), (java.lang.Integer) (-157390628));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1396069979));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 679827 + "'", int9.equals(679827));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
    }

    @Test
    public void test265() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test265");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) (-1));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) (-2233), (java.lang.Integer) 797);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-403), (java.lang.Integer) 1701552590);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 1853597184, (java.lang.Integer) (-1885225800));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 2354, (java.lang.Integer) 311282688);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) (-337227660), (java.lang.Integer) 1089);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 507);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-3030) + "'", int10.equals((-3030)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1701552187 + "'", int13.equals(1701552187));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-31628616) + "'", int16.equals((-31628616)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-311280334) + "'", int19.equals((-311280334)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-337228749) + "'", int22.equals((-337228749)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 507 + "'", int25.equals(507));
    }

    @Test
    public void test266() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test266");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.sumar((java.lang.Integer) 243122930, (java.lang.Integer) (-848979310));
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-11127039), (java.lang.Integer) 1029847456);
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-2233), (java.lang.Integer) 245049039);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + (-605856380) + "'", int14.equals((-605856380)));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 1018720417 + "'", int17.equals(1018720417));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-245051272) + "'", int20.equals((-245051272)));
    }

    @Test
    public void test267() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test267");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 2030);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 666292770, (java.lang.Integer) (-104682583));
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 2010705437, (java.lang.Integer) (-1146));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-2233) + "'", int16.equals((-2233)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1513001358) + "'", int19.equals((-1513001358)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 2010706583 + "'", int22.equals(2010706583));
    }

    @Test
    public void test268() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test268");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166122, (java.lang.Integer) (-2030));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) 703040, (java.lang.Integer) 2005416224);
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 1739462365, (java.lang.Integer) (-853072144));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 568878, (java.lang.Integer) (-23));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1331960192), (java.lang.Integer) (-2233));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 199 + "'", int6.equals(199));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-337227660) + "'", int9.equals((-337227660)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2006119264 + "'", int12.equals(2006119264));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 886390221 + "'", int16.equals(886390221));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 568901 + "'", int19.equals(568901));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-2145227392) + "'", int22.equals((-2145227392)));
    }

    @Test
    public void test269() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test269");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-304));
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 191, (java.lang.Integer) 137901733);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 2010197328, (java.lang.Integer) (-133524468));
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2010360317, (java.lang.Integer) (-674376));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 669356581, (java.lang.Integer) 1252772369);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 569427227 + "'", int13.equals(569427227));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-15) + "'", int16.equals((-15)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2037583576 + "'", int19.equals(2037583576));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1922128950 + "'", int22.equals(1922128950));
    }

    @Test
    public void test270() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test270");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) (-2452019), (java.lang.Integer) 591647436);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-594099455) + "'", int6.equals((-594099455)));
        org.junit.Assert.assertNotNull(wildcardClass7);
    }

    @Test
    public void test271() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test271");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 10, (java.lang.Integer) 302);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-676594), (java.lang.Integer) 298);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) (-2125425896), (java.lang.Integer) (-848979310));
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) (-469373608), (java.lang.Integer) 1002800788);
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) 885349357, (java.lang.Integer) 326559708);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 3020 + "'", int15.equals(3020));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-676296) + "'", int18.equals((-676296)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1320562090 + "'", int21.equals(1320562090));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 0 + "'", int26.equals(0));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 1707858604 + "'", int29.equals(1707858604));
    }

    @Test
    public void test272() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test272");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 166122, (java.lang.Integer) 166122);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 902932, (java.lang.Integer) 668453649);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 222308, (java.lang.Integer) (-872968));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1625055376), (java.lang.Integer) 2024853078);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 669356581 + "'", int9.equals(669356581));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-794241824) + "'", int12.equals((-794241824)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1027846048 + "'", int15.equals(1027846048));
    }

    @Test
    public void test273() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test273");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-203657735), (java.lang.Integer) (-98975152));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1079, (java.lang.Integer) 222612);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 99692, (java.lang.Integer) 203177900);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-203078208), (java.lang.Integer) 1888659260);
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) (-17));
        java.lang.Integer int24 = operacionesMatematicas0.restar((java.lang.Integer) 153721128, (java.lang.Integer) (-71));
        java.lang.Integer int27 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1562037730), (java.lang.Integer) (-563826261));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-104682583) + "'", int9.equals((-104682583)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-221533) + "'", int12.equals((-221533)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-203078208) + "'", int15.equals((-203078208)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-2091737468) + "'", int18.equals((-2091737468)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 17 + "'", int21.equals(17));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 153721199 + "'", int24.equals(153721199));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + (-562612214) + "'", int27.equals((-562612214)));
    }

    @Test
    public void test274() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test274");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) 0);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-98890), (java.lang.Integer) 243124842);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 204, (java.lang.Integer) 789784196);
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2 + "'", int6.equals(2));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-789783992) + "'", int12.equals((-789783992)));
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test275() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test275");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.sumar((java.lang.Integer) (-847810496), (java.lang.Integer) 3464368);
        java.lang.Integer int14 = operacionesMatematicas0.sumar((java.lang.Integer) (-1642733491), (java.lang.Integer) (-86797590));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-844346128) + "'", int11.equals((-844346128)));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + (-1729531081) + "'", int14.equals((-1729531081)));
    }

    @Test
    public void test276() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test276");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2354, (java.lang.Integer) 2679600);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-98990), (java.lang.Integer) (-4060));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-374696609), (java.lang.Integer) 985806408);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) (-6763), (java.lang.Integer) (-203658530));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 24 + "'", int16.equals(24));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1360503017) + "'", int19.equals((-1360503017)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 203651767 + "'", int22.equals(203651767));
    }

    @Test
    public void test277() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test277");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) 688);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) (-1), (java.lang.Integer) (-304));
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) (-678748), (java.lang.Integer) 2989800);
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) (-2003329837), (java.lang.Integer) (-203654745));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 12 + "'", int13.equals(12));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 222612 + "'", int17.equals(222612));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-305) + "'", int20.equals((-305)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 2311052 + "'", int23.equals(2311052));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 2087982714 + "'", int26.equals(2087982714));
    }

    @Test
    public void test278() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test278");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 1678, (java.lang.Integer) (-304));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 14, (java.lang.Integer) (-676594));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-2030), (java.lang.Integer) 98974848);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) (-1203252856));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1374 + "'", int9.equals(1374));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 0 + "'", int12.equals(0));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1203252856 + "'", int18.equals(1203252856));
    }

    @Test
    public void test279() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test279");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 298, (java.lang.Integer) 221269);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) (-480618), (java.lang.Integer) 138145238);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-220971) + "'", int13.equals((-220971)));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-138625856) + "'", int17.equals((-138625856)));
    }

    @Test
    public void test280() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test280");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 202);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) (-2030));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) (-1751447560), (java.lang.Integer) (-9430));
        java.lang.Integer int24 = operacionesMatematicas0.dividir((java.lang.Integer) 223403, (java.lang.Integer) 99);
        java.lang.Class<?> wildcardClass25 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1 + "'", int15.equals(1));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 677084 + "'", int18.equals(677084));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1751438130) + "'", int21.equals((-1751438130)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 2256 + "'", int24.equals(2256));
        org.junit.Assert.assertNotNull(wildcardClass25);
        org.junit.Assert.assertNotNull(wildcardClass26);
    }

    @Test
    public void test281() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test281");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-2030), (java.lang.Integer) 677084);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 134282741, (java.lang.Integer) 218);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-337227660), (java.lang.Integer) 218);
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 218, (java.lang.Integer) 200871);
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Integer int29 = operacionesMatematicas0.sumar((java.lang.Integer) (-1033487401), (java.lang.Integer) (-674424));
        java.lang.Integer int32 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-679114) + "'", int16.equals((-679114)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-791133534) + "'", int19.equals((-791133534)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1546915) + "'", int22.equals((-1546915)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-1034161825) + "'", int29.equals((-1034161825)));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 0 + "'", int32.equals(0));
    }

    @Test
    public void test282() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test282");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass6 = operacionesMatematicas0.getClass();
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 688, (java.lang.Integer) (-924510));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-678074), (java.lang.Integer) (-138799304));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-1910845024), (java.lang.Integer) 44906012);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-139477378) + "'", int12.equals((-139477378)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-42) + "'", int16.equals((-42)));
    }

    @Test
    public void test283() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test283");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 2, (java.lang.Integer) 1023986076);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 113851, (java.lang.Integer) (-906878093));
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-4060), (java.lang.Integer) 311282688);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-906764242) + "'", int21.equals((-906764242)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1087328256) + "'", int24.equals((-1087328256)));
    }

    @Test
    public void test284() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test284");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-56948000), (java.lang.Integer) (-86631468));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-891965756), (java.lang.Integer) 374697400);
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1266663156) + "'", int12.equals((-1266663156)));
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test285() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test285");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-28275), (java.lang.Integer) 200184);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-1896219511), (java.lang.Integer) (-669335781));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 6823529, (java.lang.Integer) (-1243099180));
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 638678568, (java.lang.Integer) (-139629624));
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) (-9351), (java.lang.Integer) 376169062);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1729412004 + "'", int12.equals(1729412004));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1249922709 + "'", int15.equals(1249922709));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 2064582464 + "'", int18.equals(2064582464));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 376159711 + "'", int21.equals(376159711));
    }

    @Test
    public void test286() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test286");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.sumar((java.lang.Integer) (-2030), (java.lang.Integer) (-674366));
        java.lang.Class<?> wildcardClass9 = operacionesMatematicas0.getClass();
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-319239361), (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-895373708), (java.lang.Integer) 990);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 1315788267, (java.lang.Integer) (-1641));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + (-676396) + "'", int8.equals((-676396)));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-319241594) + "'", int12.equals((-319241594)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-904417) + "'", int15.equals((-904417)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1315789908 + "'", int18.equals(1315789908));
    }

    @Test
    public void test287() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test287");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.dividir((java.lang.Integer) 198, (java.lang.Integer) 2989800);
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) 701863, (java.lang.Integer) (-203658530));
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) (-222612), (java.lang.Integer) 791133635);
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-319241688), (java.lang.Integer) (-169435345));
        java.lang.Integer int29 = operacionesMatematicas0.sumar((java.lang.Integer) 100004, (java.lang.Integer) (-902763319));
        java.lang.Integer int32 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-135279661));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 419736786 + "'", int20.equals(419736786));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-791356247) + "'", int23.equals((-791356247)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-1171096232) + "'", int26.equals((-1171096232)));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-902663315) + "'", int29.equals((-902663315)));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 0 + "'", int32.equals(0));
    }

    @Test
    public void test288() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test288");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) (-589628652), (java.lang.Integer) (-169435336));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) (-28676780), (java.lang.Integer) 179683468);
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) 2024308532, (java.lang.Integer) 1040387076);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 3 + "'", int19.equals(3));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 151006688 + "'", int22.equals(151006688));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 983921456 + "'", int25.equals(983921456));
    }

    @Test
    public void test289() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test289");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1913));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-300960), (java.lang.Integer) (-17));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1751447560), (java.lang.Integer) 203544205);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-904417), (java.lang.Integer) (-454172969));
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-377), (java.lang.Integer) 86235969);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 117 + "'", int9.equals(117));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-300943) + "'", int12.equals((-300943)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1073433704) + "'", int15.equals((-1073433704)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-328150775) + "'", int18.equals((-328150775)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1848778055 + "'", int21.equals(1848778055));
    }

    @Test
    public void test290() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test290");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-98975152), (java.lang.Integer) (-300960));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 200871, (java.lang.Integer) (-702061));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203177900, (java.lang.Integer) 6295);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 1919324571, (java.lang.Integer) 1106533862);
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) (-6091349), (java.lang.Integer) (-676396));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1963548160 + "'", int9.equals(1963548160));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 902932 + "'", int12.equals(902932));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-895373708) + "'", int15.equals((-895373708)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1 + "'", int18.equals(1));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 9 + "'", int21.equals(9));
    }

    @Test
    public void test291() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test291");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 1023986076, (java.lang.Integer) 28000);
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-840498), (java.lang.Integer) 1702393088);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-203678148), (java.lang.Integer) (-1815850602));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1002), (java.lang.Integer) (-6435));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 1024014076 + "'", int7.equals(1024014076));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 1701552590 + "'", int10.equals(1701552590));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 578466600 + "'", int13.equals(578466600));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 6447870 + "'", int16.equals(6447870));
    }

    @Test
    public void test292() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test292");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 223403, (java.lang.Integer) (-203658530));
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203658830, (java.lang.Integer) 1351444);
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) 1702393088, (java.lang.Integer) (-358));
        java.lang.Class<?> wildcardClass27 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass28 = operacionesMatematicas0.getClass();
        java.lang.Integer int31 = operacionesMatematicas0.restar((java.lang.Integer) 1554530409, (java.lang.Integer) (-1460280832));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-203435127) + "'", int20.equals((-203435127)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-885379048) + "'", int23.equals((-885379048)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-4755287) + "'", int26.equals((-4755287)));
        org.junit.Assert.assertNotNull(wildcardClass27);
        org.junit.Assert.assertNotNull(wildcardClass28);
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-1280156055) + "'", int31.equals((-1280156055)));
    }

    @Test
    public void test293() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test293");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1829857832, (java.lang.Integer) 808864);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-220530924), (java.lang.Integer) 29684000);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2262 + "'", int13.equals(2262));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-1119308160) + "'", int16.equals((-1119308160)));
    }

    @Test
    public void test294() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test294");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) (-1));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 696288, (java.lang.Integer) (-6763));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) 820922168);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-1681687416), (java.lang.Integer) 790457339);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 4051228, (java.lang.Integer) 191);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-1203252856), (java.lang.Integer) 589628875);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 703051 + "'", int10.equals(703051));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-2) + "'", int16.equals((-2)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 21210 + "'", int19.equals(21210));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-2) + "'", int22.equals((-2)));
    }

    @Test
    public void test295() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test295");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-203658532), (java.lang.Integer) 797);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 138125136, (java.lang.Integer) (-198));
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) 3021, (java.lang.Integer) (-607257));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) (-872882432), (java.lang.Integer) 1954229806);
        java.lang.Integer int31 = operacionesMatematicas0.dividir((java.lang.Integer) (-607257), (java.lang.Integer) (-203));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-203657735) + "'", int18.equals((-203657735)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 138124938 + "'", int21.equals(138124938));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 610278 + "'", int25.equals(610278));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 2991 + "'", int31.equals(2991));
    }

    @Test
    public void test296() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test296");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-304), (java.lang.Integer) 12);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 902932, (java.lang.Integer) 199);
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-2), (java.lang.Integer) 168);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-292) + "'", int7.equals((-292)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 179683468 + "'", int10.equals(179683468));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 166 + "'", int13.equals(166));
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test297() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test297");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 1678);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 242847, (java.lang.Integer) 674850);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1133064081, (java.lang.Integer) 86235969);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 166122 + "'", int10.equals(166122));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-682025263) + "'", int17.equals((-682025263)));
    }

    @Test
    public void test298() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test298");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 3884, (java.lang.Integer) (-1554526525));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1000 + "'", int9.equals(1000));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1554530409 + "'", int13.equals(1554530409));
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test299() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test299");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) (-1));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 2030, (java.lang.Integer) 1);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-28676780), (java.lang.Integer) (-1226993776));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2029 + "'", int10.equals(2029));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test300() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test300");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-204), (java.lang.Integer) 12);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 797, (java.lang.Integer) (-201378));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1199317410), (java.lang.Integer) 203344022);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1566625664), (java.lang.Integer) (-673460));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-17) + "'", int6.equals((-17)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1402661432) + "'", int12.equals((-1402661432)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1003415040 + "'", int15.equals(1003415040));
    }

    @Test
    public void test301() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test301");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2, (java.lang.Integer) 1000);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-907044204), (java.lang.Integer) 166111);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-480618), (java.lang.Integer) (-674366));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 834595461, (java.lang.Integer) 1463316832);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2000 + "'", int6.equals(2000));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-906878093) + "'", int9.equals((-906878093)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1989890988 + "'", int12.equals(1989890988));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
    }

    @Test
    public void test302() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test302");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-203657735), (java.lang.Integer) (-98975152));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-872882432), (java.lang.Integer) 1374);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-203), (java.lang.Integer) 3233);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-453522300), (java.lang.Integer) 2108475396);
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) 186, (java.lang.Integer) 1346166);
        java.lang.Integer int24 = operacionesMatematicas0.dividir((java.lang.Integer) 388415536, (java.lang.Integer) 847782221);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-104682583) + "'", int9.equals((-104682583)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-635285) + "'", int12.equals((-635285)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1654953096 + "'", int18.equals(1654953096));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 0 + "'", int24.equals(0));
    }

    @Test
    public void test303() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test303");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-223), (java.lang.Integer) (-1429679577));
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 134282741, (java.lang.Integer) 102968);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2108502328, (java.lang.Integer) (-1508638078));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1429679800) + "'", int13.equals((-1429679800)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1304 + "'", int16.equals(1304));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 599864250 + "'", int19.equals(599864250));
    }

    @Test
    public void test304() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test304");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 374697400, (java.lang.Integer) 473676);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 44906012);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-65), (java.lang.Integer) 99);
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 704982981, (java.lang.Integer) (-2147357664));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 791 + "'", int13.equals(791));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-44906012) + "'", int16.equals((-44906012)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-6435) + "'", int19.equals((-6435)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1200319328) + "'", int22.equals((-1200319328)));
    }

    @Test
    public void test305() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test305");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-98975152), (java.lang.Integer) (-300960));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 204, (java.lang.Integer) 0);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-5), (java.lang.Integer) 0);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-1477849338), (java.lang.Integer) (-1588443426));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1963548160 + "'", int9.equals(1963548160));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 204 + "'", int12.equals(204));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-5) + "'", int15.equals((-5)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 110594088 + "'", int18.equals(110594088));
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test306() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test306");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 298, (java.lang.Integer) (-203658532));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) 28053);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-945000448), (java.lang.Integer) (-1077114894));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 203658830 + "'", int7.equals(203658830));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-267264000) + "'", int13.equals((-267264000)));
    }

    @Test
    public void test307() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test307");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 1079, (java.lang.Integer) 679827);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 2311052, (java.lang.Integer) (-317731454));
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) (-3353), (java.lang.Integer) (-1059362566));
        java.lang.Integer int26 = operacionesMatematicas0.restar((java.lang.Integer) (-43316026), (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-678748) + "'", int17.equals((-678748)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-315420402) + "'", int20.equals((-315420402)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 1059359213 + "'", int23.equals(1059359213));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-43316026) + "'", int26.equals((-43316026)));
    }

    @Test
    public void test308() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test308");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 1996158842, (java.lang.Integer) 790454420);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 791, (java.lang.Integer) (-1566668296));
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 569414523, (java.lang.Integer) 0);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) (-1045), (java.lang.Integer) 1079);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1508354034) + "'", int13.equals((-1508354034)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 2010926408 + "'", int16.equals(2010926408));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 34 + "'", int22.equals(34));
    }

    @Test
    public void test309() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test309");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-2233));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-676396), (java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 24, (java.lang.Integer) 677084);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) 419736786);
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-674366) + "'", int9.equals((-674366)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-6763) + "'", int12.equals((-6763)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test310() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test310");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass4 = operacionesMatematicas0.getClass();
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) (-204), (java.lang.Integer) (-676296));
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-203656852), (java.lang.Integer) (-110265462));
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass4);
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-313922314) + "'", int10.equals((-313922314)));
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test311() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test311");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-882132), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) (-679114), (java.lang.Integer) 100);
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) 153262720, (java.lang.Integer) 23816997);
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1751438428, (java.lang.Integer) 6450721);
        java.lang.Class<?> wildcardClass30 = operacionesMatematicas0.getClass();
        java.lang.Integer int33 = operacionesMatematicas0.sumar((java.lang.Integer) (-610278), (java.lang.Integer) (-221533));
        java.lang.Integer int36 = operacionesMatematicas0.sumar((java.lang.Integer) (-138799304), (java.lang.Integer) (-56947941));
        java.lang.Integer int39 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) 682478);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-1480217496) + "'", int20.equals((-1480217496)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-679014) + "'", int23.equals((-679014)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 6 + "'", int26.equals(6));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 326559708 + "'", int29.equals(326559708));
        org.junit.Assert.assertNotNull(wildcardClass30);
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + (-831811) + "'", int33.equals((-831811)));
        org.junit.Assert.assertTrue("'" + int36 + "' != '" + (-195747245) + "'", int36.equals((-195747245)));
        org.junit.Assert.assertTrue("'" + int39 + "' != '" + 206108356 + "'", int39.equals(206108356));
    }

    @Test
    public void test312() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test312");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 703051, (java.lang.Integer) 151554579);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-550455336), (java.lang.Integer) 1939697086);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 996650801, (java.lang.Integer) (-319241832));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-150851528) + "'", int9.equals((-150851528)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1804814874 + "'", int12.equals(1804814874));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-3) + "'", int15.equals((-3)));
    }

    @Test
    public void test313() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test313");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-2010186930), (java.lang.Integer) 1954931645);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1) + "'", int15.equals((-1)));
    }

    @Test
    public void test314() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test314");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) 223300);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-676396), (java.lang.Integer) 1678);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-203654745), (java.lang.Integer) (-365383));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) (-9), (java.lang.Integer) (-169435336));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-164003), (java.lang.Integer) (-824464727));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2030) + "'", int9.equals((-2030)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2679600 + "'", int13.equals(2679600));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-678074) + "'", int16.equals((-678074)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-203289362) + "'", int19.equals((-203289362)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-169435345) + "'", int22.equals((-169435345)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-824628730) + "'", int25.equals((-824628730)));
    }

    @Test
    public void test315() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test315");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-237), (java.lang.Integer) 101);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 3274, (java.lang.Integer) (-1440498544));
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) (-1017), (java.lang.Integer) (-1815850602));
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) (-1554513821), (java.lang.Integer) (-150851528));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-23937) + "'", int16.equals((-23937)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1815849585 + "'", int22.equals(1815849585));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-1403662293) + "'", int25.equals((-1403662293)));
    }

    @Test
    public void test316() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test316");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) 223300);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-676396), (java.lang.Integer) 1678);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) 99990);
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1593177030, (java.lang.Integer) (-1259193738));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) (-1617874884), (java.lang.Integer) (-553));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2030) + "'", int9.equals((-2030)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2679600 + "'", int13.equals(2679600));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-678074) + "'", int16.equals((-678074)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-98990) + "'", int19.equals((-98990)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 667236676 + "'", int22.equals(667236676));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 2925632 + "'", int25.equals(2925632));
    }

    @Test
    public void test317() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test317");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-164003), (java.lang.Integer) (-45556681));
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 10376, (java.lang.Integer) 1475844812);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 1968086948, (java.lang.Integer) 1500655044);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 200183, (java.lang.Integer) 817658232);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-45720684) + "'", int13.equals((-45720684)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-1475834436) + "'", int16.equals((-1475834436)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-826225304) + "'", int19.equals((-826225304)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
    }

    @Test
    public void test318() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test318");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.dividir((java.lang.Integer) (-689499353), (java.lang.Integer) (-1566625664));
        java.lang.Integer int11 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1718578311), (java.lang.Integer) (-742442848));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 0 + "'", int8.equals(0));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 1558553504 + "'", int11.equals(1558553504));
    }

    @Test
    public void test319() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test319");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-882132), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 676495, (java.lang.Integer) 2030);
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass25 = operacionesMatematicas0.getClass();
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) 1702393088, (java.lang.Integer) 299478398);
        java.lang.Integer int31 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1711856, (java.lang.Integer) (-926090540));
        java.lang.Integer int34 = operacionesMatematicas0.restar((java.lang.Integer) 1592512321, (java.lang.Integer) 1558553504);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-1480217496) + "'", int20.equals((-1480217496)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 333 + "'", int23.equals(333));
        org.junit.Assert.assertNotNull(wildcardClass24);
        org.junit.Assert.assertNotNull(wildcardClass25);
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 1402914690 + "'", int28.equals(1402914690));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-1088946496) + "'", int31.equals((-1088946496)));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 33958817 + "'", int34.equals(33958817));
    }

    @Test
    public void test320() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test320");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1419118905), (java.lang.Integer) (-1243099180));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) (-801458764), (java.lang.Integer) 1448123273);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 47990220 + "'", int12.equals(47990220));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 646664509 + "'", int16.equals(646664509));
        org.junit.Assert.assertNotNull(wildcardClass17);
    }

    @Test
    public void test321() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test321");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 1678);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-204), (java.lang.Integer) (-669335781));
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 1738534581, (java.lang.Integer) 23420638);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) (-985707399), (java.lang.Integer) (-949939300));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 166122 + "'", int10.equals(166122));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 669335577 + "'", int13.equals(669335577));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1761955219 + "'", int16.equals(1761955219));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-1935646699) + "'", int20.equals((-1935646699)));
    }

    @Test
    public void test322() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test322");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 473676, (java.lang.Integer) 222612);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.sumar((java.lang.Integer) 1193221951, (java.lang.Integer) (-1963548160));
        java.lang.Integer int14 = operacionesMatematicas0.multiplicar((java.lang.Integer) 675054, (java.lang.Integer) (-705717816));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 696288 + "'", int7.equals(696288));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-770326209) + "'", int11.equals((-770326209)));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 137910256 + "'", int14.equals(137910256));
    }

    @Test
    public void test323() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test323");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-1085863536), (java.lang.Integer) (-391));
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-203761580), (java.lang.Integer) (-798007));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-895373708), (java.lang.Integer) 1963548060);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 1002800788, (java.lang.Integer) (-2125259776));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-1085863927) + "'", int7.equals((-1085863927)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-204559587) + "'", int10.equals((-204559587)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-1122458988) + "'", int16.equals((-1122458988)));
    }

    @Test
    public void test324() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test324");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 676495, (java.lang.Integer) (-9));
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) 2679600, (java.lang.Integer) 204);
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-680500222), (java.lang.Integer) (-853072144));
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        java.lang.Integer int27 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-309626391), (java.lang.Integer) (-698473020));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 676504 + "'", int17.equals(676504));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 13135 + "'", int20.equals(13135));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 2122583520 + "'", int23.equals(2122583520));
        org.junit.Assert.assertNotNull(wildcardClass24);
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + (-1862925468) + "'", int27.equals((-1862925468)));
    }

    @Test
    public void test325() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test325");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) (-1));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 2030, (java.lang.Integer) 1);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 696288, (java.lang.Integer) (-98890));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 2029 + "'", int10.equals(2029));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-136443584) + "'", int13.equals((-136443584)));
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test326() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test326");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 1963548160, (java.lang.Integer) 100);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-1751438130), (java.lang.Integer) 2010926408);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-969863714), (java.lang.Integer) (-2075461892));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 1963548060 + "'", int10.equals(1963548060));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 532602758 + "'", int13.equals(532602758));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
    }

    @Test
    public void test327() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test327");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 101);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) (-674366), (java.lang.Integer) 3020);
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 200183);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-679114), (java.lang.Integer) (-172199));
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 7, (java.lang.Integer) 222308);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 791, (java.lang.Integer) 198586);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 252, (java.lang.Integer) (-1377181541));
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-870834822), (java.lang.Integer) 0);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 101 + "'", int4.equals(101));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-223) + "'", int7.equals((-223)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 200184 + "'", int10.equals(200184));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-506915) + "'", int13.equals((-506915)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-197795) + "'", int19.equals((-197795)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1377181793 + "'", int22.equals(1377181793));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
    }

    @Test
    public void test328() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test328");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) 223300);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-676396), (java.lang.Integer) 1678);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) (-485640239), (java.lang.Integer) 438482334);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2030) + "'", int9.equals((-2030)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2679600 + "'", int13.equals(2679600));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-678074) + "'", int16.equals((-678074)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-47157905) + "'", int20.equals((-47157905)));
    }

    @Test
    public void test329() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test329");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) 223300);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-676396), (java.lang.Integer) 1678);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) 99990);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 1554526000, (java.lang.Integer) 1214037568);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-608580408), (java.lang.Integer) (-505887068));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) 952590536, (java.lang.Integer) 1868537856);
        java.lang.Integer int31 = operacionesMatematicas0.dividir((java.lang.Integer) 371015616, (java.lang.Integer) 11972266);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2030) + "'", int9.equals((-2030)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2679600 + "'", int13.equals(2679600));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-678074) + "'", int16.equals((-678074)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-98990) + "'", int19.equals((-98990)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 340488432 + "'", int22.equals(340488432));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-1114467476) + "'", int25.equals((-1114467476)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 30 + "'", int31.equals(30));
    }

    @Test
    public void test330() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test330");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) 1853597184);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-1796353182), (java.lang.Integer) 12);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2233), (java.lang.Integer) (-209727));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 0 + "'", int4.equals(0));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-1796353170) + "'", int7.equals((-1796353170)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 468320391 + "'", int10.equals(468320391));
    }

    @Test
    public void test331() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test331");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass9 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test332() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test332");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-203657735), (java.lang.Integer) (-98975152));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-872882432), (java.lang.Integer) 1374);
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 9727, (java.lang.Integer) 523299800);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-104682583) + "'", int9.equals((-104682583)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-635285) + "'", int12.equals((-635285)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 600908840 + "'", int16.equals(600908840));
    }

    @Test
    public void test333() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test333");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) 688);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) (-2031), (java.lang.Integer) 223300);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 2337, (java.lang.Integer) (-202979465));
        java.lang.Integer int26 = operacionesMatematicas0.restar((java.lang.Integer) (-868904743), (java.lang.Integer) 101);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 12 + "'", int13.equals(12));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 222612 + "'", int17.equals(222612));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 221269 + "'", int20.equals(221269));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 0 + "'", int23.equals(0));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-868904844) + "'", int26.equals((-868904844)));
    }

    @Test
    public void test334() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test334");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 138125136, (java.lang.Integer) 223403);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.restar((java.lang.Integer) (-391), (java.lang.Integer) 847782221);
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 1341, (java.lang.Integer) 181674816);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 137901733 + "'", int7.equals(137901733));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-847782612) + "'", int11.equals((-847782612)));
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-181673475) + "'", int15.equals((-181673475)));
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test335() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test335");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 101);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass6 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 101 + "'", int4.equals(101));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
    }

    @Test
    public void test336() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test336");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 199);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 98974848, (java.lang.Integer) 1729412004);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-50702028), (java.lang.Integer) 1161756880);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 150376532, (java.lang.Integer) 1055470776);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 298 + "'", int9.equals(298));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1205847308 + "'", int19.equals(1205847308));
    }

    @Test
    public void test337() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test337");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-2031), (java.lang.Integer) (-100));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-679014), (java.lang.Integer) 28345);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 2108473854);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-86797590), (java.lang.Integer) (-985930295));
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) (-203678148), (java.lang.Integer) 168);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-1566890908), (java.lang.Integer) (-181982338));
        java.lang.Integer int28 = operacionesMatematicas0.sumar((java.lang.Integer) (-1033577087), (java.lang.Integer) 318557266);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-2131) + "'", int10.equals((-2131)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-650669) + "'", int13.equals((-650669)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-2108473854) + "'", int16.equals((-2108473854)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1072727885) + "'", int19.equals((-1072727885)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-203678316) + "'", int22.equals((-203678316)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-1748873246) + "'", int25.equals((-1748873246)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-715019821) + "'", int28.equals((-715019821)));
    }

    @Test
    public void test338() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test338");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-2233), (java.lang.Integer) 199);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 227358, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 28000, (java.lang.Integer) 675040);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-134082459), (java.lang.Integer) 6731);
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1729412004, (java.lang.Integer) (-23417874));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-888179748), (java.lang.Integer) (-1243099180));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) (-203289362), (java.lang.Integer) (-1420242393));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-11) + "'", int10.equals((-11)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227358 + "'", int13.equals(227358));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 703040 + "'", int16.equals(703040));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-565899369) + "'", int19.equals((-565899369)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1195703928 + "'", int22.equals(1195703928));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-2131278928) + "'", int25.equals((-2131278928)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
    }

    @Test
    public void test339() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test339");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) (-304), (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 2941, (java.lang.Integer) (-673460));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-507) + "'", int6.equals((-507)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 676401 + "'", int9.equals(676401));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test340() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test340");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-98975152), (java.lang.Integer) (-300960));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 204, (java.lang.Integer) 0);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 849, (java.lang.Integer) 55);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-9351), (java.lang.Integer) 1228204032);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1963548160 + "'", int9.equals(1963548160));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 204 + "'", int12.equals(204));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 794 + "'", int15.equals(794));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1228213383) + "'", int18.equals((-1228213383)));
    }

    @Test
    public void test341() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test341");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-304), (java.lang.Integer) 12);
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-136543588), (java.lang.Integer) 811017156);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-292) + "'", int7.equals((-292)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 674473568 + "'", int10.equals(674473568));
    }

    @Test
    public void test342() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test342");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.dividir((java.lang.Integer) 675054, (java.lang.Integer) (-674366));
        java.lang.Integer int11 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1349700, (java.lang.Integer) (-136443584));
        java.lang.Integer int14 = operacionesMatematicas0.restar((java.lang.Integer) 1888659260, (java.lang.Integer) (-1480217496));
        java.lang.Class<?> wildcardClass15 = operacionesMatematicas0.getClass();
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-1085863536), (java.lang.Integer) 431);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + (-1) + "'", int8.equals((-1)));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 1702393088 + "'", int11.equals(1702393088));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + (-926090540) + "'", int14.equals((-926090540)));
        org.junit.Assert.assertNotNull(wildcardClass15);
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1085863105) + "'", int18.equals((-1085863105)));
    }

    @Test
    public void test343() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test343");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-26796), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) (-6763), (java.lang.Integer) (-4060));
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) (-28474), (java.lang.Integer) 199);
        java.lang.Integer int29 = operacionesMatematicas0.sumar((java.lang.Integer) (-319237330), (java.lang.Integer) (-86797590));
        java.lang.Integer int32 = operacionesMatematicas0.restar((java.lang.Integer) 173387, (java.lang.Integer) 2010360317);
        java.lang.Integer int35 = operacionesMatematicas0.multiplicar((java.lang.Integer) 764151000, (java.lang.Integer) 0);
        java.lang.Integer int38 = operacionesMatematicas0.restar((java.lang.Integer) (-153263374), (java.lang.Integer) 1765127922);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-28474) + "'", int20.equals((-28474)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 1 + "'", int23.equals(1));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-28275) + "'", int26.equals((-28275)));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-406034920) + "'", int29.equals((-406034920)));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + (-2010186930) + "'", int32.equals((-2010186930)));
        org.junit.Assert.assertTrue("'" + int35 + "' != '" + 0 + "'", int35.equals(0));
        org.junit.Assert.assertTrue("'" + int38 + "' != '" + (-1918391296) + "'", int38.equals((-1918391296)));
    }

    @Test
    public void test344() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test344");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-223), (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-4060), (java.lang.Integer) 2030);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.restar((java.lang.Integer) (-202981340), (java.lang.Integer) 1567906720);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223 + "'", int6.equals(223));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2) + "'", int9.equals((-2)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + (-1770888060) + "'", int14.equals((-1770888060)));
    }

    @Test
    public void test345() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test345");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) (-872882432), (java.lang.Integer) (-6763));
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1641), (java.lang.Integer) 200184);
        java.lang.Integer int26 = operacionesMatematicas0.restar((java.lang.Integer) (-674366), (java.lang.Integer) 989568000);
        java.lang.Integer int29 = operacionesMatematicas0.dividir((java.lang.Integer) (-342147), (java.lang.Integer) 9812853);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 129067 + "'", int20.equals(129067));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-328501944) + "'", int23.equals((-328501944)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-990242366) + "'", int26.equals((-990242366)));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 0 + "'", int29.equals(0));
    }

    @Test
    public void test346() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test346");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 99990, (java.lang.Integer) 298);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-90), (java.lang.Integer) (-139473728));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 38327, (java.lang.Integer) 1761955219);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1418013632, (java.lang.Integer) 1346231);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) (-6091349), (java.lang.Integer) 1623565852);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 99692 + "'", int9.equals(99692));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-139473818) + "'", int12.equals((-139473818)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1761916892) + "'", int15.equals((-1761916892)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1614297536) + "'", int18.equals((-1614297536)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1617474503 + "'", int21.equals(1617474503));
    }

    @Test
    public void test347() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test347");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166122, (java.lang.Integer) (-2030));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-203658532), (java.lang.Integer) 1566625664);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 55, (java.lang.Integer) (-684291464));
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-61), (java.lang.Integer) (-818242));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 9120400, (java.lang.Integer) (-328501944));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 199 + "'", int6.equals(199));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-337227660) + "'", int9.equals((-337227660)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1362967132 + "'", int13.equals(1362967132));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 684291519 + "'", int16.equals(684291519));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 49912762 + "'", int19.equals(49912762));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
    }

    @Test
    public void test348() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test348");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1089, (java.lang.Integer) (-1027036680));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 23816997, (java.lang.Integer) 222308);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166111, (java.lang.Integer) 1023985958);
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-548575792));
        java.lang.Integer int31 = operacionesMatematicas0.dividir((java.lang.Integer) 312147405, (java.lang.Integer) (-1336958784));
        java.lang.Integer int34 = operacionesMatematicas0.sumar((java.lang.Integer) (-2015005451), (java.lang.Integer) (-57810812));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1751447560) + "'", int19.equals((-1751447560)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-985706892) + "'", int22.equals((-985706892)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1741645850 + "'", int25.equals(1741645850));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 0 + "'", int31.equals(0));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + (-2072816263) + "'", int34.equals((-2072816263)));
    }

    @Test
    public void test349() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test349");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-2233), (java.lang.Integer) 199);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 227358, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 28000, (java.lang.Integer) 675040);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 223403, (java.lang.Integer) 30107083);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 302, (java.lang.Integer) (-678748));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) (-30573510), (java.lang.Integer) (-682380));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) 852483217, (java.lang.Integer) 927523);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-11) + "'", int10.equals((-11)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227358 + "'", int13.equals(227358));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 703040 + "'", int16.equals(703040));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 44 + "'", int25.equals(44));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 919 + "'", int28.equals(919));
    }

    @Test
    public void test350() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test350");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-204), (java.lang.Integer) 12);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1546915), (java.lang.Integer) 38790450);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-134082459), (java.lang.Integer) (-1396069979));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-98965), (java.lang.Integer) (-6198));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-17) + "'", int6.equals((-17)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-40337365) + "'", int9.equals((-40337365)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1530152438) + "'", int12.equals((-1530152438)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-92767) + "'", int15.equals((-92767)));
    }

    @Test
    public void test351() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test351");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 473676, (java.lang.Integer) (-2030));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-292), (java.lang.Integer) 2108502328);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-86797590), (java.lang.Integer) 674850);
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 135279661, (java.lang.Integer) 199);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) 134282741);
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) (-1937186348), (java.lang.Integer) 2137471735);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-961562280) + "'", int12.equals((-961562280)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-589628652) + "'", int19.equals((-589628652)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1150848763 + "'", int22.equals(1150848763));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 134284771 + "'", int25.equals(134284771));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
    }

    @Test
    public void test352() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test352");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-203657735), (java.lang.Integer) (-98975152));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-872882432), (java.lang.Integer) 1374);
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) (-202978621), (java.lang.Integer) (-747018902));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-9), (java.lang.Integer) 854220061);
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2037899052, (java.lang.Integer) 431);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-104682583) + "'", int9.equals((-104682583)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-635285) + "'", int12.equals((-635285)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-949997523) + "'", int16.equals((-949997523)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-854220070) + "'", int19.equals((-854220070)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-2133804268) + "'", int22.equals((-2133804268)));
    }

    @Test
    public void test353() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test353");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 199);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-3), (java.lang.Integer) 43818352);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-836688649));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 298 + "'", int9.equals(298));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
    }

    @Test
    public void test354() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test354");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 674850, (java.lang.Integer) (-203658532));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 338956, (java.lang.Integer) (-3030));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2233), (java.lang.Integer) (-1027036680));
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-139629624), (java.lang.Integer) 164521512);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) (-4), (java.lang.Integer) 10);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1027036680) + "'", int12.equals((-1027036680)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-139629624) + "'", int15.equals((-139629624)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1896184000) + "'", int18.equals((-1896184000)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 6 + "'", int21.equals(6));
    }

    @Test
    public void test355() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test355");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 10, (java.lang.Integer) 302);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-676594), (java.lang.Integer) 298);
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 199, (java.lang.Integer) (-1180735552));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) (-680500910), (java.lang.Integer) 967832161);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 3020 + "'", int15.equals(3020));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-676296) + "'", int18.equals((-676296)));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1256826432 + "'", int22.equals(1256826432));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
    }

    @Test
    public void test356() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test356");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 1678);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 242847, (java.lang.Integer) 674850);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 222612, (java.lang.Integer) 129067);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 1673185024, (java.lang.Integer) (-1072091024));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 166122 + "'", int10.equals(166122));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 93545 + "'", int16.equals(93545));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 601094000 + "'", int19.equals(601094000));
    }

    @Test
    public void test357() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test357");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) 688);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) (-1), (java.lang.Integer) (-304));
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) (-202806058), (java.lang.Integer) (-1126066292));
        java.lang.Integer int26 = operacionesMatematicas0.restar((java.lang.Integer) 426796764, (java.lang.Integer) (-342147));
        java.lang.Class<?> wildcardClass27 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 12 + "'", int13.equals(12));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 222612 + "'", int17.equals(222612));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-305) + "'", int20.equals((-305)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 923260234 + "'", int23.equals(923260234));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 427138911 + "'", int26.equals(427138911));
        org.junit.Assert.assertNotNull(wildcardClass27);
    }

    @Test
    public void test358() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test358");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99990, (java.lang.Integer) 14);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 218, (java.lang.Integer) 203658830);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 34527, (java.lang.Integer) (-198));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) (-2027625964), (java.lang.Integer) (-680500910));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-1034161825), (java.lang.Integer) 1072089982);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 100004 + "'", int9.equals(100004));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-26796) + "'", int12.equals((-26796)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-203658612) + "'", int15.equals((-203658612)));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 34329 + "'", int19.equals(34329));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1586840422 + "'", int22.equals(1586840422));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 37928157 + "'", int25.equals(37928157));
    }

    @Test
    public void test359() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test359");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 677082, (java.lang.Integer) 797);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-86797590), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) 990, (java.lang.Integer) 203177900);
        java.lang.Integer int32 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-747018902), (java.lang.Integer) 1358052);
        java.lang.Class<?> wildcardClass33 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass34 = operacionesMatematicas0.getClass();
        java.lang.Integer int37 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2108635446, (java.lang.Integer) (-841667757));
        java.lang.Integer int40 = operacionesMatematicas0.sumar((java.lang.Integer) 688, (java.lang.Integer) 676401);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 849 + "'", int22.equals(849));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-717341912) + "'", int29.equals((-717341912)));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + (-2058714520) + "'", int32.equals((-2058714520)));
        org.junit.Assert.assertNotNull(wildcardClass33);
        org.junit.Assert.assertNotNull(wildcardClass34);
        org.junit.Assert.assertTrue("'" + int37 + "' != '" + 106536578 + "'", int37.equals(106536578));
        org.junit.Assert.assertTrue("'" + int40 + "' != '" + 677089 + "'", int40.equals(677089));
    }

    @Test
    public void test360() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test360");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2, (java.lang.Integer) (-1));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) 338956);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) (-454172969), (java.lang.Integer) 1355331012);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) (-7812), (java.lang.Integer) (-747185024));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-2) + "'", int13.equals((-2)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1023986076 + "'", int16.equals(1023986076));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 0 + "'", int20.equals(0));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 0 + "'", int23.equals(0));
    }

    @Test
    public void test361() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test361");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166122, (java.lang.Integer) (-2030));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 202, (java.lang.Integer) (-1546915));
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 137901733, (java.lang.Integer) 1199317608);
        try {
            java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 996920, (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 199 + "'", int6.equals(199));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-337227660) + "'", int9.equals((-337227660)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
    }

    @Test
    public void test362() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test362");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-98975152), (java.lang.Integer) (-676594));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 117, (java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 146 + "'", int12.equals(146));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 118 + "'", int15.equals(118));
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test363() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test363");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1913));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-28275), (java.lang.Integer) (-98975152));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 688, (java.lang.Integer) 2029);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 1476845172, (java.lang.Integer) (-682176));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) (-688877022), (java.lang.Integer) (-288288441));
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) (-1643596625), (java.lang.Integer) 1411260476);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 117 + "'", int9.equals(117));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1796254192) + "'", int12.equals((-1796254192)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-2164) + "'", int18.equals((-2164)));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-400588581) + "'", int22.equals((-400588581)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1240110195 + "'", int25.equals(1240110195));
    }

    @Test
    public void test364() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test364");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 1352162704);
        java.lang.Integer int14 = operacionesMatematicas0.sumar((java.lang.Integer) (-1646083254), (java.lang.Integer) (-1646083254));
        java.lang.Integer int17 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1420242393, (java.lang.Integer) (-1));
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) (-679114), (java.lang.Integer) (-485640240));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-388878032) + "'", int11.equals((-388878032)));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 1002800788 + "'", int14.equals(1002800788));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-1420242393) + "'", int17.equals((-1420242393)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 0 + "'", int20.equals(0));
    }

    @Test
    public void test365() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test365");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass6 = operacionesMatematicas0.getClass();
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 688, (java.lang.Integer) (-924510));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 6295, (java.lang.Integer) 2010705437);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-1536561115), (java.lang.Integer) (-209727));
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) (-1865866569), (java.lang.Integer) (-1827));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2010711732 + "'", int13.equals(2010711732));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 7326 + "'", int16.equals(7326));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1021273 + "'", int19.equals(1021273));
    }

    @Test
    public void test366() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test366");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 199, (java.lang.Integer) 302);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.sumar((java.lang.Integer) (-3160), (java.lang.Integer) (-245051272));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + (-245054432) + "'", int14.equals((-245054432)));
    }

    @Test
    public void test367() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test367");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-26796), (java.lang.Integer) (-2131));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-674366), (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-365383), (java.lang.Integer) (-594099455));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-28927) + "'", int12.equals((-28927)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-133524468) + "'", int15.equals((-133524468)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1899059129 + "'", int18.equals(1899059129));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test368() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test368");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) 1089);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1569639104, (java.lang.Integer) 1933331090);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1728782720 + "'", int12.equals(1728782720));
    }

    @Test
    public void test369() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test369");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-337227660), (java.lang.Integer) (-2962));
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-2131));
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-317895457), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 2037894992, (java.lang.Integer) 1741645850);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 113851 + "'", int10.equals(113851));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-317895457) + "'", int16.equals((-317895457)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-515426454) + "'", int20.equals((-515426454)));
    }

    @Test
    public void test370() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test370");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2029, (java.lang.Integer) 99);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 101, (java.lang.Integer) 990);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-304), (java.lang.Integer) (-25));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 223300, (java.lang.Integer) 688);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) (-1), (java.lang.Integer) (-304));
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) (-202806058), (java.lang.Integer) (-1126066292));
        java.lang.Integer int26 = operacionesMatematicas0.restar((java.lang.Integer) 426796764, (java.lang.Integer) (-342147));
        java.lang.Integer int29 = operacionesMatematicas0.restar((java.lang.Integer) (-1124082656), (java.lang.Integer) 493792228);
        java.lang.Integer int32 = operacionesMatematicas0.sumar((java.lang.Integer) 1021273, (java.lang.Integer) 4079228);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 200871 + "'", int7.equals(200871));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 99990 + "'", int10.equals(99990));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 12 + "'", int13.equals(12));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 222612 + "'", int17.equals(222612));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-305) + "'", int20.equals((-305)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 923260234 + "'", int23.equals(923260234));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 427138911 + "'", int26.equals(427138911));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-1617874884) + "'", int29.equals((-1617874884)));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 5100501 + "'", int32.equals(5100501));
    }

    @Test
    public void test371() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test371");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) (-292));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-694249), (java.lang.Integer) (-343065842));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-593933344), (java.lang.Integer) (-388878032));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) 173387, (java.lang.Integer) (-1796353182));
        java.lang.Integer int28 = operacionesMatematicas0.sumar((java.lang.Integer) (-1033021970), (java.lang.Integer) (-1954108080));
        java.lang.Class<?> wildcardClass29 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-882132) + "'", int16.equals((-882132)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 342371593 + "'", int19.equals(342371593));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1 + "'", int22.equals(1));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 1307837246 + "'", int28.equals(1307837246));
        org.junit.Assert.assertNotNull(wildcardClass29);
    }

    @Test
    public void test372() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test372");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) 10376, (java.lang.Integer) (-1646083254));
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-1546915), (java.lang.Integer) (-314144622));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-42632), (java.lang.Integer) (-789783992));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-315691537) + "'", int10.equals((-315691537)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
    }

    @Test
    public void test373() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test373");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 101);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 2029, (java.lang.Integer) 2029);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) 99692);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-1072091024), (java.lang.Integer) (-890108686));
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 1892953498, (java.lang.Integer) 936480549);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 101 + "'", int4.equals(101));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 4058 + "'", int7.equals(4058));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 30106984 + "'", int10.equals(30106984));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-181982338) + "'", int13.equals((-181982338)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 956472949 + "'", int16.equals(956472949));
    }

    @Test
    public void test374() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test374");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 2030);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 990, (java.lang.Integer) (-304));
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 1362967132, (java.lang.Integer) 2030);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-56947983), (java.lang.Integer) 1615957977);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-2233) + "'", int16.equals((-2233)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-300960) + "'", int19.equals((-300960)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1362965102 + "'", int22.equals(1362965102));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-1865816759) + "'", int25.equals((-1865816759)));
    }

    @Test
    public void test375() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test375");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 473676, (java.lang.Integer) (-2030));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-292), (java.lang.Integer) 2108502328);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-673143), (java.lang.Integer) (-872968));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 419736786, (java.lang.Integer) 2337);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2066490878, (java.lang.Integer) 88707587);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-961562280) + "'", int12.equals((-961562280)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 199825 + "'", int19.equals(199825));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1672325394 + "'", int22.equals(1672325394));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1788694010 + "'", int25.equals(1788694010));
    }

    @Test
    public void test376() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test376");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-98975152), (java.lang.Integer) (-300960));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) (-9));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 7699977, (java.lang.Integer) (-144182695));
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) (-1105568374), (java.lang.Integer) (-229790998));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1963548160 + "'", int9.equals(1963548160));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1827) + "'", int12.equals((-1827)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1633922271) + "'", int15.equals((-1633922271)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 4 + "'", int18.equals(4));
    }

    @Test
    public void test377() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test377");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) 203);
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 791, (java.lang.Integer) (-198118));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 1310, (java.lang.Integer) 489431862);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 797 + "'", int7.equals(797));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 489433172 + "'", int13.equals(489433172));
    }

    @Test
    public void test378() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test378");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 473676, (java.lang.Integer) 222612);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 829469604, (java.lang.Integer) 680606415);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 696288 + "'", int7.equals(696288));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-491230820) + "'", int10.equals((-491230820)));
    }

    @Test
    public void test379() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test379");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-203657735), (java.lang.Integer) (-98975152));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-872882432), (java.lang.Integer) 1374);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-203), (java.lang.Integer) 3233);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1582668009, (java.lang.Integer) (-92868540));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-104682583) + "'", int9.equals((-104682583)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-635285) + "'", int12.equals((-635285)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1400704484 + "'", int19.equals(1400704484));
    }

    @Test
    public void test380() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test380");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-2233));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 849, (java.lang.Integer) 925359);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-872882432), (java.lang.Integer) (-1468325642));
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-589656652), (java.lang.Integer) (-625983112));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) (-458350076), (java.lang.Integer) (-917));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-674366) + "'", int9.equals((-674366)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-924510) + "'", int12.equals((-924510)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1215639764) + "'", int19.equals((-1215639764)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-458350993) + "'", int22.equals((-458350993)));
    }

    @Test
    public void test381() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test381");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 2354, (java.lang.Integer) (-403));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 1988598756, (java.lang.Integer) 6295);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 673023, (java.lang.Integer) (-679014));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass21 = operacionesMatematicas0.getClass();
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-203682216), (java.lang.Integer) (-1700198433));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-5) + "'", int12.equals((-5)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 315901 + "'", int16.equals(315901));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertNotNull(wildcardClass21);
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-651368792) + "'", int24.equals((-651368792)));
    }

    @Test
    public void test382() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test382");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) 203);
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 791, (java.lang.Integer) (-198118));
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 155085916, (java.lang.Integer) 1355141328);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 797, (java.lang.Integer) 2679600);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 797 + "'", int7.equals(797));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1569639104 + "'", int13.equals(1569639104));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 2135641200 + "'", int16.equals(2135641200));
    }

    @Test
    public void test383() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test383");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 674850, (java.lang.Integer) (-203658532));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 338956, (java.lang.Integer) (-3030));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2233), (java.lang.Integer) (-1027036680));
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-1427466717), (java.lang.Integer) (-975620096));
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) (-241443673), (java.lang.Integer) (-510289));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1027036680) + "'", int12.equals((-1027036680)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-139629624) + "'", int15.equals((-139629624)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1891880483 + "'", int18.equals(1891880483));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-241953962) + "'", int21.equals((-241953962)));
    }

    @Test
    public void test384() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test384");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 10, (java.lang.Integer) 302);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-844), (java.lang.Integer) 675040);
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) 797, (java.lang.Integer) (-198118));
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 3020 + "'", int15.equals(3020));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 674196 + "'", int18.equals(674196));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-157900046) + "'", int21.equals((-157900046)));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test385() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test385");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-2031), (java.lang.Integer) (-100));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-679014), (java.lang.Integer) 28345);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-203658612), (java.lang.Integer) 101);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-1174645568), (java.lang.Integer) 1096885911);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-2131) + "'", int10.equals((-2131)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-650669) + "'", int13.equals((-650669)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 905316668 + "'", int16.equals(905316668));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-77759657) + "'", int19.equals((-77759657)));
    }

    @Test
    public void test386() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test386");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99990, (java.lang.Integer) 14);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-678074), (java.lang.Integer) 3451);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-1963548160), (java.lang.Integer) 675040);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 100004 + "'", int9.equals(100004));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1954933922 + "'", int12.equals(1954933922));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1962873120) + "'", int15.equals((-1962873120)));
    }

    @Test
    public void test387() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test387");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 10, (java.lang.Integer) 302);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-676594), (java.lang.Integer) 298);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) (-2125425896), (java.lang.Integer) (-848979310));
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 3020 + "'", int15.equals(3020));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-676296) + "'", int18.equals((-676296)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1320562090 + "'", int21.equals(1320562090));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertNotNull(wildcardClass24);
    }

    @Test
    public void test388() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test388");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-203657735), (java.lang.Integer) (-98975152));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-872882432), (java.lang.Integer) 1374);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-203), (java.lang.Integer) 3233);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-78), (java.lang.Integer) (-351716320));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-104682583) + "'", int9.equals((-104682583)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-635285) + "'", int12.equals((-635285)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1664069184 + "'", int18.equals(1664069184));
    }

    @Test
    public void test389() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test389");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 101);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 2029, (java.lang.Integer) 2029);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) 99692);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-28176), (java.lang.Integer) 315901);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203658612), (java.lang.Integer) (-203761580));
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass18 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 101 + "'", int4.equals(101));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 4058 + "'", int7.equals(4058));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 30106984 + "'", int10.equals(30106984));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-344077) + "'", int13.equals((-344077)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 102968 + "'", int16.equals(102968));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertNotNull(wildcardClass18);
    }

    @Test
    public void test390() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test390");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 218, (java.lang.Integer) 3233);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1711856, (java.lang.Integer) 138125136);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-949939300), (java.lang.Integer) 2037894992);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 3451 + "'", int9.equals(3451));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-136413280) + "'", int12.equals((-136413280)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1498069824) + "'", int15.equals((-1498069824)));
    }

    @Test
    public void test391() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test391");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 99990, (java.lang.Integer) 298);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-90), (java.lang.Integer) (-139473728));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 38327, (java.lang.Integer) 1761955219);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-3354), (java.lang.Integer) (-1149207272));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 99692 + "'", int9.equals(99692));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-139473818) + "'", int12.equals((-139473818)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1761916892) + "'", int15.equals((-1761916892)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1855525776 + "'", int18.equals(1855525776));
    }

    @Test
    public void test392() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test392");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 2030);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 990, (java.lang.Integer) (-304));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) (-28474), (java.lang.Integer) (-109387168));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-203566734), (java.lang.Integer) (-729));
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) 1937733919, (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-2233) + "'", int16.equals((-2233)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-300960) + "'", int19.equals((-300960)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-109415642) + "'", int22.equals((-109415642)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-203567463) + "'", int25.equals((-203567463)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 1937733919 + "'", int28.equals(1937733919));
    }

    @Test
    public void test393() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test393");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-204), (java.lang.Integer) 12);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 797, (java.lang.Integer) (-201378));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-853072144), (java.lang.Integer) (-358));
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-17) + "'", int6.equals((-17)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-853071786) + "'", int13.equals((-853071786)));
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test394() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test394");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2, (java.lang.Integer) (-1));
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) 338956);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-300960), (java.lang.Integer) 2679600);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 298, (java.lang.Integer) (-28474));
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203658830, (java.lang.Integer) (-702086));
        java.lang.Integer int29 = operacionesMatematicas0.restar((java.lang.Integer) 1654938623, (java.lang.Integer) (-1477849338));
        java.lang.Integer int32 = operacionesMatematicas0.restar((java.lang.Integer) 2010360317, (java.lang.Integer) 234612112);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-2) + "'", int13.equals((-2)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 1023986076 + "'", int16.equals(1023986076));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1001435648 + "'", int19.equals(1001435648));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-28176) + "'", int22.equals((-28176)));
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 2037899052 + "'", int26.equals(2037899052));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-1162179335) + "'", int29.equals((-1162179335)));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 1775748205 + "'", int32.equals(1775748205));
    }

    @Test
    public void test395() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test395");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-203657735), (java.lang.Integer) (-98975152));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-872882432), (java.lang.Integer) 1374);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-203), (java.lang.Integer) 3233);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) (-223), (java.lang.Integer) 203658830);
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) 203658830, (java.lang.Integer) (-1566668296));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-104682583) + "'", int9.equals((-104682583)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-635285) + "'", int12.equals((-635285)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
    }

    @Test
    public void test396() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test396");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2, (java.lang.Integer) 1000);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-907044204), (java.lang.Integer) 166111);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-480618), (java.lang.Integer) (-674366));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 2000 + "'", int6.equals(2000));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-906878093) + "'", int9.equals((-906878093)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1989890988 + "'", int12.equals(1989890988));
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test397() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test397");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 44906012, (java.lang.Integer) 2679600);
        java.lang.Integer int10 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-223), (java.lang.Integer) (-28927));
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 42226412 + "'", int7.equals(42226412));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 6450721 + "'", int10.equals(6450721));
        org.junit.Assert.assertNotNull(wildcardClass11);
    }

    @Test
    public void test398() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test398");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-203435127), (java.lang.Integer) 2354);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-1399189976), (java.lang.Integer) (-485640240));
        java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) (-651368792), (java.lang.Integer) 1962099819);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-203437481) + "'", int19.equals((-203437481)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 2 + "'", int22.equals(2));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
    }

    @Test
    public void test399() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test399");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-507), (java.lang.Integer) 200871);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-3030), (java.lang.Integer) 1980253234);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 44906216, (java.lang.Integer) 797);
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) (-853072144), (java.lang.Integer) (-873146058));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) 1448808485, (java.lang.Integer) 902597452);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-201378) + "'", int16.equals((-201378)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1980250204 + "'", int19.equals(1980250204));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 44907013 + "'", int22.equals(44907013));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 20073914 + "'", int25.equals(20073914));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 1 + "'", int28.equals(1));
    }

    @Test
    public void test400() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test400");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-100), (java.lang.Integer) (-2233));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 675054, (java.lang.Integer) (-201378));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-292), (java.lang.Integer) 338956);
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223300 + "'", int6.equals(223300));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 473676 + "'", int9.equals(473676));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-98975152) + "'", int12.equals((-98975152)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
    }

    @Test
    public void test401() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test401");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-2031), (java.lang.Integer) (-100));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-679014), (java.lang.Integer) 28345);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 2108473854);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-344077));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 65, (java.lang.Integer) 1346166);
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) 972267014, (java.lang.Integer) (-169435345));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) 1962099819, (java.lang.Integer) (-109415642));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-2131) + "'", int10.equals((-2131)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-650669) + "'", int13.equals((-650669)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-2108473854) + "'", int16.equals((-2108473854)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1346231 + "'", int22.equals(1346231));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1141702359 + "'", int25.equals(1141702359));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-17) + "'", int28.equals((-17)));
    }

    @Test
    public void test402() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test402");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2354, (java.lang.Integer) 2679600);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-98990), (java.lang.Integer) (-4060));
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.dividir((java.lang.Integer) (-203761580), (java.lang.Integer) 44540629);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 268, (java.lang.Integer) 129067);
        java.lang.Integer int26 = operacionesMatematicas0.sumar((java.lang.Integer) 610278, (java.lang.Integer) 198586);
        java.lang.Class<?> wildcardClass27 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 24 + "'", int16.equals(24));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-4) + "'", int20.equals((-4)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 0 + "'", int23.equals(0));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 808864 + "'", int26.equals(808864));
        org.junit.Assert.assertNotNull(wildcardClass27);
    }

    @Test
    public void test403() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test403");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) (-292));
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) (-5), (java.lang.Integer) (-679014));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Integer int23 = operacionesMatematicas0.restar((java.lang.Integer) 1402914690, (java.lang.Integer) (-1566625664));
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) (-985129904), (java.lang.Integer) 98976810);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-882132) + "'", int16.equals((-882132)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-1325426942) + "'", int23.equals((-1325426942)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-9) + "'", int26.equals((-9)));
    }

    @Test
    public void test404() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test404");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 688, (java.lang.Integer) 990);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) 218, (java.lang.Integer) (-2131));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 227581, (java.lang.Integer) 2679600);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 1199317608, (java.lang.Integer) (-1073433704));
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) (-674376), (java.lang.Integer) 5050);
        java.lang.Integer int24 = operacionesMatematicas0.restar((java.lang.Integer) 9120300, (java.lang.Integer) (-605231522));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1678 + "'", int9.equals(1678));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1913) + "'", int12.equals((-1913)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-2452019) + "'", int15.equals((-2452019)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1) + "'", int18.equals((-1)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-133) + "'", int21.equals((-133)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 614351822 + "'", int24.equals(614351822));
    }

    @Test
    public void test405() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test405");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 101);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 797, (java.lang.Integer) (-890108686));
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.restar((java.lang.Integer) (-28474), (java.lang.Integer) 1815849585);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 101 + "'", int4.equals(101));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-747018902) + "'", int7.equals((-747018902)));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-1815878059) + "'", int11.equals((-1815878059)));
    }

    @Test
    public void test406() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test406");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 669356581, (java.lang.Integer) (-5));
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) (-2147357664), (java.lang.Integer) 1050105608);
        java.lang.Integer int25 = operacionesMatematicas0.restar((java.lang.Integer) 894688107, (java.lang.Integer) 1023985958);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 669356576 + "'", int19.equals(669356576));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1097252056) + "'", int22.equals((-1097252056)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-129297851) + "'", int25.equals((-129297851)));
    }

    @Test
    public void test407() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test407");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-98975152), (java.lang.Integer) (-300960));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 204, (java.lang.Integer) 0);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 849, (java.lang.Integer) 55);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-892129759), (java.lang.Integer) (-1072091024));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1963548160 + "'", int9.equals(1963548160));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 204 + "'", int12.equals(204));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 794 + "'", int15.equals(794));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1773969808) + "'", int18.equals((-1773969808)));
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test408() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test408");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 14, (java.lang.Integer) (-3260));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 1144325196, (java.lang.Integer) 2108530673);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 644054574, (java.lang.Integer) (-328150775));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) 1968816313, (java.lang.Integer) (-13));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3274 + "'", int12.equals(3274));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 315903799 + "'", int18.equals(315903799));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1968816326 + "'", int21.equals(1968816326));
    }

    @Test
    public void test409() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test409");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 990, (java.lang.Integer) 302);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 7458814, (java.lang.Integer) (-676296));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) 1710, (java.lang.Integer) 168);
        java.lang.Integer int24 = operacionesMatematicas0.restar((java.lang.Integer) 86235969, (java.lang.Integer) 959382027);
        try {
            java.lang.Integer int27 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 688 + "'", int15.equals(688));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-11) + "'", int18.equals((-11)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1542 + "'", int21.equals(1542));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-873146058) + "'", int24.equals((-873146058)));
    }

    @Test
    public void test410() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test410");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 198, (java.lang.Integer) (-3030));
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-2548), (java.lang.Integer) 532602758);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) (-319619085), (java.lang.Integer) (-1477849338));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-599940) + "'", int13.equals((-599940)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1797468423) + "'", int19.equals((-1797468423)));
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test411() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test411");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-2233), (java.lang.Integer) 199);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 227358, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 157227294, (java.lang.Integer) 2053450600);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-11) + "'", int10.equals((-11)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 227358 + "'", int13.equals(227358));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-1896223306) + "'", int16.equals((-1896223306)));
    }

    @Test
    public void test412() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test412");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3233, (java.lang.Integer) (-26796));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) (-3));
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) (-3260), (java.lang.Integer) 44906012);
        java.lang.Integer int31 = operacionesMatematicas0.dividir((java.lang.Integer) 138124938, (java.lang.Integer) 1963748327);
        java.lang.Integer int34 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) (-1566890908));
        java.lang.Class<?> wildcardClass35 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-86631468) + "'", int22.equals((-86631468)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-103) + "'", int25.equals((-103)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 0 + "'", int31.equals(0));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 1566890908 + "'", int34.equals(1566890908));
        org.junit.Assert.assertNotNull(wildcardClass35);
    }

    @Test
    public void test413() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test413");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 302, (java.lang.Integer) (-2233));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 849, (java.lang.Integer) 925359);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-872882432), (java.lang.Integer) (-1468325642));
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 118, (java.lang.Integer) 0);
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-885379048), (java.lang.Integer) 0);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1554526000, (java.lang.Integer) (-479664896));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-674366) + "'", int9.equals((-674366)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-924510) + "'", int12.equals((-924510)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 766898176 + "'", int25.equals(766898176));
    }

    @Test
    public void test414() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test414");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 101);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) (-674366), (java.lang.Integer) 3020);
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 200183);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-679114), (java.lang.Integer) (-172199));
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 7, (java.lang.Integer) 222308);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 791, (java.lang.Integer) 198586);
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) (-885379048), (java.lang.Integer) 0);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 101 + "'", int4.equals(101));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-223) + "'", int7.equals((-223)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 200184 + "'", int10.equals(200184));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-506915) + "'", int13.equals((-506915)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-197795) + "'", int19.equals((-197795)));
        org.junit.Assert.assertNotNull(wildcardClass20);
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-885379048) + "'", int23.equals((-885379048)));
    }

    @Test
    public void test415() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test415");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 100004, (java.lang.Integer) 222308);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 703051, (java.lang.Integer) 0);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-1034161825), (java.lang.Integer) 151554579);
        try {
            java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 44906012, (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 703051 + "'", int12.equals(703051));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1185716404) + "'", int15.equals((-1185716404)));
    }

    @Test
    public void test416() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test416");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1, (java.lang.Integer) 100);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 1079, (java.lang.Integer) 679827);
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) 3274, (java.lang.Integer) 333);
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) (-517446961), (java.lang.Integer) 171100924);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-678748) + "'", int17.equals((-678748)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 2941 + "'", int20.equals(2941));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-346346037) + "'", int23.equals((-346346037)));
    }

    @Test
    public void test417() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test417");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 101);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 2005917886, (java.lang.Integer) (-1243099180));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1624322327, (java.lang.Integer) (-821707220));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-304) + "'", int6.equals((-304)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-1) + "'", int9.equals((-1)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1848937749) + "'", int12.equals((-1848937749)));
    }

    @Test
    public void test418() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test418");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 679827, (java.lang.Integer) 129067);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-203658532), (java.lang.Integer) (-1174185152));
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) (-1751438130), (java.lang.Integer) (-278495984));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 996650801, (java.lang.Integer) (-136413280));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-302635422), (java.lang.Integer) 1110802624);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 808894 + "'", int10.equals(808894));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-2029934114) + "'", int16.equals((-2029934114)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1133064081 + "'", int19.equals(1133064081));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1038628480) + "'", int22.equals((-1038628480)));
    }

    @Test
    public void test419() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test419");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 10, (java.lang.Integer) 302);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-844), (java.lang.Integer) 675040);
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) (-485640240), (java.lang.Integer) 99);
        java.lang.Integer int24 = operacionesMatematicas0.restar((java.lang.Integer) (-1427973632), (java.lang.Integer) (-506915));
        java.lang.Class<?> wildcardClass25 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 3020 + "'", int15.equals(3020));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 674196 + "'", int18.equals(674196));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-485640339) + "'", int21.equals((-485640339)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1427466717) + "'", int24.equals((-1427466717)));
        org.junit.Assert.assertNotNull(wildcardClass25);
    }

    @Test
    public void test420() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test420");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 202);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) (-2030));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 134282741, (java.lang.Integer) 453522102);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 223, (java.lang.Integer) 203204);
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Integer int29 = operacionesMatematicas0.dividir((java.lang.Integer) (-650669), (java.lang.Integer) 453522102);
        java.lang.Integer int32 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) 1346166);
        java.lang.Integer int35 = operacionesMatematicas0.dividir((java.lang.Integer) (-680600602), (java.lang.Integer) 674473568);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1 + "'", int15.equals(1));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 677084 + "'", int18.equals(677084));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-319239361) + "'", int22.equals((-319239361)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 203427 + "'", int25.equals(203427));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 0 + "'", int29.equals(0));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 0 + "'", int32.equals(0));
        org.junit.Assert.assertTrue("'" + int35 + "' != '" + (-1) + "'", int35.equals((-1)));
    }

    @Test
    public void test421() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test421");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99990, (java.lang.Integer) 14);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-678074), (java.lang.Integer) 3451);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) (-403), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 100004 + "'", int9.equals(100004));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1954933922 + "'", int12.equals(1954933922));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-403) + "'", int15.equals((-403)));
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test422() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test422");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-28275), (java.lang.Integer) 200184);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-1896219511), (java.lang.Integer) (-669335781));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 312, (java.lang.Integer) (-2283703));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1729412004 + "'", int12.equals(1729412004));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-712515336) + "'", int15.equals((-712515336)));
    }

    @Test
    public void test423() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test423");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1089, (java.lang.Integer) (-1027036680));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 23816997, (java.lang.Integer) 222308);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166111, (java.lang.Integer) 1023985958);
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) 953704, (java.lang.Integer) (-319241594));
        java.lang.Integer int31 = operacionesMatematicas0.restar((java.lang.Integer) 1919324571, (java.lang.Integer) 13135);
        java.lang.Class<?> wildcardClass32 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1751447560) + "'", int19.equals((-1751447560)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-985706892) + "'", int22.equals((-985706892)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1741645850 + "'", int25.equals(1741645850));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 0 + "'", int28.equals(0));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + 1919311436 + "'", int31.equals(1919311436));
        org.junit.Assert.assertNotNull(wildcardClass32);
    }

    @Test
    public void test424() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test424");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 990, (java.lang.Integer) 302);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 7458814, (java.lang.Integer) (-676296));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) 1710, (java.lang.Integer) 168);
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) (-86797590), (java.lang.Integer) (-1646083254));
        java.lang.Integer int27 = operacionesMatematicas0.dividir((java.lang.Integer) (-1827), (java.lang.Integer) (-17));
        java.lang.Integer int30 = operacionesMatematicas0.sumar((java.lang.Integer) 161877486, (java.lang.Integer) 191);
        java.lang.Integer int33 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-853072144), (java.lang.Integer) (-1033577087));
        java.lang.Integer int36 = operacionesMatematicas0.dividir((java.lang.Integer) (-2016250560), (java.lang.Integer) 2064582464);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 688 + "'", int15.equals(688));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-11) + "'", int18.equals((-11)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1542 + "'", int21.equals(1542));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-1732880844) + "'", int24.equals((-1732880844)));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + 107 + "'", int27.equals(107));
        org.junit.Assert.assertTrue("'" + int30 + "' != '" + 161877677 + "'", int30.equals(161877677));
        org.junit.Assert.assertTrue("'" + int33 + "' != '" + 1125633776 + "'", int33.equals(1125633776));
        org.junit.Assert.assertTrue("'" + int36 + "' != '" + 0 + "'", int36.equals(0));
    }

    @Test
    public void test425() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test425");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.restar((java.lang.Integer) (-3160), (java.lang.Integer) (-203662590));
        java.lang.Integer int11 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1913), (java.lang.Integer) 191);
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-2125259776), (java.lang.Integer) (-2010186930));
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 1592563465, (java.lang.Integer) 1954931892);
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) 40325680, (java.lang.Integer) 2117069563);
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 203659430 + "'", int8.equals(203659430));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-365383) + "'", int11.equals((-365383)));
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 159520590 + "'", int15.equals(159520590));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-2093124336) + "'", int21.equals((-2093124336)));
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test426() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test426");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 1181296352, (java.lang.Integer) 333);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 1181296019 + "'", int13.equals(1181296019));
    }

    @Test
    public void test427() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test427");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.restar((java.lang.Integer) 688, (java.lang.Integer) (-674366));
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-304), (java.lang.Integer) 12);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) (-2789518), (java.lang.Integer) 1426890059);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.sumar((java.lang.Integer) (-717311), (java.lang.Integer) (-1809923325));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 675054 + "'", int4.equals(675054));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-292) + "'", int7.equals((-292)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-1429679577) + "'", int10.equals((-1429679577)));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + (-1810640636) + "'", int14.equals((-1810640636)));
    }

    @Test
    public void test428() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test428");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-4060), (java.lang.Integer) (-203658530));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) (-906764242));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) (-816794768), (java.lang.Integer) (-941899981));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203662590) + "'", int6.equals((-203662590)));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-906764242) + "'", int10.equals((-906764242)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
    }

    @Test
    public void test429() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test429");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-1085863536), (java.lang.Integer) (-391));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 200257, (java.lang.Integer) (-139423347));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-1085863927) + "'", int7.equals((-1085863927)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 139623604 + "'", int10.equals(139623604));
    }

    @Test
    public void test430() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test430");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 473676, (java.lang.Integer) (-2030));
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) 20300, (java.lang.Integer) 338956);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 3021, (java.lang.Integer) 3274);
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) (-237), (java.lang.Integer) (-318557503));
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) 2989800, (java.lang.Integer) (-12090128));
        java.lang.Class<?> wildcardClass25 = operacionesMatematicas0.getClass();
        java.lang.Integer int28 = operacionesMatematicas0.dividir((java.lang.Integer) 1593226934, (java.lang.Integer) 1181296352);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-961562280) + "'", int12.equals((-961562280)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 359256 + "'", int15.equals(359256));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 6295 + "'", int18.equals(6295));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 318557266 + "'", int21.equals(318557266));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-9100328) + "'", int24.equals((-9100328)));
        org.junit.Assert.assertNotNull(wildcardClass25);
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 1 + "'", int28.equals(1));
    }

    @Test
    public void test431() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test431");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 10, (java.lang.Integer) 302);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 961940784, (java.lang.Integer) (-292));
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-586816723), (java.lang.Integer) (-1830));
        java.lang.Integer int27 = operacionesMatematicas0.restar((java.lang.Integer) 1738534581, (java.lang.Integer) (-1338190428));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 3020 + "'", int15.equals(3020));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1) + "'", int18.equals((-1)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 961940492 + "'", int21.equals(961940492));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 132779090 + "'", int24.equals(132779090));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + (-1218242287) + "'", int27.equals((-1218242287)));
    }

    @Test
    public void test432() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test432");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 1678, (java.lang.Integer) (-304));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 44540629, (java.lang.Integer) (-833949417));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 674196, (java.lang.Integer) (-203658612));
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 691719053, (java.lang.Integer) 0);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1374 + "'", int9.equals(1374));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 878490046 + "'", int12.equals(878490046));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-12090128) + "'", int15.equals((-12090128)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 691719053 + "'", int18.equals(691719053));
    }

    @Test
    public void test433() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test433");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 99, (java.lang.Integer) 1001435648);
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1001435549) + "'", int22.equals((-1001435549)));
        org.junit.Assert.assertNotNull(wildcardClass23);
    }

    @Test
    public void test434() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test434");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 688, (java.lang.Integer) 990);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-3), (java.lang.Integer) (-103));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1024014076, (java.lang.Integer) 200183);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-365383), (java.lang.Integer) 0);
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-169435527), (java.lang.Integer) 55);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1678 + "'", int9.equals(1678));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 100 + "'", int12.equals(100));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 10672420 + "'", int15.equals(10672420));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-729019393) + "'", int21.equals((-729019393)));
    }

    @Test
    public void test435() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test435");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 679827);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-86407950), (java.lang.Integer) 419736786);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-485640240), (java.lang.Integer) 677084);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 199, (java.lang.Integer) 569427227);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) (-203658612), (java.lang.Integer) (-358));
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) 1554526000, (java.lang.Integer) (-2131278928));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-679726) + "'", int6.equals((-679726)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-835045696) + "'", int12.equals((-835045696)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-569427028) + "'", int15.equals((-569427028)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 568878 + "'", int18.equals(568878));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
    }

    @Test
    public void test436() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test436");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-100), (java.lang.Integer) (-203));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 1374, (java.lang.Integer) 28000);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-1913886717), (java.lang.Integer) (-1503913728));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 20300 + "'", int12.equals(20300));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 877166851 + "'", int18.equals(877166851));
    }

    @Test
    public void test437() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test437");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) (-292));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-694249), (java.lang.Integer) (-343065842));
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) (-970067068));
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 195080, (java.lang.Integer) 202);
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-32545458), (java.lang.Integer) 1476845172);
        java.lang.Integer int32 = operacionesMatematicas0.multiplicar((java.lang.Integer) 153721128, (java.lang.Integer) 1913817292);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-882132) + "'", int16.equals((-882132)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 342371593 + "'", int19.equals(342371593));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 970067068 + "'", int22.equals(970067068));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 195282 + "'", int25.equals(195282));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-1465414824) + "'", int29.equals((-1465414824)));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + (-78427168) + "'", int32.equals((-78427168)));
    }

    @Test
    public void test438() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test438");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2031), (java.lang.Integer) 223300);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1898863600, (java.lang.Integer) (-791133635));
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 852483217, (java.lang.Integer) 199);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-453522300) + "'", int12.equals((-453522300)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 2118109232 + "'", int15.equals(2118109232));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 4283835 + "'", int18.equals(4283835));
    }

    @Test
    public void test439() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test439");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 677082, (java.lang.Integer) 797);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) 221269);
        java.lang.Integer int28 = operacionesMatematicas0.sumar((java.lang.Integer) (-802065), (java.lang.Integer) 4058);
        java.lang.Integer int31 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) (-220971));
        java.lang.Integer int34 = operacionesMatematicas0.dividir((java.lang.Integer) (-1215639764), (java.lang.Integer) 782048165);
        java.lang.Integer int37 = operacionesMatematicas0.restar((java.lang.Integer) (-816794768), (java.lang.Integer) (-1013322));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 849 + "'", int22.equals(849));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 668453649 + "'", int25.equals(668453649));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-798007) + "'", int28.equals((-798007)));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-220971) + "'", int31.equals((-220971)));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + (-1) + "'", int34.equals((-1)));
        org.junit.Assert.assertTrue("'" + int37 + "' != '" + (-815781446) + "'", int37.equals((-815781446)));
    }

    @Test
    public void test440() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test440");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1089, (java.lang.Integer) (-1027036680));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 23816997, (java.lang.Integer) 222308);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166111, (java.lang.Integer) 1023985958);
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) (-11), (java.lang.Integer) 1418013632);
        java.lang.Integer int31 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) (-676594));
        java.lang.Integer int34 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1483608740), (java.lang.Integer) (-985706892));
        java.lang.Class<?> wildcardClass35 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1751447560) + "'", int19.equals((-1751447560)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-985706892) + "'", int22.equals((-985706892)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1741645850 + "'", int25.equals(1741645850));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-1418013643) + "'", int28.equals((-1418013643)));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-676594) + "'", int31.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + (-1709861456) + "'", int34.equals((-1709861456)));
        org.junit.Assert.assertNotNull(wildcardClass35);
    }

    @Test
    public void test441() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test441");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-2030), (java.lang.Integer) 677084);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 438482334, (java.lang.Integer) 44540629);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) (-203654655), (java.lang.Integer) 678545);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2679600, (java.lang.Integer) 676495);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-679114) + "'", int16.equals((-679114)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 1809913974 + "'", int19.equals(1809913974));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-300) + "'", int22.equals((-300)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 259803088 + "'", int25.equals(259803088));
    }

    @Test
    public void test442() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test442");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 1678, (java.lang.Integer) (-304));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 44540629, (java.lang.Integer) (-833949417));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 674196, (java.lang.Integer) (-203658612));
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 775906878, (java.lang.Integer) 696288);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1374 + "'", int9.equals(1374));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 878490046 + "'", int12.equals(878490046));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-12090128) + "'", int15.equals((-12090128)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 776603166 + "'", int18.equals(776603166));
    }

    @Test
    public void test443() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test443");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2), (java.lang.Integer) 2030);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-44906012), (java.lang.Integer) 0);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1107089785), (java.lang.Integer) (-7812));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) (-1985326896), (java.lang.Integer) (-203078208));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-4060) + "'", int12.equals((-4060)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-44906012) + "'", int15.equals((-44906012)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1478733724) + "'", int18.equals((-1478733724)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1782248688) + "'", int21.equals((-1782248688)));
    }

    @Test
    public void test444() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test444");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-204940904), (java.lang.Integer) (-678074));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 7458814, (java.lang.Integer) 227581);
        java.lang.Integer int15 = operacionesMatematicas0.sumar((java.lang.Integer) (-127444272), (java.lang.Integer) 0);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) (-42632), (java.lang.Integer) (-742442848));
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) 1918641047, (java.lang.Integer) (-406896600));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 1431676816 + "'", int9.equals(1431676816));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 972267014 + "'", int12.equals(972267014));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-127444272) + "'", int15.equals((-127444272)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-4) + "'", int21.equals((-4)));
    }

    @Test
    public void test445() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test445");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) (-198), (java.lang.Integer) 198);
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-98990));
        java.lang.Integer int23 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2117069372, (java.lang.Integer) (-4060));
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-917), (java.lang.Integer) (-1988598960));
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-203761583), (java.lang.Integer) 2108373864);
        java.lang.Integer int32 = operacionesMatematicas0.dividir((java.lang.Integer) 415729, (java.lang.Integer) (-1454200156));
        java.lang.Integer int35 = operacionesMatematicas0.sumar((java.lang.Integer) (-480618), (java.lang.Integer) 4051882);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-98890) + "'", int20.equals((-98890)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-1072091024) + "'", int23.equals((-1072091024)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + (-1815854480) + "'", int26.equals((-1815854480)));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-1948415000) + "'", int29.equals((-1948415000)));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + 0 + "'", int32.equals(0));
        org.junit.Assert.assertTrue("'" + int35 + "' != '" + 3571264 + "'", int35.equals(3571264));
    }

    @Test
    public void test446() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test446");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 202);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) (-2030));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 134282741, (java.lang.Integer) 453522102);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 223, (java.lang.Integer) 203204);
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Integer int29 = operacionesMatematicas0.restar((java.lang.Integer) (-1193296747), (java.lang.Integer) 200871);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 3233 + "'", int12.equals(3233));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 1 + "'", int15.equals(1));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 677084 + "'", int18.equals(677084));
        org.junit.Assert.assertNotNull(wildcardClass19);
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-319239361) + "'", int22.equals((-319239361)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 203427 + "'", int25.equals(203427));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-1193497618) + "'", int29.equals((-1193497618)));
    }

    @Test
    public void test447() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test447");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 99, (java.lang.Integer) 1001435648);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-498428516), (java.lang.Integer) 1707858604);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1001435549) + "'", int22.equals((-1001435549)));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-1137222448) + "'", int25.equals((-1137222448)));
    }

    @Test
    public void test448() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test448");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1913));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-300960), (java.lang.Integer) (-17));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1751447560), (java.lang.Integer) 203544205);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 886390221, (java.lang.Integer) 829469604);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 117 + "'", int9.equals(117));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-300943) + "'", int12.equals((-300943)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-1073433704) + "'", int15.equals((-1073433704)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 56920617 + "'", int18.equals(56920617));
    }

    @Test
    public void test449() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test449");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) 1996158842, (java.lang.Integer) 790454420);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 791, (java.lang.Integer) (-1566668296));
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 569414523, (java.lang.Integer) 0);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 314525, (java.lang.Integer) (-314144622));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1508354034) + "'", int13.equals((-1508354034)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 2010926408 + "'", int16.equals(2010926408));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 314459147 + "'", int22.equals(314459147));
    }

    @Test
    public void test450() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test450");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 675054, (java.lang.Integer) 14);
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.restar((java.lang.Integer) (-1827), (java.lang.Integer) (-694249));
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) (-136413280), (java.lang.Integer) (-203));
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) 669356571, (java.lang.Integer) (-619745648));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 675040 + "'", int10.equals(675040));
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 692422 + "'", int14.equals(692422));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-136413077) + "'", int17.equals((-136413077)));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-1098987216) + "'", int20.equals((-1098987216)));
    }

    @Test
    public void test451() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test451");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2031), (java.lang.Integer) 223300);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1898863600, (java.lang.Integer) (-791133635));
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-374696609), (java.lang.Integer) (-403));
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) (-1562037730), (java.lang.Integer) 8260879);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-453522300) + "'", int12.equals((-453522300)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 2118109232 + "'", int15.equals(2118109232));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-374696206) + "'", int18.equals((-374696206)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1553776851) + "'", int21.equals((-1553776851)));
    }

    @Test
    public void test452() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test452");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166122, (java.lang.Integer) (-2030));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) 703040, (java.lang.Integer) 2005416224);
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.multiplicar((java.lang.Integer) 38790450, (java.lang.Integer) (-292));
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 1651769703, (java.lang.Integer) (-50702028));
        java.lang.Class<?> wildcardClass21 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 199 + "'", int6.equals(199));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-337227660) + "'", int9.equals((-337227660)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 2006119264 + "'", int12.equals(2006119264));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 1558090488 + "'", int17.equals(1558090488));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 1601067675 + "'", int20.equals(1601067675));
        org.junit.Assert.assertNotNull(wildcardClass21);
    }

    @Test
    public void test453() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test453");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass6 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) (-203761580), (java.lang.Integer) 1427973632);
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 1788694010, (java.lang.Integer) (-1614279171));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertNotNull(wildcardClass6);
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 1224212052 + "'", int10.equals(1224212052));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1) + "'", int13.equals((-1)));
    }

    @Test
    public void test454() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test454");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-203657735), (java.lang.Integer) (-98975152));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-872882432), (java.lang.Integer) 1374);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-203), (java.lang.Integer) 3233);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) (-223), (java.lang.Integer) 203658830);
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 242847, (java.lang.Integer) (-1988598960));
        java.lang.Integer int24 = operacionesMatematicas0.sumar((java.lang.Integer) (-1830), (java.lang.Integer) (-6349702));
        java.lang.Integer int27 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-12090128), (java.lang.Integer) (-949939300));
        java.lang.Class<?> wildcardClass28 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-104682583) + "'", int9.equals((-104682583)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-635285) + "'", int12.equals((-635285)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 0 + "'", int18.equals(0));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-1988356113) + "'", int21.equals((-1988356113)));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + (-6351532) + "'", int24.equals((-6351532)));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + (-849161664) + "'", int27.equals((-849161664)));
        org.junit.Assert.assertNotNull(wildcardClass28);
    }

    @Test
    public void test455() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test455");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 2354, (java.lang.Integer) 2679600);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) (-98990), (java.lang.Integer) (-4060));
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) (-313922314), (java.lang.Integer) 222308);
        java.lang.Integer int22 = operacionesMatematicas0.sumar((java.lang.Integer) 2010926408, (java.lang.Integer) (-951117697));
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 641678328, (java.lang.Integer) (-801458764));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 24 + "'", int16.equals(24));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-314144622) + "'", int19.equals((-314144622)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1059808711 + "'", int22.equals(1059808711));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-1341357472) + "'", int25.equals((-1341357472)));
    }

    @Test
    public void test456() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test456");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 679827);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-86407950), (java.lang.Integer) 419736786);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-485640240), (java.lang.Integer) 677084);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 199, (java.lang.Integer) 569427227);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) 145112, (java.lang.Integer) (-343065842));
        java.lang.Integer int21 = operacionesMatematicas0.sumar((java.lang.Integer) 65, (java.lang.Integer) (-141165408));
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-679726) + "'", int6.equals((-679726)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-835045696) + "'", int12.equals((-835045696)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-569427028) + "'", int15.equals((-569427028)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 343210954 + "'", int18.equals(343210954));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-141165343) + "'", int21.equals((-141165343)));
        org.junit.Assert.assertNotNull(wildcardClass22);
    }

    @Test
    public void test457() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test457");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2031), (java.lang.Integer) 223300);
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1898863600, (java.lang.Integer) (-791133635));
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) (-1477851346), (java.lang.Integer) (-892129759));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) 9, (java.lang.Integer) (-299808644));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2031) + "'", int9.equals((-2031)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-453522300) + "'", int12.equals((-453522300)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 2118109232 + "'", int15.equals(2118109232));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1 + "'", int18.equals(1));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 299808653 + "'", int21.equals(299808653));
    }

    @Test
    public void test458() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test458");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 473676, (java.lang.Integer) 222612);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.dividir((java.lang.Integer) (-1868330036), (java.lang.Integer) (-1034161825));
        java.lang.Integer int14 = operacionesMatematicas0.restar((java.lang.Integer) (-1387015424), (java.lang.Integer) 1881873340);
        java.lang.Integer int17 = operacionesMatematicas0.sumar((java.lang.Integer) 967314944, (java.lang.Integer) 1771579329);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 696288 + "'", int7.equals(696288));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 1 + "'", int11.equals(1));
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 1026078532 + "'", int14.equals(1026078532));
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + (-1556073023) + "'", int17.equals((-1556073023)));
    }

    @Test
    public void test459() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test459");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 961562281, (java.lang.Integer) 1475499481);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-513937200) + "'", int10.equals((-513937200)));
    }

    @Test
    public void test460() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test460");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1678, (java.lang.Integer) 223300);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 473676, (java.lang.Integer) (-2030));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) (-292), (java.lang.Integer) 2108502328);
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-86797590), (java.lang.Integer) 674850);
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 225533, (java.lang.Integer) 0);
        java.lang.Integer int25 = operacionesMatematicas0.sumar((java.lang.Integer) 1199317608, (java.lang.Integer) 1980250204);
        java.lang.Integer int28 = operacionesMatematicas0.multiplicar((java.lang.Integer) 173085920, (java.lang.Integer) (-305));
        java.lang.Integer int31 = operacionesMatematicas0.dividir((java.lang.Integer) (-1338190428), (java.lang.Integer) 1199317608);
        java.lang.Integer int34 = operacionesMatematicas0.dividir((java.lang.Integer) 7809, (java.lang.Integer) (-203662590));
        java.lang.Integer int37 = operacionesMatematicas0.dividir((java.lang.Integer) 1771576096, (java.lang.Integer) (-1429679577));
        java.lang.Integer int40 = operacionesMatematicas0.dividir((java.lang.Integer) (-1718978089), (java.lang.Integer) 11972266);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 374697400 + "'", int9.equals(374697400));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-961562280) + "'", int12.equals((-961562280)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass16);
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-589628652) + "'", int19.equals((-589628652)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 225533 + "'", int22.equals(225533));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-1115399484) + "'", int25.equals((-1115399484)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-1251598048) + "'", int28.equals((-1251598048)));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-1) + "'", int31.equals((-1)));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 0 + "'", int34.equals(0));
        org.junit.Assert.assertTrue("'" + int37 + "' != '" + (-1) + "'", int37.equals((-1)));
        org.junit.Assert.assertTrue("'" + int40 + "' != '" + (-143) + "'", int40.equals((-143)));
    }

    @Test
    public void test461() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test461");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 99, (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 166122, (java.lang.Integer) (-2030));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 203659430, (java.lang.Integer) 1954931892);
        java.lang.Integer int16 = operacionesMatematicas0.sumar((java.lang.Integer) 2989800, (java.lang.Integer) (-50702028));
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) (-203761580), (java.lang.Integer) 200257);
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 1617522543, (java.lang.Integer) 2037583576);
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) 399541126, (java.lang.Integer) (-608580408));
        java.lang.Integer int29 = operacionesMatematicas0.restar((java.lang.Integer) 790457339, (java.lang.Integer) 1601067775);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 199 + "'", int6.equals(199));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-337227660) + "'", int9.equals((-337227660)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-47712228) + "'", int16.equals((-47712228)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1017) + "'", int19.equals((-1017)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 0 + "'", int22.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 1904969904 + "'", int26.equals(1904969904));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-810610436) + "'", int29.equals((-810610436)));
    }

    @Test
    public void test462() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test462");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1089, (java.lang.Integer) (-1027036680));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1662521780, (java.lang.Integer) 853185637);
        try {
            java.lang.Integer int25 = operacionesMatematicas0.dividir((java.lang.Integer) (-396410220), (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1751447560) + "'", int19.equals((-1751447560)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 1040387076 + "'", int22.equals(1040387076));
    }

    @Test
    public void test463() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test463");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 203);
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.dividir((java.lang.Integer) 1981811896, (java.lang.Integer) 442888614);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 4 + "'", int11.equals(4));
    }

    @Test
    public void test464() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test464");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) (-337227660), (java.lang.Integer) (-2962));
        java.lang.Integer int13 = operacionesMatematicas0.sumar((java.lang.Integer) (-460127000), (java.lang.Integer) (-1751438130));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 113851 + "'", int10.equals(113851));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2083402166 + "'", int13.equals(2083402166));
    }

    @Test
    public void test465() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test465");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Integer int19 = operacionesMatematicas0.restar((java.lang.Integer) 990, (java.lang.Integer) (-2031));
        java.lang.Integer int22 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-17), (java.lang.Integer) 243122930);
        java.lang.Class<?> wildcardClass23 = operacionesMatematicas0.getClass();
        java.lang.Integer int26 = operacionesMatematicas0.dividir((java.lang.Integer) (-57810812), (java.lang.Integer) (-608644205));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 3021 + "'", int19.equals(3021));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 161877486 + "'", int22.equals(161877486));
        org.junit.Assert.assertNotNull(wildcardClass23);
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 0 + "'", int26.equals(0));
    }

    @Test
    public void test466() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test466");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 990, (java.lang.Integer) 302);
        java.lang.Integer int18 = operacionesMatematicas0.dividir((java.lang.Integer) 7458814, (java.lang.Integer) (-676296));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) 1710, (java.lang.Integer) 168);
        java.lang.Integer int24 = operacionesMatematicas0.dividir((java.lang.Integer) (-798007), (java.lang.Integer) 902597452);
        java.lang.Integer int27 = operacionesMatematicas0.restar((java.lang.Integer) 1125242523, (java.lang.Integer) (-1208538464));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 688 + "'", int15.equals(688));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-11) + "'", int18.equals((-11)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1542 + "'", int21.equals(1542));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 0 + "'", int24.equals(0));
        org.junit.Assert.assertTrue("'" + int27 + "' != '" + (-1961186309) + "'", int27.equals((-1961186309)));
    }

    @Test
    public void test467() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test467");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-100), (java.lang.Integer) (-2233));
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 2679600, (java.lang.Integer) (-406034920));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 91982416, (java.lang.Integer) 727228036);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223300 + "'", int6.equals(223300));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 408714520 + "'", int9.equals(408714520));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-635245620) + "'", int12.equals((-635245620)));
    }

    @Test
    public void test468() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test468");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) 202);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 674850, (java.lang.Integer) (-2789518));
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) 3464368, (java.lang.Integer) (-1753913018));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 98974704, (java.lang.Integer) (-28676780));
        try {
            java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 877166851, (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 204 + "'", int6.equals(204));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 3464368 + "'", int9.equals(3464368));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1750448650) + "'", int12.equals((-1750448650)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 127651484 + "'", int16.equals(127651484));
    }

    @Test
    public void test469() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test469");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) (-203657735), (java.lang.Integer) (-98975152));
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) 1079, (java.lang.Integer) 222612);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 99692, (java.lang.Integer) 203177900);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-203078208), (java.lang.Integer) 1888659260);
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) (-17));
        java.lang.Integer int24 = operacionesMatematicas0.dividir((java.lang.Integer) (-563084124), (java.lang.Integer) (-50702028));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-104682583) + "'", int9.equals((-104682583)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-221533) + "'", int12.equals((-221533)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-203078208) + "'", int15.equals((-203078208)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-2091737468) + "'", int18.equals((-2091737468)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 17 + "'", int21.equals(17));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 11 + "'", int24.equals(11));
    }

    @Test
    public void test470() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test470");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-26796), (java.lang.Integer) (-2131));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-674366), (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 790454420, (java.lang.Integer) (-9));
        java.lang.Integer int21 = operacionesMatematicas0.dividir((java.lang.Integer) 4814, (java.lang.Integer) (-680500222));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-28927) + "'", int12.equals((-28927)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-133524468) + "'", int15.equals((-133524468)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1475844812 + "'", int18.equals(1475844812));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
    }

    @Test
    public void test471() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test471");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 10, (java.lang.Integer) 1);
        java.lang.Integer int12 = operacionesMatematicas0.restar((java.lang.Integer) (-1), (java.lang.Integer) 99);
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 0, (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) (-203658532), (java.lang.Integer) 797);
        java.lang.Integer int21 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1362965102, (java.lang.Integer) 0);
        java.lang.Integer int24 = operacionesMatematicas0.restar((java.lang.Integer) (-4372312), (java.lang.Integer) (-772698960));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 10 + "'", int9.equals(10));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-100) + "'", int12.equals((-100)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-198) + "'", int15.equals((-198)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-203657735) + "'", int18.equals((-203657735)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 0 + "'", int21.equals(0));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 768326648 + "'", int24.equals(768326648));
    }

    @Test
    public void test472() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test472");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 677082, (java.lang.Integer) 797);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-86797590), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass26 = operacionesMatematicas0.getClass();
        java.lang.Integer int29 = operacionesMatematicas0.multiplicar((java.lang.Integer) 990, (java.lang.Integer) 203177900);
        java.lang.Integer int32 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-747018902), (java.lang.Integer) 1358052);
        java.lang.Class<?> wildcardClass33 = operacionesMatematicas0.getClass();
        java.lang.Integer int36 = operacionesMatematicas0.sumar((java.lang.Integer) (-136413280), (java.lang.Integer) (-9));
        java.lang.Integer int39 = operacionesMatematicas0.restar((java.lang.Integer) 2971, (java.lang.Integer) 1346166);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 849 + "'", int22.equals(849));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 0 + "'", int25.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass26);
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-717341912) + "'", int29.equals((-717341912)));
        org.junit.Assert.assertTrue("'" + int32 + "' != '" + (-2058714520) + "'", int32.equals((-2058714520)));
        org.junit.Assert.assertNotNull(wildcardClass33);
        org.junit.Assert.assertTrue("'" + int36 + "' != '" + (-136413289) + "'", int36.equals((-136413289)));
        org.junit.Assert.assertTrue("'" + int39 + "' != '" + (-1343195) + "'", int39.equals((-1343195)));
    }

    @Test
    public void test473() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test473");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1019770691), (java.lang.Integer) (-1782702100));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 772206908 + "'", int7.equals(772206908));
    }

    @Test
    public void test474() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test474");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass11 = operacionesMatematicas0.getClass();
        java.lang.Integer int14 = operacionesMatematicas0.dividir((java.lang.Integer) 2108530673, (java.lang.Integer) 203204);
        java.lang.Class<?> wildcardClass15 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass16 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertNotNull(wildcardClass11);
        org.junit.Assert.assertTrue("'" + int14 + "' != '" + 10376 + "'", int14.equals(10376));
        org.junit.Assert.assertNotNull(wildcardClass15);
        org.junit.Assert.assertNotNull(wildcardClass16);
    }

    @Test
    public void test475() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test475");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-98975152), (java.lang.Integer) (-676594));
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 1624322327, (java.lang.Integer) 200184);
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 222308, (java.lang.Integer) 203658830);
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) 342593126, (java.lang.Integer) (-56948000));
        java.lang.Integer int24 = operacionesMatematicas0.dividir((java.lang.Integer) (-966916520), (java.lang.Integer) (-1461502113));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 146 + "'", int12.equals(146));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 8114 + "'", int15.equals(8114));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 203881138 + "'", int18.equals(203881138));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 399541126 + "'", int21.equals(399541126));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 0 + "'", int24.equals(0));
    }

    @Test
    public void test476() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test476");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) 703051, (java.lang.Integer) 191);
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 227581, (java.lang.Integer) 24);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 886029184, (java.lang.Integer) (-138799304));
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 6731, (java.lang.Integer) (-1565024324));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 134282741 + "'", int7.equals(134282741));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 227605 + "'", int10.equals(227605));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-230063104) + "'", int13.equals((-230063104)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
    }

    @Test
    public void test477() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test477");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1089, (java.lang.Integer) (-1027036680));
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) (-791133534));
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1892499688, (java.lang.Integer) (-1600748477));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-1751447560) + "'", int19.equals((-1751447560)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 791133635 + "'", int22.equals(791133635));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 1663016120 + "'", int25.equals(1663016120));
    }

    @Test
    public void test478() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test478");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-204), (java.lang.Integer) 12);
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 797, (java.lang.Integer) (-201378));
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) (-202979700), (java.lang.Integer) 221269);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 1079, (java.lang.Integer) (-159825138));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-17) + "'", int6.equals((-17)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-917) + "'", int12.equals((-917)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
    }

    @Test
    public void test479() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test479");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99990, (java.lang.Integer) 14);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-678074), (java.lang.Integer) 3451);
        java.lang.Integer int15 = operacionesMatematicas0.dividir((java.lang.Integer) 0, (java.lang.Integer) (-674366));
        java.lang.Integer int18 = operacionesMatematicas0.sumar((java.lang.Integer) 138124938, (java.lang.Integer) (-1402661432));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) 1617522543, (java.lang.Integer) 442013635);
        java.lang.Integer int24 = operacionesMatematicas0.multiplicar((java.lang.Integer) 159520590, (java.lang.Integer) 419736786);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 100004 + "'", int9.equals(100004));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + 1954933922 + "'", int12.equals(1954933922));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 0 + "'", int15.equals(0));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-1264536494) + "'", int18.equals((-1264536494)));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 1175508908 + "'", int21.equals(1175508908));
        org.junit.Assert.assertTrue("'" + int24 + "' != '" + 798459388 + "'", int24.equals(798459388));
    }

    @Test
    public void test480() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test480");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) 674850, (java.lang.Integer) (-203658532));
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 338956, (java.lang.Integer) (-3030));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-2233), (java.lang.Integer) (-1027036680));
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-678748), (java.lang.Integer) (-198118));
        java.lang.Class<?> wildcardClass19 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 0 + "'", int9.equals(0));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1027036680) + "'", int12.equals((-1027036680)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-139629624) + "'", int15.equals((-139629624)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + (-480630) + "'", int18.equals((-480630)));
        org.junit.Assert.assertNotNull(wildcardClass19);
    }

    @Test
    public void test481() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test481");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) 1023986076, (java.lang.Integer) 28000);
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 1016, (java.lang.Integer) (-127644456));
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-311834474), (java.lang.Integer) (-1358028));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 1024014076 + "'", int7.equals(1024014076));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 0 + "'", int10.equals(0));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-310476446) + "'", int13.equals((-310476446)));
    }

    @Test
    public void test482() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test482");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 990, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) 99990, (java.lang.Integer) 14);
        java.lang.Integer int12 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) (-2233));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 218, (java.lang.Integer) 203658830);
        java.lang.Integer int18 = operacionesMatematicas0.restar((java.lang.Integer) (-277440624), (java.lang.Integer) (-1448119822));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) (-153723026), (java.lang.Integer) (-139473818));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 1089 + "'", int6.equals(1089));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 100004 + "'", int9.equals(100004));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-26796) + "'", int12.equals((-26796)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-203658612) + "'", int15.equals((-203658612)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1170679198 + "'", int18.equals(1170679198));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + (-14249208) + "'", int21.equals((-14249208)));
    }

    @Test
    public void test483() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test483");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.restar((java.lang.Integer) 1000, (java.lang.Integer) 203);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.dividir((java.lang.Integer) 2136956016, (java.lang.Integer) (-1483608740));
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 797 + "'", int7.equals(797));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-1) + "'", int11.equals((-1)));
        org.junit.Assert.assertNotNull(wildcardClass12);
    }

    @Test
    public void test484() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test484");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-203), (java.lang.Integer) 203);
        java.lang.Class<?> wildcardClass8 = operacionesMatematicas0.getClass();
        java.lang.Integer int11 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-328501944), (java.lang.Integer) (-626880008));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 0 + "'", int7.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass8);
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + 1830249920 + "'", int11.equals(1830249920));
    }

    @Test
    public void test485() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test485");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 99);
        java.lang.Integer int9 = operacionesMatematicas0.restar((java.lang.Integer) 202, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 676495, (java.lang.Integer) (-678074));
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-1964430707), (java.lang.Integer) (-1162179335));
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1560148160), (java.lang.Integer) (-1869407804));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 100 + "'", int6.equals(100));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 203 + "'", int9.equals(203));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 0 + "'", int13.equals(0));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-802251372) + "'", int16.equals((-802251372)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 704826624 + "'", int19.equals(704826624));
    }

    @Test
    public void test486() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test486");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-223), (java.lang.Integer) (-1));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-4060), (java.lang.Integer) 2030);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-343065842), (java.lang.Integer) 138124938);
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.dividir((java.lang.Integer) (-1010944320), (java.lang.Integer) (-1475834436));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 223 + "'", int6.equals(223));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2) + "'", int9.equals((-2)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-204940904) + "'", int12.equals((-204940904)));
        org.junit.Assert.assertNotNull(wildcardClass13);
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 0 + "'", int17.equals(0));
    }

    @Test
    public void test487() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test487");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.restar((java.lang.Integer) (-26796), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-3260));
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-945000448), (java.lang.Integer) 2107797558);
        java.lang.Integer int29 = operacionesMatematicas0.sumar((java.lang.Integer) (-29247463), (java.lang.Integer) 1181296019);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-28474) + "'", int20.equals((-28474)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-3160) + "'", int23.equals((-3160)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 1868537856 + "'", int26.equals(1868537856));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + 1152048556 + "'", int29.equals(1152048556));
    }

    @Test
    public void test488() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test488");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.restar((java.lang.Integer) 101, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.dividir((java.lang.Integer) (-204), (java.lang.Integer) 12);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-90), (java.lang.Integer) 2063594048);
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 100 + "'", int3.equals(100));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-17) + "'", int6.equals((-17)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 2063593958 + "'", int9.equals(2063593958));
        org.junit.Assert.assertNotNull(wildcardClass10);
    }

    @Test
    public void test489() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test489");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 203, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1), (java.lang.Integer) (-100));
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 3020, (java.lang.Integer) 202);
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 223, (java.lang.Integer) 1089);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 677084, (java.lang.Integer) 2);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 222612, (java.lang.Integer) (-139629624));
        java.lang.Integer int22 = operacionesMatematicas0.dividir((java.lang.Integer) 677082, (java.lang.Integer) 797);
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) 3021, (java.lang.Integer) 221269);
        java.lang.Integer int28 = operacionesMatematicas0.sumar((java.lang.Integer) (-802065), (java.lang.Integer) 4058);
        java.lang.Integer int31 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2037583576, (java.lang.Integer) (-317895457));
        try {
            java.lang.Integer int34 = operacionesMatematicas0.dividir((java.lang.Integer) 507, (java.lang.Integer) 0);
            org.junit.Assert.fail("Expected exception of type java.lang.ArithmeticException; message: / by zero");
        } catch (java.lang.ArithmeticException e) {
        }
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 2030 + "'", int4.equals(2030));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + 100 + "'", int7.equals(100));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 14 + "'", int10.equals(14));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 242847 + "'", int13.equals(242847));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 677082 + "'", int16.equals(677082));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-139407012) + "'", int19.equals((-139407012)));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + 849 + "'", int22.equals(849));
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + 668453649 + "'", int25.equals(668453649));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-798007) + "'", int28.equals((-798007)));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-1399189976) + "'", int31.equals((-1399189976)));
    }

    @Test
    public void test490() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test490");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.sumar((java.lang.Integer) 0, (java.lang.Integer) 101);
        java.lang.Integer int7 = operacionesMatematicas0.dividir((java.lang.Integer) (-674366), (java.lang.Integer) 3020);
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 1, (java.lang.Integer) 200183);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) (-629761116), (java.lang.Integer) 681653544);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) 129067, (java.lang.Integer) 415391778);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 101 + "'", int4.equals(101));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-223) + "'", int7.equals((-223)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 200184 + "'", int10.equals(200184));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-1311414660) + "'", int13.equals((-1311414660)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-415262711) + "'", int16.equals((-415262711)));
    }

    @Test
    public void test491() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test491");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.multiplicar((java.lang.Integer) 99, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.restar((java.lang.Integer) (-3160), (java.lang.Integer) (-203662590));
        java.lang.Integer int11 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1913), (java.lang.Integer) 191);
        java.lang.Class<?> wildcardClass12 = operacionesMatematicas0.getClass();
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) 692422, (java.lang.Integer) 1635371618);
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 990 + "'", int4.equals(990));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 203659430 + "'", int8.equals(203659430));
        org.junit.Assert.assertTrue("'" + int11 + "' != '" + (-365383) + "'", int11.equals((-365383)));
        org.junit.Assert.assertNotNull(wildcardClass12);
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-841111604) + "'", int15.equals((-841111604)));
    }

    @Test
    public void test492() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test492");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.dividir((java.lang.Integer) 203, (java.lang.Integer) (-1));
        java.lang.Integer int13 = operacionesMatematicas0.dividir((java.lang.Integer) 374697400, (java.lang.Integer) 473676);
        java.lang.Integer int16 = operacionesMatematicas0.multiplicar((java.lang.Integer) 329, (java.lang.Integer) (-144));
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) 1751438428, (java.lang.Integer) (-150310400));
        java.lang.Class<?> wildcardClass20 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-203) + "'", int10.equals((-203)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 791 + "'", int13.equals(791));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-47376) + "'", int16.equals((-47376)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + (-11) + "'", int19.equals((-11)));
        org.junit.Assert.assertNotNull(wildcardClass20);
    }

    @Test
    public void test493() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test493");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 100, (java.lang.Integer) 2000);
        java.lang.Integer int19 = operacionesMatematicas0.sumar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Integer int22 = operacionesMatematicas0.restar((java.lang.Integer) (-1402914690), (java.lang.Integer) 453522102);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 2029 + "'", int19.equals(2029));
        org.junit.Assert.assertTrue("'" + int22 + "' != '" + (-1856436792) + "'", int22.equals((-1856436792)));
    }

    @Test
    public void test494() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test494");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Class<?> wildcardClass5 = operacionesMatematicas0.getClass();
        java.lang.Integer int8 = operacionesMatematicas0.sumar((java.lang.Integer) 200183, (java.lang.Integer) (-26796));
        java.lang.Class<?> wildcardClass9 = operacionesMatematicas0.getClass();
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-220971), (java.lang.Integer) (-1865645598));
        java.lang.Class<?> wildcardClass13 = operacionesMatematicas0.getClass();
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertNotNull(wildcardClass5);
        org.junit.Assert.assertTrue("'" + int8 + "' != '" + 173387 + "'", int8.equals(173387));
        org.junit.Assert.assertNotNull(wildcardClass9);
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-1865866569) + "'", int12.equals((-1865866569)));
        org.junit.Assert.assertNotNull(wildcardClass13);
    }

    @Test
    public void test495() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test495");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.restar((java.lang.Integer) (-203), (java.lang.Integer) 0);
        java.lang.Integer int19 = operacionesMatematicas0.dividir((java.lang.Integer) (-1122458988), (java.lang.Integer) 1217752351);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + (-203) + "'", int16.equals((-203)));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 0 + "'", int19.equals(0));
    }

    @Test
    public void test496() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test496");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Class<?> wildcardClass1 = operacionesMatematicas0.getClass();
        java.lang.Integer int4 = operacionesMatematicas0.dividir((java.lang.Integer) 101, (java.lang.Integer) 10);
        java.lang.Integer int7 = operacionesMatematicas0.sumar((java.lang.Integer) (-676396), (java.lang.Integer) (-198));
        java.lang.Integer int10 = operacionesMatematicas0.restar((java.lang.Integer) 302, (java.lang.Integer) 675054);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 24, (java.lang.Integer) 168);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 28000, (java.lang.Integer) 98974848);
        java.lang.Integer int19 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-761787472), (java.lang.Integer) (-2008));
        org.junit.Assert.assertNotNull(wildcardClass1);
        org.junit.Assert.assertTrue("'" + int4 + "' != '" + 10 + "'", int4.equals(10));
        org.junit.Assert.assertTrue("'" + int7 + "' != '" + (-676594) + "'", int7.equals((-676594)));
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + (-674752) + "'", int10.equals((-674752)));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + (-144) + "'", int13.equals((-144)));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertTrue("'" + int19 + "' != '" + 660886400 + "'", int19.equals(660886400));
    }

    @Test
    public void test497() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test497");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass7 = operacionesMatematicas0.getClass();
        java.lang.Integer int10 = operacionesMatematicas0.sumar((java.lang.Integer) 101, (java.lang.Integer) 101);
        java.lang.Integer int13 = operacionesMatematicas0.restar((java.lang.Integer) 203, (java.lang.Integer) 0);
        java.lang.Integer int16 = operacionesMatematicas0.dividir((java.lang.Integer) 990, (java.lang.Integer) 2029);
        java.lang.Class<?> wildcardClass17 = operacionesMatematicas0.getClass();
        java.lang.Integer int20 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-882132), (java.lang.Integer) 1678);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 676495, (java.lang.Integer) 2030);
        java.lang.Class<?> wildcardClass24 = operacionesMatematicas0.getClass();
        java.lang.Class<?> wildcardClass25 = operacionesMatematicas0.getClass();
        java.lang.Integer int28 = operacionesMatematicas0.restar((java.lang.Integer) 1702393088, (java.lang.Integer) 299478398);
        java.lang.Integer int31 = operacionesMatematicas0.multiplicar((java.lang.Integer) 1711856, (java.lang.Integer) (-926090540));
        java.lang.Integer int34 = operacionesMatematicas0.sumar((java.lang.Integer) (-1964430707), (java.lang.Integer) (-275423242));
        java.lang.Integer int37 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-203656852), (java.lang.Integer) (-136443584));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 99 + "'", int6.equals(99));
        org.junit.Assert.assertNotNull(wildcardClass7);
        org.junit.Assert.assertTrue("'" + int10 + "' != '" + 202 + "'", int10.equals(202));
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 203 + "'", int13.equals(203));
        org.junit.Assert.assertTrue("'" + int16 + "' != '" + 0 + "'", int16.equals(0));
        org.junit.Assert.assertNotNull(wildcardClass17);
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + (-1480217496) + "'", int20.equals((-1480217496)));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + 333 + "'", int23.equals(333));
        org.junit.Assert.assertNotNull(wildcardClass24);
        org.junit.Assert.assertNotNull(wildcardClass25);
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + 1402914690 + "'", int28.equals(1402914690));
        org.junit.Assert.assertTrue("'" + int31 + "' != '" + (-1088946496) + "'", int31.equals((-1088946496)));
        org.junit.Assert.assertTrue("'" + int34 + "' != '" + 2055113347 + "'", int34.equals(2055113347));
        org.junit.Assert.assertTrue("'" + int37 + "' != '" + 1187063552 + "'", int37.equals(1187063552));
    }

    @Test
    public void test498() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test498");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.multiplicar((java.lang.Integer) 0, (java.lang.Integer) (-1));
        java.lang.Integer int6 = operacionesMatematicas0.multiplicar((java.lang.Integer) 202, (java.lang.Integer) 1);
        java.lang.Integer int9 = operacionesMatematicas0.sumar((java.lang.Integer) (-204), (java.lang.Integer) 675054);
        java.lang.Integer int12 = operacionesMatematicas0.sumar((java.lang.Integer) (-26796), (java.lang.Integer) (-2131));
        java.lang.Integer int15 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-674366), (java.lang.Integer) 198);
        java.lang.Integer int18 = operacionesMatematicas0.multiplicar((java.lang.Integer) 790454420, (java.lang.Integer) (-9));
        java.lang.Integer int21 = operacionesMatematicas0.restar((java.lang.Integer) (-172199), (java.lang.Integer) (-86408168));
        java.lang.Class<?> wildcardClass22 = operacionesMatematicas0.getClass();
        java.lang.Integer int25 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-1198623488), (java.lang.Integer) (-78));
        java.lang.Integer int28 = operacionesMatematicas0.sumar((java.lang.Integer) (-117892701), (java.lang.Integer) (-1536239953));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 0 + "'", int3.equals(0));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 202 + "'", int6.equals(202));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + 674850 + "'", int9.equals(674850));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-28927) + "'", int12.equals((-28927)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + (-133524468) + "'", int15.equals((-133524468)));
        org.junit.Assert.assertTrue("'" + int18 + "' != '" + 1475844812 + "'", int18.equals(1475844812));
        org.junit.Assert.assertTrue("'" + int21 + "' != '" + 86235969 + "'", int21.equals(86235969));
        org.junit.Assert.assertNotNull(wildcardClass22);
        org.junit.Assert.assertTrue("'" + int25 + "' != '" + (-996648448) + "'", int25.equals((-996648448)));
        org.junit.Assert.assertTrue("'" + int28 + "' != '" + (-1654132654) + "'", int28.equals((-1654132654)));
    }

    @Test
    public void test499() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test499");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 100, (java.lang.Integer) 1);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) 2, (java.lang.Integer) (-203658532));
        java.lang.Integer int9 = operacionesMatematicas0.dividir((java.lang.Integer) (-91808), (java.lang.Integer) 386);
        java.lang.Integer int12 = operacionesMatematicas0.dividir((java.lang.Integer) 135279661, (java.lang.Integer) (-40337365));
        java.lang.Integer int15 = operacionesMatematicas0.restar((java.lang.Integer) 23, (java.lang.Integer) (-750172272));
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 101 + "'", int3.equals(101));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + (-203658530) + "'", int6.equals((-203658530)));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-237) + "'", int9.equals((-237)));
        org.junit.Assert.assertTrue("'" + int12 + "' != '" + (-3) + "'", int12.equals((-3)));
        org.junit.Assert.assertTrue("'" + int15 + "' != '" + 750172295 + "'", int15.equals(750172295));
    }

    @Test
    public void test500() throws Throwable {
        if (debug)
            System.out.format("%n%s%n", "RegressionTest1.test500");
        pruebas.randoop.OperacionesMatematicas operacionesMatematicas0 = new pruebas.randoop.OperacionesMatematicas();
        java.lang.Integer int3 = operacionesMatematicas0.sumar((java.lang.Integer) 202, (java.lang.Integer) 100);
        java.lang.Integer int6 = operacionesMatematicas0.sumar((java.lang.Integer) (-100), (java.lang.Integer) 100);
        java.lang.Integer int9 = operacionesMatematicas0.multiplicar((java.lang.Integer) 2030, (java.lang.Integer) (-1));
        java.lang.Class<?> wildcardClass10 = operacionesMatematicas0.getClass();
        java.lang.Integer int13 = operacionesMatematicas0.multiplicar((java.lang.Integer) 12, (java.lang.Integer) 223300);
        java.lang.Class<?> wildcardClass14 = operacionesMatematicas0.getClass();
        java.lang.Integer int17 = operacionesMatematicas0.restar((java.lang.Integer) 3020, (java.lang.Integer) (-2030));
        java.lang.Integer int20 = operacionesMatematicas0.sumar((java.lang.Integer) 1351444, (java.lang.Integer) 990);
        java.lang.Integer int23 = operacionesMatematicas0.dividir((java.lang.Integer) 299478398, (java.lang.Integer) (-23937));
        java.lang.Integer int26 = operacionesMatematicas0.multiplicar((java.lang.Integer) (-679726), (java.lang.Integer) (-203678316));
        java.lang.Integer int29 = operacionesMatematicas0.sumar((java.lang.Integer) 374697459, (java.lang.Integer) 2115813051);
        org.junit.Assert.assertTrue("'" + int3 + "' != '" + 302 + "'", int3.equals(302));
        org.junit.Assert.assertTrue("'" + int6 + "' != '" + 0 + "'", int6.equals(0));
        org.junit.Assert.assertTrue("'" + int9 + "' != '" + (-2030) + "'", int9.equals((-2030)));
        org.junit.Assert.assertNotNull(wildcardClass10);
        org.junit.Assert.assertTrue("'" + int13 + "' != '" + 2679600 + "'", int13.equals(2679600));
        org.junit.Assert.assertNotNull(wildcardClass14);
        org.junit.Assert.assertTrue("'" + int17 + "' != '" + 5050 + "'", int17.equals(5050));
        org.junit.Assert.assertTrue("'" + int20 + "' != '" + 1352434 + "'", int20.equals(1352434));
        org.junit.Assert.assertTrue("'" + int23 + "' != '" + (-12511) + "'", int23.equals((-12511)));
        org.junit.Assert.assertTrue("'" + int26 + "' != '" + 1471202152 + "'", int26.equals(1471202152));
        org.junit.Assert.assertTrue("'" + int29 + "' != '" + (-1804456786) + "'", int29.equals((-1804456786)));
    }
}

